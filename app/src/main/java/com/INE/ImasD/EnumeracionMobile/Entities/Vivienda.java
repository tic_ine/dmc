package com.INE.ImasD.EnumeracionMobile.Entities;

import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Vivienda  implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int ACT_ID;
	private int ID_USUARIO;
	private UUID ID_VIVIENDA;
	private String IDPADRE;
	private String TIPOCALLE_ID;
	private String TIPOCALLE_GLOSA;
	private String NOMBRE_CALLE_CAMINO;
	private String N_DOMICILIO;
	private String N_LETRA_BLOCK;
	private String N_PISO;
	private String N_LETRA_DEPTO;
	private int ID_ESE;
	private int ID_USODESTINO;
	private String GLOSA_USODESTINO;
	
	private boolean OK;
	private boolean NULO;
	
	private Date FECHAINGRESO;
	private double COORD_X;
	private double COORD_Y;
	private String DESCRIPCION;
	private int ID_ORIGEN;
	private Date FECHAMODIFICACION;
	private Date FECHAELIMINACION;
	private int USUARIO_ID_MODIFICACION;
	private int USUARIO_ID_ELIMINACION;
	private int USUARIO_ID_INGRESO;
	private int ORDEN;
	private int ORDEN_VIV;
	private int CODIGO_ID;
	private int POSICION;
	
	private int VIVTEMP_ID;
	private String VIVTEMP_GLOSA;
	private String KILOMETRO;
	private String LOTE;
	private String SITIO;
	private String LETRAPARCELA;
	private int ORDENEDIFICACION;
	private String UBICACIONHORIZONTAL;
	private String UBICACIONVERTICAL;
	private int HOGARES = 0;
	private int PERSONAS = 0;
	
	private boolean EnEdificio = false;
	
	/*
	+ " VIVTEMP_ID integer null,"
	+ " VIVTEMP_GLOSA text null,"
	+ " KILOMETRO text null,"
	+ " LOTE text null,"
	+ " SITIO text null,"
	+ " LETRAPARCELA text null,"
	+ " ORDENEDIFICACION integer null,"
	+ " UBICACIONHORIZONTAL text null,"
	+ " UBICACIONVERTICAL text null,"
	*/
	private boolean NUEVA = false;
	private boolean MODIFICADA = false;
	private UUID ID_EDIF;
	private String NOMBRE_EDIF;

	
	 /*
	  * NOMBRELOCALIDAD text null,"
	+ " NOMBREENTIDAD text null,"	
	+ " CAT_ID numeric null,"				
	+ " NUMERACION_TERRENO text null,"				
	+ " SITIO_SIN_EDIFICACION text null,"				
	+ " MANZANA_ALDEA numeric null,"				
	
	  * 
	  * */
	
	
	private String NOMBRELOCALIDAD;
	private String NOMBREENTIDAD;
	private int CAT_ID;
	private String NUMERACION_TERRENO;
	private String SITIO_SIN_EDIFICACION;
	private int MANZANA_ALDEA;

	private int AT_ID;

	private boolean SELECCIONADA = false;
	
	
	public int getACT_ID() {
		return ACT_ID;
	}
	public void setACT_ID(int aCT_ID) {
		ACT_ID = aCT_ID;
	}
	public int getID_USUARIO() {
		return ID_USUARIO;
	}
	public void setID_USUARIO(int iD_USUARIO) {
		ID_USUARIO = iD_USUARIO;
	}
	public UUID getID_VIVIENDA() {
		return ID_VIVIENDA;
	}
	public void setID_VIVIENDA(UUID iD_VIVIENDA) {
		ID_VIVIENDA = iD_VIVIENDA;
	}
	public String getIDPADRE() {
		return IDPADRE;
	}
	public void setIDPADRE(String iDPADRE) {
		IDPADRE = iDPADRE;
	}
	public String getTIPOCALLE_ID() {
		return TIPOCALLE_ID;
	}
	public void setTIPOCALLE_ID(String tIPOCALLE_ID) {
		TIPOCALLE_ID = tIPOCALLE_ID;
	}
	public String getTIPOCALLE_GLOSA() {
		return TIPOCALLE_GLOSA;
	}
	public void setTIPOCALLE_GLOSA(String tIPOCALLE_GLOSA) {
		TIPOCALLE_GLOSA = tIPOCALLE_GLOSA;
	}
	public String getNOMBRE_CALLE_CAMINO() {
		return NOMBRE_CALLE_CAMINO;
	}
	public void setNOMBRE_CALLE_CAMINO(String nOMBRE_CALLE_CAMINO) {
		NOMBRE_CALLE_CAMINO = nOMBRE_CALLE_CAMINO;
	}
	public String getN_DOMICILIO() {
		return N_DOMICILIO;
	}
	public void setN_DOMICILIO(String n_DOMICILIO) {
		N_DOMICILIO = n_DOMICILIO;
	}
	public String getN_LETRA_BLOCK() {
		return N_LETRA_BLOCK;
	}
	public void setN_LETRA_BLOCK(String n_LETRA_BLOCK) {
		N_LETRA_BLOCK = n_LETRA_BLOCK;
	}
	public String getN_PISO() {
		return N_PISO;
	}
	public void setN_PISO(String n_PISO) {
		N_PISO = n_PISO;
	}
	public String getN_LETRA_DEPTO() {
		return N_LETRA_DEPTO;
	}
	public void setN_LETRA_DEPTO(String n_LETRA_DEPTO) {
		N_LETRA_DEPTO = n_LETRA_DEPTO;
	}
	public int getID_ESE() {
		return ID_ESE;
	}
	public void setID_ESE(int iD_ESE) {
		ID_ESE = iD_ESE;
	}
	public int getID_USODESTINO() {
		return ID_USODESTINO;
	}
	public void setID_USODESTINO(int iD_USODESTINO) {
		ID_USODESTINO = iD_USODESTINO;
	}
	
	
	public String getGLOSA_USODESTINO() {
		return GLOSA_USODESTINO;
	}
	public void setGLOSA_USODESTINO(String GLOSA_Usodestino) {
		this.GLOSA_USODESTINO = GLOSA_Usodestino;
	}
	
	
	
	public boolean isOK() {
		return OK;
	}
	public void setOK(boolean oK) {
		OK = oK;
	}
	public boolean isNULO() {
		return NULO;
	}
	public void setNULO(boolean nULO) {
		NULO = nULO;
	}
	public Date getFECHAINGRESO() {
		return FECHAINGRESO;
	}
	public void setFECHAINGRESO(Date fECHAINGRESO) {
		FECHAINGRESO = fECHAINGRESO;
	}
	public double getCOORD_X() {
		return COORD_X;
	}
	public void setCOORD_X(double cOORD_X) {
		COORD_X = cOORD_X;
	}
	public double getCOORD_Y() {
		return COORD_Y;
	}
	public void setCOORD_Y(double cOORD_Y) {
		COORD_Y = cOORD_Y;
	}
	public String getDESCRIPCION() {
		return DESCRIPCION;
	}
	public void setDESCRIPCION(String dESCRIPCION) {
		DESCRIPCION = dESCRIPCION;
	}
	public int getID_ORIGEN() {
		return ID_ORIGEN;
	}
	public void setID_ORIGEN(int iD_ORIGEN) {
		ID_ORIGEN = iD_ORIGEN;
	}
	public Date getFECHAMODIFICACION() {
		return FECHAMODIFICACION;
	}
	public void setFECHAMODIFICACION(Date fECHAMODIFICACION) {
		FECHAMODIFICACION = fECHAMODIFICACION;
	}
	public Date getFECHAELIMINACION() {
		return FECHAELIMINACION;
	}
	public void setFECHAELIMINACION(Date fECHAELIMINACION) {
		FECHAELIMINACION = fECHAELIMINACION;
	}
	public int getUSUARIO_ID_MODIFICACION() {
		return USUARIO_ID_MODIFICACION;
	}
	public void setUSUARIO_ID_MODIFICACION(int uSUARIO_ID_MODIFICACION) {
		USUARIO_ID_MODIFICACION = uSUARIO_ID_MODIFICACION;
	}
	public int getUSUARIO_ID_ELIMINACION() {
		return USUARIO_ID_ELIMINACION;
	}
	public void setUSUARIO_ID_ELIMINACION(int uSUARIO_ID_ELIMINACION) {
		USUARIO_ID_ELIMINACION = uSUARIO_ID_ELIMINACION;
	}
	public int getUSUARIO_ID_INGRESO() {
		return USUARIO_ID_INGRESO;
	}
	public void setUSUARIO_ID_INGRESO(int uSUARIO_ID_INGRESO) {
		USUARIO_ID_INGRESO = uSUARIO_ID_INGRESO;
	}
	public int getORDEN() {
		return ORDEN;
	}
	public void setORDEN(int oRDEN) {
		ORDEN = oRDEN;
	}
	public int getORDEN_VIV() {
		return ORDEN_VIV;
	}
	public void setORDEN_VIV(int oRDEN_VIV) {
		ORDEN_VIV = oRDEN_VIV;
	}
	public int getCODIGO_ID() {
		return CODIGO_ID;
	}
	public void setCODIGO_ID(int cODIGO_ID) {
		CODIGO_ID = cODIGO_ID;
	}
	public int getPOSICION() {
		return POSICION;
	}
	public void setPOSICION(int pOSICION) {
		POSICION = pOSICION;
	}
	

	
	
	
	public int getVIVTEMP_ID() {
		return VIVTEMP_ID;
	}
	public void setVIVTEMP_ID(int vIVTEMP_ID) {
		VIVTEMP_ID = vIVTEMP_ID;
	}
	public String getVIVTEMP_GLOSA() {
		return VIVTEMP_GLOSA;
	}
	public void setVIVTEMP_GLOSA(String vIVTEMP_GLOSA) {
		VIVTEMP_GLOSA = vIVTEMP_GLOSA;
	}
	public String getKILOMETRO() {
		return KILOMETRO;
	}
	public void setKILOMETRO(String kILOMETRO) {
		KILOMETRO = kILOMETRO;
	}
	public String getLOTE() {
		return LOTE;
	}
	public void setLOTE(String lOTE) {
		LOTE = lOTE;
	}
	public String getSITIO() {
		return SITIO;
	}
	public void setSITIO(String sITIO) {
		SITIO = sITIO;
	}
	public String getLETRAPARCELA() {
		return LETRAPARCELA;
	}
	public void setLETRAPARCELA(String lETRAPARCELA) {
		LETRAPARCELA = lETRAPARCELA;
	}
	public int getORDENEDIFICACION() {
		return ORDENEDIFICACION;
	}
	public void setORDENEDIFICACION(int oRDENEDIFICACION) {
		ORDENEDIFICACION = oRDENEDIFICACION;
	}
	public String getUBICACIONHORIZONTAL() {
		return UBICACIONHORIZONTAL;
	}
	public void setUBICACIONHORIZONTAL(String uBICACIONHORIZONTAL) {
		UBICACIONHORIZONTAL = uBICACIONHORIZONTAL;
	}
	public String getUBICACIONVERTICAL() {
		return UBICACIONVERTICAL;
	}
	public void setUBICACIONVERTICAL(String uBICACIONVERTICAL) {
		UBICACIONVERTICAL = uBICACIONVERTICAL;
	}
	
	public boolean isNUEVA() {
		return NUEVA;
	}
	public void setNUEVA(boolean Nueva) {
		NUEVA = Nueva;
	}
	
	public boolean isMODIFICADA() {
		return MODIFICADA;
	}
	public void setMODIFICADA(boolean Modificada) {
		MODIFICADA = Modificada;

	}
	public UUID getID_EDIF() {
		return ID_EDIF;
	}
	public void setID_EDIF(UUID iD_EDIF) {
		ID_EDIF = iD_EDIF;
		if(ID_EDIF.equals(globales.UUID_CERO))
		{
			EnEdificio = false;
		}
		else
		{
			EnEdificio = true;
		}
	}
	public int getHOGARES() {
		return HOGARES;
	}
	public void setHOGARES(int hOGARES) {
		HOGARES = hOGARES;
	}
	public int getPERSONAS() {
		return PERSONAS;
	}
	public void setPERSONAS(int pERSONAS) {
		PERSONAS = pERSONAS;
	}
	public boolean isEnEdificio() {
		return EnEdificio;
	}
	
	
	public String getNOMBRE_EDIF()
	{
		return NOMBRE_EDIF;
	}
	public void setNOMBRE_EDIF(String NombreEdif)
	{
		 NOMBRE_EDIF = NombreEdif;
	}
	public String getNOMBRELOCALIDAD() {
		return NOMBRELOCALIDAD;
	}
	public void setNOMBRELOCALIDAD(String nOMBRELOCALIDAD) {
		NOMBRELOCALIDAD = nOMBRELOCALIDAD;
	}
	public String getNOMBREENTIDAD() {
		return NOMBREENTIDAD;
	}
	public void setNOMBREENTIDAD(String nOMBREENTIDAD) {
		NOMBREENTIDAD = nOMBREENTIDAD;
	}
	public int getCAT_ID() {
		return CAT_ID;
	}
	public void setCAT_ID(int cAT_ID) {
		CAT_ID = cAT_ID;
	}
	public String getNUMERACION_TERRENO() {
		return NUMERACION_TERRENO;
	}
	public void setNUMERACION_TERRENO(String nUMERACION_TERRENO) {
		NUMERACION_TERRENO = nUMERACION_TERRENO;
	}
	public String getSITIO_SIN_EDIFICACION() {
		return SITIO_SIN_EDIFICACION;
	}
	public void setSITIO_SIN_EDIFICACION(String sITIO_SIN_EDIFICACION) {
		SITIO_SIN_EDIFICACION = sITIO_SIN_EDIFICACION;
	}
	public int getMANZANA_ALDEA() {
		return MANZANA_ALDEA;
	}
	public void setMANZANA_ALDEA(int mANZANA_ALDEA) {
		MANZANA_ALDEA = mANZANA_ALDEA;
	}


    public boolean isSELECCIONADA() {
        return SELECCIONADA;
    }

    public void setSELECCIONADA(boolean SELECCIONADA) {
        this.SELECCIONADA = SELECCIONADA;
    }

	public int getAT_ID() {
		return AT_ID;
	}

	public void setAT_ID(int AT_ID) {
		this.AT_ID = AT_ID;
	}
}
