package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class Spin_SoloString_Adapter extends BaseAdapter{

    private Context context;
    private List<String> values;

    public Spin_SoloString_Adapter(Context context, List<String> values) {
        super();
        this.context = context;
        this.values = values;
    }

    public int getCount(){
       return values.size();
    }

    public String getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }

    public void remove(String opc)
	{
    	values.remove(opc);
	}
    
    public int getPosition(String str)
    {
    	return values.lastIndexOf(str);
    }
    
    public List<String> getList()
	{
    	return values;
	}

    public void insert(String opc, int idx)
	{
    	values.add(idx, opc);
	}
    
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; 
        TextView tvText = null;
        
        String str = values.get(position);

        if (convertView == null) { // if it's not recycled, initialize some
                                    // attributes
        LayoutInflater vi = 
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_solo_string , null);
         }
         

        tvText = (TextView) v.findViewById(R.id.tvitemsolostring); 

        tvText.setText(str);
        
        
        
        
        return v;

    }


}
