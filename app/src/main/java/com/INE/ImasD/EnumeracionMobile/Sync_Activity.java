package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Dibujo;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Localidad;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.SSLConnection;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.esri.android.map.ags.ArcGISFeatureLayer;
import com.esri.android.map.ags.ArcGISFeatureLayer.MODE;
import com.esri.core.map.CallbackListener;
import com.esri.core.map.FeatureSet;
import com.esri.core.tasks.ags.query.Query;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class Sync_Activity extends Activity{

	private static File demoDataFile;
	private static String offlineDataSDCardDirName;
	private static String Viv_filename;
	private static String Dib_filename;
	protected static String OFFLINE_FILE_EXTENSION = ".MBEJS";

	DataBaseUtil dbUtil = new DataBaseUtil(Sync_Activity.this);
	
	Button btnDescargarViviendas;
	Button btnEnviarDatos;
	
	private boolean EnvioViviendas = false;
	private boolean EnvioDibujos = false;
	@Override
	 public void onCreate(Bundle savedInstanceState) 
	{

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.sync_activity);

		btnDescargarViviendas = (Button)findViewById(R.id.btnDescargarDatosServer);
		btnEnviarDatos = (Button)findViewById(R.id.btnEnviarDatosServer);
		btnDescargarViviendas.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				if(!funciones.TieneInternet(Sync_Activity.this))
				{
					Toast.makeText(Sync_Activity.this, "Necesita conexi�n a Internet para descargar datos.", Toast.LENGTH_SHORT).show();
					return;
				}
				// TODO Auto-generated method stub
				new AlertDialog.Builder(Sync_Activity.this)
				.setIcon(R.drawable.download)
				.setTitle("Descargar Datos Remotos")
				.setMessage("�Desea comenzar la descarga de datos remotos?")
				.setPositiveButton("Si", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						new SyncObtenerDatos().execute();
					}

				})
				.setNegativeButton("No", null)
				.show();


			}
		});


		btnEnviarDatos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
			// TODO Auto-generated method stub
				
				if(!funciones.TieneInternet(Sync_Activity.this))
				{
					Toast.makeText(Sync_Activity.this, "Necesita conexi�n a Internet para enviar datos.", Toast.LENGTH_SHORT).show();
					return;
				}
				
				new AlertDialog.Builder(Sync_Activity.this)
				.setIcon(R.drawable.upload)
				.setTitle("Enviar Datos")
				.setMessage("�Desea comenzar el env�o de datos recolectados?")
				.setPositiveButton("Si", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						new AsyncEnviarDatos().execute();
					}

				})
				.setNegativeButton("No", null)
				.show();

			}
		});
		demoDataFile = Environment.getExternalStorageDirectory();
		offlineDataSDCardDirName = this.getResources().getString(R.string.SDCardOfflineDir);
		Viv_filename = this.getResources().getString(R.string.nombrearchivoViviendasJSON);
		Dib_filename = this.getResources().getString(R.string.nombrearchivoDibujosJSON);


	}

	private class SyncObtenerDatos extends AsyncTask<Void, Integer, Integer>
	{
		ProgressDialog pd; 

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(Sync_Activity.this);
			pd.setTitle("Preparando descarga...");
			pd.setMessage("Obteniendo datos de servidor");       
			pd.setIndeterminate(false);
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pd.setMax(100);
			pd.show();
		}

		@Override
		protected Integer doInBackground(Void... params) {

			//CrearArchivosFeatureServer();
			SSLConnection.allowAllSSL();
			try
			{
				Log.d("SYNC_GEO", "descarga normal de datos: ");
				return ObtenerEdificios() + ObtenerSecciones() + ObtenerLocalidades() + ObtenerManzanas() + ObtenerViviendas() + ObtenerHogares() + ObtenerImagenes() + ObtenerDibujos();
			}
			catch(Exception ex)
			{
				return -1;
			}

		}


		@Override
		protected void onPostExecute(Integer Result)
		{

			pd.dismiss();
		}

		private Integer ObtenerViviendas()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{
				
				//pd.setTitle("Descargando Viviendas...");
				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerViviendas";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Vivienda> viviendas;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				viviendas = (List<Vivienda>)funciones.ParsearViviendas(envelope.getResponse().toString());

				total = viviendas.size();
				pd.setMax(total);
				dbUtil.open();
				dbUtil.BorrarViviendasEnviadas();

				//da.BorrarTodosUsuarios();
				for(Vivienda viv : viviendas)
				{

					dbUtil.InsertarVivienda(viv);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();

				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}

		private Integer ObtenerHogares()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{
				//pd.setTitle("Descargando Hogares de viviendas ...");
				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ListarHogares";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Hogar> hogares;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				hogares = (List<Hogar>)funciones.ParsearHogares(envelope.getResponse().toString());

				total = hogares.size();
				pd.setMax(total);
				dbUtil.open();
				dbUtil.BorrarHogares();


				for(Hogar hog : hogares)
				{

					hog.setEnviado(true);
					dbUtil.InsertarHogar(hog);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();

				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}
		
		private Integer ObtenerDibujos()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{
				//pd.setTitle("Descargando Hogares de viviendas ...");
				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerDibujos";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Dibujo> dibujos;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				dibujos = (List<Dibujo>)funciones.ParsearDibujos(envelope.getResponse().toString());

				total = dibujos.size();
				pd.setMax(total);
				dbUtil.open();
				dbUtil.BorrarDibujosAntiguos();

				for(Dibujo dib : dibujos)
				{
					dib.setNUEVO(false);
					dbUtil.InsertarDibujo(dib);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();

				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}		
		private Integer ObtenerManzanas()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{
				//pd.setTitle("Descargando Manzanas...");
				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerManzanas";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Manzana> manzanas;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				manzanas = (List<Manzana>)funciones.ParsearManzanas(envelope.getResponse().toString());

				total = manzanas.size();
				pd.setMax(total);
				dbUtil.open();

				dbUtil.BorrarManzanas();

				//da.BorrarTodosUsuarios();

				for(Manzana mz : manzanas)
				{
					dbUtil.InsertarManzana(mz);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();



				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}


		private Integer ObtenerLocalidades()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{

				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerLocalidades";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Localidad> localidades;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				localidades = (List<Localidad>)funciones.ParsearLocalidades(envelope.getResponse().toString());

				total = localidades.size();
				pd.setMax(total);
				dbUtil.open();
				dbUtil.BorrarLocalidades();
				//da.BorrarTodosUsuarios();

				for(Localidad loc : localidades)
				{
					dbUtil.InsertarLocalidad(loc);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();



				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}


		private Integer ObtenerSecciones()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{

				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerSeccionesUsuario";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Seccion> secciones;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				secciones = (List<Seccion>)funciones.ParsearSecciones(envelope.getResponse().toString());

				total = secciones.size();
				pd.setMax(total);
				dbUtil.open();

				dbUtil.BorrarSecciones();

				//da.BorrarTodosUsuarios();

				for(Seccion sec : secciones)
				{
					dbUtil.InsertarSeccion(sec);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();



				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}

		private Integer ObtenerEdificios()
		{
			dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{

				//pd.setTitle("Descargando Edificios...");
				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerEdificios";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Edificio> edificios;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("region_id", globales.region.getRegion_id());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new  HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				edificios = (List<Edificio>)funciones.ParsearEdificios(envelope.getResponse().toString());

				total = edificios.size();
				pd.setMax(total);
				dbUtil.open();

				dbUtil.BorrarEdificios();

				//da.BorrarTodosUsuarios();

				for(Edificio edif : edificios)
				{
					
					dbUtil.InsertarEdificio(edif);
					i++;
					pd.setProgress(i);

				}
				dbUtil.close();



				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}
		
		private Integer ObtenerImagenes()
		{
			//dbUtil = new DataBaseUtil(Sync_Activity.this);

			try
			{
				pd.setTitle("Descargando Imagenes de Viviendas...");
				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerImagenesCargaUsuario";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Imagen> imagenes;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", globales.actualizacion.getAct_id());
				request.addProperty("id_usuario", globales.usuario.getId_usuario());


				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				transporte.call(SOAP_ACTION, envelope);
				imagenes = (List<Imagen>)funciones.ParsearImagenes(envelope.getResponse().toString());

				total = imagenes.size();
				pd.setMax(total);

				String path;
				File outfile;
				FileOutputStream outstream = null;

				
				for(Imagen img : imagenes)
				{
					try {
						path= globales.RutaPicsSinImagen + img.getID_VIVIENDA().toString() + ".jpg";
						outfile = new File(path);
						outstream = new FileOutputStream(outfile);
						outstream.write(img.getIMAGEN());
						outstream.close();
					} catch (Exception e) {	
						e.printStackTrace();
					}

					i++;
					pd.setProgress(i);

				}

				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}
	}



	
	private void CrearArchivosFeatureServer() {
		// show progress dialog while querying feature layer

		final ArcGISFeatureLayer fLayerViviendas;
		final ArcGISFeatureLayer fLayerDibujo;

		String whereclause ="ACT_ID = " + globales.actualizacion.getAct_id() + " AND ID_USUARIO = " + globales.usuario.getId_usuario(); 


		String URLViviendas = "http://192.168.1.102:6080/arcgis/rest/services/Mobile/" +  globales.region.getRegion_id() + "_EnumeracionPDA/FeatureServer/0";
		fLayerViviendas = new ArcGISFeatureLayer(URLViviendas, MODE.SNAPSHOT);
		fLayerViviendas.setDefinitionExpression(whereclause);


		String URLDibujo = "http://192.168.1.102:6080/arcgis/rest/services/Mobile/" +  globales.region.getRegion_id() + "_EnumeracionPDA/FeatureServer/1";
		fLayerDibujo = new ArcGISFeatureLayer(URLDibujo, MODE.SNAPSHOT);
		fLayerDibujo.setDefinitionExpression(whereclause);


		final Query query = new Query();

		query.setWhere("ACT_ID=" + globales.actualizacion.getAct_id() + " AND ID_USUARIO = " + globales.usuario.getId_usuario());

		//query.setSpatialRelationship(SpatialRelationship);

		//query.setInSpatialReference(SpatialReference.create(4674));
		//query.setOutSpatialReference(SpatialReference.create(102100));




		// return all features
		query.setOutFields(new String[] { "*" });
		// include geometry in result set
		query.setReturnGeometry(true);
		// run query on FeatureLayer off UI thread
		
		Log.d("SYNC_GEO", "VIVIENDAS queryFeatures:" );
		fLayerViviendas.queryFeatures(query, new CallbackListener<FeatureSet>() {

			// an error occurred, log exception
			@Override
			public void onError(final Throwable e) {
						// TODO Auto-generated method stub
				Sync_Activity.this.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Log.d("SYNC_GEO", "error en callback VIVIENDAS: " + e.getMessage());	
					}
				});
					
						
			}

			// create json from resulting FeatureSet
			@Override
			public void onCallback(final FeatureSet result) {
				Log.d("SYNC_GEO", "VIVIENDAS CALLBACK:" );
						if (result != null) {
							Sync_Activity.this.runOnUiThread(new Runnable() {
								public void run() {
									CrearArchivo(result, 0);
									
								}
							});
							


						}
					}
				}); // FIN QUERYFEATURES
		
		Log.d("SYNC_GEO", "DIBUJOS queryFeatures:" );
		fLayerDibujo.queryFeatures(query, new CallbackListener<FeatureSet>() {

			// an error occurred, log exception
			@Override
			public void onError(final Throwable e) {
						// TODO Auto-generated method stub
						Log.d("SYNC_GEO", "error en callback VIVIENDAS: " + e.getMessage());
					}

			// create json from resulting FeatureSet
			@Override
			public void onCallback(final FeatureSet result) {
				Log.d("SYNC_GEO", "DIBUJOS CALLBACK:" );
					if (result != null) {
						Sync_Activity.this.runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								CrearArchivo(result, 1);		
								//GuardarDibujosDB(result);
							}
						});
						

					}

			}
		});
	}
	public void CrearArchivo(FeatureSet result, int CAPA_ID) {
		if (result != null) {
			if(result.getGraphics().length>0)
			{
				String path;
				String fsstring;
				File outfile;
				FileOutputStream outstream = null;
				try {
					// create feature set as json string
					fsstring = FeatureSet.toJson(result);
					// create fully qualified path for json file
					path= createJsonFile(CAPA_ID, "UPDS");
					// create a File from json fully qualified path
					outfile = new File(path);
					// create output stream to write to json file
					outstream = new FileOutputStream(outfile);
					outstream.write(fsstring.getBytes());
					// close output stream
					outstream.close();
					Log.d("SYNC_GEO", "Creaci�n de archivo capa: " + CAPA_ID);
					
				} catch (Exception e) {	
					e.printStackTrace();
				}
			}
		}
	}
/*
	@SuppressWarnings("deprecation")
	public void GuardarDibujosDB(FeatureSet result) {
		if (result != null) {
			if(result.getGraphics().length>0)
			{
				Dibujo dib;
				try {
					
					DataBaseUtil db = new DataBaseUtil(Sync_Activity.this);
					db.open();
					
					for(Graphic gp : result.getGraphics())
					{
						dib = new Dibujo();
						dib.setACT_ID(Integer.valueOf(gp.getAttributeValue("ACT_ID").toString()));
						dib.setID_USUARIO(Integer.valueOf(gp.getAttributeValue("ID_USUARIO").toString()));
						dib.setID_DIBUJO(gp.getUid());
						dib.setTIPO(TIPO_DIBUJO.UPDS.getValue());
						dib.setDESCRIPCION(gp.getAttributeValue("DESCRIPCION").toString());
						dib.setDIBUJO_TEXT(GeometryEngine.geometryToJson(102100, gp.getGeometry()));
						db.InsertarDibujo(dib);
					}
					
					db.close();
					
					Log.d("SYNC_GEO", "DIBUJOS GRABADOS EN BD: ");
					
				} catch (Exception e) {	
					e.printStackTrace();
				}
			}
		}
	}
	*/
	public String createJsonFile(int CapaID, String Tipo) {


		int id_usuario = globales.usuario.getId_usuario();
		int act_id = globales.actualizacion.getAct_id();
		StringBuilder sb = new StringBuilder();
		sb.append(demoDataFile.getAbsolutePath());
		sb.append(File.separator);
		sb.append(offlineDataSDCardDirName);
		sb.append(File.separator);

		File outfile = new File(sb.toString());
		boolean creo = outfile.mkdirs();
		
		sb.append(act_id);
		sb.append("_");
		sb.append(id_usuario);
		sb.append("_");

		if(Tipo=="ADDS")
		{
			if(CapaID==0)
			{
				sb.append(globales.ViviendasAdds);	
			}
			else if (CapaID==1)
			{
				sb.append(globales.DibujoAdds);	
			}
		}
		else if (Tipo=="UPDS")
		{
			if(CapaID==0)
			{
				sb.append(globales.ViviendasUpds);	
			}
			else if (CapaID==1)
			{
				sb.append(globales.DibujoUpds);	
			}
		}		
		else // DELS
		{
			if(CapaID==0)
			{
				sb.append(globales.ViviendasDels);	
			}
			else if (CapaID==1)
			{
				sb.append(globales.DibujoDels);	
			}
		}

		sb.append(OFFLINE_FILE_EXTENSION);
		return sb.toString();
	}

	//comienzo enviar
	private class AsyncEnviarDatos extends AsyncTask<Void, Void, Integer>   
	{

		ProgressDialog pd; 

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(Sync_Activity.this);
			pd.setTitle("Conectando con servidor... Por favor espere.");
			pd.setMessage("Enviando Datos de levantamiento...");       
			pd.setIndeterminate(false);
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pd.setMax(100);
			pd.show();
		}

		@Override
		protected Integer doInBackground(Void... params) {


			try
			{

				ObtenerEnviarViviendas();
				ObtenerEnviarDibujosDV();
				EnviarImagenes();
				//EnviarGeometrias();

				//LimpiarArchivosNuevosFeature(0, "ADDS");
				//LimpiarArchivosNuevosFeature(1, "ADDS");
				return 1;	
			}
			catch(Exception ex)
			{
				return -1;
			}

		}

	/*	
		private void EnviarGeometrias()
		{
			
			//pd.setTitle("Enviando geometr�as recolectadas a servicio de Mapas...");
			ArcGISFeatureLayer fLayerViviendas;
			ArcGISFeatureLayer fLayerDibujo;

			String whereclause ="ACT_ID = " + globales.actualizacion.getAct_id() + " AND ID_USUARIO = " + globales.usuario.getId_usuario(); 


			String URLViviendas = "http://192.168.1.102:6080/arcgis/rest/services/Mobile/" + globales.region.getRegion_id() + "_EnumeracionPDA/FeatureServer/0";
			fLayerViviendas = new ArcGISFeatureLayer(URLViviendas, MODE.SNAPSHOT);
			fLayerViviendas.setDefinitionExpression(whereclause);


			String URLDibujo = "http://192.168.1.102:6080/arcgis/rest/services/Mobile/" + globales.region.getRegion_id() + "_EnumeracionPDA/FeatureServer/1";
			fLayerDibujo = new ArcGISFeatureLayer(URLDibujo, MODE.SNAPSHOT);
			fLayerDibujo.setDefinitionExpression(whereclause);

			FeatureSet viviendas = ObtenerFeatureSetJSON(0, "ADDS");
			FeatureSet dibujos = ObtenerFeatureSetJSON(1,"ADDS");


			if(viviendas!=null)
			{
				Graphic[] layerGraphicsViv = new Graphic[viviendas.getGraphics().length];
				Graphic newgpv;
				int idxviv = 0;

				for(Graphic gp : viviendas.getGraphics())
				{
					//newgpv = new Graphic(GeometryEngine.project(gp.getGeometry(), SpatialReference.create(102100), SpatialReference.create(4674)) , new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxviv+1);
					newgpv = new Graphic(gp.getGeometry(), new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxviv+1);

					layerGraphicsViv[idxviv] = newgpv;
					idxviv++;
				}	


				fLayerViviendas.applyEdits(layerGraphicsViv, null, null, new CallbackListener<FeatureEditResult[][]>() {

					@Override
					public void onError(Throwable arg0) {
						Toast.makeText(Sync_Activity.this,
								 "Error en env�o de Viviendas!", Toast.LENGTH_SHORT)
								.show();
						
						EnvioViviendas = true;
						if(EnvioDibujos && EnvioViviendas && pd.isShowing()) pd.dismiss();
					}

					@Override
					public void onCallback(FeatureEditResult[][] arg0) {
						// TODO Auto-generated method stub
						if(arg0[0][0].isSuccess())
						{
							Toast.makeText(Sync_Activity.this,
									 "Viviendas enviadas exitosamente!", Toast.LENGTH_SHORT)
									.show();
						}
						
						EnvioViviendas = true;
						if(EnvioDibujos && EnvioViviendas && pd.isShowing()) pd.dismiss();

					}
				});

			}

			if(dibujos!=null)
			{
				Graphic[] layerGraphics = new Graphic[dibujos.getGraphics().length];
				Graphic newgpd;
				int idxdib = 0;

				for(Graphic gp : dibujos.getGraphics())
				{
					//newgpd = new Graphic(GeometryEngine.project(gp.getGeometry(), SpatialReference.create(102100), SpatialReference.create(4674)) , new SimpleLineSymbol(Color.BLUE, 5), gp.getAttributes(),idxdib+1);
					newgpd = new Graphic(gp.getGeometry(), new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxdib+1);

					layerGraphics[idxdib] = newgpd;
					idxdib++;
				}


				fLayerDibujo.applyEdits(layerGraphics, null, null, new CallbackListener<FeatureEditResult[][]>()	{

					@Override
					public void onError(Throwable arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(Sync_Activity.this,
								 "Error al enviar dibujos!", Toast.LENGTH_SHORT)
								.show();
						EnvioDibujos = true;
						if(EnvioDibujos && EnvioViviendas && pd.isShowing()) pd.dismiss();
					}

					@Override
					public void onCallback(FeatureEditResult[][] arg0) {
						// TODO Auto-generated method stub
						//Toast.makeText(Sync_Activity.this, "Success! al enviar Dibujos", Toast.LENGTH_SHORT).show();
						Toast.makeText(Sync_Activity.this,
								 "Dibujos enviados exitosamente!", Toast.LENGTH_SHORT)
								.show();
						EnvioDibujos = true;
						
						if(EnvioDibujos && EnvioViviendas && pd.isShowing()) pd.dismiss();
					}
					
				});


			}


			
			 //* envio de actualizaciones
			 			


			FeatureSet viviendasUpds = ObtenerFeatureSetJSON(0, "UPDS");


			if(viviendasUpds!=null)
			{
				Graphic[] layerGraphicsVivUpds = new Graphic[viviendasUpds.getGraphics().length];
				Graphic newgpv;
				int idxviv = 0;

				for(Graphic gp : viviendasUpds.getGraphics())
				{
					//newgpv = new Graphic(GeometryEngine.project(gp.getGeometry(), SpatialReference.create(102100), SpatialReference.create(4674)) , new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxviv+1);
					newgpv = new Graphic(gp.getGeometry(), new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxviv+1);

					layerGraphicsVivUpds[idxviv] = newgpv;
					idxviv++;
				}	


				fLayerViviendas.applyEdits(null, null, layerGraphicsVivUpds, new CallbackListener<FeatureEditResult[][]>() {

					@Override
					public void onError(Throwable arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(Sync_Activity.this, "Error al enviar Viviendas!", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onCallback(FeatureEditResult[][] arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(Sync_Activity.this, "Success! al enviar Viviendas", Toast.LENGTH_SHORT).show();

					}
				});

			}


			FeatureSet dibujosUpds = ObtenerFeatureSetJSON(1,"UPDS");			
			if(dibujosUpds!=null)
			{
				Graphic[] layerGraphicsDibUpds = new Graphic[dibujosUpds.getGraphics().length];
				Graphic newgpd;
				int idxdib = 0;

				for(Graphic gp : dibujos.getGraphics())
				{
					//newgpd = new Graphic(GeometryEngine.project(gp.getGeometry(), SpatialReference.create(102100), SpatialReference.create(4674)) , new SimpleLineSymbol(Color.BLUE, 5), gp.getAttributes(),idxdib+1);
					newgpd = new Graphic(gp.getGeometry(), new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxdib+1);

					layerGraphicsDibUpds[idxdib] = newgpd;
					idxdib++;
				}


				fLayerDibujo.applyEdits(null, null, layerGraphicsDibUpds, new CallbackListener<FeatureEditResult[][]>()	{

					@Override
					public void onError(Throwable arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(Sync_Activity.this, "Error al enviar Dibujos!", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onCallback(FeatureEditResult[][] arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(Sync_Activity.this, "Success! al enviar Dibujos", Toast.LENGTH_SHORT).show();
					}
				});


			}



		}
*/
/*
		public FeatureSet ObtenerFeatureSetJSON(int CAPA_ID, String Tipo) {
			FeatureSet fs = null;
			JsonParser parser;

			try {
				JsonFactory factory = new JsonFactory();

				parser = factory.createJsonParser(new FileInputStream(createJsonFile(CAPA_ID, Tipo)));        	  

				parser.nextToken();
				fs = FeatureSet.fromJson(parser);
				//fs.setSpatialReference(SpatialReference.create(4674));

			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return fs;
		} 
		*/
		@Override
		protected void onPostExecute(Integer Result)
		{
			ActualizarViviendasEnviadas();
			ActualizarDibujosEnviados();
			pd.dismiss();
			btnDescargarViviendas.setEnabled(true);
			//CrearArchivosFeatureServer();
			
		}
		
		
		private void ObtenerEnviarViviendas()
		{
			DataBaseUtil db = new DataBaseUtil(Sync_Activity.this);
			db.open();
			String XMLViviendas;
			int i = 0;
			int total;
			Cursor c;
			
			c = db.ObtenerViviendas(true);
			
			total = c.getCount();
			

			if(total>0)
			{
				pd.setMax(total);
				
				while(c.moveToNext())
				{
					XMLViviendas = funciones.SerializarFilaCursor(c, "Viviendas", "Vivienda");
					EnviarViviendas(XMLViviendas);
					i++;
					pd.setProgress(i);
					
				}
				//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
				//EnviarViviendas(XMLViviendas);
			}
			
			c.close();
			db.close();
			
		}
		
		private void ObtenerEnviarDibujosDV()
		{
			DataBaseUtil db = new DataBaseUtil(Sync_Activity.this);
			db.open();
			String XMLDibujos;
			int i = 1;
			int total;
			Cursor c;
			
			c = db.ObtenerDibujosCursor(false, true);
			
			total = c.getCount();
			

			if(total>0)
			{
				pd.setMax(total);
				
				if(c.moveToFirst())
				{
					pd.setProgress(0);
					do
					{
						XMLDibujos = funciones.SerializarFilaCursor(c, "Dibujos", "Dibujo");
						EnviarDibujos(XMLDibujos);
						pd.setProgress(i);
						i++;
						
					}while(c.moveToNext());
					
				}
				//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
				//EnviarViviendas(XMLViviendas);
			}
			
			c.close();
			db.close();
			
		}		
		
		private String EnviarViviendas(String XMLViviendas)
		{
			try
		{

			//pd.setTitle("Enviando viviendas nuevas o modificadas...");
			
			String METHOD_NAME = "InsertarModificarViviendaManzana";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

	        request.addProperty("ViviendaXML",  XMLViviendas);
	        
	        
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);

			//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
			transporte.call(SOAP_ACTION, envelope);

			
			return envelope.getResponse().toString();
			}
			catch (IOException e) {
			    e.printStackTrace();
			    return null;
			} catch (XmlPullParserException e) {
			    e.printStackTrace();
			    return null;
			} catch (Exception e) {
			    return null;
			}
			
		}

		private String EnviarDibujos(String XMLDibujos)
		{
			try
		{

			//pd.setTitle("Enviando viviendas nuevas o modificadas...");
			
			String METHOD_NAME = "InsertarModificarDibujos";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

	        request.addProperty("DibujosXML",  XMLDibujos);
	        
	        
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);

			//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
			transporte.call(SOAP_ACTION, envelope);

			String retorno =envelope.getResponse().toString(); 
			
			return retorno;
			}
			catch (IOException e) {
			    e.printStackTrace();
			    return null;
			} catch (XmlPullParserException e) {
			    e.printStackTrace();
			    return null;
			} catch (Exception e) {
			    return null;
			}
			
		}
		private void ActualizarViviendasEnviadas()
		{
			DataBaseUtil db = new DataBaseUtil(Sync_Activity.this);
			db.open();
			db.ActualizarViviendasEnviadas();
			db.close();
			
		}
		
		private void ActualizarDibujosEnviados()
		{
			DataBaseUtil db = new DataBaseUtil(Sync_Activity.this);
			db.open();
			db.ActualizarDibujosEnviados();
			//db.BorrarDibujosNulos();
			db.close();
			
		}		
		
		private void EnviarImagenes()
		{
			///pd.setTitle("Enviando imagenes de viviendas...");
			
			File dir = new File(globales.RutaPicsSinImagen);
			Bitmap bm;
			ByteArrayOutputStream baos;
			String encodedImage ;
			
			String METHOD_NAME = "SubirArchivo";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

			SoapObject request;// = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

	        SoapSerializationEnvelope envelope;
	        
	        HttpTransportSE transporte;
			UUID ID_VIVIENDA;
			int i = 1;
			pd.setMax(dir.list().length);
			
			for(File img : dir.listFiles())
			{
				
				ID_VIVIENDA = UUID.fromString(img.getName().replace(".jpg", ""));
				
				bm = BitmapFactory.decodeFile(img.getAbsolutePath());
				baos = new ByteArrayOutputStream();  
				bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object   
				byte[] b = baos.toByteArray(); 
				encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
				
				request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
		        request.addProperty("b",  encodedImage);
		        request.addProperty("ID_VIVIENDA",  ID_VIVIENDA.toString());
		        
				envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				transporte = new HttpTransportSE(globales.WS_URL);

				//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
				try {
					transporte.call(SOAP_ACTION, envelope);
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally
				{
					pd.setProgress(i);
					i++;
					
				}
			}

			
		}

	}

	// fin enviar
	/*
	private void LimpiarArchivosNuevosFeature(int CAPA_ID, String Tipo)
	{
		File outfile = new File(createJsonFile(CAPA_ID, Tipo));
		outfile.delete();

	}

*/
@Override
public void onResume() {
 super.onResume();
 
 DataBaseUtil db = new DataBaseUtil(Sync_Activity.this);
 db.open();
 
 if(db.ExistenModificadas())
 {
	 btnDescargarViviendas.setEnabled(false);
	 Toast.makeText(Sync_Activity.this, "No puede descargar hasta que haya enviado las modificaciones pendientes", Toast.LENGTH_LONG).show();
 }
 else
 {
	 btnDescargarViviendas.setEnabled(true);
 }
 db.close();
}
	
}
