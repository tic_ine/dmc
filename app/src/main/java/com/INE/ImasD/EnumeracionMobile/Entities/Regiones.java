package com.INE.ImasD.EnumeracionMobile.Entities;

public class Regiones {
	private int region_id;
	private String region_glosa;
	
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}
	public String getRegion_glosa() {
		return region_glosa;
	}
	public void setRegion_glosa(String region_glosa) {
		this.region_glosa = region_glosa;
	}
	
public Regiones(int region_id, String NombreRegion)
	{
	this.region_id = region_id;
	this.region_glosa = NombreRegion;
	
	}
public Regiones()
{

}	
	
}
