package com.INE.ImasD.EnumeracionMobile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Actualizaciones_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Regiones_Adapter;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_DownloadNewVersion;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Actualizacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.Entities.CategoriaPOI;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Encuesta;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.Regiones;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Usuario;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.SSLConnection;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.TripleDES;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.io.UserCredentials;
import com.esri.core.map.CallbackListener;
import com.esri.core.map.Feature;
import com.esri.core.map.FeatureResult;
import com.esri.core.map.Graphic;
import com.esri.core.tasks.query.QueryParameters;
import com.esri.core.tasks.query.QueryTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Locale;

public class Login_Activity extends Activity {
	
	private int REQUEST_MAIN_ACTIVITY = 1;
	private static final int REQUEST_CODE_PERMISSION = 2;
	private DataBaseUtil dbUtil;
	final private Usuario usu =  new Usuario();
	final private Actualizacion act = new Actualizacion();
	final private Regiones reg = new Regiones();
	EditText txtusername;
	EditText txtContrasena;
	Spinner spRegion;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		getActionBar().setDisplayShowHomeEnabled(true);
       
		spRegion = (Spinner)findViewById(R.id.spRegiones);
		
		Button btnIngresar = (Button)findViewById(R.id.btnValidar);
		txtusername = (EditText)findViewById(R.id.txtUsername);
		txtContrasena = (EditText)findViewById(R.id.txtContrasena);
		TextView aa;


		String[] mPermission = {Manifest.permission.RECORD_AUDIO,
				Manifest.permission.CAMERA,
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.INTERNET,
				Manifest.permission.ACCESS_NETWORK_STATE,
				Manifest.permission.CHANGE_NETWORK_STATE,

		};
/*
		try {
			if (ActivityCompat.checkSelfPermission(this, mPermission[0])
					!= MockPackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(this, mPermission[1])
							!= MockPackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(this, mPermission[2])
							!= MockPackageManager.PERMISSION_GRANTED ||
					ActivityCompat.checkSelfPermission(this, mPermission[3])
							!= MockPackageManager.PERMISSION_GRANTED) {

				ActivityCompat.requestPermissions(this,
						mPermission, REQUEST_CODE_PERMISSION);

				// If any permission aboe not allowed by user, this condition will execute every tim, else your else part will work
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

*/

		CargarRegiones();

		//CargarActualizaciones();

		btnIngresar.setOnClickListener(new OnClickListener() {


			public void onClick(View v) 
			{
				String username, pass;

				username = txtusername.getEditableText().toString().trim().toUpperCase();
				pass = txtContrasena.getEditableText().toString().trim();

				try {
					pass = new TripleDES().encryptText(pass);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if(pass==null)
				{
					Toast.makeText(Login_Activity.this, "Error!!!", Toast.LENGTH_SHORT).show();
					return;
				}

				if(username.equals(""))
				{
					Toast.makeText(Login_Activity.this, "�Debe ingresar Nombre de Usuario!", Toast.LENGTH_SHORT).show();
				}
				else if (pass.equals(""))
				{
					Toast.makeText(Login_Activity.this, "�Debe ingresar Contrase�a!", Toast.LENGTH_SHORT).show();
				}
				else // todos los datos
				{

					usu.setUsername(username);
					usu.setContrasena(pass);

					dbUtil = new DataBaseUtil(Login_Activity.this);
					dbUtil.open();
					boolean existeusuario = dbUtil.ObtenerUsuario(usu);
					
					if(!existeusuario)
					{
						Toast.makeText(Login_Activity.this, "Usuario no v�lido o no vigente!", Toast.LENGTH_SHORT).show();
						dbUtil.close();
					}
					else //Correcto
					{
						//
						final HashSet<String> out = new HashSet<String>();
						String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
						String s = "";
						try {
							final Process process = new ProcessBuilder().command("mount")
									.redirectErrorStream(true).start();
							process.waitFor();
							final InputStream is = process.getInputStream();
							final byte[] buffer = new byte[1024];
							while (is.read(buffer) != -1) {
								s = s + new String(buffer);
							}
							is.close();
						} catch (final Exception e) {
							e.printStackTrace();
						}

						// parse output
						final String[] lines = s.split("\n");
						for (String line : lines) {
							if (!line.toLowerCase(Locale.US).contains("asec")) {
								if (line.matches(reg)) {
									String[] parts = line.split(" ");
									for (String part : parts) {
										if (part.startsWith("/"))
											if (!part.toLowerCase(Locale.US).contains("vold"))
												out.add(part);
									}
								}
							}
						}

						String Ruta_TPK = "file://" + Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator;
						String Ruta_MapaBase = Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator + "base.mbtiles";

						if (out.size() != 0){
							String[] test = out.toArray(new String[out.size()]);
							String temp = "/storage/"+test[0].substring(14);
							String ruta_tpkTEMP 	 = temp + "/MapaBase/";
							String ruta_MapaBaseTEMP =  temp +"/MapaBase/base.mbtiles";
							//VERIFICO QUE ESTE EN AL SD CARD
							File fileTPK = new File(ruta_tpkTEMP);
							File fileBASE = new File(ruta_MapaBaseTEMP);
							if(fileTPK.exists()){
								globales.Ruta_TPK = temp + "/MapaBase/";
							}else{
								//BUSCA DE LA MEMORIA INTERNA
								globales.Ruta_TPK =  Ruta_TPK;
							}
							if(fileBASE.exists()){
								globales.Ruta_MapaBase=  temp +"/MapaBase/base.mbtiles";
							}else{
								//BUSCA DE LA MEMORIA INTERNA
								globales.Ruta_MapaBase =   Ruta_MapaBase;
							}
							// NO TIENE SD
						}else{
							globales.Ruta_TPK =  Ruta_TPK;
							globales.Ruta_MapaBase =   Ruta_MapaBase;
						}

						dbUtil.close();
						globales.usuario = usu;
						globales.actualizacion = act;
						globales.region = ((Regiones)spRegion.getItemAtPosition(spRegion.getSelectedItemPosition()));
						//globales.Ruta_SPATIALITE =  Environment.getExternalStorageDirectory().getPath() + "/SPATIALITE/spatialite_"+ globales.region.getRegion_id() + ".sqlite";
						globales.Ruta_SPATIALITE = 	 Environment.getExternalStorageDirectory().getPath() + "/SPATIALITE/spatialite.sqlite";
						//globales.Ruta_TPK = "file://" + Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator + globales.region.getRegion_id() + "_TPK.tpk";

						funciones.LlenarParametroWS(Login_Activity.this);

						Intent i = new Intent(Login_Activity.this, Main_Activity.class);
						startActivityForResult(i, REQUEST_MAIN_ACTIVITY);
						//finish();

					}

				}

			}
		});

		Spinner sp = (Spinner)findViewById(R.id.spActualizacion);
		sp.setPrompt("Seleccione Actualizaci�n");
		
		sp.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				// TODO Auto-generated method stub
				Integer Miid;
				if(parent.getId()== R.id.spActualizacion)
				{
					Miid = ((Actualizacion)parent.getItemAtPosition(pos)).getAct_id();
					act.setAct_id(Miid);

					//Toast.makeText(getApplicationContext(), Miid.toString(), Toast.LENGTH_SHORT).show();	
				}
			}


			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spRegion.setPrompt("Seleccione Regi�n");
		spRegion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				// TODO Auto-generated method stub
				Integer Miid;
				if(parent.getId()== R.id.spRegiones)
				{
					Miid = ((Regiones)parent.getItemAtPosition(pos)).getRegion_id();
					reg.setRegion_id(Miid);

					Toast.makeText(Login_Activity.this, Miid.toString(), Toast.LENGTH_SHORT).show();	
					CargarActualizaciones();
				}
			
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		CrearMenu(menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			if(funciones.TieneInternet(Login_Activity.this))
			{
				new ObtenerDatosAsync().execute();
			}
			else
			{
				Toast.makeText(Login_Activity.this, "Sin conexi�n de datos", Toast.LENGTH_LONG).show();
			}

			return true;
		case 1:
			
			Intent i = new Intent(Login_Activity.this, File_Browser_Activity.class);
			i.putExtra("accepted_file_extensions", new String[] {".mmhdb"});
			startActivity(i);			
			
			return true;

		case 2:

			Intent i2 = new Intent(Login_Activity.this, LandingFormularioDinamico_Activity.class);
			startActivity(i2);
			return true;

		case 3:
            new AsyncTask_DownloadNewVersion(Login_Activity.this, new AsyncTask_DownloadNewVersion.AsyncResponse() {
                @Override
                public void processFinish(boolean retorno) {
                    if(retorno)
                    OpenNewVersion();
                }
            }).execute();
            return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}


	private void CrearMenu(Menu menu) {
		MenuItem item2 = menu.add(0, 0, 0, "Sync Usuarios");
		{
			// --Copio las imagenes que van en cada item
			item2.setIcon(R.drawable.download);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
		MenuItem item1 = menu.add(0, 1, 1, "Buscar Archivo Datos.");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.file_explorer);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item3 = menu.add(0, 2, 2, "JSON.");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.checklist_32);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
		MenuItem item4 = menu.add(0, 3, 3, "Update");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.appicon);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_MAIN_ACTIVITY) {
			if (resultCode == RESULT_OK) 
			{ 
				txtusername.setText("");
				txtContrasena.setText("");
			}
			else 
			{
				// se mantienen datos ingresados
			}
		}
	}	

	private class ObtenerDatosAsync extends AsyncTask<Void, Integer, Integer> 
	{
		ProgressDialog prog;

		@Override
		protected void onPreExecute() 
		{
			prog = new ProgressDialog(Login_Activity.this);
			prog.setTitle("Sincronizaci�n de Datos con Directorio Viviendas");
			prog.setMessage("Conectando. Por favor espere.");       
			prog.setIndeterminate(false);
			prog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			prog.setMax(100);
			prog.setCancelable(false);
			prog.setButton(Dialog.BUTTON_NEGATIVE, "Cancelar", (DialogInterface.OnClickListener)null);
			prog.show();

		}

		// -- called from the publish progress
		// -- notice that the datatype of the second param gets passed to this method

		protected void onProgressUpdate(Integer... progress) {
			setProgress(progress[0]);
		}


		@Override
		protected Integer doInBackground(Void... params) 
		{
			SSLConnection.allowAllSSL();
			return 	ObtenerActualizaciones() +
					ObtenerUsuarios() + 
					ObtenerTiposCalle() + 
					ObtenerUsoDestino() + 
					ObtenerCategorias() + 
					ObtenerTiposViviendaTemporada() + 
					ObtenerCodigosAnotacion() + 
					ObtenerParametros() +
                    ObtenerEncuestas() +
					ObtenerCategoriasPOI();

					//+ ObtenerLayerService();

		}
		private int ObtenerActualizaciones()
		{
			

			dbUtil = new DataBaseUtil(getApplicationContext());

			try
			{
				/*************ACTUALIZACIONES********************/
				String METHOD_NAME = "ListarActualizaciones";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;
				
				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);
				//return "0C419D62-8688-464B-8F83-66531933F5FA

				
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
				transporte.call(SOAP_ACTION, envelope);
				
				
				prog.setProgress(0);
				//NAMESPACE = "http://www.ine.cl/InvestigacionYDesarrollo/WebServices";
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";
				//URL="http://192.168.5.196/wsSyncDirectorioViviendas/wsSyncDV.asmx";
				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
	
				ArrayList<Actualizacion> misactualizaciones;
	
	
				misactualizaciones = (ArrayList<Actualizacion>)funciones.ParsearActualizaciones(envelope.getResponse().toString());
	
				total = misactualizaciones.size();
	
				prog.setMax(total);
				dbUtil.open();
				dbUtil.BorrarTodosActualizaciones();
				//da.BorrarTodosUsuarios();
	
				for(Actualizacion act : misactualizaciones)
				{
					dbUtil.InsertarActualizaciones(act);
					i++;
					prog.setProgress(i);
	
				}
	
	
	
				dbUtil.close();
	
				/**********FIN ACTUALIZACIONES***********/
	
				return 1;
	
			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}
		}
	private int ObtenerUsuarios()
	{
		dbUtil = new DataBaseUtil(getApplicationContext());

		try
		{


			//String NAMESPACE = "http://www.ine.cl/InvestigacionYDesarrollo/WebServices";
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";
			//String URL="http://192.168.5.196/wsSyncDirectorioViviendas/wsSyncDV.asmx";
			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerUsuarios";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			ArrayList<Usuario> misusuarios;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", -1);
			request.addProperty("perfil_id", 1);
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			//SSLConnection.allowAllSSL();
			transporte.call(SOAP_ACTION, envelope);
			misusuarios = (ArrayList<Usuario>)funciones.ParsearUsuarios(envelope.getResponse().toString());

			total = misusuarios.size();
			prog.setMax(total);

			dbUtil = new DataBaseUtil(Login_Activity.this);
			dbUtil.open();
			dbUtil.BorrarTodosUsuarios();
			//da.BorrarTodosUsuarios();

			for(Usuario usuario : misusuarios)
			{
				dbUtil.InsertarUsuario(usuario);
				i++;
				prog.setProgress(i);

			}
			dbUtil.close();


			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
		private Integer ObtenerUsoDestino()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);
	    	  
            try
            {
            	          	  
                //String NAMESPACE = globales.WS_NAMESPACE;
                //String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
                //String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";
                
                //String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
                String METHOD_NAME = "ListarUsoDestino";
                String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
                int total = 0;
                int i = 0;

                List<UsoDestino> usos;


                SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			//	SSLConnection.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);
                usos = (List<UsoDestino>)funciones.ParsearUsoDestino(envelope.getResponse().toString());
                
                total = usos.size();
                prog.setMax(total);
                dbUtil.open();
                dbUtil.BorrarUsoDestino();
          	 //da.BorrarTodosUsuarios();
          	 
				for(UsoDestino uso : usos)
				{
				 	dbUtil.InsertarUsosDestino(uso);
					i++;
					prog.setProgress(i);
				}
				dbUtil.close();

                return 1;
                
             }
            catch (Exception e)
            {
                //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
                return 0;
            }

        }

		private Integer ObtenerTiposCalle()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);
	    	  
            try
            {
            	          	  
                //String NAMESPACE = globales.WS_NAMESPACE;
                //String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
                //String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";
                
                //String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
                String METHOD_NAME = "ListarTiposCalle";
                String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
                int total = 0;
                int i = 0;

                List<TipoCalle> tipos;

                SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);
                tipos = (List<TipoCalle>)funciones.ParsearTipoCalle(envelope.getResponse().toString());
                
                total = tipos.size();
                prog.setMax(total);
                dbUtil.open();
                dbUtil.BorrarTiposCalle();
          	 //da.BorrarTodosUsuarios();
          	 
				 for(TipoCalle tc : tipos)
				 {
					 dbUtil.InsertarTiposCalle(tc);
					i++;
					prog.setProgress(i);

				 }
				 dbUtil.close();
          	 
          	
          	 
                return 1;
                
             }
            catch (Exception e)
            {
                //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
                return 0;
            }

        }
		
		private Integer ObtenerCategorias()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);
	    	  
            try
            {
            	          	  
                String METHOD_NAME = "ObtenerCategorias";
                String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
                int total = 0;
                int i = 0;

                List<Categoria> cats;

                SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);
                cats = (List<Categoria>)funciones.ParsearCategoria(envelope.getResponse().toString());
                
                total = cats.size();
                prog.setMax(total);
                dbUtil.open();
                dbUtil.BorrarCategoria();
          	 //da.BorrarTodosUsuarios();
          	 
          	 for(Categoria cat : cats)
          	 {
          		 dbUtil.InsertarCategoria(cat);
          		i++;
          		prog.setProgress(i);

          	 }
          	 dbUtil.close();
          	 
          	
          	 
                return 1;
                
             }
            catch (Exception e)
            {
                //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
                return 0;
            }

        }	
		
		private Integer ObtenerTiposViviendaTemporada()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);
	    	  
            try
            {
            	          	  
                String METHOD_NAME = "ObtenerTiposViviendasTemporada";
                String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
                int total = 0;
                int i = 0;

                List<TipoViviendaTemporada> tipos;


                SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);
                tipos = (List<TipoViviendaTemporada>)funciones.ParsearTiposViviendaTemporada(envelope.getResponse().toString());
                
                total = tipos.size();
                prog.setMax(total);
                dbUtil.open();
                dbUtil.BorrarTipoViviendaTemporada();
          	    //da.BorrarTodosUsuarios();

                 for(TipoViviendaTemporada tipo : tipos)
                 {
                     dbUtil.InsertarTipoViviendaTemporada(tipo);
                    i++;
                    prog.setProgress(i);

                 }

          	 dbUtil.close();

                return 1;
                
            }
            catch (Exception e)
            {
                //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
                return 0;
            }

        }
		
		
		private Integer ObtenerCodigosAnotacion()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);
	    	  
            try
            {
            	          	  
                String METHOD_NAME = "ListarCodigos";
                String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
                int total = 0;
                int i = 0;

                List<CodigoAnotacion> codigos;

                SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);
                codigos = (List<CodigoAnotacion>)funciones.ParsearCodigoAnotacion(envelope.getResponse().toString());
                
                total = codigos.size();
                prog.setMax(total);
                dbUtil.open();
                dbUtil.BorrarCodigosAnotacion();
          	 //da.BorrarTodosUsuarios();
          	 
                for(CodigoAnotacion codigo : codigos)
                {
                    dbUtil.InsertarCodigosAnotacion(codigo);
                    i++;
                    prog.setProgress(i);

                }
                dbUtil.close();
          	 
          	
          	 
                return 1;
                
             }
            catch (Exception e)
            {
                //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
                return 0;
            }

        }
		
		
		private Integer ObtenerParametros()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);

            try
            {

                //String NAMESPACE = globales.WS_NAMESPACE;
                //String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
                //String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

                //String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
                String METHOD_NAME = "ObtenerParametros";
                String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
                int total = 0;
                int i = 0;

                List<Parametro> params;


                SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
                transporte.call(SOAP_ACTION, envelope);
                params = (List<Parametro>)funciones.ParsearParametros(envelope.getResponse().toString());

                total = params.size();
                prog.setMax(total);
                dbUtil.open();
                dbUtil.BorrarParametros();
          	 //da.BorrarTodosUsuarios();

                 for(Parametro param : params)
                 {
                     dbUtil.InsertarParametro(param);
                    i++;
                    prog.setProgress(i);

                 }
                 dbUtil.close();



                return 1;

             }
            catch (Exception e)
            {
                //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
                return 0;
            }

        }

		private Integer ObtenerEncuestas()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);

			try
			{

				//String NAMESPACE = globales.WS_NAMESPACE;
				//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
				//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

				//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
				String METHOD_NAME = "ObtenerEncuestas";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<Encuesta> encuestas;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("act_id", 26);//globales.actualizacion.getAct_id());
				request.addProperty("key", globales.key);

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
				transporte.call(SOAP_ACTION, envelope);
				encuestas = (List<Encuesta>)funciones.ParsearEncuestas(envelope.getResponse().toString());

				total = encuestas.size();
				prog.setMax(total);
				dbUtil.open();
				dbUtil.BorrarEncuestas();
				//da.BorrarTodosUsuarios();

				for(Encuesta enc : encuestas)
				{
					dbUtil.InsertarEncuesta(enc);
					i++;
					prog.setProgress(i);

				}

				dbUtil.close();



				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}

		private Integer ObtenerCategoriasPOI()
		{
			dbUtil = new DataBaseUtil(Login_Activity.this);

			try
			{
				String METHOD_NAME = "ObtenerCategoriasPOI";
				String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
				int total = 0;
				int i = 0;

				List<CategoriaPOI> cats;


				SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
				request.addProperty("key", globales.key);

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
				//SSLConnection.allowAllSSL();
				transporte.call(SOAP_ACTION, envelope);
				cats = funciones.ParsearCategoriasPOI(envelope.getResponse().toString());

				total = cats.size();
				prog.setMax(total);


				dbUtil.open();
				dbUtil.BorrarCategoriasPOI();

				for(CategoriaPOI cat : cats)
				{
					try {

						dbUtil.InsertarCategoriaPOI(cat);

					} catch (Exception e) {
						e.printStackTrace();
					}

					i++;
					prog.setProgress(i);

				}

				return 1;

			}
			catch (Exception e)
			{
				//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
				return 0;
			}

		}

        private int  ObtenerLayerService() {
            UserCredentials creds = null;
            String usermarco = "";
            String passmarco= "";
            String UrlToken= "";
            String UrlDirectoryService= "";
            String SeccionesLayer= "";
            String ManzanasLayer= "";

            List<Parametro> Gis_params;
            try
            {
                DataBaseUtil db = new DataBaseUtil(Login_Activity.this);
                db.open();
                usermarco = db.ObtenerParametro("GIS_USERNAME").getValor();
                passmarco = db.ObtenerParametro("GIS_USER_PASS").getValor();
                UrlToken = db.ObtenerParametro("GIS_URL_TOKEN").getValor();
                UrlDirectoryService = db.ObtenerParametro("GIS_MARCO_SERVICE").getValor();
                SeccionesLayer = db.ObtenerParametro("GIS_SECCIONES_LAYER").getValor();
                ManzanasLayer = db.ObtenerParametro("GIS_MANZANAS_LAYER").getValor();
                db.close();


                creds = new UserCredentials();
                try {
                    creds.setUserAccount(usermarco, new TripleDES().decryptText(passmarco));
                }
                catch (Exception ex)
                {

                }



                creds.setTokenServiceUrl(UrlToken);
            }
            catch ( Exception e)
            {
                Toast.makeText(Login_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            try
            {

                QueryParameters querypar = new QueryParameters();

                querypar.setOutFields(new String[] {"*"});
                querypar.setInSpatialReference(SpatialReference.create(3857));
                querypar.setOutSpatialReference(SpatialReference.create(3857));

                querypar.setReturnGeometry(true);
                querypar.setWhere("CUT = 13401");

                QueryTask qt = new QueryTask(UrlDirectoryService +  SeccionesLayer, creds);

                qt.execute(querypar, new CallbackListener<FeatureResult>() {
                    @Override
                    public void onCallback(FeatureResult objects)
                    {

                        // iterate through results
                        for (Object element : objects)
                        {
                            // if object is feature cast to feature
                            if (element instanceof Feature)
                            {
                                Feature feature = (Feature) element;
                                // convert feature to graphic
                                Graphic graphic = new Graphic(feature.getGeometry(), feature.getSymbol(), feature.getAttributes());
                                globales.Graphics_SeccionesService.add(graphic);
                            }
                        }

                        try
                        {
                            DataBaseSpatialite dbsl = new DataBaseSpatialite();

							dbsl.BorrarSecciones();
                            dbsl.InsertarSecciones();
                            dbsl.CrearSpatialIndex("marco_secciones", "geometry");
                            dbsl.CerrarBD();

                        }
                        catch (jsqlite.Exception e)
                        {

                        }


                        Toast.makeText(Login_Activity.this, "polys!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Toast.makeText(Login_Activity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
            catch (Exception e)
            {
                Toast.makeText(Login_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            return 1;
		}

		@Override
		protected void onPostExecute(Integer result) 
		{
			super.onPostExecute(result);

			Context context = Login_Activity.this;
//			context.getDatabasePath("dv.db");
			
			int duration = Toast.LENGTH_SHORT;

			if(result>0)
			{
				prog.dismiss();
				Toast.makeText(context, "Datos Obtenidos correctamente", duration).show();
				CargarActualizaciones();
				
				//String ok = funciones.DescomprimirBD(Login_Activity.this, Environment.getExternalStorageDirectory() + "/DV/dv.zip");
				
			}
			else
			{
				Toast.makeText(context, "Error al Obtener Datos", Toast.LENGTH_SHORT).show();

			}

		}
	}
	
	

	private void CargarActualizaciones()
	{
		dbUtil = new DataBaseUtil(Login_Activity.this);

		dbUtil.open();
		List<Actualizacion> misact = dbUtil.ObtenerActualizaciones(reg.getRegion_id());
		Spin_Actualizaciones_Adapter sa = new Spin_Actualizaciones_Adapter(this, misact);	  
		Spinner s = (Spinner) findViewById( R.id.spActualizacion );
		s.setAdapter(sa);
		dbUtil.close();
	}


	
	private void CargarRegiones()
	{
		Spinner spRegion = (Spinner) findViewById(R.id.spRegiones);
		 
		List<Regiones> regiones = new ArrayList<Regiones>();
		regiones.add(new Regiones(1, "Regi�n de Tarapac�"));
		regiones.add(new Regiones(2, "Regi�n de Antofagasta"));
		regiones.add(new Regiones(3, "Regi�n de Atacama"));
		regiones.add(new Regiones(4, "Regi�n de Coquimbo"));
		regiones.add(new Regiones(5, "Regi�n de Valpara�so"));
		regiones.add(new Regiones(6, "Regi�n del Lib. Bdo. O\'Higgins"));
		regiones.add(new Regiones(7, "Regi�n del Maule"));
		regiones.add(new Regiones(8, "Regi�n del B�o B�o"));
		regiones.add(new Regiones(9, "Regi�n de la Araucan�a"));
		regiones.add(new Regiones(10, "Regi�n de los Lagos"));
		regiones.add(new Regiones(11, "Regi�n de Ays�n"));
		regiones.add(new Regiones(12, "Regi�n de Magallanes"));
		regiones.add(new Regiones(13, "Regi�n Metropolitana"));
		regiones.add(new Regiones(14, "Regi�n de Los R�os"));
		regiones.add(new Regiones(15, "Regi�n de Arica y Parinacota"));
		regiones.add(new Regiones(16, "Regi�n de �uble"));

		Spin_Regiones_Adapter sr = new Spin_Regiones_Adapter(this, regiones);	   
		spRegion.setAdapter(sr);
		
		sr.notifyDataSetChanged();
		 
	}

    private void OpenNewVersion()
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File( Environment.getExternalStorageDirectory()+"/Download/" + "app.apk")),
                "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }

}

