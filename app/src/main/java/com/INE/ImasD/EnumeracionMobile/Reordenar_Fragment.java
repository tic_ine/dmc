package com.INE.ImasD.EnumeracionMobile;


import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;

//import android.support.v4.app.DialogFragment;

public class Reordenar_Fragment  extends DialogFragment {
	TextView tvFR_RM_GlosaDireccion;
	TextView tvFR_RM_GlosaUsoDestino;
	EditText etvFR_RM_OrdenListado;
	EditText etvFR_RM_txtDescripcion;
	
	ImageButton imgbtnFR_RM_Grabar;
	ImageButton imgbtn_FR_RM_Cancelar;
	
	Button btnFR_RM_Sugerencia;
	private int orden;
	Vivienda viv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.reordenar_fragment, container, false);
        
        tvFR_RM_GlosaDireccion = (TextView)view.findViewById(R.id.tvFR_RM_GlosaDireccion);
        tvFR_RM_GlosaUsoDestino = (TextView)view.findViewById(R.id.tvFR_RM_GlosaUsoDestino);
        etvFR_RM_OrdenListado = (EditText)view.findViewById(R.id.etvFR_RM_OrdenListado);
        etvFR_RM_txtDescripcion = (EditText)view.findViewById(R.id.etvFR_RM_txtDescripcion);
        
        imgbtnFR_RM_Grabar = (ImageButton)view.findViewById(R.id.imgbtnFR_RM_GrabarOrden);
        imgbtn_FR_RM_Cancelar = (ImageButton)view.findViewById(R.id.imgbtnFR_RM_CancelarOrden);

        btnFR_RM_Sugerencia = (Button)view.findViewById(R.id.btnFR_RM_Sugerencia);
        
		String direccion;
		String usodestino;

		viv = (Vivienda)getArguments().getSerializable("VIVIENDA");
		
		orden = viv.getORDEN();
		direccion = viv.getNOMBRE_CALLE_CAMINO() + " N�" + viv.getN_DOMICILIO();
		usodestino = viv.getGLOSA_USODESTINO();
		
		tvFR_RM_GlosaDireccion.setText(direccion);
		tvFR_RM_GlosaUsoDestino.setText(usodestino);
		etvFR_RM_OrdenListado.setText(String.valueOf(viv.getORDEN()));
		etvFR_RM_txtDescripcion.setText(viv.getDESCRIPCION());

		
		btnFR_RM_Sugerencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), Descripcion_Activity.class);
				i.putExtra("DESCRIPCION", etvFR_RM_txtDescripcion.getText().toString());
				
				startActivityForResult(i, 2);
			}
		});
		
		
		imgbtnFR_RM_Grabar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				ConfirmarGrabar();	
				
				
			}
		});
		
		imgbtn_FR_RM_Cancelar.setOnClickListener(new OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				ConfirmarCancelar();
			}
		});
		
		etvFR_RM_OrdenListado.setSelectAllOnFocus(true);
		
		return view;
    }
    
	@Override
	public void onResume() {
	    super.onResume();

	    Window window = getDialog().getWindow();
	    android.graphics.Point size = new android.graphics.Point();

	    Display display = window.getWindowManager().getDefaultDisplay();
	    display.getSize(size);

	    int width = size.x;
	    int height = size.y;

	    window.setLayout((int) (width * 0.75), ((int) (height * 0.75)));
	    window.setGravity(Gravity.TOP);
	}
	
	
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode==65538)
		{
			if (resultCode == -1 && data !=null) {// si grabo se agrega el punto a la capa 
				etvFR_RM_txtDescripcion.setText(data.getStringExtra("DESCRIPCION"));
			}
		}
    }
    
    private boolean GrabarDatos()
    {
		DataBaseUtil db = new DataBaseUtil(getActivity());
		db.open();
		
		boolean ret = db.ReordenarPuntoViviendaConDescripcion(viv);
		
		db.close();
		
		return ret;
    	
    }
    
    private void ConfirmarGrabar()
    {
    	
    	new AlertDialog.Builder(getActivity())
		.setIcon(R.drawable.pregunta)
		.setTitle("Grabar Datos")
		.setMessage("�Est� seguro que desea grabar datos?") 
		.setPositiveButton("S�", new DialogInterface.OnClickListener() 
		{

			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				viv.setORDEN(Integer.valueOf(etvFR_RM_OrdenListado.getText().toString()));
				viv.setDESCRIPCION(etvFR_RM_txtDescripcion.getText().toString());
				
				if(GrabarDatos())
				{
					getActivity().getFragmentManager().beginTransaction().remove(Reordenar_Fragment.this).commit();
					Toast.makeText(getActivity(), "Datos Grabados!", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(getActivity(), "Error al grabar datos!", Toast.LENGTH_SHORT).show();
				}
				
			}

		})
		.setNegativeButton("No", null)
		.show();
    }
    
    private void ConfirmarCancelar()
    {
    	
    	new AlertDialog.Builder(getActivity())
		.setIcon(R.drawable.pregunta)
		.setTitle("Grabar Datos")
		.setMessage("�Est� seguro que desea cancelar la modificaci�n de orden?") 
		.setPositiveButton("S�", new DialogInterface.OnClickListener() 
		{

			@Override
			public void onClick(DialogInterface dialog, int which) 
			{

				getActivity().getFragmentManager().beginTransaction().remove(Reordenar_Fragment.this).commit();
				
			}

		})
		.setNegativeButton("No", null)
		.show();
    }
}