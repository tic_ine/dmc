package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.UUID;

public class Hogar {
	
	private UUID id_vivienda;
	private int act_id;
	private int id_usuario;
	private int ordenhogar;
	private int numpersonas;
	private String jefe;
	private boolean enviado;
	
	public UUID getId_vivienda() {
		return id_vivienda;
	}
	public void setId_vivienda(UUID id_vivienda) {
		this.id_vivienda = id_vivienda;
	}
	public int getAct_id() {
		return act_id;
	}
	public void setAct_id(int act_id) {
		this.act_id = act_id;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getOrdenhogar() {
		return ordenhogar;
	}
	public void setOrdenhogar(int ordenhogar) {
		this.ordenhogar = ordenhogar;
	}
	public int getNumpersonas() {
		return numpersonas;
	}
	public void setNumpersonas(int numpersonas) {
		this.numpersonas = numpersonas;
	}
	
	
	public String getJefe() {
		return jefe;
	}
	public void setJefe(String jefe) {
		this.jefe = jefe;
	}
	public boolean isEnviado() {
		return enviado;
	}
	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	
	

}
