package com.INE.ImasD.EnumeracionMobile.OtrasClases;

import android.content.Context;
import android.graphics.Color;

import com.esri.android.map.GraphicsLayer;
import com.esri.core.geometry.MultiPath;
import com.esri.core.geometry.MultiPoint;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.SimpleFillSymbol;
import com.esri.core.symbol.SimpleLineSymbol;


public class ARCGISCircle {
    private Point center;
    private double radius;
    private int strokeColor;
    private double strokeWidth;
    private SimpleLineSymbol.STYLE strokePattern;
    private int fillColor;
    private SimpleFillSymbol.STYLE fillPattern;
    private int pointCount;
    private int zIndex;

    private Context context;
    private SpatialReference spatialReference;
    private Graphic graphicCircle;
    private static final int POINT_COUNT = 36;

    private static int EARTH_RADIUS = 6378800;

    public ARCGISCircle(Builder builder) {
        center = builder.center;
        radius = builder.radius;
        strokeColor = builder.strokeColor;
        strokeWidth = builder.strokeWidth;
        fillColor = builder.fillColor;
        context = builder.context;
        spatialReference = builder.spatialReference;
        pointCount = builder.pointCount;
        zIndex = builder.zIndex;
        strokePattern = builder.strokePattern;
        fillPattern = builder.fillPattern;
    }

    /**
     * Add a circle on a graphic overlay
     * @param graphicOverlay drawing sheet
     * @return if the action was successfully completed
     */
    public final boolean addCircleOn(GraphicsLayer graphicOverlay){
        boolean drawingOk = false;
        if(graphicOverlay != null && radius > 0 && drawCircle()){
            graphicOverlay.addGraphic(this.graphicCircle);
            drawingOk = true;
        }
        return drawingOk;
    }

    /**
     * Delete a circle
     * @param graphicOverlay drawing sheet
     * @return if the action was successfully completed
     */
    public final boolean removeCircleFrom(GraphicsLayer graphicOverlay){
        boolean deletionOk = false;
        if(graphicOverlay != null){
            if(graphicOverlay.getNumberOfGraphics() > 0){
                graphicOverlay.removeGraphic(this.graphicCircle.getUid());
                deletionOk = true;
            }
        }
        return deletionOk;
    }

    /**
     * Draw a graphic circle
     * @return if the action was successfully completed
     */
    private boolean drawCircle(){
        boolean operationOk = false;
        if(this.spatialReference != null ){

            Polygon circlePolygon = drawPolygonCircle(this.center,this.radius,this.pointCount,this.spatialReference);                      //simple line symbol
            if(circlePolygon != null){ //was successfully drawn
                SimpleLineSymbol outlineSymbol = new SimpleLineSymbol(this.strokeColor,  (float) this.strokeWidth, this.strokePattern);
                SimpleFillSymbol fillSymbol = new SimpleFillSymbol(this.fillColor,this.fillPattern);

                fillSymbol.setOutline(outlineSymbol);

                this.graphicCircle = new Graphic(circlePolygon,fillSymbol);
                //if(zIndex > -1)
                    //this.graphicCircle.setZIndex(this.zIndex);
            }

            if(graphicCircle != null){
                operationOk = true;
            }
        }

        return operationOk;
    }

    /**
     * Draw a polygon circle
     * @param origin origin of the polygon
     * @param radius in meters
     * @param pointCount the number of point on the arc of the circle
     * @param spatialReference the spatial reference
     * @return a polygon formed of arc of circles
     */
    private Polygon drawPolygonCircle(Point origin, double radius, int pointCount, SpatialReference spatialReference ){
        if(radius > 0 && spatialReference != null && origin != null){
            Polygon corners = new Polygon();

            //double radianFactor = 2*Math.PI/pointCount;

            // add point to the circle

            double radians = 2 * Math.PI / pointCount;
            for(int i = 0; i <= pointCount; i++ ){

                radians = 2 * Math.PI / pointCount * i;
                double x = (origin.getX() + radius * Math.cos(radians * i));
                double y = (origin.getY() + radius * Math.sin(radians * i));
                if(i==0)
                {

                    corners.startPath(new Point(x, y));// getPolygonPoint(origin, radius, i * radianFactor));
                }
                else
                {
                    corners.lineTo(new Point(x, y));//getPolygonPoint(origin, radius, i * radianFactor));
                }

            }

            return corners;
        }
        return null;
    }

    /**
     * Defines the location of each point that will form the circle
     * @param center the center of the final circle
     * @param radius the radius of the circle
     * @param angle the angle (formed by segment from the center to the radius)
     * @return a new Location point
     * Source: original code from : https://stackoverflow.com/a/13901270/3808178
     */
    private Point getPolygonPoint(Point center, double radius, double angle) {
        // Get the coordinates of a circle point at the given angle
        double east = radius * Math.cos(angle);
        double north = radius * Math.sin(angle);

        double cLat = center.getY();
        double cLng = center.getX();

        double latRadius = EARTH_RADIUS * Math.cos(cLat / 180 * Math.PI);

        double norte = (north / EARTH_RADIUS / Math.PI * 180);
        double este = (east / latRadius / Math.PI * 180);

        double newLat =  cLat + norte;//(north / EARTH_RADIUS / Math.PI * 180);
        double newLng = cLng + este;//(east / latRadius / Math.PI * 180);

        return new Point(newLng,newLat);
    }

    /**
     * Static builder class
     */
    public static class Builder {
        private Point center;
        private double radius = 0;
        private int strokeColor = Color.RED;
        private double strokeWidth = 2;
        private int fillColor = Color.RED;
        private int pointCount = POINT_COUNT;
        private int zIndex = -1;
        private SimpleLineSymbol.STYLE strokePattern = SimpleLineSymbol.STYLE.SOLID;
        private SimpleFillSymbol.STYLE fillPattern = SimpleFillSymbol.STYLE.SOLID;

        private SpatialReference spatialReference;
        private Context context;

        public Builder(Context context,SpatialReference spatialReference) {
            this.context = context;
            this.spatialReference = spatialReference;
        }

        public Builder center(Point value) {
            if(value == null)
                throw new NullPointerException ("The location is null, it shouldn't be so.");

            center = value;
            return this;
        }

        public Builder radius(double value) {
            radius = value;
            return this;
        }

        public Builder strokeColor(int value) {
            strokeColor = value;
            return this;
        }

        public Builder strokeWidth(double value) {
            strokeWidth = value;
            return this;
        }

        public Builder fillColor(int value) {
            fillColor = value;
            return this;
        }

        public Builder pointCount(int value) {
            pointCount = value;
            return this;
        }

        public Builder zIndex(int value) {
            zIndex = value;
            return this;
        }

        public Builder strokePattern(SimpleLineSymbol.STYLE value) {
            strokePattern = value;
            return this;
        }

        public Builder fillPattern(SimpleFillSymbol.STYLE value) {
            fillPattern = value;
            return this;
        }

        public ARCGISCircle build() {
            return new ARCGISCircle(this);
        }
    }

    /**
     * Get the graphic representing the circle
     * @return
     */
    public Graphic getGraphicCircle() {
        return graphicCircle;
    }
/*
    public void setVisible(boolean visible){
        //if(graphicCircle != null)
            //graphicCircle.setVisible(visible);
    }

    public boolean isVisible(){
        //return graphicCircle != null && graphicCircle.isVisible();
    }
    */
}