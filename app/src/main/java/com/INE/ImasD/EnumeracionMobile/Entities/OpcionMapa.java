package com.INE.ImasD.EnumeracionMobile.Entities;

public class OpcionMapa {


	private int id_img;
	private String glosa_opcion;
	
	
	public int getId_img() {
		return id_img;
	}
	public void setId_img(int id_img) {
		this.id_img = id_img;
	}
	public String getGlosa_opcion() {
		return glosa_opcion;
	}
	public void setGlosa_opcion(String glosa_opcion) {
		this.glosa_opcion = glosa_opcion;
	}

	
	public OpcionMapa(int idimg, String opcion_glosa)
	{
		
		this.setId_img(idimg);
		this.setGlosa_opcion(opcion_glosa);
	}
	
}
