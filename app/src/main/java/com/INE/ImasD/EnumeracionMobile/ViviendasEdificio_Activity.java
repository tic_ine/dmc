package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ListView_ViviendasEdificio_Adapter;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTaskCompleteListener;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_ReordenarViviendas;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.mobeta.android.dslv.DragSortListView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ViviendasEdificio_Activity  extends Activity
{
	DragSortListView lstViviendasEdificio;
	Button btnCOVE_Cerrar;
    Button btnGrabarOrdenDeptos;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    public String mCurrentPhotoPath = "";
    UUID ID_VIVIENDA;
    UUID ID_EDIF;
	public ListView_ViviendasEdificio_Adapter gva;
	
	@Override
	 public void onCreate(Bundle savedInstanceState)
    {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viviendasedificio_activity);
		
		lstViviendasEdificio = (DragSortListView) findViewById( R.id.lstViviendasEdificio);

		//lstViviendasEdificio.setChoiceMode(AbsListView.);
		lstViviendasEdificio.setDropListener(new DragSortListView.DropListener() {
			@Override
			public void drop(int from, int to)
            {
				if (from != to) {
					//DragSortListView list = lstViviendas;
					Vivienda item = gva.getItem(from);
					gva.remove(item);
					gva.insert(item, to);
					gva.notifyDataSetChanged();
				}
			}
		});

		btnCOVE_Cerrar= (Button) findViewById( R.id.btnCOVE_Cerrar);

        btnGrabarOrdenDeptos= (Button) findViewById( R.id.btnGrabarOrdenDeptos);

		Intent i = this.getIntent();
		
		ID_EDIF = UUID.fromString(i.getStringExtra("ID_EDIF"));
		
		final Edificio edif = ObtenerEdificio(ID_EDIF);
		
		this.setTitle("Viviendas Edificio " + edif.getNombre_edif());
		
		CargarViviendas(ObtenerViviendasEdificio(ID_EDIF));
		btnCOVE_Cerrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				
			}
		});

        btnGrabarOrdenDeptos.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                dialog_message(ViviendasEdificio_Activity.this,
                        "�Desea reordenar el listado seg�n orden actual?\nIngrese el # de orden de inicio.",
                        "Ordenamiento dentro de Edificio");

            }

        });
    }


    public void dialog_message(Context ctxt, String mensaje, String titulo)
    {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctxt);
        final EditText input = new EditText(ctxt);
        alertDialog.setTitle(titulo);
        alertDialog.setIcon(R.drawable.mensajes32);
        alertDialog.setMessage(mensaje);

        input.setInputType(InputType.TYPE_CLASS_NUMBER );
        alertDialog.setView(input);


        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener()
        {

            public void onClick(DialogInterface dialog, int which)
            {


            }
        });

        alertDialog.setPositiveButton("S�", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int orden = Integer.valueOf(input.getEditableText().toString());

                new AsyncTask_ReordenarViviendas(ViviendasEdificio_Activity.this, new AsyncTaskCompleteListener<List<Vivienda>>()
                {
                    @Override
                    public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
                        CargarViviendas(viviendas);
                    }

                    @Override
                    public void onTaskCompleteManzanas(List<Manzana> manzanas) {

                    }

                    @Override
                    public void onTaskCompleteSecciones(List<Seccion> secciones) {

                    }

                    @Override
                    public void onTaskCompleteEnvioDatos(Integer resultado) {

                    }

                    @Override
                    public void onTaskCompleteDescargarDatos(Integer resultado) {

                    }
                },orden, gva.getCount()).execute(gva.getList());

                            }
                        });


        alertDialog.show();


    }
	private Edificio ObtenerEdificio(UUID ID_EDIF)
	{
		DataBaseUtil dbUtil = new DataBaseUtil(ViviendasEdificio_Activity.this);
		dbUtil.open();
		Edificio edif = dbUtil.ObtenerEdificio(ID_EDIF);
		dbUtil.close();
		
		return edif;
	}
	private List<Vivienda> ObtenerViviendasEdificio(UUID ID_EDIF)
	{
		if(globales.IDPADRE!=null)
		{
			DataBaseUtil dbUtil = new DataBaseUtil(ViviendasEdificio_Activity.this);
			dbUtil.open();
			List<Vivienda> viviendas = dbUtil.ObtenerViviendasEdificio(ID_EDIF);
			dbUtil.close();
			return viviendas;
		}
		else
		{
			return new ArrayList<Vivienda>();	
		}
		
	}
	
	private void CargarViviendas(List<Vivienda> viviendas)
    {

        gva = new ListView_ViviendasEdificio_Adapter(ViviendasEdificio_Activity.this, viviendas);
        lstViviendasEdificio.setAdapter(gva);
        gva.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

            try {

                bm = BitmapFactory.decodeFile(globales.RutaPicsSinImagen + globales.NombreFotoTemp);

                bm = funciones.RotateBitmap(bm, 90);
                if (bm != null)
                {
                    funciones.ResizeAndSaveImage(ViviendasEdificio_Activity.this, bm, 320, 240, 80, globales.ID_VIVIENDA_LISTADO);

                } else
                {
                    Toast.makeText(ViviendasEdificio_Activity.this, "Error al asociar foto", Toast.LENGTH_SHORT).show();
                }


            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }


            Toast.makeText(ViviendasEdificio_Activity.this, "Foto creada", Toast.LENGTH_SHORT).show();
            //Bundle extras = data.getExtras();
            CargarViviendas(ObtenerViviendasEdificio(ID_EDIF));
        }

    }
}
