package com.INE.ImasD.EnumeracionMobile.OtrasClases;

import android.app.Application;
import android.os.Environment;

import com.INE.ImasD.EnumeracionMobile.Entities.Actualizacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Regiones;
import com.INE.ImasD.EnumeracionMobile.Entities.Usuario;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.map.Graphic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class globales extends Application{

	public static Usuario usuario = new Usuario();
	public static Actualizacion actualizacion = new Actualizacion();
	
	public static String IDPADRE = ""; //= new Padre();
	
	public static String LOCALIDAD = ""; //= new Padre();

	public static Integer AT_ID = 0; //= new Padre();
	
	public static Regiones region  = new Regiones();
	public static int RPC  = 0;
	public static double Latitud = 0.0;
	public static double Longitud = 0.0;
	
	public static boolean ConCentroide = false;
	public static Polygon Polygon_UPM;
	
	public static List<Polygon> Polygons_AreaLevantamiento = new ArrayList<Polygon>();
	
	public static List<Polygon> Polygons_Localidades = new ArrayList<Polygon>();

	public static List<Graphic> Graphics_SeccionesService = new ArrayList<Graphic>();
	
	//public static String WS_URL = "http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

	//Desarrollo
	public static String WS_URL = "http://haya.ine.cl/wssyncdirectorioviviendas/wssyncdv.asmx";
	//Testing
	//public static String WS_URL = "http://wssyncdirectorioviviendas-t.ine.cl/wssyncdv.asmx";
	//Producción
	//public static String WS_URL = "https://wssyncdirectorioviviendas.ine.cl/wsSyncDV.asmx";
	// WS NAMESPACE
	//public static String WS_NAMESPACE = "http://wssyncdirectorioviviendas.ine.cl";
	public static String WS_NAMESPACE = "http://www.ine.cl/InvestigacionYDesarrollo/WebServices";


	public static String JSONtoken = "https://enumeracionviviendas.ine.cl/token.txt";

	public  static String URLAPK = "https://enumeracionviviendas.ine.cl/app.apk";
	
	public static String ViviendasAdds = "VivAdds";
	
	public static String DibujoAdds = "DibAdds";
	
	public static String ViviendasUpds = "VivUpds";
	
	public static String DibujoUpds = "DibUpds";

	public static String ViviendasDels = "VivDels";
	public static String DibujoDels = "DibDels";
	
	public static Point UltimaPuntoObtenido_GPS;
	
	public static Point PuntoViviendaListado;
	
	public static UUID ID_VIVIENDA_LISTADO;
	
	public static String NombreFotoTemp = "/temp.jpg";

	public static String RutaAudioTemp =  Environment.getExternalStorageDirectory().getPath() + "/temp.aac";

	public static String RutaPicsSinImagen = Environment.getExternalStorageDirectory().getPath() + "/EnumVivPics/";

	public static String Ruta_TPK =  "file://" + Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator;

	public static String Ruta_MapaBase =   Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator + "base.mbtiles";

	public static String Ruta_MBTILES = Environment.getExternalStorageDirectory().getPath() + "/MapaBase/rm.mbtiles";

	public static String Ruta_SPATIALITE = Environment.getExternalStorageDirectory().getPath() + "/SPATIALITE/spatialite.sqlite";


    public static int SRID_WebMercator_Spherical = 3857;
	public static int SRID_WGS84 = 3857; //3857
	public static boolean ActualizarCapas = false;
	public static boolean EsCargaDesdeArchivo  = false;
	public static String Ruta_MMHDB;
	
	public static UUID UUID_CERO= UUID.fromString("00000000-0000-0000-0000-000000000000");
	
	public static boolean boolOcultarNulos = false;
	public static boolean boolMostrarDescripciones= false;
	public static boolean boolMostrarNombreEdificio= true;
	public static String strMetrosBusquedaCalles = "0.13";
	public static String strMostrarOrdenes = "1";
	public static String strDistanciaRegistroPosicion = "10";
	
	public static boolean boolCentrarPosicion= true;

    public static boolean boolHabilitarRegistroPosicion = true;
	
	public static String key = "0C419D62-8688-464B-8F83-66531933F5FA";
	
	public static int intPosicionListado = 1;
	
	public static int intCUT = 0;
	
	public static ACCIONES_TABS accion;
	public enum ACCIONES_TABS 
	{
		NINGUNA(0),
		SETEAR_CENTROIDE(1),
		BUSCAR_VIVIENDA(2);
		
		private int value;
		private ACCIONES_TABS(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
	
	
	public static PADRES padres;
	
	public enum PADRES 
	{
		MANZANAS(1),
		SECCIONES(2),
		UPM(3);
		
		private int value;
		private PADRES(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
}
