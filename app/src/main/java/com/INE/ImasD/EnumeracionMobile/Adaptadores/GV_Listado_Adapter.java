package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTaskCompleteListener;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_CargarViviendas;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_ReordenarViviendas;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.Listado_Activity;
import com.INE.ImasD.EnumeracionMobile.Mapa_Simple_DialogFragment;
import com.INE.ImasD.EnumeracionMobile.Mapa_Simple_DialogFragment.AccionesMapaSimple;
import com.INE.ImasD.EnumeracionMobile.ModificarVivienda_Activity;
import com.INE.ImasD.EnumeracionMobile.ModificarVivienda_Rural_Activity;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;
import com.INE.ImasD.EnumeracionMobile.R;
import com.esri.core.geometry.Point;

import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class GV_Listado_Adapter extends BaseAdapter
{

	final private int REQUEST_IMAGE_CAPTURE = 1;
	final private int ASYNC_CARGA_VIVIENDAS = 1000;
	private String mCurrentPhotoPath;
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    public List<Vivienda> values;
    
    //Vivienda MiViv;
    

    public GV_Listado_Adapter(Context context, List<Vivienda> viviendas) {
        super();
        this.context = context;
        this.values = viviendas;
    }

    public int getCount(){
       return values.size();
    }

    public Vivienda getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }

    public void remove(Vivienda vivienda)
	{
    	values.remove(vivienda);
	}
    
    public List<Vivienda> getList()
	{
    	return values;
	}

    public void insert(Vivienda vivienda, int idx)
	{
    	values.add(idx, vivienda);
	}
    
    public View getView(final int position, final View convertView, ViewGroup parent) {

    	View v = convertView; 
        TextView txtDireccion = null;
        TextView txtDescripcion = null;
        
        TextView txtOrdenVivienda = null;
        
        TextView tvIVEdificacion = null;
        TextView txtViviendaUsoDestino = null;
        TextView tvIVFechaIngreso = null;
        
        TextView tvIVFechaModificacion = null;
        TextView tvIVFechaEliminacion = null;

        
        TextView tvIVUbicaciones;
        
        TextView tvIVNombreEdificio = null;
        
        ImageView imgPreviewViv;
        final ImageView imgEstadoViv;
        final ImageView imgMnuVivienda;
        
        ImageView imgTipoVivienda;
        CheckBox chkOk;
        
        TextView tvIVHogaresPersonas; 
		final Vivienda viv = values.get(position);

        if (convertView == null) { // if it's not recycled, initialize some
                                    // attributes
        LayoutInflater vi = 
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_vivienda , null);
         }
        
        
        globales.intPosicionListado = ((android.widget.ListView)parent).getFirstVisiblePosition();
        
        		
        txtDireccion = (TextView) v.findViewById(R.id.tvIVDireccion);
        txtDescripcion = (TextView) v.findViewById(R.id.tvIVDescripcion);
        txtOrdenVivienda = (TextView) v.findViewById(R.id.tvIVOrdenViv);
        txtViviendaUsoDestino = (TextView) v.findViewById(R.id.tvIVGlosaUsoDestino); 
        
        tvIVFechaIngreso = (TextView) v.findViewById(R.id.tvIVFechaIngreso);
        tvIVFechaModificacion = (TextView) v.findViewById(R.id.tvIVFechaModificacion);
        tvIVFechaEliminacion = (TextView) v.findViewById(R.id.tvIVFechaEliminacion);
        
        tvIVUbicaciones = (TextView) v.findViewById(R.id.tvIVUbicacionSitio);
        
        imgPreviewViv = (ImageView)v.findViewById(R.id.imgPreviewViv);
        imgEstadoViv = (ImageView)v.findViewById(R.id.imgEstadoViv);
        
        imgTipoVivienda = (ImageView)v.findViewById(R.id.imgTipoVivienda);
        
        imgMnuVivienda = (ImageView)v.findViewById(R.id.imgMnuVivienda);
        
        chkOk = (CheckBox)v.findViewById(R.id.chkOk);
        
        tvIVHogaresPersonas = (TextView)v.findViewById(R.id.tvIVHogaresPersonas);
        
        tvIVEdificacion = (TextView)v.findViewById(R.id.tvIVEdificacion);
        
        tvIVNombreEdificio = (TextView)v.findViewById(R.id.tvIVNombreEdificio);
        
        txtViviendaUsoDestino.setText(viv.getORDEN() + ".- " + viv.getGLOSA_USODESTINO());
        
        txtDireccion.setText(viv.getNOMBRE_CALLE_CAMINO() + " " + viv.getN_DOMICILIO() + 
        		(viv.getN_LETRA_BLOCK().equals("") ? "" : ".  Block:" + viv.getN_LETRA_BLOCK())+
        		(viv.getN_PISO().equals("") ? "" : ".  Piso:" + viv.getN_PISO())+
        		(viv.getN_LETRA_DEPTO().equals("") ? "" : ". Depto:" + viv.getN_LETRA_DEPTO())
        		);
        
        tvIVFechaIngreso.setTypeface(null, Typeface.ITALIC);
        
        tvIVFechaModificacion.setTypeface(null, Typeface.ITALIC);
        
        txtDescripcion.setText(viv.getDESCRIPCION());
        //chkOK.setChecked(viv.getNULO());
        txtOrdenVivienda.setText("Viv:" + String.valueOf(viv.getORDEN_VIV()));
        
        tvIVEdificacion.setText("Edif:"+ String.valueOf(viv.getORDENEDIFICACION()));

        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        //String dateString = sdf.format(viv.getFECHAINGRESO()); 
        //tvFechaIngreso.setText(dateString);
        tvIVFechaIngreso.setText("Ingr:" + funciones.FormatearFecha(new DateTime(viv.getFECHAINGRESO().getTime())));

        
        if(viv.getFECHAMODIFICACION() != null)
        {
            tvIVFechaModificacion.setText("Modif:" + funciones.FormatearFecha(new DateTime(viv.getFECHAMODIFICACION().getTime())));
        }
        else
        {
        	tvIVFechaModificacion.setText("");
        }
        
        if(viv.getFECHAELIMINACION() != null)
        {
        	tvIVFechaEliminacion.setText("Elim:" + funciones.FormatearFecha(new DateTime(viv.getFECHAELIMINACION().getTime())));
        }
        else
        {
        	tvIVFechaEliminacion.setText("");
        }
        tvIVUbicaciones.setText("Ubicada " + viv.getUBICACIONHORIZONTAL() + " y " + viv.getUBICACIONVERTICAL() + " dentro del domicilio.");
    
        
        if(globales.padres==PADRES.MANZANAS)
        {
            tvIVNombreEdificio.setText(viv.getNOMBRE_EDIF());
        }
        else if(globales.padres==PADRES.SECCIONES)
        {
            tvIVNombreEdificio.setText("Entidad:" + viv.getNOMBREENTIDAD());
        }
    

        
        chkOk.setChecked(viv.isOK());
        
        chkOk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(buttonView.isPressed())
				{
					// TODO Auto-generated method stub
					DataBaseUtil dbUtil = new DataBaseUtil(context);
					dbUtil.open();
					dbUtil.ActualizarOKVivienda(viv.getID_VIVIENDA(), isChecked);
					dbUtil.close();

					funciones.PlaySound(context, (isChecked ? R.raw.confirmar : R.raw.noconfirmar));

					imgEstadoViv.setImageResource(R.drawable.listado);
					Toast.makeText(context, "Vivienda" + (isChecked ? " " : " NO ") + "confirmada", Toast.LENGTH_SHORT).show();
						

					((Listado_Activity)context).ObtenerConteos();
				}
			}
		});
        
        
       
        
        if(viv.isNULO())
        {
        	//chkOk.setEnabled(false);
        	imgEstadoViv.setImageResource(R.drawable.borrar);
        	v.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_corner_deleted));
        	tvIVUbicaciones.setTextColor(context.getResources().getColor(R.color.Black));
        }
        else
        {
        	tvIVUbicaciones.setTextColor(context.getResources().getColor(R.color.White));
        	//v.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.gradienteazul));
        	//chkOk.setEnabled(true);
        	v.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_corner));
        	
        	tvIVHogaresPersonas.setText("Hogares:" + viv.getHOGARES() + ". Personas:" + viv.getPERSONAS());
        	 
			if(viv.isNUEVA())
			{
				imgEstadoViv.setImageResource(R.drawable.star_icon);
			}
			else if(viv.isMODIFICADA())
			{
				imgEstadoViv.setImageResource(R.drawable.listado);
			}
			else
			{
				imgEstadoViv.setImageResource(R.drawable.remoto);
			}
        }
        

        imgMnuVivienda.setTag(position);
        
        imgMnuVivienda.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View menuview) {
				// TODO Auto-generated method stub
				//v.showContextMenu();

				
				PopupMenu popup = new PopupMenu(context, menuview);
                   popup.getMenuInflater().inflate(R.menu.menu_ctx_vivienda,popup.getMenu());
                   funciones.setForceShowIcon(popup);
                   
                   imgMnuVivienda.setImageResource(R.drawable.ic_drawer_selected);
                   //v.setBackgroundResource(R.drawable.rounded_corner_spinner);
                   
                   popup.show();

                   popup.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(PopupMenu menu) {
						// TODO Auto-generated method stub
						imgMnuVivienda.setImageResource(R.drawable.ic_drawer);
						//v.setBackgroundResource(R.drawable.rounded_corner);
					}
				});
                   popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
           			
           			@Override
           			public boolean onMenuItemClick(MenuItem item) {
           				// TODO Auto-generated method stub
           				globales.ID_VIVIENDA_LISTADO = viv.getID_VIVIENDA();
           				
           				//MiViv = getItem((Integer)v.getTag());
           				if (item.getItemId()== R.id.CtxLstCambiarOrden) // Cambiar Orden
           				{
							AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
							alertDialog.setTitle("Ingrese el nuevo Orden: \nEl orden actual es:" + viv.getORDEN() );
							alertDialog.setMessage("Orden debe ser num�rico y no exceder el total de " + values.size() + " viviendas");

							final EditText input = new EditText(context);
							LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
									LinearLayout.LayoutParams.MATCH_PARENT,
									LinearLayout.LayoutParams.MATCH_PARENT);
							input.setLayoutParams(lp);
							alertDialog.setView(input);
							alertDialog.setIcon(R.drawable.dibujar);

							alertDialog.setPositiveButton("Modificar",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											String nuevoorden = input.getEditableText().toString();
											int intnuevoorden;

											//Toast.makeText(context, nuevoorden, Toast.LENGTH_SHORT).show();
											if(funciones.isNumeric(input.getEditableText().toString()))
											{
												intnuevoorden = Integer.valueOf(nuevoorden);

												if(intnuevoorden <= getCount())
												{
													values.remove(viv);
													values.add(intnuevoorden-1, viv);

													((Listado_Activity)context).gva = GV_Listado_Adapter.this;
													notifyDataSetChanged();
													globales.intPosicionListado = intnuevoorden;

													Toast.makeText(context, "�Este cambio de orden es temporal!. Debe GRABAR ORDEN ACTUAL para confirmar este cambio y renumerar o REFRESCAR VIVIENDAS para deshacer el cambio", Toast.LENGTH_LONG).show();

												}
												else
												{
													Toast.makeText(context, "Orden supera el m�ximo de registros!", Toast.LENGTH_SHORT).show();
												}

											}

									}
									});

							alertDialog.setNegativeButton("Cancelar",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											dialog.cancel();
										}
									});


							alertDialog.show();

           				}
           				else if(item.getItemId()==R.id.CtxLstModificarViv) // MODIFICACION DE VIVIENDA
           				{
	           				 if(context instanceof Listado_Activity)
	           				 {
           					 
	           					 if(globales.padres==PADRES.MANZANAS)
	           					 {
		           					 Intent i = new Intent(context, ModificarVivienda_Activity.class);
		           					 i.putExtra("ID_VIVIENDA", viv.getID_VIVIENDA().toString());
		           					((Activity) context).startActivityForResult(i,ASYNC_CARGA_VIVIENDAS);
	           					 }
	           					 
	           					 if(globales.padres==PADRES.SECCIONES)
	           					 {
		           					 Intent i = new Intent(context, ModificarVivienda_Rural_Activity.class);
		           					 i.putExtra("ID_VIVIENDA", viv.getID_VIVIENDA().toString());
		           					((Activity) context).startActivityForResult(i,ASYNC_CARGA_VIVIENDAS);
	           					 }	           					 

	           				 }
	
           				}
           				else if(item.getItemId()==R.id.CtxLstTomarFotoViv) // SACAR FOTO VIVIENDA
           				{
           					SacarFoto(viv.getID_VIVIENDA());

           				
           				}
           				else if(item.getItemId()==R.id.CtxLstBuscarVivMapa) // VER VIVIENDA EN MAPA
           				{
           					Point pt = new Point(viv.getCOORD_X(), viv.getCOORD_Y());
           					
           					//Point pt= (Point) GeometryEngine.project(new Point(viv.getCOORD_X(), viv.getCOORD_Y()) ,SpatialReference.create(4674), SpatialReference.create(102100));
           					//globales.accion=ACCIONES_TABS.BUSCAR_VIVIENDA;
           					//Intent i = new Intent(context, Mapa_Activity.class);
           					
           					//globales.PuntoViviendaListado = pt;
           					//Toast.makeText(context, "Seleccione pesta�a de Mapa para ver la vivienda", Toast.LENGTH_LONG).show();
           					//i.putExtra("PT", pt);
           					//context.startActivity(i);
           					
           					
           					FragmentManager fm = ((Activity)context).getFragmentManager();
          					
           					Mapa_Simple_DialogFragment dialogo =  Mapa_Simple_DialogFragment.newInstance("Punto Vivienda", AccionesMapaSimple.PUNTO_VIVIENDA, viv.getID_VIVIENDA(), false);
           					dialogo.geometria = pt;
           					dialogo.show(fm, AccionesMapaSimple.PUNTO_VIVIENDA.toString());
           					

           					
           				}
           				else if(item.getItemId()==R.id.CtxLstGeorrefViv) // GEORREFERENCIACI�N DE VIVIENDA
           				{
        					if(globales.UltimaPuntoObtenido_GPS==null)
        					{
        						Toast.makeText(context, "No existe una posici�n v�lida actual. Utilice Agregar Vivienda Punteo", Toast.LENGTH_SHORT).show();
        					}
        					else
        					{
        						Point ptgps =  globales.UltimaPuntoObtenido_GPS;
        						boolean retorno = GeorreferenciarVivienda(viv.getID_VIVIENDA(), ptgps);
        						
        						if(retorno)
        						{
        							globales.ActualizarCapas = true;
        							//new Mapa_Activity().AgregarPuntoViviendaGPS(viv.getID_VIVIENDA(), context, ptgps);
        							//new Mapa_Activity().ActualizarCapas();
        							
        							
        							Toast.makeText(context, "Coordenada de vivienda actualizada", Toast.LENGTH_SHORT).show();
        						}
        						else
        						{
        							Toast.makeText(context, "Hubo un error al actualizar Coordenada de vivienda", Toast.LENGTH_SHORT).show();
        						}
        						//DialogoCreacionVivienda(CrearPuntoMapa_GPS());
        					}
           				}
           				else if(item.getItemId()==R.id.CtxLstAnularViv) // ANULACI�N DE VIVIENDA
           				{
           					String accion = viv.isNULO() ? "Restablecer " : "Anular ";
           					AlertDialog.Builder ad = new AlertDialog.Builder(context);
           					ad.setTitle(accion + " Vivienda");

      		            	  
    		              ad.setIcon(R.drawable.mensajes32);
    		              ad.setMessage("�Est� seguro que desea " + accion + " la vivienda?");
    		              ad.setPositiveButton(accion + " Vivienda", new DialogInterface.OnClickListener() {
    						
    						@Override
    						public void onClick(DialogInterface dialog, int which) {
    							// TODO Auto-generated method stub
    							AnularVivienda(viv.getID_VIVIENDA(), !viv.isNULO());
    							
    							new AsyncTask_CargarViviendas(context, new AsyncTaskCompleteListener<List<Vivienda>>() {
									
									@SuppressWarnings("unchecked")
									@Override
									public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
										// TODO Auto-generated method stub
										values = viviendas;
							            //  TaskReordenarViviendas task = new TaskReordenarViviendas(lst.size());
							            //task.execute(lst);
						                
						                
						                new AsyncTask_ReordenarViviendas(context, new AsyncTaskCompleteListener<List<Vivienda>>() {
											
											@Override
											public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
												values = viviendas;
												// TODO Auto-generated method stub
												notifyDataSetChanged();
											}
											
											@Override
											public void onTaskCompleteManzanas(List<Manzana> manzanas) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onTaskCompleteEnvioDatos(
													Integer resultado) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onTaskCompleteDescargarDatos(
													Integer resultado) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onTaskCompleteSecciones(
													List<Seccion> secciones) {
												// TODO Auto-generated method stub
												
											}
										},1, viviendas.size()).execute(viviendas);
										//new Listado_Activity().CargarViviendas(viviendas, context);
									}
									
									@Override
									public void onTaskCompleteManzanas(List<Manzana> manzanas) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onTaskCompleteEnvioDatos(
											Integer resultado) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onTaskCompleteDescargarDatos(
											Integer resultado) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onTaskCompleteSecciones(
											List<Seccion> secciones) {
										// TODO Auto-generated method stub
										
									}
								}).execute();
    						}
    					});
    		              
    		              ad.setNegativeButton("Cancelar " + accion, new DialogInterface.OnClickListener() {
    						
    						@Override
    						public void onClick(DialogInterface dialog, int which) {
    							// TODO Auto-generated method stub
    							
    						}
    					});
    		              ad.show();
    		              
           					//new Listado_Activity().AnularVivienda(context, viv.getID_VIVIENDA(), !viv.isNULO());
          					
           				}           				
           				
           				return false;
           			}
           		});
           		
 			}
		});
   

        DataBaseUtil db = new DataBaseUtil(context);
        db.open();
        final Imagen img = db.ObtenerImagen(viv.getID_VIVIENDA());
        db.close();


        if(img!=null)
        {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            final Bitmap bitmap = BitmapFactory.decodeByteArray(img.getIMAGEN(), 0, img.getIMAGEN().length);
            imgPreviewViv.setImageBitmap(bitmap);

	        imgPreviewViv.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
/*
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inPreferredConfig = Bitmap.Config.ARGB_8888;
					Bitmap bitmap = BitmapFactory.decodeByteArray(img.getIMAGEN(), 0, img.getIMAGEN().length);
*/
					MostrarFotoBitMap(bitmap);
					
				}
			});
        }
        else
        {
        	imgPreviewViv.setImageDrawable(context.getResources().getDrawable(R.drawable.camera_icon));
        }
        
        

        if(viv.getID_USODESTINO()==1) // otro uso
        {
        	imgTipoVivienda.setImageDrawable(context.getResources().getDrawable(R.drawable.otro_uso32));
        }        
        else if(viv.getID_EDIF().equals(globales.UUID_CERO)) //Tiene edificio
        {
        	imgTipoVivienda.setImageDrawable(context.getResources().getDrawable(R.drawable.vivienda_32));
        }
        else if(!viv.getID_EDIF().equals(globales.UUID_CERO)) // No tiene edificio
        {
        	imgTipoVivienda.setImageDrawable(context.getResources().getDrawable(R.drawable.edificio));
        }
        else  
        {
        	imgTipoVivienda.setImageDrawable(context.getResources().getDrawable(R.drawable.sin_edificacion));
        }
        
        return v;

    }

    private void AnularVivienda(UUID ID_VIVIENDA, boolean Anular)
    {
    	
    	DataBaseUtil db = new DataBaseUtil(context);
    	db.open();
    	db.AnularViviendas(ID_VIVIENDA, Anular);
    	db.close();


    	
    }
    
    private boolean GeorreferenciarVivienda(UUID ID_VIVIENDA, Point pt)
    {
    	boolean retorno = false;

    	DataBaseUtil db = new DataBaseUtil(context);
    	db.open();
    	retorno = db.GeorreferenciarVivienda(ID_VIVIENDA, pt);
    	db.close();
    	
    	return retorno;
    }
    

    private void MostrarFoto(UUID ID_VIVIENDA) {
        Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
            new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        Uri imageUri =  Uri.fromFile(new File(globales.RutaPicsSinImagen + ID_VIVIENDA.toString() + ".jpg"));
        
        ImageView imageView = new ImageView(context);
        imageView.setImageURI(imageUri);
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }
    
    private void MostrarFotoBitMap(Bitmap bm) {
        Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
            new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        //Uri imageUri =  Uri.fromFile(new File(globales.RutaPicsSinImagen + ID_VIVIENDA.toString() + ".jpg"));
        
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(bm);
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }
    
    
    private void SacarFoto(UUID ID_VIVIENDA) {
    	
       	
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ComponentName act = takePictureIntent.resolveActivity(context.getPackageManager());
        
        if (act != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            	Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                ((Activity) context).startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {

        File storageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/EnumVivPics/");
        
        storageDir.mkdirs();
        File image = new File(storageDir + "/temp.jpg");
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        
        return image;
    }
    


}