package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TabHost;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ExpandableList_Adapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import android.support.v7.appcompat;

public class Descripcion_Activity  extends ExpandableListActivity {
	TabHost tabs;
	
	List<Integer> listDataHeaderIconId;
	List<String> listDataHeader;

	HashMap<String, List<String>> listDataChild;
    List<String> items;
    
    ExpandableListView expListDescripciones;
    ExpandableList_Adapter expList_Adapter;
    
    EditText etvDescripciones;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.descripcion_activity);
		listDataHeader = new ArrayList<String>();
		
		listDataHeaderIconId = new ArrayList<Integer>();
		listDataChild = new HashMap<String, List<String>>();
		
		expListDescripciones = (ExpandableListView) findViewById(android.R.id.list);
		
		etvDescripciones = (EditText) findViewById(R.id.etvDescripciones);
	      
		CargarLista();
		
		expList_Adapter = new ExpandableList_Adapter(this, listDataHeader, listDataChild, listDataHeaderIconId);
		   
	        // setting list adapter
		expListDescripciones.setAdapter(expList_Adapter);
		expListDescripciones.setOnChildClickListener(new OnChildClickListener() {
			 @Override
	            public boolean onChildClick(ExpandableListView parent, View v,
	                    int groupPosition, int childPosition, long id) {
				 
				 etvDescripciones.setText(etvDescripciones.getEditableText().toString() + " "  
	                                + listDataChild.get(
	                                        listDataHeader.get(groupPosition)).get(
	                                        childPosition));
	                        
	                return false;
	            }
		});
		
		getActionBar().setDisplayHomeAsUpEnabled(false);
		getActionBar().setDisplayShowHomeEnabled(true);
		
		
	    Intent i = getIntent();
	    Vivienda viv = (Vivienda)i.getParcelableExtra("VIVIENDA");
	    String desc ="";
	    
	    
	    if(viv!=null)
	    {
	    	desc = viv.getDESCRIPCION().toUpperCase().trim();
	    }
	    else
	    {
	    	desc = i.getStringExtra("DESCRIPCION");
	    }
	    
	    etvDescripciones.setText(desc);
	    etvDescripciones.setSelection(desc.length());
	    
	    //etvDescripciones.setText(i.getStringExtra("DESCRIPCION").toUpperCase());

	    
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	CrearMenu(menu);
    	return true;
    	
    	
    }
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Grabar");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.confirmado);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
		{
			item2.setIcon(R.drawable.cancelar);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}


	}
	
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionado
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item)
	{
		switch (item.getItemId()) 
		{

		case 0: //Grabar
			
			Intent resultIntent = new Intent();
			resultIntent.putExtra("DESCRIPCION", etvDescripciones.getText().toString());
			setResult(Activity.RESULT_OK, resultIntent);
			finish();
			
			return true;

		case 1://Cancelar
			
			ConfirmarCancelar();
			
			return true;
		
		default:
			return false;
		}
	}
	
		@Override
		public void onBackPressed()
		{
			//super.onBackPressed();
			ConfirmarCancelar();
				
				
			  // optional depending on your needs
		}
		
	    private void ConfirmarCancelar()
	    {
	    	
	    	new AlertDialog.Builder(Descripcion_Activity.this)
			.setIcon(R.drawable.pregunta)
			.setTitle("Cerrar formulario de Ingreso")
			.setMessage("�Est� seguro que desea cancelar el ingreso de la descripci�n?") 
			.setPositiveButton("S�", new DialogInterface.OnClickListener() 
			{

				@Override
				public void onClick(DialogInterface dialog, int which) 
				{

					Descripcion_Activity.this.setResult(Activity.RESULT_CANCELED);
					finish();

				}

			})
			.setNegativeButton("No", null)
			.show();
	    }
	    
	    
    private void CargarLista()
    {
    	
    	CargarSugerenciasDescripcion(R.drawable.muro,  "MATERIALES", "Materiales");
    	CargarSugerenciasDescripcion(R.drawable.otro_uso, "OTRO_USO", "Otros Usos");
    	CargarSugerenciasDescripcion(R.drawable.puerta, "OBJETO", "Objetos a describir");
    	CargarSugerenciasDescripcion(R.drawable.vivienda_sitio_32 ,"VIVIENDA", "Orden Vivienda dentro de Sitio");
    	CargarSugerenciasDescripcion(R.drawable.palette, "COLORES", "Colores");
    	CargarSugerenciasDescripcion(R.drawable.checklist_32, "OTROS", "Otras caracter�sticas");

    	
    }
    private void CargarSugerenciasDescripcion(int IconId, String TipoSugerencia, String Titulo)
    {
    	items = new ArrayList<String>();
    	
    	DataBaseUtil dbutil = new DataBaseUtil(Descripcion_Activity.this);
    	dbutil.open();
    	List<Parametro> params =  dbutil.ObtenerParametrosMapa(TipoSugerencia, "VALOR");
    	dbutil.close();

    	
    	for(Parametro param : params)
    	{
    		items.add(param.getValor());
    	}

    	listDataHeader.add(Titulo);
    	listDataHeaderIconId.add(IconId);
    	listDataChild.put(Titulo, items);
    	

    }
    
    
    
}
