package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.UUID;

public class Imagen {

	private UUID ID_VIVIENDA;
	private byte[] IMAGEN;
	
	public UUID getID_VIVIENDA() {
		return ID_VIVIENDA;
	}
	public void setID_VIVIENDA(UUID iD_VIVIENDA) {
		ID_VIVIENDA = iD_VIVIENDA;
	}
	public byte[] getIMAGEN() {
		return IMAGEN;
	}
	public void setIMAGEN(byte[] iMAGEN) {
		IMAGEN = iMAGEN;
	}
	
	
	
}
