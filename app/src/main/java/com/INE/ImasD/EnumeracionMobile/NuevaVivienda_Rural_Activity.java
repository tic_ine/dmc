package com.INE.ImasD.EnumeracionMobile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ColorAdapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Categorias_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_CodigosAnotacion_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Edificios_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_SoloString_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_TipoCalle_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_UsoDestino_Adapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.Mapa_Simple_DialogFragment.AccionesMapaSimple;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;
import com.INE.ImasD.EnumeracionMobile.R.id;
import com.esri.core.geometry.Point;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class NuevaVivienda_Rural_Activity extends Activity{
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int AGREGAREDIFICIO = 2;  // The request code
	static final int SUGERENCIAS = 3;  // The request code
	static final int GRABAR_AUDIO = 4;
	
	private static final String TAG = "" + NuevaVivienda_Rural_Activity.class;
			
	double lat;
	double lng;

	double latEdificio;
	double lngEdificio;
	
	TextView lblLatitud;
	TextView lblLongitud;


	
	/*CONTROLES FORM*/
	ImageView imgNV_R_Foto;
	Spinner spNV_R_Edificios;
	Spinner spNV_R_TiposCalle;
	Spinner spNV_R_Categoria;
	Spinner spNV_R_UsoDestinoEdificacion;
	Spinner spNV_R_CodigoAnotacion;
	Spinner spNV_R_UbicacionHorizontal;
	Spinner spNV_R_UbicacionVertical;
	
	ImageButton btnAddHogar;
	
	
	Button btnNV_R_VerCoordenada;
	
	Button btnNV_R_Sugerencia;
	Spinner spNV_R_OrdenEdificacion ;
	AutoCompleteTextView acNV_R_NombreCalle;
	AutoCompleteTextView acNV_R_NumeroDomicilio;
	EditText etvNV_R_Block;
	EditText etvNV_R_Piso;
	EditText etvNV_R_Depto;
	EditText etvNV_R_Descripcion;

	AutoCompleteTextView acNV_R_Localidad;
	AutoCompleteTextView acNV_R_NombreEntidad;

	RadioButton rbNV_R_Izquierda;
	RadioButton rbNV_R_Derecha;

	/*FIN CONTROLES FORM*/
	UUID ID_VIVIENDA;
	
	LinearLayout loNV_R_Hogares;
	public String mCurrentPhotoPath = "";
	
	TipoCalle tipocalle;
	Categoria categoria;
	UsoDestino usodestino;
	CodigoAnotacion codigoanotacion;
	
	String UbicacionHorizontal;
	String UbicacionVertical;
	
	int OrdenEdif;
	
	ProgressDialog pd;
	
	ImageButton btnNV_R_GrabarAudio;
	ImageButton btnNV_R_PlayAudio;

	ImageButton imgbtnNV_R_NuevoEdificio;
	ImageView imgNV_R_Rotar;
	
	TextView tvNV_R_OrdenVivienda;

    
    boolean HayFoto = false;

	boolean HayAudio = false;
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.nuevaviv_r_activity);


	    
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		
	    //mCurrentPhotoPath = "";

	    ID_VIVIENDA = UUID.randomUUID();

	    loNV_R_Hogares =  (LinearLayout)findViewById(R.id.loNV_R_HogaresVivienda);
    	
	    imgNV_R_Foto = (ImageView)findViewById(R.id.imgNV_R_FotoViv);
	    
	    btnAddHogar = (ImageButton)findViewById(R.id.btnNV_R_AddHog);
	    
	    /*
	    btnNV_R_Materiales = (Button)findViewById(R.id.btnNV_R_Materiales);
	    btnNV_R_Colores = (Button)findViewById(R.id.btnNV_R_Colores);
	    btnNV_R_OtrosTextos = (Button)findViewById(R.id.btnNV_R_OtrosTextos);
	    btnNV_R_OtrosUsos = (Button)findViewById(R.id.btnNV_R_OtrosUsos);
	    btnNV_R_OrdenVivSitio = (Button)findViewById(R.id.btnNV_R_OrdenVivSitio);
	    btnNV_R_Objetos = (Button)findViewById(R.id.btnNV_R_Objetos);
	    */

	    btnNV_R_VerCoordenada = (Button)findViewById(R.id.btnNV_R_VerCoordenada);

	    
	    spNV_R_Edificios= (Spinner)findViewById(R.id.spNV_R_Edificios);
	    spNV_R_TiposCalle = (Spinner)findViewById(R.id.spNV_R_TiposCalle);
	    spNV_R_Categoria = (Spinner)findViewById(R.id.spNV_R_Categorias);
	    spNV_R_UsoDestinoEdificacion = (Spinner)findViewById(R.id.spNV_R_UsoDestinoEdificacion);
	    spNV_R_CodigoAnotacion = (Spinner)findViewById(R.id.spNV_R_CodigoAnotacion);
	    
	    spNV_R_UbicacionHorizontal = (Spinner)findViewById(R.id.spNV_R_UbicacionHorizontal);
	    spNV_R_UbicacionVertical = (Spinner)findViewById(R.id.spNV_R_UbicacionVertical);
	    
	    
	    spNV_R_OrdenEdificacion = (Spinner)findViewById(id.spNV_R_OrdenEdificacion);
	    tvNV_R_OrdenVivienda = (TextView)findViewById(id.tvNV_R_OrdenVivienda);
	    acNV_R_NombreCalle = (AutoCompleteTextView)findViewById(id.acNV_R_NombreCalle);
	    acNV_R_NumeroDomicilio = (AutoCompleteTextView)findViewById(id.acNV_R_NumeroDomicilio);
	    etvNV_R_Block = (EditText)findViewById(id.etvNV_R_Block);
	    etvNV_R_Piso = (EditText)findViewById(id.etvNV_R_Piso);
	    etvNV_R_Depto = (EditText)findViewById(id.etvNV_R_Depto);
	    etvNV_R_Descripcion = (EditText)findViewById(id.etvNV_R_Descripcion);
	    
	    acNV_R_NombreEntidad = (AutoCompleteTextView)findViewById(id.acNV_R_NombreEntidad);
	    
	    rbNV_R_Izquierda = (RadioButton)findViewById(id.rbNV_R_Izquierda);
	    rbNV_R_Derecha = (RadioButton)findViewById(id.rbNV_R_Derecha);
	    
	    imgbtnNV_R_NuevoEdificio = (ImageButton)findViewById(R.id.imgbtnNV_R_NuevoEdificio);
	    
	    imgNV_R_Rotar = (ImageView)findViewById(R.id.imgNV_R_Rotar);
	    
	    btnNV_R_Sugerencia = (Button)findViewById(R.id.btnNV_R_Sugerencia);

		acNV_R_Localidad = (AutoCompleteTextView) findViewById(R.id.acNV_R_Localidad);

		acNV_R_Localidad.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
		acNV_R_NombreEntidad.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

		acNV_R_Localidad.setText(globales.LOCALIDAD);
		acNV_R_Localidad.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(!globales.LOCALIDAD.equals(s.toString().trim()))
				{
					tvNV_R_OrdenVivienda.setText("1");
					spNV_R_OrdenEdificacion.setSelection(1);
					
				}
				else
				{
					tvNV_R_OrdenVivienda.setText(String.valueOf(ObtenerMaxOrdenViv()+1));
					spNV_R_OrdenEdificacion.setSelection(spNV_R_OrdenEdificacion.getCount() - 1);
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

		acNV_R_Localidad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				globales.LOCALIDAD = parent.getAdapter().getItem(position).toString();
			}
		});
	    
	    
	    imgNV_R_Foto.setOnClickListener(new OnClickListener() {
	    	
			@Override
			public void onClick(View arg0) {
				SacarFoto();
			}
		});
	    
	    btnAddHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AgregarNuevoHogar();
				
			}
		});
	    
    
	    spNV_R_TiposCalle.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				tipocalle =  (TipoCalle)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNV_R_UsoDestinoEdificacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				usodestino =  (UsoDestino)parent.getItemAtPosition(position);
				
				if(usodestino.getId_usodestino()<=1)
				{
					tvNV_R_OrdenVivienda.setText("0");
				}
				else
				{
					if(!globales.LOCALIDAD.equals(acNV_R_Localidad.getText().toString().trim()))
					{
						tvNV_R_OrdenVivienda.setText("1");
					}
					else
					{
						tvNV_R_OrdenVivienda.setText(String.valueOf(ObtenerMaxOrdenViv()+1));	
					}
					
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNV_R_CodigoAnotacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				codigoanotacion =  (CodigoAnotacion)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    spNV_R_Categoria.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				categoria =  (Categoria)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNV_R_UbicacionHorizontal.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionHorizontal = (String)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNV_R_UbicacionVertical.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionVertical= (String)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    acNV_R_NombreCalle.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				acNV_R_NombreCalle.showDropDown();
	            acNV_R_NombreCalle.requestFocus();
	            return false;
			}
		});
	    
	    
	    acNV_R_NombreEntidad.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				acNV_R_NombreEntidad.showDropDown();
				acNV_R_NombreEntidad.requestFocus();
	            return false;
			}
		});	    
	    imgbtnNV_R_NuevoEdificio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ine = new Intent(NuevaVivienda_Rural_Activity.this, NuevoEdificio_Activity.class);
				ine.putExtra("lat", lat);
				ine.putExtra("lng", lng);
				startActivityForResult(ine, AGREGAREDIFICIO);
				
			}
		}); 
	    
	    Intent i = getIntent();
	    
	    lat = i.getDoubleExtra("lat", -33.9999);
	    lng = i.getDoubleExtra("lng", -70.9999);
	    
	    //editor.putString("coordenada", String.valueOf(lat) + ", " +String.valueOf(lng));
	    
	    
	    lblLatitud = (TextView)findViewById(R.id.tvNV_R_Latitud);
	    lblLongitud = (TextView)findViewById(R.id.tvNV_R_Longitud);
	    
	    lblLatitud.setText(String.format("%.2f", lat));
	    lblLongitud.setText(String.format("%.2f", lng));
	    
	    CargarEdificios();
	    CargarTiposCalle();



	    new TaskCargarSugerenciasCalles().execute();

	    new TaskCargarLocalidades().execute();

	    new TaskCargarEntidades().execute();
	    
	    CargarCategorias();
	    CargarUsoDestino();
	    CargarCodigosAnotacion();
	    
	    CargarUbicacionHorizontal();
	    CargarUbicacionVertical();
	    
	    CargarOrdenEdificacion();
	 
	    funciones.LLenarSpinnerSN(NuevaVivienda_Rural_Activity.this, acNV_R_NumeroDomicilio,true);


		btnNV_R_GrabarAudio= (ImageButton) findViewById(id.btnNV_R_GrabarAudio);

		btnNV_R_PlayAudio= (ImageButton) findViewById(id.btnNV_R_PlayAudio);

		btnNV_R_GrabarAudio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v)
			{

				Intent i = new Intent(NuevaVivienda_Rural_Activity.this, VoiceRecognition_Activity.class);
				i.putExtra("ID_VIVIENDA", ID_VIVIENDA.toString());
				startActivityForResult(i, GRABAR_AUDIO);

			}

		});

		btnNV_R_PlayAudio.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				if(HayAudio)
				{
					ReproducirAudio();
				}

			}
		});

		
		
		tvNV_R_OrdenVivienda.setText(String.valueOf(ObtenerMaxOrdenViv()+1));
		
		/*
		btnNV_R_Materiales.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("MATERIALES", "materiales");
				
			}
		});
		
		btnNV_R_Colores.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("COLORES", "Colores");
				
			}
		});
		
		btnNVOtrosTextos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("OTROS", "texto");
				
			}
		});		
		
		btnNVOtrosUsos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OTRO_USO", " ejemplos Otros Usos");
			}
		});
		
		btnNVObjetos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OBJETO", " objeto a describir");
			}
		});
		
		btnNVOrdenVivSitio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("VIVIENDA", "Orden Vivienda dentro de Sitio");
			}
		});
		
		 */
		spNV_R_Edificios.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			
				Edificio edif = (Edificio)spNV_R_Edificios.getItemAtPosition(position);
				//"00000000-0000-0000-0000-000000000000"
				if(position==0)
				{
					
				    lblLatitud.setText(String.format("%.2f", lat));
				    lblLongitud.setText(String.format("%.2f", lng));
					acNV_R_NombreCalle.setText("");
					acNV_R_NumeroDomicilio.setText("");
					acNV_R_NombreCalle.setEnabled(true);
					acNV_R_NumeroDomicilio.setEnabled(true);
					Toast.makeText(NuevaVivienda_Rural_Activity.this, "Se asumir� coordenada original", Toast.LENGTH_SHORT).show();
				}
				else
				{
				    
					lblLatitud.setText(String.format("%.2f", edif.getCoord_lat()));
					lblLongitud.setText(String.format("%.2f", edif.getCoord_lng()));
					acNV_R_NombreCalle.setText(edif.getCalle());
					acNV_R_NumeroDomicilio.setText(edif.getNumero());

					acNV_R_NombreCalle.setEnabled(false);
					acNV_R_NumeroDomicilio.setEnabled(false);
					
					latEdificio = edif.getCoord_lat();
					lngEdificio= edif.getCoord_lng();
					
					Toast.makeText(NuevaVivienda_Rural_Activity.this, "Se asumir� coordenada de edificio", Toast.LENGTH_SHORT).show();
					
				}
				
	
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		btnNV_R_VerCoordenada.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				//Point pt= (Point) GeometryEngine.project(new Point(viv.getCOORD_X(), viv.getCOORD_Y()) ,SpatialReference.create(4674), SpatialReference.create(102100));
				Point pt;
				if(((Edificio)spNV_R_Edificios.getSelectedItem()).getId_edif().equals(globales.UUID_CERO))
				{
					pt= new Point(lng, lat);	
				}
				else
				{
					pt= new Point(lngEdificio, latEdificio);
				}
				
				FragmentManager fm = ((Activity)NuevaVivienda_Rural_Activity.this).getFragmentManager();
				
				Mapa_Simple_DialogFragment dialogo = Mapa_Simple_DialogFragment.newInstance("Punto Vivienda", AccionesMapaSimple.PUNTO_VIVIENDA, ID_VIVIENDA, true);
				dialogo.geometria = pt;
				dialogo.show(fm, AccionesMapaSimple.PUNTO_VIVIENDA.toString());
			}
		});
		
		
		imgNV_R_Rotar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(HayFoto)
				{
					// TODO Auto-generated method stub
					Bitmap bm = ((BitmapDrawable)imgNV_R_Foto.getDrawable()).getBitmap();
					bm = funciones.RotateBitmap(bm, 90);
					imgNV_R_Foto.setImageBitmap(bm);
				}
			
			
				
			}
		});
		
		btnNV_R_Sugerencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(NuevaVivienda_Rural_Activity.this, Descripcion_Activity.class);
				i.putExtra("DESCRIPCION", etvNV_R_Descripcion.getEditableText().toString());

				
				startActivityForResult(i, SUGERENCIAS);
			}
		});
		
		
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	CrearMenu(menu);
    	return true;
    	
    	
    }
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Grabar");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.grabar);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
		{
			item2.setIcon(R.drawable.cancelar);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}


	}
	
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionado
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item) {
		switch (item.getItemId()) {

		case 0: //Grabar
			//Toast.makeText(this, "Has pulsado el Item 1 del Action Bar",Toast.LENGTH_SHORT).show();
			GrabarNuevaVivienda();
			return true;

		case 1://Cancelar
			
    		//setResult(Activity.RESULT_CANCELED);
    		
    		//finish();
			
			ConfirmarCancelar();
			
			return true;

		case android.R.id.home:
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		}
		return false;
	}
	
	
	@Override
	public void onBackPressed()
	{
		//super.onBackPressed();
		ConfirmarCancelar();
			
			
		  // optional depending on your needs
	}
	
    private void ConfirmarCancelar()
    {
    	
    	new AlertDialog.Builder(NuevaVivienda_Rural_Activity.this)
		.setIcon(R.drawable.pregunta)
		.setTitle("Cerrar formulario de Ingreso")
		.setMessage("�Est� seguro que desea cancelar el ingreso del registro?") 
		.setPositiveButton("S�", new DialogInterface.OnClickListener() 
		{

			@Override
			public void onClick(DialogInterface dialog, int which) 
			{

				NuevaVivienda_Rural_Activity.this.setResult(Activity.RESULT_CANCELED);
				finish();

			}

		})
		.setNegativeButton("No", null)
		.show();
    }
    
    private void CargarEdificios()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Edificio> edificios =  dbutil.ObtenerEdificios();
    	dbutil.close();
    	
    	Spin_Edificios_Adapter adapter = new Spin_Edificios_Adapter(NuevaVivienda_Rural_Activity.this, edificios);
    	spNV_R_Edificios.setPrompt("Seleccione Edificio si corresponde");
    	spNV_R_Edificios.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    private void CargarSugerenciasDescripcion(String TipoSugerencia, String Titulo)
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Parametro> params =  dbutil.ObtenerParametrosMapa(TipoSugerencia, "VALOR");
    	dbutil.close();
    	
    	final List<String> items = new ArrayList<String>();
    	
    	
    	for(Parametro param : params)
    	{
    		items.add(param.getValor());
    	}



    	final ArrayAdapter<String> adapter = new ColorAdapter(this, items);

    	AlertDialog.Builder builder3=new AlertDialog.Builder(NuevaVivienda_Rural_Activity.this);
    	builder3.setAdapter(adapter, new DialogInterface.OnClickListener() {

    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			// TODO Auto-generated method stub

    			etvNV_R_Descripcion.getText().insert(etvNV_R_Descripcion.getSelectionStart(),adapter.getItem(which) + " ");
    			etvNV_R_Descripcion.setSelection(etvNV_R_Descripcion.getText().length());
    		}
    	});

    		
    		  builder3.setTitle("Seleccione " + Titulo);

    		  builder3.show();


    }
    
    
    private void CargarTiposCalle()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<TipoCalle> tiposcalle =  dbutil.ObtenerTiposCalle();
    	dbutil.close();
    	
    	Spin_TipoCalle_Adapter adapter = new Spin_TipoCalle_Adapter(NuevaVivienda_Rural_Activity.this, tiposcalle);
    	spNV_R_TiposCalle.setPrompt("Seleccione Tipo de Calle");
    	spNV_R_TiposCalle.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
    	
    	
    }
    
    private void CargarCategorias()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Categoria> categorias =  dbutil.ObtenerCategoria();
    	dbutil.close();
    	
    	Spin_Categorias_Adapter adapter = new Spin_Categorias_Adapter(NuevaVivienda_Rural_Activity.this, categorias);
    	spNV_R_Categoria.setPrompt("Seleccione Categor�a");
    	spNV_R_Categoria.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
   
    
    private void CargarUsoDestino()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<UsoDestino> usos =  dbutil.ObtenerUsosDestino();
    	dbutil.close();
    	
    	Spin_UsoDestino_Adapter adapter = new Spin_UsoDestino_Adapter(NuevaVivienda_Rural_Activity.this, usos);
    	spNV_R_UsoDestinoEdificacion.setPrompt("Uso o Destino Edificaci�n");
    	spNV_R_UsoDestinoEdificacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
   }

    private void CargarCodigosAnotacion()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<CodigoAnotacion> usos =  dbutil.ObtenerCodigosAnotacion();
    	dbutil.close();
    	
    	Spin_CodigosAnotacion_Adapter adapter = new Spin_CodigosAnotacion_Adapter(NuevaVivienda_Rural_Activity.this, usos);
    	spNV_R_CodigoAnotacion.setPrompt("C�digos de Anotaci�n");
    	spNV_R_CodigoAnotacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    
    private void CargarUbicacionHorizontal()
    {
    
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionhorizontal);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spNV_R_UbicacionHorizontal.setPrompt("Ubicaci�n horizontal dentro del sitio");
    	spNV_R_UbicacionHorizontal.setAdapter(adapter);
    	
    	adapter.notifyDataSetChanged();
    }
    
    private void CargarUbicacionVertical()
    {
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionvertical);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spNV_R_UbicacionVertical.setPrompt("Ubicaci�n vertical dentro del sitio");
    	spNV_R_UbicacionVertical.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
    
    
    private void CargarOrdenEdificacion()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	dbutil.open();
    	int maxedif =  dbutil.ObtenerMaxOrdenEdificio(globales.IDPADRE);
    	dbutil.close();
    	
    	
    	Integer[] ints = new Integer[maxedif+2];
    	
    	for(int i = 0;i<=maxedif+1;i++)
    	{
    		ints[i] = i;
    	}
    	
    
    	ArrayAdapter<Integer> adaptermaxedif =  new ArrayAdapter<Integer>(NuevaVivienda_Rural_Activity.this, R.layout.item_solo_string, ints);
    	spNV_R_OrdenEdificacion.setPrompt("Orden de Edificaci�n");
    	spNV_R_OrdenEdificacion.setAdapter(adaptermaxedif);
    	adaptermaxedif.notifyDataSetChanged();
    	spNV_R_OrdenEdificacion.setSelection(maxedif);
    	
    }
    private void AgregarNuevoHogar()
    {
    	ImageButton imgBtnBorrarHogar;
    	TextView txtNumHogar;
    	EditText etvNumPersonasHogar;
    	

    	
    	int total =loNV_R_Hogares.getChildCount();
    	if(total>0)
    	{
	    	View v;
			v = loNV_R_Hogares.getChildAt(total-1);
			etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
			
			if(etvNumPersonasHogar.getText().toString().equals(""))
			{
				Toast.makeText(NuevaVivienda_Rural_Activity.this, "Ingrese Personas al hogar anterior para agregar otro", Toast.LENGTH_SHORT).show();
				return;
			}
    	
    	}

    	final View child = getLayoutInflater().inflate(R.layout.item_hogar, null);
    	
    	txtNumHogar = (TextView)child.findViewById(R.id.lblNumeroHogar);
    	etvNumPersonasHogar = (EditText)child.findViewById(R.id.etvNumPersonasHogar);
    	
    	imgBtnBorrarHogar = (ImageButton)child.findViewById(R.id.imgBtnBorrarHog);

    	imgBtnBorrarHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loNV_R_Hogares.removeView(child);
				RenumerarHogares();
				
			}
		});
    	
    	
    	loNV_R_Hogares.addView(child);
    	
    	//etvNumPersonasHogar.requestFocus();
    	
    	RenumerarHogares();

    }
    
    private void RenumerarHogares()
    {
    	TextView txtNumHogar;
    	
    	int total =loNV_R_Hogares.getChildCount();
    	View v;
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		v = loNV_R_Hogares.getChildAt(i);
        	txtNumHogar = (TextView)v.findViewById(R.id.lblNumeroHogar);
        	txtNumHogar.setText(String.valueOf(i+1));
    	}
    	
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	

    	if(requestCode == AGREGAREDIFICIO && resultCode == RESULT_OK)
    	{
    		 UUID ID_EDIF = (UUID) data.getExtras().get("ID_EDIF");
    		 UUID ID_E;
    		 CargarEdificios();
    		 for (int i = 0; i < spNV_R_Edificios.getCount(); i++) {
    			 ID_E = ((Edificio)spNV_R_Edificios.getItemAtPosition(i)).getId_edif();
    		        if (ID_E.equals(ID_EDIF)) {
    		        	spNV_R_Edificios.setSelection(i);
    		            break;
    		        }
    		    }
    		 
    		 
    	}
    	
    	if (requestCode ==SUGERENCIAS && resultCode == RESULT_OK && null != data) {

			String DESCRIPCION = data.getExtras().get("DESCRIPCION").toString();
			etvNV_R_Descripcion.setText(DESCRIPCION);
		}
    	
    	
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) 
        {
        	
        	BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

        	try {

        		bm = BitmapFactory.decodeFile(mCurrentPhotoPath);
        		if(bm != null)
        		{
        			
    				resizeImage(bm,320, 240);
    		      	imgNV_R_Foto.setImageBitmap(bm);     
    		      	HayFoto = true;
        		}
        		else
        		{
        			Toast.makeText(NuevaVivienda_Rural_Activity.this, "Error al asociar foto", Toast.LENGTH_SHORT).show();
        		}
        		

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
                    
       	
        	Toast.makeText(NuevaVivienda_Rural_Activity.this, "Foto creada", Toast.LENGTH_SHORT).show();
            //Bundle extras = data.getExtras();

        }
		else if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_CANCELED)
		{
			HayFoto = false;
		}


		if (requestCode == GRABAR_AUDIO)
		{
			if (resultCode == RESULT_OK) {
				GrabarAudio();
			} else
				{
				HayAudio = false;
			}
		}

    }

	private void GrabarAudio()
	{

		String mCurrentAudioPath =  globales.RutaAudioTemp;

		byte[] getBytes = {};
		try
		{
			File file = new File(mCurrentAudioPath);
			getBytes = new byte[(int) file.length()];
			InputStream is = new FileInputStream(file);
			is.read(getBytes);
			is.close();
			//file.delete();

			DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
			db.open();
			db.BorrarAudio(ID_VIVIENDA);
			db.InsertarAudio(ID_VIVIENDA, getBytes);
			db.close();

			HayAudio = true;

			btnNV_R_PlayAudio.setVisibility(View.VISIBLE);

		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
    private void resizeImage(Bitmap bm, int width, int height) throws IOException {
    	Bitmap photo;
    	
    	photo = Bitmap.createScaledBitmap(bm, width, height, false);
    	ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    	photo.compress(Bitmap.CompressFormat.JPEG, 80, bytes);

    	File f = new File(mCurrentPhotoPath);
    	f.createNewFile();
    	FileOutputStream fo = new FileOutputStream(f);
    	fo.write(bytes.toByteArray());
    	fo.close();
    	
    	photo.recycle();
    	
    }
    
    private void SacarFoto()
	{
    	
   	
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            	Toast.makeText(NuevaVivienda_Rural_Activity.this, "Error", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                
                mCurrentPhotoPath = photoFile.getAbsolutePath();
                
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {

        File storageDir = new File(globales.RutaPicsSinImagen);
        
        storageDir.mkdirs();
        File image = new File(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        
        return image;
    }
   
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
    private void GrabarNuevaVivienda()
    {
    	if(ValidarDatos() && ValidarHogares())
    	{
    		
    		Vivienda viv = new Vivienda();
    		viv.setACT_ID(globales.actualizacion.getAct_id());
    		viv.setID_USUARIO(globales.usuario.getId_usuario());
    		viv.setID_VIVIENDA(ID_VIVIENDA);
    		viv.setID_ORIGEN(1);
    		viv.setIDPADRE(globales.IDPADRE);

    		viv.setNOMBRE_CALLE_CAMINO(acNV_R_NombreCalle.getEditableText().toString());
    		viv.setN_DOMICILIO(acNV_R_NumeroDomicilio.getEditableText().toString());
    		viv.setN_LETRA_BLOCK(etvNV_R_Block.getEditableText().toString());
    		viv.setN_PISO(etvNV_R_Piso.getEditableText().toString());
    		viv.setN_LETRA_DEPTO(etvNV_R_Depto.getEditableText().toString());
    		viv.setFECHAINGRESO(new Date());
    		
    		viv.setTIPOCALLE_ID(tipocalle.getTipocalle_id());
    		viv.setTIPOCALLE_GLOSA(tipocalle.getTipocalle_glosa());
    		
    		viv.setID_USODESTINO(usodestino.getId_usodestino());
    		viv.setCODIGO_ID(codigoanotacion.getCodigo_id());

    		viv.setDESCRIPCION(etvNV_R_Descripcion.getEditableText().toString());
    		
    		viv.setPOSICION(rbNV_R_Izquierda.isChecked()? 1 : 2);

    		viv.setID_ESE(0);
    		viv.setNULO(false);
    		viv.setNUEVA(true);
    		viv.setMODIFICADA(false);
    		
    		viv.setUSUARIO_ID_INGRESO(globales.usuario.getId_usuario());
    		
			if(!globales.LOCALIDAD.equals(acNV_R_Localidad.getText().toString().trim()))
			{
				
				viv.setORDEN(1);
			}
			else
			{
				viv.setORDEN(ObtenerCorrelativoPadre()+1);
			}
				
    		
    		viv.setVIVTEMP_ID(0);
    		viv.setVIVTEMP_GLOSA("No");
    		
    		if(usodestino.getId_usodestino()<=1)
    		{
    			viv.setORDEN_VIV(0);	
    		}
    		else
    		{
    			viv.setORDEN_VIV(ObtenerMaxOrdenViv()+1);
    		}
    		
    		
    		viv.setUBICACIONHORIZONTAL(UbicacionHorizontal);
    		viv.setUBICACIONVERTICAL(UbicacionVertical);
    		
    		
    		viv.setORDENEDIFICACION((int)spNV_R_OrdenEdificacion.getSelectedItemId());
    		
    		Edificio myedif =(Edificio)spNV_R_Edificios.getSelectedItem();
    		viv.setID_EDIF(myedif.getId_edif());
    		
    		if(myedif.getId_edif().equals(globales.UUID_CERO))
    		{
        		viv.setCOORD_X(lng);
        		viv.setCOORD_Y(lat);
    		}
    		else
    		{
        		viv.setCOORD_X(lngEdificio);
        		viv.setCOORD_Y(latEdificio);
    		}
    		

    		viv.setNOMBRELOCALIDAD(acNV_R_Localidad.getText().toString());
    		
    		viv.setNOMBREENTIDAD(acNV_R_NombreEntidad.getEditableText().toString());
    		Categoria cat =(Categoria)spNV_R_Categoria.getSelectedItem();
    		viv.setCAT_ID(cat.getCat_id());
    		
    		
    		DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    		db.open();
    		
    		db.InsertarVivienda(viv);
    		
    		db.close();
    		db = null;
    		
    		Toast.makeText(NuevaVivienda_Rural_Activity.this, "Vivienda Grabada", Toast.LENGTH_SHORT).show();
    		
    		GrabarHogares();

    		GrabarImagen();
    		
    		Intent resultIntent = new Intent();	
    		resultIntent.putExtra("NUEVA_VIVIENDA", viv);
    		setResult(Activity.RESULT_OK, resultIntent);
    		 
    		globales.ActualizarCapas = true;
    		finish();
    		
    	}

    }
    
    private boolean ValidarDatos()
    {
    	boolean retorno;
    	Toast toast = Toast.makeText(NuevaVivienda_Rural_Activity.this, "", Toast.LENGTH_SHORT);
    /*
    	if(etvNV_R_Localidad.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Localidad. Si no tiene digite SN");
    		retorno = false;
    	}
    	else if(acNV_R_NombreEntidad.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Entidad. Si no tiene digite SN");
    		retorno = false;
    	}  
    	else */
    	if(spNV_R_OrdenEdificacion.getSelectedItem()==null)
    	{
    		toast.setText("Debe ingresar Orden de Edificaci�n.");
    		retorno = false;
    	}
    	else if(acNV_R_NombreCalle.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Calle.");
    		retorno = false;
    	}
    	else if(acNV_R_NumeroDomicilio.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar N�mero de Domicilio. Si  no tiene digite SN");
    		retorno = false;
    	}
  	
    	else
    	{
    		retorno = true;
    	}
    	
    	if(!retorno)
    	{
    		toast.show();
    	}
    	
		return retorno;
    	
    }
    
    private boolean ValidarHogares()
    {
    	EditText etvNumPersonasHogar;
    	int total =loNV_R_Hogares.getChildCount();
    	View v;
    	boolean ret = true;
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		
    		v = loNV_R_Hogares.getChildAt(i);
    		etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
        	if(etvNumPersonasHogar.getEditableText().toString().equals(""))
        	{
        	
        		Toast.makeText(NuevaVivienda_Rural_Activity.this, "Debe ingresar N� de Personas en el Hogar " + (i+1), Toast.LENGTH_SHORT).show();
        		ret = false;
        		break;
        	}
        			

    	}
    	
    	return ret;
    	
    	
    }
    
    private void GrabarHogares()
    {
    	EditText etvNumPersonasHogar;
		EditText etvJefeHogar;

    	Hogar hog;
    	int total =loNV_R_Hogares.getChildCount();
    	View v;
    	
    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	db.open();
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		hog = new Hogar();
    		
    		v = loNV_R_Hogares.getChildAt(i);
    		etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
			etvJefeHogar = (EditText)v.findViewById(id.etvJefeHogar);
        	hog.setAct_id(globales.actualizacion.getAct_id());
        	hog.setId_usuario(globales.usuario.getId_usuario());
        	hog.setId_vivienda(ID_VIVIENDA);
        	hog.setOrdenhogar(i+1);
        	hog.setNumpersonas(Integer.valueOf(etvNumPersonasHogar.getEditableText().toString()));
        	hog.setJefe(etvJefeHogar.getText().toString());
        	db.InsertarHogar(hog);
        	
    	}
    	
    	db.close();
    	
    }
    
    private void GrabarImagen()
    {
    	if(HayFoto)
    	{
    		BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;

        	try 
        	{
        		bm = BitmapFactory.decodeFile(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
        		
        		bm = funciones.RotateBitmap(bm, 90);
        		if(bm != null)
        		{
    				funciones.ResizeAndSaveImage(NuevaVivienda_Rural_Activity.this, bm,320, 240, 100, ID_VIVIENDA);
        		}
        	}
    		catch (Exception e) 
    		{
    			
    		}
					// TODO: handle exception
    	}
    }
    
    private int ObtenerCorrelativoPadre()
    {
    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	db.open();
    	int ret = db.ObtenerCorrelativoVivienda(globales.IDPADRE);
    	db.close();
		return ret;
    	
    }
    
    private int ObtenerMaxOrdenViv()
    {
    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
    	db.open();
    	int ret = db.ObtenerOrdenViviendaMaximo(globales.IDPADRE);
    	db.close();
		return ret;
    	
    }
    

    
		private class TaskCargarSugerenciasCalles extends AsyncTask<Void, Void, List<String>>
		{
			

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				pd = new ProgressDialog(NuevaVivienda_Rural_Activity.this);
				pd.setTitle("Cargando Sugerencias Calles..");
				pd.setMessage("Por favor espere...");       
				pd.setIndeterminate(true);
				pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//				pd.setMax(100);
				pd.show();
			}
			
			@Override
			protected List<String> doInBackground(Void... args) 
			{
				
				try 
				{
					return CargarSugerencias();
					
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}

			}
			
		    private List<String> CargarSugerencias()
		    {
		    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
		    	db.open();
		    	List<String> calles = db.ObtenerSugerenciasCalle(globales.IDPADRE);
		    	db.close();
		    	
		    	try {
					calles = AgregarSugerenciasCallesMapa(calles);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	
		    	return calles;

		    }
		    
			 public List<String> AgregarSugerenciasCallesMapa(List<String> calles) throws Exception 
			 {
				 
				 try 
				 {
					 if(globales.padres==PADRES.MANZANAS)
					 {
						 DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
						 db.open();
						 Manzana mz = db.ObtenerManzana(globales.IDPADRE,0);
						 db.close();
						 new DataBaseSpatialite().ObtenerCallesPadre(String.valueOf(mz.getGeocodigo()), calles);
					 }
					 
					 if(globales.padres==PADRES.SECCIONES)
					 {
						 //new DataBaseSpatialite().ObtenerCallesPadre(globales.IDPADRE, calles);
					 }					 
					 
					 //return sldb.AgregarSugerenciasCalles(calles, lat, lng);
					 new DataBaseSpatialite().AgregarSugerenciasCalles(calles, lat, lng);
					 
					 return calles;
		
				 } 
				 catch (Exception e) 
				 {
			            e.printStackTrace();
			            Log.d("EnumeracionViviendas","open database"  +  e.getMessage().toString());
			            return calles; 
			      }
			    }


			
			@Override 
			protected void onPostExecute(List<String> calles) 
			{
				
				if(calles !=null)
				{
					Collections.sort(calles);
					calles.add(0, "SN");
					
			    	ArrayAdapter<String> adapterCalles =  new ArrayAdapter<String>(NuevaVivienda_Rural_Activity.this, R.layout.item_solo_string, calles);
			    	
			    	acNV_R_NombreCalle.setThreshold(1);
			    	acNV_R_NombreCalle.setAdapter(adapterCalles);
			    	adapterCalles.setNotifyOnChange(true);
			    	
			    	
					if (pd != null) {
						pd.dismiss();
					}

				}
			}
		}

	private class TaskCargarLocalidades extends AsyncTask<Void, Void, List<String>>
	{


		@Override
		protected List<String> doInBackground(Void... args)
		{

			try
			{
				return CargarLocalidades();

			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}

		private List<String> CargarLocalidades()
		{

			List<String> locs = null;
			try {


				DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
				db.open();
				locs = db.ObtenerLocalidadesPadre(globales.IDPADRE);
				db.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return locs;

		}




		@Override
		protected void onPostExecute(List<String> locs)
		{

			if(locs !=null)
			{

				Collections.sort(locs);

				ArrayAdapter<String> adapterLocalidades=  new ArrayAdapter<String>(NuevaVivienda_Rural_Activity.this, R.layout.item_solo_string, locs);

				acNV_R_Localidad.setThreshold(1);
				acNV_R_Localidad.setAdapter(adapterLocalidades);
				adapterLocalidades.setNotifyOnChange(true);

				acNV_R_Localidad.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if(hasFocus)
						{
							acNV_R_Localidad.showDropDown();
						}
					}
				});

			}
		}
	}
		private class TaskCargarEntidades extends AsyncTask<Void, Void, List<String>>
		{
			

			@Override
			protected List<String> doInBackground(Void... args) 
			{
				
				try 
				{
					return CargarEntidades();
					
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}

			}
			
		    private List<String> CargarEntidades()
		    {
		    	
		    	List<String> entidades = null;
		    	try {
		    		
		    		
		    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Rural_Activity.this);
		    	db.open();
		    	entidades = db.ObtenerEntidadesPadre(globales.IDPADRE);
		    	db.close();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	
		    	return entidades;

		    }
		    


			
			@Override 
			protected void onPostExecute(List<String> entidades) 
			{
				
				if(entidades !=null)
				{
					
					Collections.sort(entidades);
					
			    	ArrayAdapter<String> adapterEntidades=  new ArrayAdapter<String>(NuevaVivienda_Rural_Activity.this, R.layout.item_solo_string, entidades);
			    	
			    	acNV_R_NombreEntidad.setThreshold(1);
			    	acNV_R_NombreEntidad.setAdapter(adapterEntidades);
			    	adapterEntidades.setNotifyOnChange(true);
			    	
			    	acNV_R_NombreEntidad.setOnFocusChangeListener(new OnFocusChangeListener() {
						
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if(hasFocus)
							{
								acNV_R_NombreEntidad.showDropDown();
							}
						}
					});

				}
			}
		}

	private void ReproducirAudio()
	{
		MediaPlayer mp = new MediaPlayer();
		try
		{
			FileInputStream fis = new FileInputStream(globales.RutaAudioTemp);
			mp.setDataSource(fis.getFD());
			fis.close();
			mp.prepare();
		}
		catch (Exception e)
		{
			Toast.makeText(NuevaVivienda_Rural_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}

		mp.start();

	}
}
