package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;

public class Seccion  implements Serializable
{
	
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		private long cu_seccion;
	    
	    private int cut;
	    
	    private String comuna_glosa ;
	    
	    private String cod_distrito;
	    
	    private int cod_carto;
	    
	    private int estrato_muestral;
	    
	    private int estrato_geo;
	    
	    private int estrato_ene;
	    
	    private int tipo_est;
	    
	    private int codigo_seccion;

	    private int act_id;
	    
	    private int id_usuario;

	    private boolean checked;
	    
		public long getCu_seccion() {
			return cu_seccion;
		}

		public void setCu_seccion(long cu_seccion) {
			this.cu_seccion = cu_seccion;
		}

		public int getCut() {
			return cut;
		}

		public void setCut(int cut) {
			this.cut = cut;
		}

		public String getCod_distrito() {
			return cod_distrito;
		}

		public void setCod_distrito(String cod_distrito) {
			this.cod_distrito = cod_distrito;
		}

		public int getCod_carto() {
			return cod_carto;
		}

		public void setCod_carto(int cod_carto) {
			this.cod_carto = cod_carto;
		}

		public int getEstrato_muestral() {
			return estrato_muestral;
		}

		public void setEstrato_muestral(int estrato_muestral) {
			this.estrato_muestral = estrato_muestral;
		}

		public int getEstrato_geo() {
			return estrato_geo;
		}

		public void setEstrato_geo(int estrato_geo) {
			this.estrato_geo = estrato_geo;
		}

		public int getEstrato_ene() {
			return estrato_ene;
		}

		public void setEstrato_ene(int estrato_ene) {
			this.estrato_ene = estrato_ene;
		}

		public int getTipo_est() {
			return tipo_est;
		}

		public void setTipo_est(int tipo_est) {
			this.tipo_est = tipo_est;
		}

		public int getCodigo_seccion() {
			return codigo_seccion;
		}

		public void setCodigo_seccion(int codigo_seccion) {
			this.codigo_seccion = codigo_seccion;
		}

		public int getAct_id() {
			return act_id;
		}

		public void setAct_id(int act_id) {
			this.act_id = act_id;
		}

		public int getId_usuario() {
			return id_usuario;
		}

		public void setId_usuario(int id_usuario) {
			this.id_usuario = id_usuario;
		}

		
		public boolean isChecked() {
			return checked;
		}
		
		public void setChecked(boolean checked) {
			this.checked = checked;
		}

		public String getComuna_glosa() {
			return comuna_glosa;
		}

		public void setComuna_glosa(String comuna_glosa) {
			this.comuna_glosa = comuna_glosa;
		}
		

		
		
}