package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil_MMHDB;
import com.INE.ImasD.EnumeracionMobile.Entities.Dibujo;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.util.List;

public class AsyncTask_DescargarDesdeArchivo_UsuarioAct extends AsyncTask<Void, Integer, Integer> 
{
	ProgressDialog pd;
	private Context context;
    private AsyncTaskCompleteListener<Integer> listener;
    DataBaseUtil dbUtil;
	DataBaseUtil_MMHDB dbUtilMMHDB;

    public AsyncTask_DescargarDesdeArchivo_UsuarioAct(Context ctx, AsyncTaskCompleteListener<Integer> listener)
    {
        this.context = ctx;
        this.listener = listener;
    }
    
	@Override
	protected void onPreExecute() 
	{
		pd = new ProgressDialog(context);
		pd.setTitle("Cargando datos desde archivo");
		pd.setMessage("Leyendo datos, por favor espere...");       
		pd.setIndeterminate(false);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMax(100);
		pd.show();

	}

	// -- called from the publish progress
	// -- notice that the datatype of the second param gets passed to this method

	@Override
	protected Integer doInBackground(Void... params) 
	{
		return ObtenerManzanas() + ObtenerSecciones() + ObtenerHogares() + ObtenerViviendas() + 
				ObtenerImagenes() + ObtenerDibujos() + ObtenerEdificios();
				
		

	}

	@Override
	protected void onPostExecute(Integer result) 
	{
		super.onPostExecute(result);
		
		listener.onTaskCompleteDescargarDatos(result);
		
		int duration = Toast.LENGTH_SHORT;

		if(result>0)
		{
			
			Toast.makeText(context, "Datos Obtenidos correctamente", duration).show();
		}
		else
		{
			Toast.makeText(context, "Error al Obtener Datos", Toast.LENGTH_SHORT).show();

		}
		
		pd.dismiss();

	}
	
	private Integer ObtenerViviendas()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			List<Vivienda> viviendas;


			dbUtilMMHDB.open();
			viviendas = dbUtilMMHDB.ObtenerViviendas();
			dbUtilMMHDB.close();
			
			
			total = viviendas.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarViviendasEnviadas();

			//da.BorrarTodosUsuarios();
			for(Vivienda viv : viviendas)
			{

				dbUtil.InsertarVivienda(viv);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	


	private Integer ObtenerHogares()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			List<Hogar> hogares;

			dbUtilMMHDB.open();
			hogares = dbUtilMMHDB.ObtenerHogares();
			dbUtilMMHDB.close();
			

			total = hogares.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarHogares();


			for(Hogar hog : hogares)
			{

				hog.setEnviado(true);
				dbUtil.InsertarHogar(hog);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	
	private Integer ObtenerDibujos()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			dbUtilMMHDB.open();
			List<Dibujo> dibujos = dbUtilMMHDB.ObtenerDibujos(false);
			dbUtilMMHDB.close();
			

			total = dibujos.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarDibujosAntiguos();

			for(Dibujo dib : dibujos)
			{
				dib.setNUEVO(false);
				dbUtil.InsertarDibujo(dib);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}		
	private Integer ObtenerManzanas()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			List<Manzana> manzanas;

			dbUtilMMHDB.open();
			manzanas = dbUtilMMHDB.ObtenerManzanas();
			dbUtilMMHDB.close();

			total = manzanas.size();
			pd.setMax(total);
			dbUtil.open();

			dbUtil.BorrarManzanas();

			//da.BorrarTodosUsuarios();

			for(Manzana mz : manzanas)
			{
				dbUtil.InsertarManzana(mz);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}

	private Integer ObtenerSecciones()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			List<Seccion> secciones;

			dbUtilMMHDB.open();
			secciones = dbUtilMMHDB.ObtenerSecciones();
			dbUtilMMHDB.close();

			total = secciones.size();
			pd.setMax(total);
			dbUtil.open();

			dbUtil.BorrarSecciones();

			//da.BorrarTodosUsuarios();

			for(Seccion sec : secciones)
			{
				dbUtil.InsertarSeccion(sec);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	

	

	private Integer ObtenerEdificios()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			List<Edificio> edificios;

			dbUtilMMHDB.open();
			edificios = dbUtilMMHDB.ObtenerEdificios();
			dbUtilMMHDB.close();

			total = edificios.size();
			pd.setMax(total);
			dbUtil.open();

			dbUtil.BorrarEdificios();

			//da.BorrarTodosUsuarios();

			for(Edificio edif : edificios)
			{
				
				dbUtil.InsertarEdificio(edif);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	
	private Integer ObtenerImagenes()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		try
		{

			int total = 0;
			int i = 0;

			List<Imagen> imagenes;

			dbUtilMMHDB.open();
			imagenes = dbUtilMMHDB.ObtenerImagenesUsuario();
			dbUtilMMHDB.close();
			
			total = imagenes.size();
			pd.setMax(total);


			dbUtil.open();
			dbUtil.BorrarImagenes();
			
			for(Imagen img : imagenes)
			{
				try {
					
					dbUtil.InsertarImagen(img.getID_VIVIENDA(), img.getIMAGEN());
					
				} catch (Exception e) {	
					e.printStackTrace();
				}

				i++;
				pd.setProgress(i);

			}

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	
}