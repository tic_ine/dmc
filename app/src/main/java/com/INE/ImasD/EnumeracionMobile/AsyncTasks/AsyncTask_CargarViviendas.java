package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AsyncTask_CargarViviendas extends AsyncTask<Void, Integer, List<Vivienda>>
{
	private Context context;
    private AsyncTaskCompleteListener<List<Vivienda>> listener;
    ProgressDialog pd;
    private UUID ID_EDIFICIO;
    
    public AsyncTask_CargarViviendas(Context ctx, AsyncTaskCompleteListener<List<Vivienda>> listener, UUID... uuids)
    {
        this.context = ctx;
        this.listener = listener;

		if(uuids.length>0)
        this.ID_EDIFICIO = uuids[0];

	}
    
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
		if (pd == null) {
			pd = new ProgressDialog(this.context);
		}
		
		
		pd.setTitle("Cargando Viviendas async");
		pd.setMessage("Por favor espere...");       
		pd.setIndeterminate(true);
		pd.setMax(100);
		pd.show();

	}

	@Override
	protected List<Vivienda> doInBackground(Void... params) {

    	if(this.ID_EDIFICIO==null)
		{
			return ObtenerViviendas();
		}
		else

		{
			return ObtenerViviendasEdificio(this.ID_EDIFICIO);
		}

	}
	

	@Override 
	protected void onPostExecute(List<Vivienda> viviendas) 
	   {
		super.onPostExecute(viviendas);
		listener.onTaskCompleteViviendas(viviendas);
		pd.dismiss();
			
	     }

	private List<Vivienda> ObtenerViviendas()
	{
		if(globales.IDPADRE!=null)
		{
			DataBaseUtil dbUtil = new DataBaseUtil(this.context);
			dbUtil.open();
			List<Vivienda> viviendas = dbUtil.ObtenerViviendas();
			dbUtil.close();
			return viviendas;
		}
		else
		{
			return new ArrayList<Vivienda>();	
		}
		
	}

	private List<Vivienda> ObtenerViviendasEdificio(UUID ID_EDIF)
	{
		if(globales.IDPADRE!=null)
		{
			DataBaseUtil dbUtil = new DataBaseUtil(this.context);
			dbUtil.open();
			List<Vivienda> viviendas = dbUtil.ObtenerViviendasEdificio(ID_EDIF);
			dbUtil.close();
			return viviendas;
		}
		else
		{
			return new ArrayList<Vivienda>();
		}

	}

	
	}
