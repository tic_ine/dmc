package com.INE.ImasD.EnumeracionMobile.OtrasClases;

import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.MultiPath;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.Polyline;

public class WKT_Utilities {
	
	public static Geometry wkt2Geometry(String wkt) {
		Geometry geo = null;
		if (wkt == null || wkt == "") {
			return null;
		}
		String headStr = wkt.substring(0, wkt.indexOf("("));
		String temp = wkt.substring(wkt.indexOf("(") + 1, wkt.lastIndexOf(")"));
		if (headStr.trim().toUpperCase().equals("POINT")) {
			String[] values = temp.split(" ");
			geo = new Point(Double.valueOf(values[0]),
					Double.valueOf(values[1]));
		} else if (headStr.trim().toUpperCase().equals("LINESTRING") || headStr.trim().toUpperCase().equals("POLYGON") || headStr.trim().toUpperCase().equals("MULTIPOLYGON")) {
			
			geo = parseWKT(temp, headStr);
		} else if (headStr.trim().toUpperCase().equals("ENVELOPE")) {
			String[] extents = temp.split(",");
			geo = new Envelope(Double.valueOf(extents[0]),
					Double.valueOf(extents[1]), Double.valueOf(extents[2]),
					Double.valueOf(extents[3]));
		} 
		/*
		else if (headStr.trim().equals("MultiPoint")) 
		{
		}
		else if (headStr.trim().equals("MULTIPOLYGON")) 
		{
		}		
		*/
		else {
			return null;
		}
		return geo;
	}

	private static Geometry parseWKT(String multipath, String type) {
		//String subMultipath = multipath.substring(1, multipath.length() - 1);
		String subMultipath = multipath;
		String[] paths;
		double addX = 0;//1D;
		double addY = 0;//36D;
		
		//double addX = 1D;
		//double addY = 36D;		
		
		if (subMultipath.indexOf("),(") >= 0) {
			paths = subMultipath.split("),(");
		}
		else {
			subMultipath = subMultipath.replace("(","").replace(")","");
			paths = new String[] { subMultipath };
		}
		

				
				
		Point startPoint = null;
		Point pt = null;
		MultiPath path = null;
		if (type.trim().toUpperCase().equals("LINESTRING")) {
			path = new Polyline();
			addX = 0;
			addY = 0;
		} else {
			path = new Polygon();
		}
		
		
		for (int i = 0; i < paths.length; i++) {
			String[] points = paths[i].split(",");
			startPoint = null;
			for (int j = 0; j < points.length; j++) {
				String[] pointStr = points[j].trim().split(" ");
				//pt = (Point)GeometryEngine.project(new Point(DoubXle.valueOf(pointStr[0]), Double.valueOf(pointStr[1])),SpatialReference.create(4674), SpatialReference.create(102100)); 
				////pt = (Point)GeometryEngine.project(new Point(Double.valueOf(pointStr[0]), Double.valueOf(pointStr[1])),SpatialReference.create(4326), SpatialReference.create(102100));
				pt = new Point(Double.valueOf(pointStr[0]) + addX, Double.valueOf(pointStr[1]) - addY);
				
				if (startPoint == null) {
					startPoint = pt;
					path.startPath(startPoint);
				} 
				else 
				{
					path.lineTo(pt);
				}
			}
		}


		return path;
	}
	
	public static String Geometry2WKT(Geometry geometry)
	{
		String header = "";
		String content = "";
		String retorno = "";
		Point pt;
		MultiPath mp;

		
		switch(geometry.getType())
		{
		case POINT:
			pt = (Point)geometry;
			
			header = "POINT";
			content = "(" + pt.getY() + " " + pt.getX() + ")";

			break;
		case POLYLINE :
			mp = (MultiPath)geometry;
			header = "LINESTRING";
			content = "(" ; 
			for(int i = 0; i<= mp.getPointCount() -1; i++)
			{
				content +=  mp.getPoint(i).getX() + " " + mp.getPoint(i).getY();
				
				if(i<mp.getPointCount()-1) {
					content += ",";
				};
					
			}
			content += ")";
			
			break;
		case POLYGON:
			mp = (MultiPath)geometry;
			header = "POLYGON";

			content = "((" ; 
			for(int i = 0; i<= mp.getPointCount() -1; i++)
			{
				content +=  mp.getPoint(i).getX() + " " + mp.getPoint(i).getY();
				
				if(i<mp.getPointCount()-1) {
					content += ",";
				};
					
			}

			content += "))";
			
			break;			
		default:
			break;
		}
		
		retorno = header + content;
		
		return retorno;
		
	}
}




