package com.INE.ImasD.EnumeracionMobile;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.OpcionesMapa_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_RegistroPosicion_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_TipoCalle_Adapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Area_Trabajo;
import com.INE.ImasD.EnumeracionMobile.Entities.Dibujo;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.MBTilesLayer;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.OpcionMapa;
import com.INE.ImasD.EnumeracionMobile.Entities.POI;
import com.INE.ImasD.EnumeracionMobile.Entities.RegistroPosicion;
import com.INE.ImasD.EnumeracionMobile.Entities.RegistroPosicionDiario;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.CapaGrafica;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.Compass;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.WKT_Utilities;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;
import com.esri.android.map.Callout;
import com.esri.android.map.FeatureLayer;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.Layer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.LocationDisplayManager.AutoPanMode;
import com.esri.android.map.MapOnTouchListener;
import com.esri.android.map.MapView;
import com.esri.android.map.RasterLayer;
import com.esri.android.map.ags.ArcGISDynamicMapServiceLayer;
import com.esri.android.map.ags.ArcGISFeatureLayer;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.osm.OpenStreetMapLayer;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geodatabase.Geopackage;
import com.esri.core.geodatabase.GeopackageFeatureTable;
import com.esri.core.geometry.AngularUnit;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.LinearUnit;
import com.esri.core.geometry.MultiPath;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.Polyline;
import com.esri.core.geometry.ProjectionTransformation;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.Unit;
import com.esri.core.map.Graphic;
import com.esri.core.map.LabelingInfo;
import com.esri.core.map.RasterDataSource;
import com.esri.core.map.ResamplingProcessType;
import com.esri.core.raster.FileRasterSource;
import com.esri.core.raster.RasterSource;
import com.esri.core.renderer.Renderer;
import com.esri.core.renderer.SimpleRenderer;
import com.esri.core.symbol.PictureMarkerSymbol;
import com.esri.core.symbol.SimpleFillSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol.STYLE;
import com.esri.core.symbol.TextSymbol;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.joda.time.DateTime;
import org.json.JSONObject;


import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Mapa_Activity extends FragmentActivity {


    private int MODIFICAR_VIV = 1000;
    private int REQUEST_IMAGE_CAPTURE = 2000;
    private int AGREGARPOI = 2018;

    private static File demoDataFile;
    private static String offlineDataSDCardDirName;
    protected static String OFFLINE_FILE_EXTENSION = ".MBEJS";

    protected static String MENU_AGREGAR_VIVIENDA_PUNTEO = "Agregar Vivienda Punteo";
    protected static String MENU_AGREGAR_VIVIENDA_GPS = "Agregar Vivienda GPS";
    protected static String MENU_AGREGAR_POI = "Agregar Punto de Inter�s";
    protected static String MENU_OPCIONES_VIVIENDA = "Opciones Punto Registrado";
    protected static String MENU_DIBUJAR = "Dibujar en Mapa";
    protected static String MENU_UBICACION_MAPA = "�D�nde Estoy?";
    protected static String MENU_REGISTROS_POSICION = "Ver Registros de Posici�n";
    protected static String MENU_MEDIR_DISTANCIA = "Medir Distancia";
    protected static String MENU_RECARGAR_CAPAS = "Recargar Capas";
    protected static String MENU_IR_A_UPM = "Ir a UPM";
    protected static String MENU_MOVER_VIVIENDA = "Mover Posici�n de Vivienda";
    protected static String MENU_BORRAR_DIBUJO = "Borrar Dibujo";
    protected static String MENU_CANCELAR_ACCION = "Cancelar Acciones";

    static final int AGREGARVIVIENDA = 1;  // The request code
    static final int AGREGARDESCRIPCION = 2;  // The request code
    static Handler handler;
    String path;

        OpenStreetMapLayer osmlayer;
    MapView map = null;
    boolean IsDibujo;
    Button btnMenuMapa;

    ImageButton imgbtnMapa_Dibujar;

    Button btnMapa_GrabarDibujoAct;
    Button btnMapa_ReiniciarDibujo;
    Button btnMapa_CancelarDibujoAct;


    ToggleButton tbVerRegistroPosicion;
    ToggleButton tbVermeMapa;
    ToggleButton tbMedirDistancia;

    ToggleButton tbOSM;


    ImageButton imgbtnMapa_AgregarViviendaPunteo;
    ImageButton imgbtnMapa_AgregarViviendaGPS;

    ImageButton imgbtnMapa_BorrarDibujo;

    ImageButton imgbtnMapa_MoverPunto;

    ImageButton imgbtnMapa_OpcionesMenu;

    ImageButton imgbtnMapa_ActualizarCapas;

    ImageButton imgbtnMapaLayers;

    CheckBox chkMapa_VerLocalidades;
    CheckBox chkMapa_VerUPM;
    CheckBox chkMapa_VerAreas;
    CheckBox chkMapa_VerNumeros;

    CheckBox chkOK;
    CheckBox chkMapaPOI;



    CapaGrafica glviv = new CapaGrafica(true);
    CapaGrafica glDescripciones = new CapaGrafica(true);
    CapaGrafica gldib = new CapaGrafica(true);
    CapaGrafica glVivTEMP = new CapaGrafica(false);
    CapaGrafica glDibTEMP = new CapaGrafica(false);
    CapaGrafica glCentroides = new CapaGrafica(true);
    CapaGrafica glRegistroPosicion = new CapaGrafica(false);

    //GraphicsLayer glPosicionActual = new GraphicsLayer();

    CapaGrafica glOrdenesRegistro = new CapaGrafica(true);

    CapaGrafica glMedirDistancia = new CapaGrafica(false);

    CapaGrafica glUPM_seleccionada = new CapaGrafica(false);

    CapaGrafica glEditLayer = new CapaGrafica(false);

    CapaGrafica glEdificios = new CapaGrafica(true);
    CapaGrafica glNombresEdificios = new CapaGrafica(true);

    CapaGrafica glAreasLevantamiento = new CapaGrafica(true);

    CapaGrafica glNombreLocalidades = new CapaGrafica(true);
    CapaGrafica glLocalidades = new CapaGrafica(true);

    CapaGrafica glOK = new CapaGrafica(true);

    CapaGrafica glCirculoRadio = new CapaGrafica(false);


    CapaGrafica glPOI = new CapaGrafica(true);

    CapaGrafica glAreaTrabajo = new CapaGrafica(true);

    CapaGrafica glPseudoManzana = new CapaGrafica(true);
    Compass brujula;
    AccionesMapa AccionMapa;

    MyTouchListener myListener;

    String DescripcionNuevoDibujo;
    String TipoViaNuevoDibujo;
    int EstadoNuevoDibujo;

    LocationDisplayManager ls;

    ListView lstOpcionesMapa;
    DrawerLayout drawerLayout;

    Point PtProyectado;

    MBTilesLayer mbLayer;

    ProgressBar pbCargaMapa;
    View rootView;
    boolean SeleccionoRegistroPosicion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa_activity);



        pbCargaMapa = (ProgressBar) findViewById(R.id.pb_CargaMapa);

        lstOpcionesMapa = (ListView) findViewById(R.id.lstOpcionesMapa);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        tbVerRegistroPosicion = (ToggleButton) findViewById(R.id.tbVerRegPos);
        tbVermeMapa = (ToggleButton) findViewById(R.id.tbVerMiPosicion);

        tbMedirDistancia = (ToggleButton) findViewById(R.id.tbMedirDistancia);

        //tbOSM = (ToggleButton)findViewById(R.id.tbOSM);

        ArcGISRuntime.setClientId("bGwXe4VPRYAJATlD");

        imgbtnMapa_AgregarViviendaPunteo = (ImageButton) findViewById(R.id.imgbtnMapa_AgregarViviendaPunteo);
        imgbtnMapa_AgregarViviendaGPS = (ImageButton) findViewById(R.id.imgbtnMapa_OpcionesMenu);


        //imgbtnMapa_AgregarViviendaGPS.setOnTouchListener(new MyDragTouchListener());
        //imgbtnMapa_BorrarDibujo =(ImageButton)findViewById(R.id.imgbtnMapa_BorrarDibujo);

        imgbtnMapa_MoverPunto = (ImageButton) findViewById(R.id.imgbtnMapa_EdicioCarto);

        imgbtnMapa_ActualizarCapas = (ImageButton) findViewById(R.id.imgbtnMapa_ActualizarCapas);


        imgbtnMapaLayers = (ImageButton) findViewById(R.id.imgbtnMapaLayers);
        chkMapa_VerLocalidades = (CheckBox) findViewById(R.id.chkMapa_VerLocalidades);
        chkMapa_VerUPM = (CheckBox) findViewById(R.id.chkMapa_VerUPM);
        chkMapa_VerAreas = (CheckBox) findViewById(R.id.chkMapa_VerAreas);

        chkMapa_VerNumeros = (CheckBox) findViewById(R.id.chkMapa_VerNumeros);

        chkOK = (CheckBox) findViewById(R.id.chkMapa_VerOK);

        chkMapaPOI= (CheckBox) findViewById(R.id.chkMapaPOI);


        List<OpcionMapa> opciones = new ArrayList<OpcionMapa>();
        opciones.add(new OpcionMapa(R.drawable.agregarvivienda32, MENU_AGREGAR_VIVIENDA_PUNTEO));
        opciones.add(new OpcionMapa(R.drawable.agregar_vivienda_gps_32, MENU_AGREGAR_VIVIENDA_GPS));
        opciones.add(new OpcionMapa(R.drawable.info_icon, MENU_AGREGAR_POI));
        opciones.add(new OpcionMapa(R.drawable.info_vivienda, MENU_OPCIONES_VIVIENDA));
        opciones.add(new OpcionMapa(R.drawable.dibujo_mapa, MENU_DIBUJAR));
        opciones.add(new OpcionMapa(R.drawable.mapa, MENU_UBICACION_MAPA));
        opciones.add(new OpcionMapa(R.drawable.red_dot, MENU_REGISTROS_POSICION));
        opciones.add(new OpcionMapa(R.drawable.regla, MENU_MEDIR_DISTANCIA));
        opciones.add(new OpcionMapa(R.drawable.refresh, MENU_RECARGAR_CAPAS));
        opciones.add(new OpcionMapa(R.drawable.bluepushpin32, MENU_IR_A_UPM));
        opciones.add(new OpcionMapa(R.drawable.cancelar, MENU_CANCELAR_ACCION));

        OpcionesMapa_Adapter adapter = new OpcionesMapa_Adapter(Mapa_Activity.this, opciones);

        adapter.notifyDataSetChanged();
        lstOpcionesMapa.setAdapter(adapter);


        lstOpcionesMapa.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                drawerLayout.closeDrawers();
                OpcionMapa opc = (OpcionMapa) adapter.getItemAtPosition(position);

                Toast.makeText(Mapa_Activity.this, opc.getGlosa_opcion(), Toast.LENGTH_SHORT).show();

                if (opc.getGlosa_opcion().equals(MENU_AGREGAR_VIVIENDA_PUNTEO)) {
                    AgregarViviendaPunteo();
                } else if (opc.getGlosa_opcion().equals(MENU_AGREGAR_VIVIENDA_GPS)) {
                    AgregarViviendaGPS();
                } else if (opc.getGlosa_opcion().equals(MENU_AGREGAR_POI)) {
                    AgregarPOI();
                } else if (opc.getGlosa_opcion().equals(MENU_OPCIONES_VIVIENDA)) {
                    AccionMapa = AccionesMapa.INFO_VIV;
                    Toast.makeText(Mapa_Activity.this, "Seleccione el punto en el mapa para ver sus opciones.", Toast.LENGTH_SHORT).show();
                } else if (opc.getGlosa_opcion().equals(MENU_UBICACION_MAPA)) {
                    AccionMapa = AccionesMapa.NINGUNA;
                    //ls.setAutoPan(true);
                }
                //
                else if (opc.getGlosa_opcion().equals(MENU_REGISTROS_POSICION)) {
                    myListener.setType("POINT");
                    AccionMapa = AccionesMapa.REGISTRO_POSICION;
                    tbVerRegistroPosicion.setChecked(true);
                    //new AsyncCargarRegistrosPosicion().execute();


                    //new AsyncCargarMapasLocales().execute();
                } else if (opc.getGlosa_opcion().equals(MENU_MEDIR_DISTANCIA)) {
                    Toast.makeText(Mapa_Activity.this, "Indique el punto de inicio y arrastre. Levante el dedo para finalizar", Toast.LENGTH_SHORT).show();
                    tbMedirDistancia.setChecked(true);

                    //new AsyncCargarMapasLocales().execute();
                } else if (opc.getGlosa_opcion().equals(MENU_RECARGAR_CAPAS)) {
                    AccionMapa = AccionesMapa.NINGUNA;
                    ActualizarCapas();

                }
                //
                else if (opc.getGlosa_opcion().equals(MENU_DIBUJAR)) {
                    AccionMapa = AccionesMapa.DIBUJAR;
                    Dibujar(true);
                } else if (opc.getGlosa_opcion().equals(MENU_IR_A_UPM)) {
                    AccionMapa = AccionesMapa.NINGUNA;

                    if (glCentroides.getNumberOfGraphics() == 1) {
                        map.setExtent(globales.Polygon_UPM);
                    }

                } else if (opc.getGlosa_opcion().equals(MENU_CANCELAR_ACCION)) {
                    AccionMapa = AccionesMapa.NINGUNA;
                    Dibujar(false);
                }


            }
        });

        demoDataFile = Environment.getExternalStorageDirectory();
        offlineDataSDCardDirName = this.getResources().getString(R.string.SDCardOfflineDir);
        //Viv_filename = this.getResources().getString(R.string.nombrearchivoViviendasJSON);
        //Dib_filename = this.getResources().getString(R.string.nombrearchivoDibujosJSON);

        DescripcionNuevoDibujo = "";

        // Retrieve the map and initial extent from XML layout


        map = (MapView) findViewById(R.id.map);


        map.enableWrapAround(true);
        map.setAllowRotationByPinch(true);


        String ruta = "file://" + Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator;

        CrearCapasOperativas();
        //ActualizarCapas();



        //map.addLayer(ObtenerCapaMapaCache());


		/*seteo de criterio de location*/
        /*
		LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
	    criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
	    criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
	    criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
	    criteria.setAltitudeRequired(false); // Choose if you use altitude.
	    criteria.setBearingRequired(false); // Choose if you use bearing.
	    criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

	    // Provide your criteria and flag enabledOnly that tells
	    // LocationManager only to return active providers.
	    lm.getBestProvider(criteria, true);
		 */
		/*fin seteo de location*/

		/*INICIO DE SERVICIO DE LOCALIZACION*/


        ls = map.getLocationDisplayManager();

        ls.start();
        ls.setAutoPanMode(AutoPanMode.LOCATION);
        ls.setAllowNetworkLocation(true);

        try {
            ls.setCourseSymbol(new PictureMarkerSymbol(getResources().getDrawable(R.drawable.enumerador12)));
        } catch (NotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //ls.setBearing(true);
        //ls.setAccuracyCircleOn(true);

        ls.setLocationListener(new LocationListener() {

            public void onStatusChanged(String provider, int status, Bundle extras) {
                // TODO Auto-generated method stub

            }

            public void onProviderEnabled(String provider) {
                // TODO Auto-generated method stub

            }

            public void onProviderDisabled(String provider) {
                // TODO Auto-generated method stub

            }

            //@SuppressLint("NewApi")
            public void onLocationChanged(Location location) {
                // TODO Auto-generated method stub
                if (location != null) {

                    if (location.hasBearing()) {
                        brujula.setRotationAngle(location.getBearing());
                    }

                    Point nuevopunto = GeometryEngine.project(location.getLongitude(), location.getLatitude(), SpatialReference.create(globales.SRID_WGS84));
                    //Point nuevopunto = new Point(location.getLongitude(), location.getLatitude());
                    double dist;

                    if (globales.UltimaPuntoObtenido_GPS == null) {
                        globales.UltimaPuntoObtenido_GPS = nuevopunto;

                        globales.UltimaPuntoObtenido_GPS = nuevopunto;//new Point(location.getLongitude(), location.getLatitude());
                        DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                        db.open();
                        db.InsertarRegistroPosicion();
                        db.close();
                        db = null;
                        //Toast.makeText(Mapa_Activity.this, "?Primera coordenada obtenida!", Toast.LENGTH_SHORT).show();
                    }


                    if (globales.boolHabilitarRegistroPosicion) {
                        Polyline pl = new Polyline();
                        pl.startPath(nuevopunto);
                        pl.lineTo(globales.UltimaPuntoObtenido_GPS);


                        dist = GeometryEngine.geodesicLength(pl, SpatialReference.create(globales.SRID_WGS84), new LinearUnit(LinearUnit.Code.METER));

                        if (dist > Integer.valueOf(globales.strDistanciaRegistroPosicion)) {
                            globales.UltimaPuntoObtenido_GPS = nuevopunto;//new Point(location.getLongitude(), location.getLatitude());
                            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                            db.open();
                            db.InsertarRegistroPosicion();
                            db.close();
                            db = null;
                        }

                    }

                    if ((globales.boolCentrarPosicion)&&(ls.getAutoPanMode() == AutoPanMode.LOCATION)) {

                        map.centerAt(nuevopunto, true);
                        //map.zoomToScale(nuevopunto, zoom);
                    }


                }


            }
        });



		/*FIN DE SERVICIO DE LOCALIZACION*/


        imgbtnMapa_Dibujar = (ImageButton) findViewById(R.id.imgbtnMapa_Dibujar);

        imgbtnMapa_Dibujar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AccionMapa = AccionesMapa.DIBUJAR;
                glDibTEMP.removeAll();
                Dibujar(true);
                ToogleDescripcionDibujo(View.VISIBLE);
                imgbtnMapa_Dibujar.setVisibility(View.INVISIBLE);
            }
        });

        btnMapa_GrabarDibujoAct = (Button) findViewById(R.id.btnMapa_GrabarDibujoAct);

        btnMapa_GrabarDibujoAct.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                //GrabarDibujoCapaInternet();

                //GrabarNuevoDibujoEnLocal();
                Dibujar(false);

            }
        });


        btnMapa_ReiniciarDibujo = (Button) findViewById(R.id.btnMapa_ReiniciarDibujo);
        btnMapa_ReiniciarDibujo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                glDibTEMP.removeAll();
                //Dibujar(true);
                myListener.startPoint = null;
                AccionMapa = AccionesMapa.DIBUJAR;
                myListener.setType("POLYLINE");
                Toast.makeText(Mapa_Activity.this, "Puntee con dedo sobre la pantalla para a?adir v?rtices al dibujo", Toast.LENGTH_SHORT).show();

            }
        });


        btnMapa_CancelarDibujoAct = (Button) findViewById(R.id.btnMapa_CancelarDibujoAct);

        btnMapa_CancelarDibujoAct.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Dibujar(false);
            }
        });

        btnMenuMapa = (Button) findViewById(R.id.btnMenuMapa);

        btnMenuMapa.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                drawerLayout.openDrawer(lstOpcionesMapa);
            }
        });
        IsDibujo = false;


        imgbtnMapa_OpcionesMenu = (ImageButton) findViewById(R.id.imgbtnMapa_OpcionesMenu);
        imgbtnMapa_OpcionesMenu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                AccionMapa = AccionesMapa.INFO_VIV;

            }
        });

        map.setOnStatusChangedListener(new OnStatusChangedListener() {

            private static final long serialVersionUID = 1L;

            @Override
            public void onStatusChanged(Object arg0, STATUS arg1) {
                // TODO Auto-generated method stub

                if (STATUS.LAYER_LOADED == arg1)
                {
                    //,-33.4512,17

                    if (arg0 instanceof MBTilesLayer)
                    {
                        //						if(((MBTilesLayer)arg0).getName() == "MBTILES")
                        //						{
//                        map.setExtent(globales.Polygon_UPM);
                        //map.zoomToScale(globales.Polygon_UPM.getPoint(0),8D);
                        //						}

                    }

                    if (arg0 instanceof ArcGISLocalTiledLayer)
                    {
                        map.setExtent(((ArcGISLocalTiledLayer) arg0).getExtent());
                    }


                    //map.zoomToScale((Point)GeometryEngine.project(new Point(-70.6521,-33.4512), SpatialReference.create(4326), SpatialReference.create(102100)) ,10D);

                }
            }
        });


        myListener = new MyTouchListener(Mapa_Activity.this, map);
        map.setOnTouchListener(myListener);



        brujula = new Compass(Mapa_Activity.this, null, map);

        map.addView(brujula);


        tbVerRegistroPosicion.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (isChecked) {
                    SeleccionoRegistroPosicion = false;
                    myListener.setType("POINT");
                    AccionMapa = AccionesMapa.REGISTRO_POSICION;

                    DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                    db.open();

                    List<RegistroPosicionDiario> registros = null;
                    try {
                        registros = db.ObtenerDiasRegistroPosicion();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    db.close();

                    final ListView lstDias = new ListView(Mapa_Activity.this);

                    final AlertDialog dialog = new AlertDialog.Builder(Mapa_Activity.this)
                            .setView(lstDias)
                            .setTitle("Seleccione un d�a para ver registros de posici�n")
                            .create();


                    dialog.setOnDismissListener(new OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            if (!SeleccionoRegistroPosicion) {
                                tbVerRegistroPosicion.setChecked(false);
                                AccionMapa = AccionesMapa.NINGUNA;
                            }
                        }
                    });

                    final Spin_RegistroPosicion_Adapter adapter = new Spin_RegistroPosicion_Adapter(Mapa_Activity.this, registros);

                    lstDias.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    lstDias.setAdapter(adapter);

                    lstDias.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position,
                                                long id) {
                            // TODO Auto-generated method stub
                            RegistroPosicionDiario reg = adapter.getItem(position);
                            new AsyncCargarRegistrosPosicion().execute(reg.getDia());
                            SeleccionoRegistroPosicion = true;
                            dialog.dismiss();

                        }
                    });

                    //AlertDialog.Builder builder = new AlertDialog.Builder(Mapa_Activity.this);

                    dialog.show();

                    //new AsyncCargarRegistrosPosicion().execute();

                } else {
                    glRegistroPosicion.removeAll();
                    map.getCallout().hide();
                }

            }
        });

        tbVermeMapa.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                map.getCallout().hide();
                if (tbVermeMapa.isChecked()) {
                    ls.start();
                    ls.setAutoPanMode(AutoPanMode.LOCATION);

                    ls.setAccuracyCircleOn(true);
                    ls.setOpacity(1f);
                    try {
                        ls.setCourseSymbol(new PictureMarkerSymbol(getResources().getDrawable(R.drawable.enumerador12)));


                    } catch (NotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    //ls.setAutoPan(true);
                } else {
                    ls.setAccuracyCircleOn(false);
                    ls.setOpacity(0);
                    ls.setAutoPanMode(AutoPanMode.OFF);
                    //ls.setAutoPan(false);

                }

            }
        });

        tbVermeMapa.setChecked(true);

        tbMedirDistancia.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                glMedirDistancia.removeAll();
                map.getCallout().hide();
                if (isChecked) {
                    myListener.setType("POLYLINE");
                    AccionMapa = AccionesMapa.MEDIR_DISTANCIA;
                } else {
                    glDibTEMP.removeAll();
                    myListener.setType("");
                    AccionMapa = AccionesMapa.NINGUNA;

                }

            }
        });


        chkMapa_VerLocalidades.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                glLocalidades.setVisible(isChecked);
                glNombreLocalidades.setVisible(isChecked);

            }
        });

        chkMapa_VerAreas.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                glAreasLevantamiento.setVisible(isChecked);

            }
        });

        chkMapa_VerUPM.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                glCentroides.setVisible(isChecked);

            }
        });


        chkMapa_VerNumeros.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                glOrdenesRegistro.setVisible(isChecked);

            }
        });

        chkOK.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                glOK.setVisible(isChecked);
            }
        });

        chkMapaPOI.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                glPOI.setVisible(isChecked);
            }
        });


        imgbtnMapa_ActualizarCapas.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AccionMapa = AccionesMapa.NINGUNA;
                ActualizarCapas();
            }
        });

        imgbtnMapaLayers.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new LayersDialogFragment().show(getFragmentManager(), "LayersDialogFragment");
            }
        });
		/*
		tbOSM.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
				{
					//map.removeLayer(mbLayer);
					map.addLayer(osm,0);
				}
				else
				{
					map.removeLayer(osm);
					//map.addLayer(mbLayer,0);
				}


			}
		});
		 */

        ToogleDescripcionDibujo(View.INVISIBLE);


    }

    private void AgregarViviendaPunteo() {
        AccionMapa = AccionesMapa.AGREGARVIVIENDA_PUNTEO;
        myListener.setType("POINT");
        Toast.makeText(Mapa_Activity.this, "Pinche en alg?n punto del mapa para crear nueva vivienda", Toast.LENGTH_SHORT).show();
    }

    private void AgregarViviendaGPS() {
        AccionMapa = AccionesMapa.AGREGARVIVIENDA_GPS;
        myListener.setType("POINT");
        if (globales.UltimaPuntoObtenido_GPS == null) {
            Toast.makeText(Mapa_Activity.this, "No existe una posici?n v?lida actual. Utilice Agregar Vivienda Punteo", Toast.LENGTH_SHORT).show();
        } else {
            DialogoCreacionVivienda(globales.UltimaPuntoObtenido_GPS);
        }

    }

    private void AgregarPOI() {
        AccionMapa = AccionesMapa.AGREGAR_POI;
        myListener.setType("POINT");
        Toast.makeText(Mapa_Activity.this, "Pinche en alg�n punto del mapa para crear nuevo punto de inter�s", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        // code here to show dialog

        if (drawerLayout.isDrawerOpen(lstOpcionesMapa)) {
            drawerLayout.closeDrawers();
        } else {
            Toast.makeText(Mapa_Activity.this, "Recuerde enviar sus datos antes de salir de la aplicaci�n presionando el bot�n Enviar en el men� superior.\n\nSi desea salir, presione Cerrar Sesi?n en el men? superior", Toast.LENGTH_SHORT).show();
        }
        //super.onBackPressed();  // optional depending on your needs
    }


    private void Dibujar(boolean Activar) {

        IsDibujo = Activar;
        if (IsDibujo) {
            //Limpiar();
            //mp = new Polyline();
            final Dialog dialog = new Dialog(Mapa_Activity.this);
            dialog.setTitle(R.string.tituloDibujo);
            dialog.setContentView(R.layout.descripciondibujo);


            final Button btnNuevoDibujoAceptar = (Button) dialog.findViewById(R.id.btnNuevoDibujoAceptar);
            final Button btnNuevoDibujoCancelar = (Button) dialog.findViewById(R.id.btnNuevoDibujoCancelar);
            final TextView txtNuevoDibujoDescripcion = (TextView) dialog.findViewById(R.id.txtNuevoDibujoDescripcion);

            final Spinner spNuevoDibujo_TipoVia = (Spinner) dialog.findViewById(R.id.spNuevoDibujo_TipoVia);


            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            List<TipoCalle> tiposcalle = db.ObtenerTiposCalle();
            db.close();

            Spin_TipoCalle_Adapter adapt = new Spin_TipoCalle_Adapter(Mapa_Activity.this, tiposcalle);
            spNuevoDibujo_TipoVia.setAdapter(adapt);
            adapt.notifyDataSetChanged();


            btnNuevoDibujoCancelar.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ToogleDescripcionDibujo(View.INVISIBLE);
                    imgbtnMapa_Dibujar.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                    myListener.startPoint = null;
                    glDibTEMP.removeAll();
                }
            });


            btnNuevoDibujoAceptar.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (txtNuevoDibujoDescripcion.getEditableText().toString().equals("")) {
                        Toast.makeText(Mapa_Activity.this, "Ingrese descripci�n de v�a", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    DescripcionNuevoDibujo = txtNuevoDibujoDescripcion.getEditableText().toString();
                    TipoViaNuevoDibujo = ((TipoCalle) spNuevoDibujo_TipoVia.getSelectedItem()).getTipocalle_id();

                    ToogleDescripcionDibujo(View.VISIBLE);
                    imgbtnMapa_Dibujar.setVisibility(View.INVISIBLE);
                    dialog.dismiss();
                    myListener.startPoint = null;
                    Toast.makeText(Mapa_Activity.this, R.string.punteo, Toast.LENGTH_SHORT).show();
                    myListener.setType("POLYLINE");
                    glDibTEMP.removeAll();
                }
            });
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.show();

        } else {

            if (glDibTEMP.getNumberOfGraphics() > 0) {
                int idnuevodibujo = glDibTEMP.getGraphicIDs()[0];
                Graphic nuevodibujo = glDibTEMP.getGraphic(idnuevodibujo);

                String wkt = WKT_Utilities.Geometry2WKT(GeometryEngine.project(nuevodibujo.getGeometry(), SpatialReference.create(globales.SRID_WGS84), SpatialReference.create(globales.SRID_WebMercator_Spherical)));

                GrabarDibujo(wkt);
            }

            myListener.startPoint = null;
            ToogleDescripcionDibujo(View.INVISIBLE);
            imgbtnMapa_Dibujar.setVisibility(View.VISIBLE);
            glDibTEMP.removeAll();
            AccionMapa = AccionesMapa.NINGUNA;
            myListener.setType("");
            ActualizarCapas();
            ;
        }
    }

    private void ToogleDescripcionDibujo(int Visibility) {

        btnMapa_GrabarDibujoAct.setVisibility(Visibility);
        btnMapa_CancelarDibujoAct.setVisibility(Visibility);
        btnMapa_ReiniciarDibujo.setVisibility(Visibility);
    }

    public enum AccionesMapa {
        NINGUNA(0),
        AGREGARVIVIENDA_PUNTEO(1),
        AGREGARVIVIENDA_GPS(2),
        DIBUJAR(3),
        REGISTRO_POSICION(4),
        MEDIR_DISTANCIA(5),
        INFO_UPM(6),
        INFO_VIV(7),
        INFO_DIB(8),
        INFO_RP(9),
        BORRAR_DIBUJO(10),
        PREVIO_MOVER_PUNTO(11),
        MOVER_PUNTO(12),
        AGREGAR_OTRO_USO(13),
        AGREGAR_EDIFICIO(14),
        ILUMINAR_RECORRIDO_PUNTOS(15),
        AGREGAR_POI(16),
        INFO_POI(17);



        private int value;

        private AccionesMapa(int value)
        {

            this.value = value;
        }


        public int getValue() {
            return value;
        }


    }








    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        map.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        map.unpause();


        ConfigurarActivity();

        glDescripciones.setVisible(globales.boolMostrarDescripciones);
        glNombresEdificios.setVisible(globales.boolMostrarNombreEdificio);
        //ls.setAutoPan(globales.boolCentrarPosicion);


    }


    private class AsyncCargarMapasLocales extends AsyncTask<Void, Integer, Integer> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            pd = new ProgressDialog(Mapa_Activity.this);
            pd.setTitle("Cargando Mapa...");
            pd.setMessage("Accediendo a capas... Por favor espere.");
            pd.setIndeterminate(true);
            pd.show();


            pbCargaMapa.setVisibility(View.VISIBLE);
            //CrearCapasOperativas();
        }

        @Override
        protected Integer doInBackground(Void... params) {

            MarcarUPMCarga();
            SetearCentroide();
            CargarAreasLevantamiento();
            CargarLocalidadesPoligonos();
            CargarViviendas();
            CargarDibujos();
            CargarPOI();
            CargarAreasTrabajo();
            CargarPseudoManzanas();
            //openGeoPackage();
            return 0;
        }

        private void CargarPseudoManzanas() {
            glPseudoManzana.removeAll();
            List<Polygon> pseudomanzanas = new ArrayList<Polygon>();

            try {
                pseudomanzanas= new DataBaseSpatialite().ObtenerGeometriasPseudoManzana();
//SimpleFillSymbol sfs = new SimpleFillSymbol(Color.YELLOW, com.esri.core.symbol.SimpleFillSymbol.STYLE.HORIZONTAL);
                SimpleFillSymbol sfsAL = new SimpleFillSymbol(Color.TRANSPARENT);
                sfsAL.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Violet), 8,
                        com.esri.core.symbol.SimpleLineSymbol.STYLE.SOLID));
//graphic = new Graphic(poly, sfs);

                Graphic graphAL;
                for (Polygon poly : pseudomanzanas) {
                    graphAL = new Graphic(poly, sfsAL);
                    glPseudoManzana.addGraphic(graphAL);
                }
            }
            catch (Exception e)
            {

            }
        }



        private void MarcarUPMCarga() {
            //glUPMCargaLaboral.removeAll();
            int mRegistros = 0;
            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            //List<Manzana> manzanas = db.ObtenerManzanas();
            Graphic graphic;
            Polygon poly;
            //for(Manzana mz : manzanas)
            //{
            try {

                poly = new DataBaseSpatialite().ObtenerGeometriaManzana(globales.IDPADRE);

                if (null == poly) {
                    poly = new DataBaseSpatialite().ObtenerGeometriaSeccion(globales.IDPADRE);
                }

                mRegistros = db.ObtenerTotalRegistrosUPM(globales.IDPADRE);
                SimpleFillSymbol sfs = new SimpleFillSymbol(Color.TRANSPARENT);
                sfs.setOutline(new SimpleLineSymbol((mRegistros == 0 ? Color.RED : Color.GREEN), 5, com.esri.core.symbol.SimpleLineSymbol.STYLE.DASHDOTDOT));
                graphic = new Graphic(poly, sfs);


                globales.Polygon_UPM = poly;
                glCentroides.addGraphic(graphic);

            } catch (jsqlite.Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            db.close();

        }



        private void CargarAreasLevantamiento() {
            glAreasLevantamiento.removeAll();
            //SimpleFillSymbol sfs = new SimpleFillSymbol(Color.YELLOW, com.esri.core.symbol.SimpleFillSymbol.STYLE.HORIZONTAL);
            SimpleFillSymbol sfsAL = new SimpleFillSymbol(Color.TRANSPARENT);
            sfsAL.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.DarkSlateBlue), 8, com.esri.core.symbol.SimpleLineSymbol.STYLE.SOLID));
            //graphic = new Graphic(poly, sfs);

            Graphic graphAL;
            for (Polygon poly : globales.Polygons_AreaLevantamiento) {
                graphAL = new Graphic(poly, sfsAL);
                glAreasLevantamiento.addGraphic(graphAL);
            }


        }

        private void CargarLocalidadesPoligonos() {
            glLocalidades.removeAll();
            SimpleFillSymbol sfsAL = new SimpleFillSymbol(Color.TRANSPARENT);
            sfsAL.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Indigo), 8, SimpleLineSymbol.STYLE.SOLID));


            Graphic graphLoc;
            for (Polygon poly : globales.Polygons_Localidades) {
                graphLoc = new Graphic(poly, sfsAL);
                glLocalidades.addGraphic(graphLoc);
            }

            if (globales.Polygons_Localidades != null) {
                for (Polygon poly : globales.Polygons_Localidades) {
                    Graphic gr = new Graphic(poly.getPoint(0), new TextSymbol(25, globales.LOCALIDAD.equals("") ? "(LOCALIDAD SIN NOMBRE)" : globales.LOCALIDAD.toUpperCase(), Color.BLACK)); // agrego el nombre de localidad
                    glNombreLocalidades.addGraphic(gr);
                }

            }
        }

        private void CargarViviendas() {
            glviv.removeAll();
            glOrdenesRegistro.removeAll();
            glNombresEdificios.removeAll();
            glDescripciones.removeAll();

            int total = 0;
            publishProgress(0);

            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            List<Vivienda> viviendas = db.ObtenerViviendasID_Todas();
            Graphic graphic;
            Edificio miedif;
            Graphic gr;
            Graphic grOK;
            Point pt;
            List<UUID> edificios = new ArrayList<UUID>();
            List<UUID> descripciones = new ArrayList<UUID>();


            Map<String, Object> atributos;


            int i = 1;
            total = viviendas.size();

            for (Vivienda viv : viviendas) {
                try {

                    pt = (Point)GeometryEngine.project(new Point(viv.getCOORD_X(), viv.getCOORD_Y()), SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));

                    atributos = new HashMap<String, Object>();
                    atributos.put("ID_VIVIENDA", viv.getID_VIVIENDA().toString());

                    if (viv.isEnEdificio()) {
                        if (!edificios.contains(viv.getID_EDIF())) // SI NO SE HA PINTADO EL EDIFICIO
                        {
                            atributos.put("ID_EDIF", viv.getID_EDIF().toString());
                            graphic = new Graphic(pt, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.edificio)), atributos, i);
                            glEdificios.addGraphic(graphic);

                            miedif = db.ObtenerEdificio(viv.getID_EDIF());
                            pt.setX(pt.getX() - 0.006F);
                            gr = new Graphic(pt, new TextSymbol(12, miedif.getNombre_edif(), Color.BLACK), atributos, 0); // agrego el n?mero
                            glNombresEdificios.addGraphic(gr);
                            edificios.add(viv.getID_EDIF());
                        }

                    } else {
                        if (viv.isNULO()) {
                            //PictureMarkerSymbol pms = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.borrar));
                            //graphic = new Graphic(pt, pms,atributos, i);
                            graphic = new Graphic(pt, new SimpleMarkerSymbol(getResources().getColor(R.color.Black), 10, STYLE.CIRCLE), atributos, i);
                            glviv.addGraphic(graphic);
                        } else {


                            Point p = new Point(pt.getX() + 0.00004F, pt.getY() - 0.00004F);
                            //AQUI SE AGREGA EL ICONO EN EL MAPA
                            if (viv.isOK()) {
                                grOK = new Graphic(p, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.confirmado)), atributos, i);
                                glOK.addGraphic(grOK);
                            }


                            if (viv.getID_USODESTINO() <= 1)  // SIN EDIFICACION U OTRO USO
                            {
                                //PictureMarkerSymbol pms = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.otro_uso32));
                                //graphic = new Graphic(pt, pms,atributos, i);

                                graphic = new Graphic(pt, new SimpleMarkerSymbol(getResources().getColor(R.color.Orange), 10, STYLE.CIRCLE), atributos, i);
                                glviv.addGraphic(graphic);


                                if (!descripciones.contains(viv.getID_VIVIENDA())) {
                                    pt.setX(pt.getX() - 0.00004F);
                                    gr = new Graphic(pt, new TextSymbol(12, String.valueOf(viv.getDESCRIPCION()), Color.BLACK), atributos, 0); // agrego el n?mero
                                    glDescripciones.addGraphic(gr);
                                    descripciones.add(viv.getID_VIVIENDA());
                                }

                                String orden = String.valueOf(globales.strMostrarOrdenes.equals("1") ? "" : viv.getORDEN());
                                pt.setX(pt.getX() - 0.00003F);
                                gr = new Graphic(pt, new TextSymbol(15, String.valueOf(orden), Color.BLACK), atributos, 0); // agrego el n?mero


                                glOrdenesRegistro.addGraphic(gr);
                            } else {
                                //PictureMarkerSymbol pms = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.vivienda_32));
                                //graphic = new Graphic(pt, pms,atributos, i);
                                graphic = new Graphic(pt, new SimpleMarkerSymbol(getResources().getColor(R.color.Blue), 10, STYLE.CIRCLE), atributos, i);
                                glviv.addGraphic(graphic);


                                String orden = String.valueOf(globales.strMostrarOrdenes.equals("1") ? viv.getORDEN_VIV() : viv.getORDEN());
                                pt.setX(pt.getX() - 0.00003F);
                                gr = new Graphic(pt, new TextSymbol(15, String.valueOf(orden), Color.BLACK), atributos, 0); // agrego el n?mero

                                glOrdenesRegistro.addGraphic(gr);

                            }
                            //graphic = new Graphic(pt, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.vivienda)),atributos, i);

                        }

                    }


                    if (AccionMapa == AccionesMapa.ILUMINAR_RECORRIDO_PUNTOS) {
                        Thread.sleep(100, 0);
                    }

                    publishProgress((i * 100) / total);

                    i++;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            db.close();

        }

        private void CargarDibujos()
        {
            gldib.removeAll();
            int total = 0;
            publishProgress(0);

            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            List<Dibujo> dibujos = db.ObtenerDibujos(false);
            Graphic graphic;
            Polyline pl;

            Map<String, Object> atributos;


            int i = 1;
            total = dibujos.size();
            for (Dibujo dib : dibujos) {
                try {

                    pl = (Polyline) GeometryEngine.project(WKT_Utilities.wkt2Geometry(dib.getDIB_TEXT()), SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));

                    atributos = new HashMap<String, Object>();
                    atributos.put("ID_DIBUJO", dib.getID_DIBUJO());

                    graphic = new Graphic(pl, new SimpleLineSymbol(getResources().getColor(R.color.Orange), 8, SimpleLineSymbol.STYLE.SOLID), atributos, i);

                    gldib.addGraphic(graphic);

                    publishProgress((i * 100) / total);

                    i++;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            db.close();

        }


        private void CargarPOI()
        {
            glPOI.removeAll();
            int total = 0;
            publishProgress(0);

            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            List<POI> pois = db.ObtenerPOIS();
            Graphic graphic;
            Point pt;

            int i = 1;
            total = pois.size();
            Bitmap bitmap;

            Map<String, Object> atributos ;

            for (POI poi : pois) {
                try {

                    atributos = new HashMap<String, Object>();
                    atributos.put("IDPOI", poi.getIdpoi().toString());

                    pt = (Point) GeometryEngine.project(new Point(poi.getCoord_x(),poi.getCoord_y()), SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));

                    Drawable image = new BitmapDrawable(BitmapFactory.decodeByteArray(poi.getCATEGORIAPOI().getIcon(), 0, poi.getCATEGORIAPOI().getIcon().length));
                    graphic = new Graphic(pt, new PictureMarkerSymbol(image), atributos);

                    glPOI.addGraphic(graphic);

                    publishProgress((i * 100) / total);

                    i++;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            db.close();

        }

        private void CargarAreasTrabajo()
        {
            glAreaTrabajo.removeAll();
            int total = 0;
            publishProgress(0);

            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            List<Area_Trabajo> ats = db.ObtenerAreasTrabajo(false);
            Graphic graphic;
            Point pt;

            int i = 1;
            total = ats.size();

            Map<String, Object> atributos ;


            Graphic graphAT;
            Polygon poly;
            for (Area_Trabajo at : ats) {
                try {

                    atributos = new HashMap<String, Object>();
                    atributos.put("AT_ID", at.getAt_id());

                    SimpleFillSymbol sfsAL = new SimpleFillSymbol(Color.TRANSPARENT);
                    sfsAL.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Red), 5, com.esri.core.symbol.SimpleLineSymbol.STYLE.SOLID));

                    poly = (Polygon) WKT_Utilities.wkt2Geometry(at.getWkt());


                    poly = (Polygon)GeometryEngine.project(poly, SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));

                    graphAT = new Graphic(poly, sfsAL, atributos);

                    glAreaTrabajo.addGraphic(graphAT);

                    publishProgress((i * 100) / total);

                    i++;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            db.close();

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //Main.getApp().progressBar.setProgress(values[0]);
            //Main.getApp().txt_percentage.setText("downloading" + values[0] + "%");
            pbCargaMapa.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Integer Result) {
            //pd.dismiss();

            //fLayerViviendas.refresh();
            //fLayerDibujo.refresh();
            //map.invalidate();
            Toast.makeText(Mapa_Activity.this, "Capas Cargadas", Toast.LENGTH_LONG).show();
            pd.dismiss();

            AccionMapa = AccionesMapa.NINGUNA;

            pbCargaMapa.setVisibility(View.INVISIBLE);


        }

		/*
		private FeatureSet ObtenerDibujos()
		{
			FeatureSet fs = new FeatureSet();
			List<Field> campos = new ArrayList<Field>();
			List<Dibujo> dibujos;

			DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
			db.open();
			dibujos = db.ObtenerDibujos(false);
			db.close();


			Graphic[] graphics = new Graphic[dibujos.size()];

			try {

				campos.add(new Field("ID_DIBUJO", "ID_DIBUJO", "esriFieldTypeInteger"));
				campos.add(new Field("DIBUJO_TEXT", "DIBUJO_TEXT", "esriFieldTypeString"));
				campos.add(new Field("ACT_ID", "ACT_ID", "esriFieldTypeInteger"));
				campos.add(new Field("ID_USUARIO", "ID_USUARIO", "esriFieldTypeInteger"));
				campos.add(new Field("DESCRIPCION", "DIBUJO_TEXT", "esriFieldTypeString"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			fs.setFields(campos);

			fs.setSpatialReference(SpatialReference.create(102100));

			//newgpv = new Graphic(GeometryEngine.project(gp.getGeometry(), SpatialReference.create(102100), SpatialReference.create(4674)) , new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), gp.getAttributes(), idxviv+1);

			Graphic gp;

			MapGeometry geom;
			JsonParser parser;
			int i = 0;

			Map<String, Object> atributos;


			for(Dibujo dib : dibujos)
			{
				atributos = new HashMap<String, Object>();
				atributos.put("DESCRIPCION", dib.getDIB_GLOSA());
				atributos.put("ACT_ID" , dib.getACT_ID());
				atributos.put("ID_USUARIO", dib.getID_USUARIO());

				try {
					parser = new JsonFactory().createJsonParser(dib.getDIB_TEXT());
					geom = GeometryEngine.jsonToGeometry(parser);


					gp = new Graphic(geom.getGeometry(), new SimpleMarkerSymbol(Color.BLUE, 5, STYLE.CIRCLE), atributos,i) ;
					graphics[i] = gp;
					i++;


				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}


			fs.setGraphics(graphics);

			return fs;

		}
		 */
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == AGREGARVIVIENDA) {
            if (resultCode == RESULT_OK && data != null) {// si grabo se agrega el punto a la capa
                Bundle b = data.getExtras();
                if (b != null) {
                    Vivienda viv = (Vivienda) b.getSerializable("NUEVA_VIVIENDA");
                    //ActualizarCapas();
                    //globales.ActualizarCapas = false;
                    ActualizarCapas();

                }

            } else // Cancelar ingreso
            {
                glVivTEMP.removeAll();
                glCirculoRadio.removeAll();
                //this.AccionMapa = AccionesMapa.NINGUNA;
            }
        } else if (requestCode == MODIFICAR_VIV)
        {

        }
        else if (requestCode == REQUEST_IMAGE_CAPTURE) {

            Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

            try {

                bm = BitmapFactory.decodeFile(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
                bm = funciones.RotateBitmap(bm, 90);
                if (bm != null) {
                    funciones.ResizeAndSaveImage(Mapa_Activity.this, bm, 320, 240, 100, globales.ID_VIVIENDA_LISTADO);
                }
            } catch (Exception e) {

            }

        }
        else if (requestCode == AGREGARPOI)
        {
            if (resultCode == RESULT_OK) {
                ActualizarCapas();
            }
            else
            {
                glVivTEMP.removeAll();
                glCirculoRadio.removeAll();
            }
        }
		/*
		else
		{
			if(resultCode == RESULT_OK && data !=null)
				{
					Bundle b = data.getExtras();
					if (b != null) {
						Vivienda viv = (Vivienda) b.getSerializable("VIVIENDA");
						//ActualizarCapas();
		
					} 
				}
			Reordenar_Fragment fragment = (Reordenar_Fragment)getSupportFragmentManager().getFragments().get(0);
		    fragment.onActivityResult(AGREGARDESCRIPCION, resultCode, data);
		}
		*/
    }


    private class MyTouchListener extends MapOnTouchListener {
        // ArrayList<Point> polylinePoints = new ArrayList<Point>();

        MultiPath poly;
        String type = "";
        Point startPoint = null;

        public MyTouchListener(Context context, MapView view) {
            super(context, view);
        }

        public void setType(String geometryType) {
            this.type = geometryType;
        }

        public String getType() {
            return this.type;
        }

		/*
		 * Invoked when user single taps on the map view. This event handler
		 * draws a point at user-tapped location, only after "Draw Point" is
		 * selected from Spinner.
		 * 
		 * @see
		 * com.esri.android.map.MapOnTouchListener#onSingleTap(android.view.
		 * MotionEvent)
		 */

        @SuppressLint("NewApi")
        public boolean onSingleTap(MotionEvent e) {

            map.getCallout().hide();
            //glviv.clearSelection();
            gldib.clearSelection();
            glDibTEMP.clearSelection();
            //glNuevasViv.clearSelection();
            //glNuevosDib.clearSelection();
            glUPM_seleccionada.removeAll();

            PtProyectado = (Point) GeometryEngine.project(map.toMapPoint(e.getX(), e.getY()), SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));

            if (AccionMapa == AccionesMapa.INFO_UPM) {

                try
                {

                    glviv.clearSelection();
                    poly = null;
                    startPoint = null;

                    //PtProyectado = (Point) GeometryEngine.project(map.toMapPoint(e.getX(), e.getY()), SpatialReference.create(3857), SpatialReference.create(4326));
                    long geoc = 0;
                    Polygon polygono;


                    polygono = new DataBaseSpatialite().ObtenerGeometriaManzana(PtProyectado.getY(), PtProyectado.getX());

                    if (polygono != null) // si es manzana
                    {
                        DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                        db.open();
                        geoc = new DataBaseSpatialite().ObtenerGeocodigoManzana(PtProyectado.getY(), PtProyectado.getX());

                        Manzana mz = db.ObtenerManzana(null, geoc);
                        db.close();

                        boolean IsMzSpatial = false;

                        if (mz == null) {
                            mz = new DataBaseSpatialite().ObtenerManzana(geoc);

                            if (mz == null) {
                                return true;
                            } else
                            {
                                IsMzSpatial = true;
                                mz.setCentroide_lng(PtProyectado.getX());
                                mz.setCentroide_lat(PtProyectado.getY());
                            }

                        }

                        MostrarInfoManzana(mz, IsMzSpatial, polygono);
                    } else {
                        polygono = new DataBaseSpatialite().ObtenerGeometriaSeccion(PtProyectado.getY(), PtProyectado.getX());

                        if (polygono != null) // es seccion
                        {

                            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                            db.open();
                            long cu_seccion = new DataBaseSpatialite().ObtenerCU_Seccion(PtProyectado.getY(), PtProyectado.getX());

                            Seccion sec = db.ObtenerSeccion(String.valueOf(cu_seccion));

                            db.close();

                            MostrarInfoSeccionUPM(sec, PtProyectado);

                        }

                    }

                    SimpleFillSymbol fill = new SimpleFillSymbol(Color.TRANSPARENT);
                    fill.setOutline(new SimpleLineSymbol(Color.CYAN, 5));


                    glUPM_seleccionada.addGraphic(new Graphic(polygono, fill));


                } catch (jsqlite.Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            } else if (AccionMapa == AccionesMapa.INFO_RP) {
                glviv.clearSelection();
                EsTapRegistroPosicion(e.getX(), e.getY());
            } else if (AccionMapa == AccionesMapa.INFO_POI) {
                glPOI.clearSelection();
                EsTapPOI(e.getX(), e.getY());
            } else if (AccionMapa == AccionesMapa.INFO_VIV) {
                glviv.clearSelection();
                EsTapViv(e.getX(), e.getY());

                glEdificios.clearSelection();
                EsTapEdificio(e.getX(), e.getY());
            } else if (AccionMapa == AccionesMapa.PREVIO_MOVER_PUNTO) {
                MoverPunto(e.getX(), e.getY());
            } else if (AccionMapa == AccionesMapa.MOVER_PUNTO) {
                glEditLayer.removeAll();
                int[] ids = null;

                ids = glviv.getSelectionIDs();
                if (ids == null) {

                    AccionMapa = AccionesMapa.PREVIO_MOVER_PUNTO;
                    return true;
                } else {
                    Graphic gpOld = glviv.getGraphic(ids[0]);

                    Point puntoviv = map.toMapPoint(e.getX(), e.getY());

                    boolean coincidepuntopadre = CoincideManzanaNuevoPunto(puntoviv);

                    final Graphic gpNew = new Graphic(puntoviv, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.moverpunto)), gpOld.getAttributes(), 1);
                    glEditLayer.addGraphic(gpNew);

                    new AlertDialog.Builder(Mapa_Activity.this)
                            .setIcon(R.drawable.agregarvivienda)
                            .setTitle("Nueva Coordenada de Vivienda")
                            .setMessage((coincidepuntopadre ? "�Desea modificar la coordenada de la vivienda a la nueva posici�n?" : "Advertencia: �Al parecer el punto marcado se encuentra alejado de la UPM original!\n\n?Desea modificar la posici?n de la vivienda en el punto marcado de todos modos?"))
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                                    db.open();
                                    boolean retorno = db.GeorreferenciarVivienda(UUID.fromString(gpNew.getAttributeValue("ID_VIVIENDA").toString()), (Point) gpNew.getGeometry());
                                    db.close();

                                    glviv.clearSelection();
                                    glEditLayer.removeAll();
                                    AccionMapa = AccionesMapa.NINGUNA;

                                    ActualizarCapas();
                                }

                            })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    glviv.clearSelection();
                                    glEditLayer.removeAll();
                                    AccionMapa = AccionesMapa.PREVIO_MOVER_PUNTO;
                                }
                            })
                            .show();
                }

            } else if (AccionMapa == AccionesMapa.INFO_DIB) {
                glviv.clearSelection();
                EsTapDib(e.getX(), e.getY());
            } else if (AccionMapa == AccionesMapa.BORRAR_DIBUJO) {
                glviv.clearSelection();
                SeleccionarBorrarDibujo(e.getX(), e.getY());
            } else if (AccionMapa == AccionesMapa.AGREGAR_POI) {
                glVivTEMP.removeAll();
                AgregarPOI(map.toMapPoint(e.getX(), e.getY()));
            } else if (type.length() > 1 && type.equalsIgnoreCase("POINT")) {

                if (AccionMapa == AccionesMapa.AGREGARVIVIENDA_PUNTEO) {
                    glviv.clearSelection();
                    DialogoCreacionVivienda(CrearPuntoMapa_Punteo(e));
                }

            } else if (AccionMapa == AccionesMapa.DIBUJAR && (type.equalsIgnoreCase("POLYLINE") || type.equalsIgnoreCase("POLYGON"))) {
                glviv.clearSelection();
                Point mapPt = map.toMapPoint(e.getX(), e.getY());

                if (startPoint == null)
                {
                    poly = type.equalsIgnoreCase("POLYLINE") ? new Polyline() : new Polygon();
                    startPoint = map.toMapPoint(e.getX(), e.getY());
                    poly.startPath((float) startPoint.getX(), (float) startPoint.getY());
                }

                poly.lineTo((float) mapPt.getX(), (float) mapPt.getY());


                glDibTEMP.removeAll();
                glDibTEMP.addGraphic(new Graphic(poly, new SimpleLineSymbol(Color.CYAN, 8)));
                ToogleDescripcionDibujo(View.VISIBLE);
                //GrabarDibujo(GeometryEngine.geometryToJson(102100, poly));
                return true;
            } else {

            }


            return true;
        }


        public boolean onDragPointerMove(MotionEvent from, MotionEvent to) {


            if (AccionMapa == AccionesMapa.MEDIR_DISTANCIA
                    && (type.equalsIgnoreCase("POLYLINE") || type
                    .equalsIgnoreCase("POLYGON"))) {


                Point mapPt = map.toMapPoint(to.getX(), to.getY());

                if (startPoint == null) {
                    if (AccionMapa == AccionesMapa.MEDIR_DISTANCIA) {
                        glMedirDistancia.removeAll();
                    }

                    poly = type.equalsIgnoreCase("POLYLINE") ? new Polyline()
                            : new Polygon();
                    startPoint = map.toMapPoint(from.getX(), from.getY());
                    poly.startPath((float) startPoint.getX(),
                            (float) startPoint.getY());

                }

                poly.lineTo((float) mapPt.getX(), (float) mapPt.getY());

                return true;

            } else if (AccionMapa == AccionesMapa.DIBUJAR) {
                return true;
            } else if (AccionMapa == AccionesMapa.MOVER_PUNTO) {
                return true;
            } else
                return super.onDragPointerMove(from, to);
			/*
			else if (AccionMapa== AccionesMapa.DIBUJAR)
			{
				return true;
			}
			else
			{
				return super.onDragPointerMove(from, to);
			}
			 */
        }


        @Override
        public boolean onDragPointerUp(MotionEvent from, MotionEvent to) {
            if (AccionMapa == AccionesMapa.MEDIR_DISTANCIA
                    && (type.equalsIgnoreCase("POLYLINE") || type
                    .equalsIgnoreCase("POLYGON"))) {
                glMedirDistancia.addGraphic(new Graphic(poly, new SimpleLineSymbol(Color.YELLOW, 5)));

                double dist = GeometryEngine.geodesicLength(poly, SpatialReference.create(globales.SRID_WGS84), new LinearUnit(LinearUnit.Code.METER));
                Toast.makeText(Mapa_Activity.this, "Distancia: " + String.valueOf(Math.round(dist)) + " metros.", Toast.LENGTH_LONG).show();
                startPoint = null;

                return true;
            } else if (AccionMapa == AccionesMapa.DIBUJAR)
                return true;
            else
                return super.onDragPointerUp(from, to);

        }


        private boolean EsTapRegistroPosicion(float x, float y) {
            String fecha;
            glRegistroPosicion.clearSelection();

            int[] ids = glRegistroPosicion.getGraphicIDs(x, y, 20, 1);

            if (ids.length > 0) {
                glRegistroPosicion.setSelectionColor(getResources().getColor(R.color.GreenYellow));

                glRegistroPosicion.setSelectedGraphics(ids, true);
                Graphic gp = glRegistroPosicion.getGraphic(ids[0]);

                final Callout callout;
                View calloutView;

                LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
                calloutView = inflater.inflate(R.layout.callout_registro_posicion, null);

                callout = map.getCallout();
                callout.setCoordinates((Point) gp.getGeometry());

                TextView tvFechaRegistroPosicion;
                ImageButton btnCerrarCalloutRegistroPosicion;

                tvFechaRegistroPosicion = (TextView) calloutView.findViewById(R.id.lblMVAgregarHogar);
                btnCerrarCalloutRegistroPosicion = (ImageButton) calloutView.findViewById(R.id.btnCerrarCalloutRegistroPosicion);

                fecha = gp.getAttributeValue("FECHA") == null ? "" : gp.getAttributeValue("FECHA").toString();

                tvFechaRegistroPosicion.setText(fecha);
                btnCerrarCalloutRegistroPosicion.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        callout.hide();
                        glRegistroPosicion.clearSelection();
                    }
                });


                callout.setContent(calloutView);
                callout.refresh();
                callout.show();

                //Toast.makeText(Mapa_Activity.this, gp.getAttributeValue("FECHA").toString(), Toast.LENGTH_SHORT).show();
            }

            return ids.length > 0;
        }

        private boolean EsTapViv(float x, float y) {

            //String fecha;
            glviv.clearSelection();
            //glNuevasViv.clearSelection();
            Graphic gp;

            int[] ids;
            ids = glviv.getGraphicIDs(x, y, 20, 1);

            glviv.setSelectedGraphics(ids, true);


            if (ids.length > 0) {
                gp = glviv.getGraphic(ids[0]);
                DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                db.open();
                UUID ID_VIVIENDA = UUID.fromString(gp.getAttributeValue("ID_VIVIENDA").toString().toLowerCase().replace("{", "").replace("}", ""));
                Imagen preview = db.ObtenerImagen(ID_VIVIENDA);

                final Vivienda viv = db.ObtenerVivienda(ID_VIVIENDA);
                if (viv != null) {

                    globales.intPosicionListado = viv.getORDEN();
                    db.close();


                    String direccion;
                    String usodestino;
                    final Callout callout;
                    View calloutView;

                    LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
                    calloutView = inflater.inflate(R.layout.callout_vivienda, null);

                    callout = map.getCallout();
                    callout.setCoordinates((Point) gp.getGeometry());

                    TextView tvCOV_Localidad;
                    TextView tvCOV_Entidad;

                    TextView tvCOV_Direccion;
                    TextView tvCOV_UsoDestino;
                    TextView tvCOV_OrdenViv;
                    TextView tvCOV_Hogares_Personas;
                    TextView tvCOV_Descripcion;

                    ImageButton btnCOV_ModificarReg;
                    ImageButton btnCOV_AnularViv;
                    ImageButton imgbtnOK;
                    ImageButton btnCOV_Foto;


                    ImageView imgbtnPreviewRegistro;


                    tvCOV_Localidad  = (TextView) calloutView.findViewById(R.id.tvCOV_Localidad);
                    tvCOV_Entidad  = (TextView) calloutView.findViewById(R.id.tvCOV_Entidad);

                    tvCOV_Direccion = (TextView) calloutView.findViewById(R.id.tvCOV_Direccion);
                    tvCOV_UsoDestino = (TextView) calloutView.findViewById(R.id.tvCOV_UsoDestino);
                    tvCOV_OrdenViv = (TextView) calloutView.findViewById(R.id.tvCOV_OrdenViv);

                    tvCOV_Hogares_Personas = (TextView) calloutView.findViewById(R.id.tvCOV_Hogar_Persona);
                    tvCOV_Descripcion = (TextView) calloutView.findViewById(R.id.tvCOV_Descripcion);

                    btnCOV_ModificarReg = (ImageButton) calloutView.findViewById(R.id.btnCOV_ModificarReg);
                    btnCOV_AnularViv = (ImageButton) calloutView.findViewById(R.id.btnCOV_AnularViv);
                    btnCOV_Foto = (ImageButton) calloutView.findViewById(R.id.btnCOV_Foto);
                    imgbtnOK = (ImageButton) calloutView.findViewById(R.id.imgbtnOK);
                    imgbtnPreviewRegistro = (ImageView) calloutView.findViewById(R.id.imgbtnPreviewRegistro );

                    direccion = viv.getNOMBRE_CALLE_CAMINO() + " N�" + viv.getN_DOMICILIO();
                    usodestino = viv.getGLOSA_USODESTINO();

                    tvCOV_Localidad.setText("Loc:" + viv.getNOMBRELOCALIDAD());
                    tvCOV_Entidad.setText("Ent:" + viv.getNOMBREENTIDAD());

                    tvCOV_Direccion.setText("Dir:" + direccion);
                    tvCOV_UsoDestino.setText("Uso:" + usodestino);
                    tvCOV_OrdenViv.setText("Ord:" + viv.getORDEN() + ".OrdViv:" + viv.getORDEN_VIV());
                    tvCOV_Hogares_Personas.setText("Hog.:" + viv.getHOGARES() + ". Pers.:" + viv.getPERSONAS());
                    tvCOV_Descripcion.setText("Desc:" + viv.getDESCRIPCION());

                    imgbtnOK.setImageDrawable((viv.isOK() ? getResources().getDrawable(R.drawable.confirmado) : getResources().getDrawable(R.drawable.noconfirmado)));

                    //CrearViewParaArrastre(x,y);


                    btnCOV_ModificarReg.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            if (globales.padres == PADRES.MANZANAS) {
                                Intent i = new Intent(Mapa_Activity.this, ModificarVivienda_Activity.class);
                                i.putExtra("ID_VIVIENDA", viv.getID_VIVIENDA().toString());
                                startActivityForResult(i, MODIFICAR_VIV);
                            }

                            if (globales.padres == PADRES.SECCIONES) {
                                Intent i = new Intent(Mapa_Activity.this, ModificarVivienda_Rural_Activity.class);
                                i.putExtra("ID_VIVIENDA", viv.getID_VIVIENDA().toString());
                                startActivityForResult(i, MODIFICAR_VIV);

                            }
                        }
                    });

                    btnCOV_AnularViv.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            String accion = viv.isNULO() ? "Restablecer " : "Anular ";

                            AlertDialog.Builder ad = new AlertDialog.Builder(Mapa_Activity.this);
                            ad.setTitle(accion + " Vivienda");


                            ad.setIcon(R.drawable.pregunta);
                            ad.setMessage("�Est� seguro que desea " + accion + " el punto seleccionado?");
                            ad.setPositiveButton("S�", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    AnularVivienda(viv.getID_VIVIENDA(), !viv.isNULO());
                                    callout.hide();
                                    glviv.clearSelection();
                                    ActualizarCapas();
                                    // TODO Auto-generated method stub

                                }
                            });

                            ad.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    callout.hide();
                                    glviv.clearSelection();
                                }
                            });
                            ad.show();

                        }
                    });

                    btnCOV_Foto.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            SacarFoto(viv);
                            callout.hide();


                        }
                    });

                    imgbtnOK.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            boolean isChecked = !viv.isOK();

                            DataBaseUtil dbUtil = new DataBaseUtil(Mapa_Activity.this);
                            dbUtil.open();
                            dbUtil.ActualizarOKVivienda(viv.getID_VIVIENDA(), isChecked);
                            dbUtil.close();

                            viv.setOK(isChecked);

                            Toast.makeText(Mapa_Activity.this, "Vivienda" + (isChecked ? " " : " NO ") + "confirmada", Toast.LENGTH_SHORT).show();

                            ((ImageButton) v).setImageDrawable((isChecked ? getResources().getDrawable(R.drawable.confirmado) : getResources().getDrawable(R.drawable.noconfirmado)));
                            globales.intPosicionListado = viv.getORDEN();
                            ActualizarCapas();
                            AccionMapa = AccionesMapa.INFO_VIV;


                        }
                    });


                    if(preview!=null)
                    {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        final Bitmap bitmap = BitmapFactory.decodeByteArray(preview.getIMAGEN(), 0, preview.getIMAGEN().length);
                        imgbtnPreviewRegistro.setImageBitmap(bitmap);

                      }
                    else
                    {
                        imgbtnPreviewRegistro.setImageDrawable(getResources().getDrawable(R.drawable.vivienda));
                    }
                    calloutView.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            callout.hide();
                            glviv.clearSelection();
                        }
                    });


                    callout.setContent(calloutView);
                    callout.refresh();
                    callout.show();
                }
                //Toast.makeText(Mapa_Activity.this, gp.getAttributeValue("ID_VIVIENDA").toString(), Toast.LENGTH_SHORT).show();
            } else {
                if (map.getCallout().isShowing()) {
                    map.getCallout().hide();
                }
                Toast.makeText(Mapa_Activity.this, "Punto no asociado a registro", Toast.LENGTH_SHORT).show();
            }
            return ids.length > 0;
        }

        private boolean EsTapPOI(float x, float y) {

            String fecha = "";
            glPOI.clearSelection();
            Graphic gp;

            int[] ids = glPOI.getGraphicIDs(x, y, 20, 1);

            if (ids.length > 0) {
                glPOI.setSelectionColor(getResources().getColor(R.color.GreenYellow));

                glPOI.setSelectedGraphics(ids, true);
                gp = glPOI.getGraphic(ids[0]);

                final Callout callout;
                View calloutView;

                LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
                calloutView = inflater.inflate(R.layout.callout_poi, null);

                callout = map.getCallout();
                callout.setCoordinates((Point) gp.getGeometry());

                TextView tvInfoPOI_Glosa;
                ImageView imgInfoPOI_Icon;
                TextView tvInfoPOI_FechaCaptura;
                TextView tvInfoPOI_GlosaCat;

                tvInfoPOI_GlosaCat= (TextView) calloutView.findViewById(R.id.tvInfoPOI_GlosaCat);
                tvInfoPOI_Glosa = (TextView) calloutView.findViewById(R.id.tvInfoPOI_Glosa);
                tvInfoPOI_FechaCaptura = (TextView) calloutView.findViewById(R.id.tvInfoPOI_FechaCaptura);
                imgInfoPOI_Icon = (ImageView) calloutView.findViewById(R.id.imgInfoPOI_Icon);

                String IDPOI = gp.getAttributeValue("IDPOI") == null ? "" : gp.getAttributeValue("IDPOI").toString();

                POI poi;
                DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                db.open();
                poi = db.ObtenerPOI(IDPOI);
                db.close();

                final Bitmap bitmap = BitmapFactory.decodeByteArray(poi.getCATEGORIAPOI().getIcon(), 0, poi.getCATEGORIAPOI().getIcon().length);
                imgInfoPOI_Icon.setImageBitmap(bitmap);

                tvInfoPOI_GlosaCat.setText(poi.getCATEGORIAPOI().getGlosa());
                tvInfoPOI_Glosa.setText(poi.getGlosa().trim());
                tvInfoPOI_FechaCaptura.setText(funciones.FormatearFecha( new DateTime(poi.getFechacaptura())));

                callout.setContent(calloutView);
                callout.refresh();
                callout.show();
            }

            return  true;
        }


        private void AnularVivienda(UUID ID_VIVIENDA, boolean Anular) {

            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            db.AnularViviendas(ID_VIVIENDA, Anular);
            db.close();


        }

        private boolean MoverPunto(float x, float y) {
            //String fecha;
            glviv.clearSelection();
            //glNuevasViv.clearSelection();
            Graphic gp;

            int[] ids;
            ids = glviv.getGraphicIDs(x, y, 20, 1);

            if (ids.length > 0) {
                glviv.setSelectedGraphics(ids, true);
                gp = glviv.getGraphic(ids[0]);
                UUID ID_VIVIENDA = UUID.fromString(gp.getAttributeValue("ID_VIVIENDA").toString());

                DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                db.open();
                Vivienda viv = db.ObtenerVivienda(ID_VIVIENDA);
                db.close();

                if (viv.isEnEdificio()) {
                    AccionMapa = AccionesMapa.PREVIO_MOVER_PUNTO;
                    Toast.makeText(Mapa_Activity.this, "No puede modificar un punto de vivienda si tiene un edificio asociado", Toast.LENGTH_SHORT).show();
                } else {
                    AccionMapa = AccionesMapa.MOVER_PUNTO;
                    Toast.makeText(Mapa_Activity.this, "Indique la nueva posici?n de la vivienda", Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(Mapa_Activity.this, "Punto no seleccionado, intente nuevamente", Toast.LENGTH_SHORT).show();
                AccionMapa = AccionesMapa.PREVIO_MOVER_PUNTO;
            }


            return true;
        }

        private boolean EsTapDib(float x, float y) {
            //String fecha;
            gldib.clearSelection();
            //glNuevasViv.clearSelection();
            Graphic gp;

            int[] ids;

            ids = gldib.getGraphicIDs(x, y, 20, 1);
            //gldib.setSelectionColor(getResources().getColor(R.color.Red));
            gldib.setSelectedGraphics(ids, true);


            if (ids.length > 0) {

                gp = gldib.getGraphic(ids[0]);

                DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                db.open();
                UUID ID_DIBUJO = UUID.fromString(gp.getAttributeValue("ID_DIBUJO").toString().toLowerCase().replace("{", "").replace("}", ""));

                Cursor c = db.ObtenerInfoDibujo(ID_DIBUJO);
                int estado;
                String strEstado;

                if (c.moveToFirst()) {
                    db.close();

                    final Callout callout;
                    View calloutView;

                    LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
                    calloutView = inflater.inflate(R.layout.callout_dibujo, null);

                    callout = map.getCallout();

                    callout.setCoordinates(map.toMapPoint(x, y));

                    TextView tvCOD_TipoDibujo;
                    TextView tvCOD_Glosa;
                    TextView tvCOD_Estado;
                    TextView tvCOD_FechaCreacion;
                    TextView tvCOD_Enviado;


                    tvCOD_TipoDibujo = (TextView) calloutView.findViewById(R.id.tvCOD_TipoDibujo);
                    tvCOD_Glosa = (TextView) calloutView.findViewById(R.id.tvCOD_Glosa);
                    tvCOD_Estado = (TextView) calloutView.findViewById(R.id.tvCOD_Estado);
                    tvCOD_FechaCreacion = (TextView) calloutView.findViewById(R.id.tvCOD_FechaCreacion);
                    tvCOD_Enviado = (TextView) calloutView.findViewById(R.id.tvCOD_Enviado);

                    tvCOD_TipoDibujo.setText("Tipo V�a:" + c.getString(c.getColumnIndex("TIPOCALLE_GLOSA")));
                    tvCOD_Glosa.setText("Nombre:" + c.getString(c.getColumnIndex("DIB_GLOSA")));
                    estado = c.getInt(c.getColumnIndex("DIB_ESTADO"));
                    switch (estado) {
                        case 0:
                            strEstado = "Nuevo";
                            break;
                        case 1:
                            strEstado = "Modificado";
                            break;
                        case 2:
                            strEstado = "Eliminado";
                            break;
                        default:
                            strEstado = "N/A";
                            break;
                    }
                    tvCOD_Estado.setText("Estado:" + strEstado);
                    tvCOD_FechaCreacion.setText("Creaci�n:" + funciones.FormatearFecha(new DateTime(c.getLong(c.getColumnIndex("FECHA_CREACION")))));
                    tvCOD_Enviado.setText(c.getInt(c.getColumnIndex("NUEVO")) == 1 ? "No Enviado" : "Enviado");


                    calloutView.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            callout.hide();
                            gldib.clearSelection();
                        }
                    });


                    callout.setContent(calloutView);
                    callout.refresh();
                    callout.show();
                }
                //Toast.makeText(Mapa_Activity.this, gp.getAttributeValue("ID_VIVIENDA").toString(), Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(Mapa_Activity.this, "Punto no asociado a registro", Toast.LENGTH_SHORT).show();
            }
            return ids.length > 0;
        }


        private boolean EsTapEdificio(float x, float y) {
            //String fecha;
            glEdificios.clearSelection();
            //glNuevasViv.clearSelection();
            Graphic gp;

            int[] ids;
            int TotalViviendas = 0;

            ids = glEdificios.getGraphicIDs(x, y, 20, 1);
            //gldib.setSelectionColor(getResources().getColor(R.color.Red));
            glEdificios.setSelectedGraphics(ids, true);

            final UUID ID_EDIF;

            if (ids.length > 0) {

                gp = glEdificios.getGraphic(ids[0]);

                DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                db.open();
                ID_EDIF = UUID.fromString(gp.getAttributeValue("ID_EDIF").toString().toLowerCase().replace("{", "").replace("}", ""));

                Edificio edif = db.ObtenerEdificio(ID_EDIF);
                if (edif.getId_edif() != null) {
                    TotalViviendas = db.ObtenerTotalViviendasEdificio(ID_EDIF);

                    db.close();

                    final Callout callout;
                    View calloutView;

                    LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
                    calloutView = inflater.inflate(R.layout.callout_edificio, null);

                    callout = map.getCallout();

                    callout.setCoordinates(map.toMapPoint(x, y));

                    TextView tvCOE_NombreEdificio;

                    TextView tvCOE_TotalViviendas;
                    Button btnCOE_VerViv;


                    tvCOE_NombreEdificio = (TextView) calloutView.findViewById(R.id.tvCOE_NombreEdificio);
                    tvCOE_TotalViviendas= (TextView) calloutView.findViewById(R.id.tvCOE_TotalViviendas);

                    btnCOE_VerViv = (Button) calloutView.findViewById(R.id.btnCOE_VerViv);

                    tvCOE_NombreEdificio.setText(edif.getNombre_edif());

                    tvCOE_TotalViviendas.setText("Total de Viviendas:" + TotalViviendas);
                    calloutView.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            callout.hide();
                            glEdificios.clearSelection();
                        }
                    });

                    btnCOE_VerViv.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            //Toast.makeText(Mapa_Activity.this, "Ver viviendas de edificio", Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(Mapa_Activity.this, ViviendasEdificio_Activity.class);
                            i.putExtra("ID_EDIF", ID_EDIF.toString());
                            startActivity(i);

                        }
                    });
                    callout.setContent(calloutView);
                    callout.refresh();
                    callout.show();
                }
                //
            } else {
                //Toast.makeText(Mapa_Activity.this, "Punto no asociado a registro", Toast.LENGTH_SHORT).show();
            }
            return ids.length > 0;
        }

        private boolean SeleccionarBorrarDibujo(float x, float y) {


            final float mix = x;
            final float miy = y;

            gldib.clearSelection();


            final int[] ids;

            ids = gldib.getGraphicIDs(mix, miy, 20, 1);
            //gldib.setSelectionColor(getResources().getColor(R.color.Red));
            gldib.setSelectedGraphics(ids, true);

            if (ids.length == 0) {
                Toast.makeText(Mapa_Activity.this, "No existe dibujo asociado", Toast.LENGTH_SHORT).show();
                return false;
            }

            new AlertDialog.Builder(Mapa_Activity.this)
                    .setIcon(R.drawable.borrar)
                    .setTitle("Eliminar Dibujo")
                    .setMessage("�Desea eliminar el dibujo seleccionado?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Graphic gp;

                            if (ids.length > 0) {
                                gp = gldib.getGraphic(ids[0]);

                                DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
                                db.open();
                                UUID ID_DIBUJO = UUID.fromString(gp.getAttributeValue("ID_DIBUJO").toString().toLowerCase().replace("{", "").replace("}", ""));

                                db.BorrarDibujo(ID_DIBUJO);
                                gldib.clearSelection();
                                AccionMapa = AccionesMapa.NINGUNA;
                                ActualizarCapas();
                                ;


                            }
                        }

                    })

                    .setNegativeButton("No", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            gldib.clearSelection();
                            AccionMapa = AccionesMapa.NINGUNA;
                        }
                    })
                    .show();

            return true;

        }

        private void MostrarInfoManzana(Manzana mz, boolean IsMzSpatial, Polygon polygon) {

            if (IsMzSpatial) {
                Toast.makeText(Mapa_Activity.this, "La Manzana seleccionada no es parte de su carga laboral.", Toast.LENGTH_SHORT).show();
            }

            if (mz == null) return;

            String msg = "";

            msg = "Comuna:" + mz.getComuna_id() + "\n";
            msg += "Distrito:" + mz.getDistrito() + "\n";
            msg += "Zona:" + mz.getZona() + "\n";
            msg += "Manzana:" + mz.getManzana() + "";

            final Callout callout;
            View calloutView;

            LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
            calloutView = inflater.inflate(R.layout.callout_manzana, null);


            callout = map.getCallout();

            callout.setCoordinates(GeometryEngine.getLabelPointForPolygon(polygon, map.getSpatialReference()));

            TextView tvCOGlosaAreaPadre;
            TextView tvCOPadreInfoGeografica1;
            TextView tvCOPadreInfoGeografica2;
            TextView tvCOPadreInfoGeografica3;
            TextView tvCOPadreInfoGeografica4;


            tvCOGlosaAreaPadre = (TextView) calloutView.findViewById(R.id.tvCOGlosaAreaPadre);
            tvCOPadreInfoGeografica1 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica1);
            tvCOPadreInfoGeografica2 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica2);
            tvCOPadreInfoGeografica3 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica3);
            tvCOPadreInfoGeografica4 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica4);

            tvCOGlosaAreaPadre.setText("URBANO");
            tvCOPadreInfoGeografica1.setText("Comuna:" + mz.getComuna_id());
            tvCOPadreInfoGeografica2.setText("Distrito:" + mz.getDistrito());
            tvCOPadreInfoGeografica3.setText("Zona:" + mz.getZona());
            tvCOPadreInfoGeografica4.setText("Manzana:" + mz.getManzana());

            calloutView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    callout.hide();
                    glUPM_seleccionada.clearSelection();
                }
            });


            callout.setContent(calloutView);
            callout.refresh();
            callout.show();

        }


        private void MostrarInfoSeccionUPM(Seccion sec, Point centroide) {

            if (sec == null) return;

            String msg = "";

            msg = "Comuna:" + sec.getComuna_glosa() + "\n";
            msg += "Distrito:" + sec.getCod_distrito() + "\n";
            msg += "C�digo Secci�n:" + sec.getCodigo_seccion() + "\n";
            msg += "C�digo Carto:" + sec.getCod_carto() + "";

            final Callout callout;
            View calloutView;

            LayoutInflater inflater = Mapa_Activity.this.getLayoutInflater();
            calloutView = inflater.inflate(R.layout.callout_manzana, null);

            callout = map.getCallout();
            callout.setCoordinates(centroide);

            TextView tvCOGlosaAreaPadre;
            TextView tvCOPadreInfoGeografica1;
            TextView tvCOPadreInfoGeografica2;
            TextView tvCOPadreInfoGeografica3;
            TextView tvCOPadreInfoGeografica4;


            tvCOGlosaAreaPadre = (TextView) calloutView.findViewById(R.id.tvCOGlosaAreaPadre);
            tvCOPadreInfoGeografica1 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica1);
            tvCOPadreInfoGeografica2 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica2);
            tvCOPadreInfoGeografica3 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica3);
            tvCOPadreInfoGeografica4 = (TextView) calloutView.findViewById(R.id.tvCOPadreInfoGeografica4);

            if (sec.getTipo_est() == 2) {
                tvCOGlosaAreaPadre.setText("Secci�n RAU:" + sec.getCu_seccion());
            } else {
                tvCOGlosaAreaPadre.setText("Secci�n Rural: " + sec.getCu_seccion() );
            }


            tvCOPadreInfoGeografica1.setText("Comuna:" + sec.getComuna_glosa());
            tvCOPadreInfoGeografica2.setText("Distrito:" + sec.getCod_distrito());
            tvCOPadreInfoGeografica3.setText("C�digo Secci�n:" + sec.getCodigo_seccion());
            tvCOPadreInfoGeografica4.setText("C�digo Carto:" + sec.getCod_carto());

            calloutView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    callout.hide();
                    glUPM_seleccionada.clearSelection();
                }
            });


            callout.setContent(calloutView);
            callout.refresh();
            callout.show();

        }
		/*
		@Override
		public boolean onTouch (View view, MotionEvent motionEvent)
		{
			if(AccionMapa == AccionesMapa.INFO_VIV)
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					ClipData data = ClipData.newPlainText("", "");
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);
					view.startDrag(data, shadowBuilder, view, 0);
					//view.setVisibility(View.INVISIBLE);
				}
				return true;
			} 
			else {
				return false;
			}

		}
		*/

    }


    private void GrabarDibujo(String DIBUJO_TEXT) {

        DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
        db.open();

        Dibujo dib = new Dibujo();
        dib.setACT_ID(globales.actualizacion.getAct_id());
        dib.setID_USUARIO(globales.usuario.getId_usuario());
        dib.setDIB_GLOSA(DescripcionNuevoDibujo);

        dib.setIDPADRE(globales.IDPADRE);

        dib.setID_DIBUJO(UUID.randomUUID());

        dib.setDIB_TEXT(DIBUJO_TEXT);

        dib.setNULO(false);
        dib.setFECHA_CREACION(new Date());
        dib.setDIB_TIPO(TipoViaNuevoDibujo);
        dib.setDIB_ESTADO(EstadoNuevoDibujo);
        dib.setNUEVO(true);

        db.InsertarDibujo(dib);
        db.close();

    }

    public void SetearCentroide() {
        glCentroides.removeAll();
        //SimpleFillSymbol sfs = new SimpleFillSymbol(Color.YELLOW, com.esri.core.symbol.SimpleFillSymbol.STYLE.HORIZONTAL);
        SimpleFillSymbol sfs = new SimpleFillSymbol(Color.TRANSPARENT);
        sfs.setOutline(new SimpleLineSymbol(Color.GREEN, 6, com.esri.core.symbol.SimpleLineSymbol.STYLE.SOLID));
        //graphic = new Graphic(poly, sfs);
        Graphic graph = new Graphic(globales.Polygon_UPM, sfs);

        glCentroides.addGraphic(graph);

        map.setExtent(globales.Polygon_UPM);
        //map.zoomToScale(globales.Polygon_UPM.getPoint(0), 8);

        //ActualizarCapas();
        globales.ActualizarCapas = false;


    }
    /*
    private void openRasterGeoPackage()
    {

            String rastergeoPackagePath =
                    Environment.getExternalStorageDirectory() + "/MapaBase/marco.gpkg";

            Geopackage geoRasterPackage = new Geopackage(rastergeoPackagePath);


            FileRasterSource frd = new FileRasterSource(geoRasterPackage.getPath());

            // Make sure a feature table was found in the package
            if (frd == null)
            {
                //Toast.makeText(Mapa_Activity.this, "No feature table found in the package!", Toast.LENGTH_LONG).show();
                return;
            }
            //frd.buildPyramid(8, true, ResamplingProcessType.RSP_BILINEAR_INTERPOLATION, "JPEG");
            frd.project(SpatialReference.create(4326));
            RasterLayer rl = new RasterLayer(frd);

            rl.setName("Base Satelital");

            map.addLayer(rl);




        }
        catch (Exception e)
        {
            if(e==null)
            {

            }
        }
    }
*/
    private void openGeoPackage()
    {

        // Get the full path to the local GeoPackage
        String geoPackagePath =
                Environment.getExternalStorageDirectory() + "/MapaBase/spatialMMV.gpkg";


        try
        {
            String region = String.valueOf(globales.region.getRegion_id());
            boolean menordiez = region.length()==1;

            String cut = globales.IDPADRE.substring(0, menordiez ? 4 : 5);

            Geopackage geoPackage = new Geopackage(geoPackagePath);


            GeopackageFeatureTable geoTableCOM = geoPackage.getGeopackageFeatureTable("Comuna");
            FeatureLayer featureLayerCOM = new FeatureLayer(geoTableCOM);
            //featureLayerCOM.setDefinitionExpression("REGION = "+ region);
            featureLayerCOM.setDefinitionExpression("CUT = "+ cut);
            featureLayerCOM.setEnableLabels(true);
            SimpleFillSymbol fillSymbolCOM = new SimpleFillSymbol(getResources().getColor(R.color.AntiqueWhite), SimpleFillSymbol.STYLE.SOLID);
            fillSymbolCOM.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Black), 6, SimpleLineSymbol.STYLE.SOLID));
            Renderer rendererCOM = new SimpleRenderer(fillSymbolCOM);
            featureLayerCOM.setRenderer(rendererCOM);

            //ProjectionTransformation.create()
            featureLayerCOM.setEnableLabels(true);

            //featureLayerCOM.set
            map.addLayer(featureLayerCOM);





/*
            GeopackageFeatureTable geoTableAV = geoPackage.getGeopackageFeatureTable("Areas_Verdes");
            FeatureLayer featureLayerAV = new FeatureLayer(geoTableAV);
            //featureLayerAV.setDefinitionExpression("REGION = "+ region);
            featureLayerAV.setEnableLabels(true);
            SimpleFillSymbol fillSymbolAV = new SimpleFillSymbol(getResources().getColor(R.color.Green), SimpleFillSymbol.STYLE.SOLID);
            fillSymbolAV.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Black), 3, SimpleLineSymbol.STYLE.SOLID));
            Renderer rendererAV = new SimpleRenderer(fillSymbolAV);
            featureLayerAV.setRenderer(rendererAV);
            featureLayerAV.setVisible(false);
            featureLayerAV.setName("�reas Verdes");
            map.addLayer(featureLayerAV);


            GeopackageFeatureTable geoTableMZ = geoPackage.getGeopackageFeatureTable("Marco_Manzanas");
            FeatureLayer featureLayerMZ = new FeatureLayer(geoTableMZ);
            featureLayerMZ.setDefinitionExpression("CUT = "+ cut);
            featureLayerMZ.setEnableLabels(true);
            SimpleFillSymbol fillSymbolMZ = new SimpleFillSymbol(getResources().getColor(R.color.Aqua), SimpleFillSymbol.STYLE.SOLID);
            fillSymbolMZ.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Black), 4, SimpleLineSymbol.STYLE.SOLID));
            Renderer rendererMZ = new SimpleRenderer(fillSymbolMZ);
            featureLayerMZ.setRenderer(rendererMZ);
            featureLayerMZ.setVisible(false);
            featureLayerMZ.setName("Marco Manzanas");
            map.addLayer(featureLayerMZ);


            GeopackageFeatureTable geoPackageTable = geoPackage.getGeopackageFeatureTable("Marco_Secciones");
            FeatureLayer featureLayer = new FeatureLayer(geoPackageTable);
            featureLayer.setEnableLabels(true);
            featureLayer.setDefinitionExpression("CU_SECCION = " + globales.IDPADRE);
            SimpleFillSymbol fillSymbol = new SimpleFillSymbol(Color.TRANSPARENT, SimpleFillSymbol.STYLE.SOLID);
            fillSymbol.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Yellow), 6, com.esri.core.symbol.SimpleLineSymbol.STYLE.SOLID));
            Renderer renderer = new SimpleRenderer(fillSymbol);
            featureLayer.setRenderer(renderer);
            featureLayer.setName("Marco Secciones");
            map.addLayer(featureLayer);


            GeopackageFeatureTable geoTableEje = geoPackage.getGeopackageFeatureTable("Eje_Vial");
            FeatureLayer featureLayerEje = new FeatureLayer(geoTableEje);
            featureLayerEje.setDefinitionExpression("CUT = "+ cut);
            featureLayerEje.setEnableLabels(true);
            SimpleLineSymbol LineaEje = new SimpleLineSymbol(getResources().getColor(R.color.Blue), 2);
            Renderer rendererEje = new SimpleRenderer(LineaEje);
            featureLayerEje.setRenderer(rendererEje);
            featureLayerEje.setName("Ejes Viales");
            featureLayerEje.setVisible(false);
            map.addLayer(featureLayerEje);


            GeopackageFeatureTable geoTableDist = geoPackage.getGeopackageFeatureTable("Distrito_Censal");
            FeatureLayer  featureLayerDist = new FeatureLayer(geoTableDist);
            featureLayerDist.setDefinitionExpression("CUT = "+ cut);
            featureLayerDist.setEnableLabels(true);
            SimpleLineSymbol LineaDist = new SimpleLineSymbol(getResources().getColor(R.color.DarkKhaki), 2);
            Renderer rendererDist = new SimpleRenderer(LineaDist);
            featureLayerDist.setRenderer(rendererDist);
            featureLayerDist.setName("Distritos Censales");
            featureLayerDist.setVisible(false);
            map.addLayer(featureLayerDist);


            GeopackageFeatureTable geoTableMisc = geoPackage.getGeopackageFeatureTable("Miscelaneos");
            FeatureLayer featureLayerMisc = new FeatureLayer(geoTableMisc);
            //featureLayerMisc.setDefinitionExpression("CUT = "+ cut);
            featureLayerMisc.setEnableLabels(true);
            SimpleLineSymbol LineaMisc = new SimpleLineSymbol(getResources().getColor(R.color.FireBrick), 2);
            Renderer rendererMisc = new SimpleRenderer(LineaMisc);
            featureLayerMisc.setRenderer(rendererMisc);
            featureLayerMisc.setName("Miscelaneos");
            featureLayerMisc.setVisible(false);
            map.addLayer(featureLayerMisc);

            GeopackageFeatureTable geoTableZona = geoPackage.getGeopackageFeatureTable("Zona_Censal");
            FeatureLayer featureLayerZona = new FeatureLayer(geoTableZona);
            featureLayerZona.setDefinitionExpression("CUT = "+ cut);
            featureLayerZona.setEnableLabels(true);
            SimpleLineSymbol LineaZona = new SimpleLineSymbol(getResources().getColor(R.color.OliveDrab), 2);
            Renderer rendererZona = new SimpleRenderer(LineaZona);
            featureLayerZona.setRenderer(rendererZona);
            featureLayerZona.setName("Zonas Censales");
            featureLayerZona.setVisible(false);
            map.addLayer(featureLayerZona);


            GeopackageFeatureTable geoTableAgua = geoPackage.getGeopackageFeatureTable("Cuerpos_Agua");
            FeatureLayer featureLayerAgua = new FeatureLayer(geoTableAgua);
            //featureLayerZona.setDefinitionExpression("CUT = "+ cut);
            featureLayerAgua.setEnableLabels(true);
            SimpleFillSymbol fillSymbolAgua = new SimpleFillSymbol(getResources().getColor(R.color.Aqua), SimpleFillSymbol.STYLE.SOLID);
            fillSymbolAgua.setOutline(new SimpleLineSymbol(getResources().getColor(R.color.Black), 2, com.esri.core.symbol.SimpleLineSymbol.STYLE.SOLID));
            Renderer rendererAgua = new SimpleRenderer(fillSymbolAgua);
            featureLayerAgua.setName("Cuerpos de Agua");
            featureLayerAgua.setVisible(false);
            map.addLayer(featureLayerAgua);

*/
            // Make sure a feature table was found in the package
            if (geoTableCOM == null)
            {
                //Toast.makeText(Mapa_Activity.this, "No feature table found in the package!", Toast.LENGTH_LONG).show();
                return;
            }

        }
        catch (Exception e)
        {
            if(e==null)
            {

            }
        }
    }

    /*
    public void SetearCentroVivienda()
    {
        glViviendaListado.removeAll();
        PictureMarkerSymbol pms = new PictureMarkerSymbol(Mapa_Activity.this.getResources().getDrawable(R.drawable.vivienda));
        Graphic graph = new Graphic(globales.PuntoViviendaListado, pms);

        glViviendaListado.addGraphic(graph);

        map.zoomToScale(globales.PuntoViviendaListado, 15);
        map.centerAt(globales.PuntoViviendaListado, true);

    }
     */
    private Point CrearPuntoMapa_Punteo(MotionEvent e) {

        return map.toMapPoint(new Point(e.getX(), e.getY()));
    }

    /*
    private Point CrearPuntoMapa_GPS()
    {
        return (Point)GeometryEngine.project(globales.UltimaPuntoObtenido_GPS, SpatialReference.create(4326), SpatialReference.create(102100));
    }
     */
    private void DialogoCreacionVivienda(Point pt) {
        glVivTEMP.removeAll();
        boolean coincidepuntopadre = false;
        //final Point PtProyectado = (Point)GeometryEngine.project(pt, SpatialReference.create(102100), SpatialReference.create(4674));
        final Point PtProyectado = pt;
        Graphic graphic = new Graphic(pt, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.star_32)));

        coincidepuntopadre = CoincidePadreConPunto(pt);

        glVivTEMP.addGraphic(graphic);

        glCirculoRadio.removeAll();

        Unit medida;
        if(map.getSpatialReference().isAnyWebMercator())
        {
            medida = Unit.create(LinearUnit.Code.METER);
        }
        else
        {
            //globales.strMetrosBusquedaCalles =  (Double.valueOf(globales.strMetrosBusquedaCalles) ;//"0.1349661235030007";
            medida = Unit.create(AngularUnit.Code.DEGREE);
        }

        glCirculoRadio.addGraphic(
                new Graphic(

                        GeometryEngine.buffer(PtProyectado, map.getSpatialReference(), Double.valueOf(globales.strMetrosBusquedaCalles), medida)//Unit.create(AngularUnit.Code.DEGREE))
                        ,new SimpleLineSymbol(Color.GREEN, 5)
                )
        );

        new AlertDialog.Builder(Mapa_Activity.this)
                .setIcon(coincidepuntopadre ? R.drawable.agregarvivienda : R.drawable.warning_vivienda)
                .setTitle("Agregar Nuevo Registro")
                .setMessage((coincidepuntopadre ? "�Desea agregar nuevo registro en el punto marcado?" : "ADVERTENCIA: ���EL PUNTO MARCADO NO SE ENCUENTRA EN LA UPM SELECCIONADA!!!\n\n�Desea agregar nueva vivienda en el punto marcado de todos modos?"))
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i;
                        if (globales.padres == PADRES.MANZANAS) {
                            i = new Intent(Mapa_Activity.this, NuevaVivienda_Activity.class);
                        } else {
                            i = new Intent(Mapa_Activity.this, NuevaVivienda_Rural_Activity.class);
                        }

                        i.putExtra("lat", PtProyectado.getY());
                        i.putExtra("lng", PtProyectado.getX());
                        startActivityForResult(i, AGREGARVIVIENDA);
                    }

                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        glVivTEMP.removeAll();
                        glCirculoRadio.removeAll();
                    }
                })
                .show();


    }


    private class AsyncCargarRegistrosPosicion extends AsyncTask<String, Integer, Integer>

    {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
			/*
			pd = new ProgressDialog(Mapa_Activity.this);
			pd.setTitle("Cargando Registro de Posici?n...");
			pd.setMessage("Accediendo a Datos... Por favor espere.");       
			pd.setIndeterminate(true);
			pd.show();
			 */
            pbCargaMapa.setVisibility(View.VISIBLE);
        }


        @Override
        protected Integer doInBackground(String... params) {
            // TODO Auto-generated method stub
            CargarRegistrosPosicion(params[0]);

            return 0;
        }

        private void CargarRegistrosPosicion(String dia)
        {


            //if(glRegistroPosicion.getNumberOfGraphics()>0)
            //{
            glRegistroPosicion.removeAll();
            //Toast.makeText(Mapa_Activity.this, "Registro de Posici?n desactivado", Toast.LENGTH_SHORT).show();
            //}
            //else
            //{
            List<RegistroPosicion> registros;
            Map<String, Object> atributos;
            TextSymbol txt;
            int i = 0;
            int porc = 0;
            int total = 0;
            DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
            db.open();
            try {
                registros = db.ObtenerRegistrosPosicionPorDia(dia);
                total = registros.size();

                //tvFechaRegistroPosicion

                for (RegistroPosicion reg : registros) {
                    txt = new TextSymbol(10, String.valueOf(i + 1), Color.BLACK);
                    atributos = new HashMap<String, Object>();
                    atributos.put("FECHA", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(reg.getFecha()));

                    //Point Pt = (Point)GeometryEngine.project(new Point(reg.getCoord_x(), reg.getCoord_y()), SpatialReference.create(4674), SpatialReference.create(102100));
                    Point Pt = new Point(reg.getCoord_x(), reg.getCoord_y());

                    if (i == 0) {
                        map.zoomToScale(Pt, 16d);
                    }

                    Graphic graphic = new Graphic(Pt, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.registro_posicion)), atributos, 0);

                    glRegistroPosicion.addGraphic(graphic);

                    Pt.setX(Pt.getX() - 0.001F);

                    Graphic gr = new Graphic(Pt, txt, atributos, 0); // agrego el n?mero
                    glRegistroPosicion.addGraphic(gr);


                    i++;
                    porc = i * 100 / total;
                    publishProgress(porc);

                }

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            db.close();

            //}
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //Main.getApp().progressBar.setProgress(values[0]);
            //Main.getApp().txt_percentage.setText("downloading" + values[0] + "%");
            pbCargaMapa.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Integer Result) {
            //pd.dismiss();
            pbCargaMapa.setVisibility(View.INVISIBLE);

            //AccionMapa = AccionesMapa.NINGUNA;
            Toast.makeText(Mapa_Activity.this, "Registro de Posici�n cargados", Toast.LENGTH_LONG).show();

        }
    }


    //public void AgregarPuntoViviendaGPS(UUID ID_VIVIENDA, Context ctxt, Point pt)

    public void CrearCapasOperativas() {

        glviv.setSelectionColor(Color.CYAN);
        gldib.setSelectionColor(Color.CYAN);

        glRegistroPosicion.setSelectionColor(Color.CYAN);
        glCentroides.setSelectionColor(Color.CYAN);
        glUPM_seleccionada.setSelectionColor(Color.CYAN);
        glEdificios.setSelectionColor(Color.CYAN);

        //chile.mbtiles

        //openGeoPackage();

        //mbLayer = new MBTilesLayer(Environment.getExternalStorageDirectory().getPath() + File.separator + "MapaBase" + File.separator + "base.mbtiles");
        mbLayer = new MBTilesLayer(globales.Ruta_MapaBase);
        mbLayer.setName("BASE");
        map.addLayer(mbLayer);

/*
        ArcGISTiledMapServiceLayer tileLayer = new ArcGISTiledMapServiceLayer(
                "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer");
        // Add tiled layer to MapView
        map.addLayer(tileLayer);
        */

        ArcGISLocalTiledLayer AGltl = new ArcGISLocalTiledLayer(globales.Ruta_TPK + globales.IDPADRE + ".tpk", true);
        map.addLayer(AGltl);
        //openRasterGeoPackage();

        glRegistroPosicion.setName("Registros Posici�n");
        map.addLayer(glRegistroPosicion);

        glCentroides.setName("U.P.M. Actual");
        map.addLayer(glCentroides);

        glMedirDistancia.setName("DISTANCIA");
        map.addLayer(glMedirDistancia);

        glUPM_seleccionada.setName("UPMSELECCIONADA");
        map.addLayer(glUPM_seleccionada);


        glviv.setName("Viviendas y Establecimientos");
        map.addLayer(glviv);

        gldib.setName("Dibujos");
        map.addLayer(gldib);

        glVivTEMP.setName("VIVIENDASTEMP");
        map.addLayer(glVivTEMP);

        glDibTEMP.setName("DIBUJOSTEMP");
        map.addLayer(glDibTEMP);

        glEditLayer.setName("EDITLAYER");
        map.addLayer(glEditLayer);

        glEdificios.setName("Edificios");
        map.addLayer(glEdificios);

        glNombresEdificios.setName("Nombres de Edificios");
        map.addLayer(glNombresEdificios);

        glDescripciones.setName("Descripciones");
        map.addLayer(glDescripciones);

        glAreasLevantamiento.setName("�reas de Levantamiento");
        map.addLayer(glAreasLevantamiento);

        glLocalidades.setName("Localidades");
        map.addLayer(glLocalidades);

        glOrdenesRegistro.setName("N�meros de Orden");
        map.addLayer(glOrdenesRegistro);

        glNombreLocalidades.setName("Nombres de Localidad");
        map.addLayer(glNombreLocalidades);

        glOK.setName("Reg. Verificados");
        map.addLayer(glOK);

        glCirculoRadio.setName("glCirculoRadio");
        map.addLayer(glCirculoRadio);

        glPOI.setName("Puntos de Inter�s");
        map.addLayer(glPOI);

        glAreaTrabajo.setName("�reas de Trabajo");
        map.addLayer(glAreaTrabajo);

        glPseudoManzana.setName("Pseudo Manzanas");
        map.addLayer(glPseudoManzana);
    }

    public void ActualizarCapas() {
        map.invalidate();
        map.postInvalidate();


        for (Layer capa : map.getLayers()) {

            if (capa instanceof CapaGrafica) {
                ((GraphicsLayer) capa).removeAll();
            }

            if (capa instanceof ArcGISFeatureLayer) {
                ((ArcGISFeatureLayer) capa).removeAll();
            }


        }

        new AsyncCargarMapasLocales().execute();
    }


    public void showPopup(View v) {
        String MENU_TAG = v.getTag().toString();
        int menuactual = 0;

        PopupMenu popup = new PopupMenu(this, v);
        funciones.setForceShowIcon(popup);
        MenuInflater inflater = popup.getMenuInflater();

        if (MENU_TAG.equals("ID")) {
            menuactual = R.menu.menu_mapa_info;

            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.mnuinfo_upm) {
                        AccionMapa = AccionesMapa.INFO_UPM;
                    } else if (item.getItemId() == R.id.mnuinfo_vivienda) {
                        AccionMapa = AccionesMapa.INFO_VIV;
                    } else if (item.getItemId() == R.id.mnuinfo_dibujo) {
                        AccionMapa = AccionesMapa.INFO_DIB;
                    } else if (item.getItemId() == R.id.mnuinfo_RP) {
                        AccionMapa = AccionesMapa.INFO_RP;}
                    else if (item.getItemId()== R.id.mnuinfo_poi){
                        AccionMapa = AccionesMapa.INFO_POI;
                    } else {
                        AccionMapa = AccionesMapa.NINGUNA;
                    }
                    Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        } else if (MENU_TAG.equals("EDIT")) {
            menuactual = R.menu.menu_mapa_editar_geometrias;

            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.mnuedit_moverpunto) {
                        AccionMapa = AccionesMapa.PREVIO_MOVER_PUNTO;
                        Toast.makeText(Mapa_Activity.this, "Seleccione con el dedo el punto que desea mover y arr�strelo a su nueva posici�n", Toast.LENGTH_LONG).show();
                        //AccionMapa= AccionesMapa.NINGUNA;
                        //Toast.makeText(Mapa_Activity.this, "Opci�n no disponible en esta oportunidad", Toast.LENGTH_LONG).show();
                    } else if (item.getItemId() == R.id.mnuedit_borrardibujo) {
                        AccionMapa = AccionesMapa.BORRAR_DIBUJO;
                        Toast.makeText(Mapa_Activity.this, "Seleccione con el dedo el dibujo a eliminar", Toast.LENGTH_LONG).show();
                    } else if (item.getItemId() == R.id.mnuedit_iluminarrecorrido) {

                        AccionMapa = AccionesMapa.ILUMINAR_RECORRIDO_PUNTOS;
                        new AsyncCargarMapasLocales().execute();
                    } else {
                        Toast.makeText(Mapa_Activity.this, "Edici�n de geometr�as cancelada", Toast.LENGTH_LONG).show();
                        AccionMapa = AccionesMapa.NINGUNA;
                    }
                    //Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        } else if (MENU_TAG.equals("ADD")) {
            menuactual = R.menu.menu_agregar_elemento_mapa;

            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.mnuadd_vivienda) {

                        AccionMapa = AccionesMapa.AGREGARVIVIENDA_PUNTEO;
                        AgregarViviendaPunteo();
                        //Toast.makeText(Mapa_Activity.this, "Seleccione con el dedo el punto que desea mover y arr�strelo a su nueva posici�n", Toast.LENGTH_LONG).show();
                    } else if (item.getItemId() == R.id.mnuadd_viviendaGPS) {
                        AccionMapa = AccionesMapa.AGREGARVIVIENDA_GPS;
                        AgregarViviendaGPS();
                    }else if (item.getItemId() == R.id.mnuadd_poi) {
                        AccionMapa = AccionesMapa.AGREGAR_POI;
                        Toast.makeText(Mapa_Activity.this, "Presione con el dedo el punto donde agregar un Punto de Inter�s", Toast.LENGTH_LONG).show();
                    }
                    else

                    {
                        //Toast.makeText(Mapa_Activity.this, "Edici�n de geometr�as cancelada", Toast.LENGTH_LONG).show();
                        AccionMapa = AccionesMapa.NINGUNA;
                    }
                    //Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }
        inflater.inflate(menuactual, popup.getMenu());


        popup.show();
    }
private void AgregarPOI(Point pt)
{

    //PtProyectado = (Point)GeometryEngine.project(pt, SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));

    Graphic graphic = new Graphic(pt, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.info_icon)));
    glVivTEMP.addGraphic(graphic);

    Intent i = new Intent(Mapa_Activity.this, AgregarPOI_Activity.class);
    i.putExtra("POI", pt);
    startActivityForResult(i, AGREGARPOI);


}
    private boolean CoincidePadreConPunto(Point puntoviv) {
        DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
        db.open();
        long geoc;
        long cu_seccion;
        boolean ret = false;
        try {
            geoc = new DataBaseSpatialite().ObtenerGeocodigoManzana(puntoviv.getY(), puntoviv.getX());
            Manzana mz = db.ObtenerManzana(null, geoc);

            if (mz != null) {
                if (String.valueOf(mz.getIdmanzana()).equals(globales.IDPADRE)) {
                    ret = true;
                } else {
                    ret = false;
                }
            } else // SECCION
            {
                cu_seccion = new DataBaseSpatialite().ObtenerCU_Seccion(puntoviv.getY(), puntoviv.getX());
                if (String.valueOf(cu_seccion).equals(globales.IDPADRE)) {
                    ret = true;
                } else {
                    ret = false;
                }
            }


        } catch (jsqlite.Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        db.close();
        return ret;

    }

    private boolean CoincideManzanaNuevoPunto(Point puntoviv) {
        DataBaseUtil db = new DataBaseUtil(Mapa_Activity.this);
        db.open();
        long geoc;
        boolean ret = false;
        try {
            geoc = new DataBaseSpatialite().ObtenerGeocodigoManzana(puntoviv.getY(), puntoviv.getX()); //MANZANA DEL PUNTO DADO
            Manzana mz = db.ObtenerManzana(null, geoc);

            if (mz != null) {
                if (mz.getGeocodigo() == geoc) //COMPARO SI LA MANZANA DEL PUNTO COINCIDE CON MANZANA DEL REGISTRO DE VIVIENDA
                {
                    ret = true;
                } else {
                    ret = false;
                }
            } else // secciones
            {
                long cu_sec = new DataBaseSpatialite().ObtenerCU_Seccion(puntoviv.getY(), puntoviv.getX()); //SECCION DEL PUNTO DADO

                ret = cu_sec > 0;

            }


        } catch (jsqlite.Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        db.close();
        return ret;

    }

	/*
	private void CrearViewParaArrastre(float x, float y)
	{
		ImageView imgDragViv = new ImageView(Mapa_Activity.this);
		imgDragViv.setImageDrawable(Mapa_Activity.this.getResources().getDrawable(R.drawable.agregarvivienda32));
		imgDragViv.setOnTouchListener(new MyDragTouchListener());
		imgDragViv.setX(x);
		imgDragViv.setY(y);
		
		RelativeLayout root = (RelativeLayout) findViewById(R.id.mapa_activity); 
		root.addView(imgDragViv);

	}
	
	
	private final class MyDragTouchListener implements OnTouchListener 
	{
		public boolean onTouch(View view, MotionEvent motionEvent) 
		{
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
			{
				ClipData data = ClipData.newPlainText("", "");
				DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
						view);
				view.startDrag(data, shadowBuilder, view, 0);
				
				view.setVisibility(View.INVISIBLE);
				//return true;
			} 
			else if (motionEvent.getAction() == MotionEvent.ACTION_UP) 
			{
				view.setVisibility(View.VISIBLE);
				//return false;
			}
			else
			{
				
			}
			return false;
		}
		
		public boolean onDrag(View view, DragEvent event)
		{
			if(event.getAction()==DragEvent.ACTION_DRAG_STARTED)
			{
				view.setVisibility(View.VISIBLE);
			}
			return false;
			
		}
		
	}
	*/

    private void SacarFoto(Vivienda viv) {


        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ComponentName act = takePictureIntent.resolveActivity(getPackageManager());

        if (act != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

                Toast.makeText(Mapa_Activity.this, "Error", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                globales.ID_VIVIENDA_LISTADO = viv.getID_VIVIENDA();
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {

        File storageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/EnumVivPics/");

        storageDir.mkdirs();
        File image = new File(storageDir + "/temp.jpg");
        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }


    private void ConfigurarActivity() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Mapa_Activity.this);

        globales.boolOcultarNulos = preferences.getBoolean("prefOcultarNulos", false);
        globales.boolMostrarDescripciones = preferences.getBoolean("prefMostrarDescripciones", false);
        globales.boolMostrarNombreEdificio = preferences.getBoolean("prefMostrarNombreEdificio", true);
        globales.strMetrosBusquedaCalles = preferences.getString("prefMetrosBusquedaCalles", "0.005");
        globales.strMostrarOrdenes = preferences.getString("prefMostrarOrdenes", "1");
        globales.strDistanciaRegistroPosicion = preferences.getString("prefDistanciaRegistroPosicion", "10");
        globales.boolCentrarPosicion = preferences.getBoolean("prefCentrarPosicion", true);
        globales.boolHabilitarRegistroPosicion = preferences.getBoolean("prefHabilitarRegistroPosicion", true);


        //Toast.makeText(Listado_Activity.this, String.valueOf(preferences.getBoolean("prefOcultarNulos", false)) , Toast.LENGTH_SHORT).show() ;

    }

    }


