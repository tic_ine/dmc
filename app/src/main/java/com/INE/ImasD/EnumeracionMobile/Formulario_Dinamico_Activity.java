    package com.INE.ImasD.EnumeracionMobile;

    import android.app.Activity;
    import android.app.AlertDialog;
    import android.app.ProgressDialog;
    import android.content.DialogInterface;
    import android.content.Intent;
    import android.graphics.Color;
    import android.os.Bundle;
    import android.os.Environment;
    import android.os.Handler;
    import android.support.v4.widget.DrawerLayout;
    import android.text.InputFilter;
    import android.text.InputType;
    import android.text.method.ScrollingMovementMethod;
    import android.util.TypedValue;
    import android.view.Gravity;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.WindowManager;
    import android.widget.AdapterView;
    import android.widget.AutoCompleteTextView;
    import android.widget.CheckBox;
    import android.widget.CheckedTextView;
    import android.widget.EditText;
    import android.widget.ExpandableListView;
    import android.widget.ImageButton;
    import android.widget.ImageView;
    import android.widget.LinearLayout;
    import android.widget.ListView;
    import android.widget.ProgressBar;
    import android.widget.ScrollView;
    import android.widget.Spinner;
    import android.widget.TextView;
    import android.widget.Toast;

    import com.INE.ImasD.EnumeracionMobile.Adaptadores.OpcionPregunta_Adapter;
    import com.INE.ImasD.EnumeracionMobile.Adaptadores.OpcionesMapa_Adapter;
    import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_CodigosAnotacion_Adapter;
    import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
    import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
    import com.INE.ImasD.EnumeracionMobile.Entities.Encuesta;
    import com.INE.ImasD.EnumeracionMobile.Entities.OpcionMapa;
    import com.INE.ImasD.EnumeracionMobile.Entities.Opcion_Pregunta;
    import com.INE.ImasD.EnumeracionMobile.Entities.Pregunta;
    import com.INE.ImasD.EnumeracionMobile.Entities.Seccion_Encuesta;
    import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
    import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
    import com.atv.MyHolder;
    import com.atv.model.TreeNode;
    import com.atv.view.AndroidTreeView;
    import com.esri.core.geometry.Line;

    import org.apache.http.HttpEntity;
    import org.apache.http.HttpResponse;
    import org.apache.http.client.methods.HttpPost;
    import org.apache.http.conn.scheme.LayeredSocketFactory;
    import org.apache.http.impl.client.DefaultHttpClient;
    import org.apache.http.params.BasicHttpParams;
    import org.json.JSONArray;
    import org.json.JSONObject;

    import java.io.BufferedReader;
    import java.io.File;
    import java.io.FileReader;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.util.ArrayList;
    import java.util.List;
    import java.util.UUID;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;

    public class Formulario_Dinamico_Activity extends Activity {

        Encuesta encuesta;
        Pregunta pregunta_actual;

        int OrdenSeccion = 0;
        int OrdenPregunta = 0;

        int TotalPreguntas = 0;

        ImageButton btnFD_Anterior;
        ImageButton btnFD_Siguiente;

        TextView tvSeccion;
        TextView tvPregunta;
        ImageButton imgbtnFD_Ayuda;
        TextView tvFD_Navegacion;

        LinearLayout linearlayout_control;
        ImageButton imgbtnFD_Grabar;

        LinearLayout linearLayout_otro;
        EditText FD_otro_etvOtro;

        ImageButton imgbtnFD_MostrarArbol;
        List<Opcion_Pregunta> preguntas_selec;

        ImageView imgRespondida;

        String tipo;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.formulario_dinamico_activity);

            getActionBar().setDisplayShowHomeEnabled(true);

            preguntas_selec = new ArrayList<Opcion_Pregunta>();


            tvSeccion = (TextView) findViewById(R.id.tvFD_GlosaSeccion);
            tvPregunta = (TextView) findViewById(R.id.tvFD_GlosaPregunta);


            tvFD_Navegacion= (TextView) findViewById(R.id.tvFD_Navegacion);

            btnFD_Anterior = (ImageButton)findViewById(R.id.btnFD_Anterior);

            btnFD_Siguiente = (ImageButton)findViewById(R.id.btnFD_Siguiente);

            imgbtnFD_Grabar= (ImageButton)findViewById(R.id.imgbtnFD_Grabar);

            imgbtnFD_Ayuda= (ImageButton) findViewById(R.id.imgbtnFD_Ayuda);

            imgbtnFD_MostrarArbol= (ImageButton) findViewById(R.id.imgbtnFD_MostrarArbol);

            linearlayout_control =  (LinearLayout) findViewById(R.id.linearlayout_control);

            imgRespondida = (ImageView) findViewById(R.id.imgRespondida);

            encuesta = (Encuesta) getIntent().getSerializableExtra( "encuesta");

            TotalPreguntas = getIntent().getExtras().getInt( "TotalPreguntas");


            btnFD_Anterior.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GenerarProgress();
                    OrdenPregunta--;
                    GenerarPregunta();
                }
            });

            btnFD_Siguiente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GenerarProgress();
                    OrdenPregunta++;
                    GenerarPregunta();

                }
            });

            btnFD_Siguiente.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    MostrarArbol();
                    return true;
                }
            });

            imgbtnFD_MostrarArbol.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MostrarArbol();
                }
            });
            imgbtnFD_Grabar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isIngresoValido())
                    {
                        GrabarRespuesta();
                    }
                }
            });

            GenerarPregunta();
        }


        private void GenerarProgress()
        {
            /*
            final ProgressDialog dialog = ProgressDialog.show(this, "Formulario " + encuesta.getNombreencuesta(), "Cargando siguiente pregunta...",
                    true);
            dialog.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    dialog.dismiss();
                }
            }, 250); // 3000 milliseconds delay
*/
        }

        private boolean isIngresoValido()
        {
            boolean retorno = false;

            pregunta_actual = encuesta.getSecciones().get(OrdenSeccion).getPreguntas().get(OrdenPregunta);

            if(pregunta_actual.isRequerido())
            {
                if(pregunta_actual.getTipo().equals("EditText"))
                {
                    EditText control = (EditText) linearlayout_control.getChildAt(0);

                    if(control.getText().toString().equals(""))
                    {
                        Toast.makeText(this, "Debe ingresar un valor v�lido", Toast.LENGTH_SHORT).show();
                        retorno = false;
                    }
                    else
                    {
                        if(pregunta_actual.getEstilo().equals("n�meros"))
                        { // SI ES NUMERO

                            Double retVal;

                            try
                            {
                                retVal = Double.parseDouble(control.getText().toString());

                                if (control.getText().toString().length() > pregunta_actual.getLargomaximo())
                                {
                                    Toast.makeText(this, "Valor ingresado no puede superar los " + pregunta_actual.getLargomaximo() + " caracteres.", Toast.LENGTH_SHORT).show();
                                    retorno = false;
                                }
                                else if (retVal < pregunta_actual.getValorminimo() || retVal > pregunta_actual.getValormaximo()) {
                                    Toast.makeText(this, "Valor ingresado debe estar entre el " + pregunta_actual.getValorminimo() + " y " + pregunta_actual.getValormaximo(), Toast.LENGTH_SHORT).show();
                                    retorno = false;
                                }
                                else if(!pregunta_actual.getRegex().equals(""))
                                {
                                    Pattern pattern = Pattern.compile(pregunta_actual.getRegex());
                                    if(pattern.matcher(control.getText().toString()).matches())
                                    {
                                        retorno = true;
                                    }
                                    else
                                    {
                                        Toast.makeText(this, "Valor ingresado no coincide con la expresi�n de validaci�n: " + pregunta_actual.getRegex(), Toast.LENGTH_SHORT).show();
                                        retorno = false;
                                    }

                                }
                            }
                            catch (NumberFormatException nfe)
                            {
                                retVal = -1d; // or null if that is your preference
                                Toast.makeText(this, "Valor ingresado no es v�lido", Toast.LENGTH_SHORT).show();
                                retorno = false;

                            }

                        }
                        else
                        {
                            retorno = true;
                        }
                    }
                }
                else if(pregunta_actual.getTipo().equals("DropDown"))
                {

                    Spinner control = (Spinner) linearlayout_control.getChildAt(0);

                    Opcion_Pregunta preg = (Opcion_Pregunta) control.getSelectedItem();

                    if(preg.getCodigo_opcion().equals(""))
                    {
                        Toast.makeText(this, "Debe ingresar un valor v�lido", Toast.LENGTH_SHORT).show();
                        retorno = false;
                    }
                    else
                    {
                        retorno = true;
                    }

                }
                else if(pregunta_actual.getTipo().equals("CheckList"))
                {

                    CheckBox chk;
                    for (int i = 0;i<=linearlayout_control.getChildCount()-1;++i)
                    {
                        if( linearlayout_control.getChildAt(i) instanceof  CheckBox)
                        {
                            chk = (CheckBox) linearlayout_control.getChildAt(i);
                            if (chk.isChecked()) {
                                retorno = true;
                                break;
                            }
                        }
                    }

                    if(!retorno)
                    {
                        Toast.makeText(this, "Debe chequear al menos un valor de la lista", Toast.LENGTH_SHORT).show();
                        retorno = false;
                    }

                }
            }
            else
            {
                retorno = true;
            }

            return retorno;
        }
        private void GenerarNavegacion()
        {
            StringBuilder sb = new StringBuilder();

            sb.append("Encuesta:" + encuesta.getNombreencuesta() + System.getProperty("line.separator"));
            sb.append("Tipo Encuesta:" + encuesta.getTipoencuesta() + System.getProperty("line.separator"));
            sb.append("Seccion:" + String.valueOf(OrdenSeccion + 1) + " de " + encuesta.getSecciones().size() + System.getProperty("line.separator"));
            sb.append("Pregunta:" + String.valueOf(OrdenPregunta + 1) + " de " + encuesta.getSecciones().get(OrdenSeccion).getPreguntas().size() + System.getProperty("line.separator"));

            if(pregunta_actual.getEstilo().equals("n�meros"))
            {
                sb.append("Valor esperado entre: " + pregunta_actual.getValorminimo() + " y " + pregunta_actual.getValormaximo());
            }

            tvFD_Navegacion.setText(sb.toString());
        }


        private String ObtenerJSON()
        {
            DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httppost            = new HttpPost(globales.JSONtoken);

            httppost.setHeader("Content-type", "application/json");

            InputStream inputStream = null;
            String result = null;
            try {
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();

                inputStream = entity.getContent();
                // json is UTF-8 by default
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();

                return  result;
            }
            catch (Exception e)
            {

                return  "";
                // Oops
            }
            finally
            {
                try
                {
                    if(inputStream != null)
                        inputStream.close();
                }
                catch(Exception ex)
                {

                }
            }

        }

        private void GenerarPregunta()
        {

            preguntas_selec.clear();

            Pregunta preg;

            if (OrdenPregunta==0 && OrdenSeccion==0)
            {
                // si es la primera pregunta de la primera secci�n
                btnFD_Anterior.setVisibility(View.INVISIBLE);
                btnFD_Siguiente.setVisibility(View.VISIBLE);
            }
            else if((OrdenPregunta == encuesta.getSecciones().get(OrdenSeccion).getPreguntas().size()-1 && (OrdenSeccion == encuesta.getSecciones().size()-1)))
                // si es la �ltima pregunta de la �ltima secci�n
            {
                btnFD_Anterior.setVisibility(View.VISIBLE);
                btnFD_Siguiente.setVisibility(View.INVISIBLE);

            }
            else if((OrdenPregunta > encuesta.getSecciones().get(OrdenSeccion).getPreguntas().size()-1 && (OrdenSeccion < encuesta.getSecciones().size()-1)))
                // si es la �ltima pregunta de la secci�n
            {
                // si superamos la �ltima pregunta y a�n quedan secciones
                OrdenSeccion++;
                OrdenPregunta = 0;
                GenerarPregunta();

            }
            else if((OrdenPregunta ==-1 && (OrdenSeccion <= encuesta.getSecciones().size()-1)))
            // si presiona atr�s y quedan secciones anteriores
            {

                OrdenPregunta = encuesta.getSecciones().get(OrdenSeccion-1).getPreguntas().size()-1;
                OrdenSeccion--;
                GenerarPregunta();

            }
            else
            {
                btnFD_Anterior.setVisibility(View.VISIBLE);
                btnFD_Siguiente.setVisibility(View.VISIBLE);
            }

            linearlayout_control.removeAllViews();

            tipo = encuesta.getTipoencuesta();

            pregunta_actual = encuesta.getSecciones().get(OrdenSeccion).getPreguntas().get(OrdenPregunta);

            tvSeccion.setText(encuesta.getSecciones().get(OrdenSeccion).getNombreseccion());
            tvPregunta.setText(pregunta_actual.getDescripcion().replace("\n", System.getProperty("line.separator")));
            imgRespondida.setImageResource( pregunta_actual.isRespondida() ?  R.drawable.confirmado : R.drawable.dibujar );

            imgbtnFD_Ayuda.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        funciones.MostrarMensaje(Formulario_Dinamico_Activity.this,
                                R.drawable.pregunta,
                                 pregunta_actual.getAyuda(),
                                "Ayuda de Pregunta",
                                "Cerrar Ayuda",
                                "",
                                null,
                                null);

                }
            });

            if(pregunta_actual.getTipo().equals("EditText"))
            {
                OpcionPregunta_Adapter adapt = new OpcionPregunta_Adapter(this, 0, pregunta_actual.getOpciones());

                final AutoCompleteTextView control = new AutoCompleteTextView(this);
                control.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                control.setThreshold(1);

                control.setMovementMethod(new ScrollingMovementMethod());
                control.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_rounded_corner));
                control.setAdapter(adapt);
                control.setTextColor(Color.BLACK);
                control.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);

                if(pregunta_actual.getEstilo().equals("n�meros"))
                {
                   control.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                }
                else if(pregunta_actual.getEstilo().equals("texto"))
                {
                    int rows = 1 +(pregunta_actual.getLargomaximo() / 300);
                    if(rows>1)
                    {
                        control.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                    }
                    control.setGravity(Gravity.LEFT | Gravity.TOP);
                    control.setMinLines(rows);
                    control.setInputType(InputType.TYPE_CLASS_TEXT);
                }

                control.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(pregunta_actual.getLargomaximo()) });
                control.setHint(pregunta_actual.getHintTexto());

                linearlayout_control.addView(control);

                control.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        control.showDropDown();
                    }
                });


                control.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Opcion_Pregunta preg = (Opcion_Pregunta) control.getAdapter().getItem(position);

                        control.setText(preg.getGlosa_opcion());
                    }
                });

                if(pregunta_actual.getValores().size()>0)
                {
                    control.setText(pregunta_actual.getValores().get(0));
                }

            }


            if(pregunta_actual.getTipo().equals("DropDown"))
            {

                OpcionPregunta_Adapter adapt = new OpcionPregunta_Adapter(this, 0, pregunta_actual.getOpciones());

                final Spinner control = new Spinner(this);
                control.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                control.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_corner_spinner));
                control.setAdapter(adapt);


                linearlayout_control.addView(control);

                String id = pregunta_actual.getValores().get(0);

                if(!id.equals(""))
                {
                    int sel = adapt.getPositionById(id);

                    control.setSelection(sel);
                }

            }

            if(pregunta_actual.getTipo().equals("CheckList"))
            {
                CheckBox control;
                linearlayout_control.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_corner_spinner));

                List<Opcion_Pregunta> opcs = pregunta_actual.getOpciones();

                for (Opcion_Pregunta opc : opcs)
                {
                    control  = new CheckBox(this);
                    control.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    control.setGravity(Gravity.CENTER_HORIZONTAL);
                    control.setText(opc.getGlosa_opcion());

                    control.setTextColor(Color.BLACK);
                    control.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
                    linearlayout_control.addView(control);
                }

                CheckBox chk;
                for (int i = 0;i<=pregunta_actual.getValores().size()-1;++i)
                {
                    chk = (CheckBox) linearlayout_control.getChildAt(i);
                    chk.setChecked(pregunta_actual.getValores().get(i).equals("1"));
                }
            }

            if(pregunta_actual.isOpcionOtro())
            {
                linearLayout_otro = (LinearLayout) getLayoutInflater().inflate(R.layout.fd_pregunta_otro, null);
                FD_otro_etvOtro = (EditText) linearLayout_otro.findViewById(R.id.FD_otro_etvOtro);
                FD_otro_etvOtro.setText(pregunta_actual.getOtro());
                linearlayout_control.addView(linearLayout_otro);
            }
//            }
            GenerarNavegacion();

            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        }

        private void GrabarRespuesta()
        {

            List<String> valores = new ArrayList<String>();
            if (pregunta_actual.getTipo().equals("EditText"))
            {
                EditText control = (EditText) linearlayout_control.getChildAt(0);

                valores.add(control.getText().toString());
            }
            else if (pregunta_actual.getTipo().equals("DropDown"))
            {
                Spinner control = (Spinner) linearlayout_control.getChildAt(0);
                Opcion_Pregunta opc = (Opcion_Pregunta) control.getSelectedItem();

                valores.add(opc.getCodigo_opcion());

            }
            else if (pregunta_actual.getTipo().equals("CheckList"))
            {
                CheckBox chk;
                for (int i = 0; i <= linearlayout_control.getChildCount() - 1; ++i)
                {
                    if(linearlayout_control.getChildAt(i) instanceof CheckBox)
                    {
                        chk = (CheckBox) linearlayout_control.getChildAt(i);
                        valores.add(chk.isChecked() ? "1" : "0");
                    }
                }
            }

            pregunta_actual.setValores(valores);
            pregunta_actual.setRespondida(true);


            if(pregunta_actual.isOpcionOtro())
            {
                pregunta_actual.setTextoOtro(FD_otro_etvOtro.getText().toString());
            }

            //encuesta.setJson(funciones.EncuestaToJSON(encuesta));

            /*
            DataBaseUtil db = new DataBaseUtil(Formulario_Dinamico_Activity.this);
            db.open();
            db.ModificarEncuesta(encuesta);
            db.close();
            */

            if(encuesta.getTotalPreguntasRespondidasEncuesta() == encuesta.getTotalPreguntasEncuesta()) // si es la �ltima en responder
            {
                funciones.MostrarMensaje(Formulario_Dinamico_Activity.this,
                        R.drawable.confirmado,
                        "��Felicidades!!, ha completado el formulario " + encuesta.getNombreencuesta(),
                        "Formulario completo",
                        "Cerrar Formulario",
                        "Continuar en el formulario",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent resultIntent = getIntent();

                                resultIntent.putExtra("encuesta", encuesta);

                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }
                        }, null
                );
            }
            else
            {
                GenerarProgress();
                OrdenPregunta++;
                GenerarPregunta();
            }
        }

        public void MostrarArbol()
        {

            final AlertDialog alertDialog = new AlertDialog.Builder(Formulario_Dinamico_Activity.this).create();

            TreeNode root = TreeNode.root();

            TreeNode nodosec;
            TreeNode nodopreg;
            int ordensec = 0;
            int ordenpreg = 0;

            for (Seccion_Encuesta sec : encuesta.getSecciones())
            {
                nodosec = new TreeNode(new MyHolder.IconTreeItem(R.drawable.checklist_32, sec.getNombreseccion(), ordensec, ordenpreg, R.drawable.spinner_arrow_icon )).setViewHolder(new MyHolder(this));
                nodosec.setExpanded(true);
                root.addChild(nodosec);

                for (Pregunta preg : sec.getPreguntas())
                {

                    nodopreg = new TreeNode(new MyHolder.IconTreeItem(R.drawable.textfile,
                            preg.getDescripcion(),
                            ordensec, ordenpreg,
                            (preg.isRespondida() ? R.drawable.confirmado : R.drawable.grabar)))
                            .setViewHolder(new MyHolder(this));

                    nodopreg.setExpanded(true);

                    nodopreg.setClickListener(new TreeNode.TreeNodeClickListener() {
                        @Override
                        public void onClick(TreeNode node, Object value) {
                            MyHolder.IconTreeItem nodo = (MyHolder.IconTreeItem) value;

                            OrdenSeccion = nodo.OrdenSeccion;
                            OrdenPregunta = nodo.OrdenPregunta;
                            //Toast.makeText(Formulario_Dinamico_Activity.this, "Seccion:" + String.valueOf(nodo.OrdenSeccion) + " - Pregunta:" + String.valueOf(nodo.OrdenPregunta), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            GenerarPregunta();
                        }
                    }) ;


                    nodosec.addChildren(nodopreg);
                    ordenpreg++;
                }

                //root.addChild(nodosec);

                ordenpreg = 0;
                ordensec++;
            }

            AndroidTreeView tView = new AndroidTreeView(this, root);
            tView.setDefaultAnimation(true);
            tView.setDefaultContainerStyle(R.style.TreeNodeStyle);
            tView.setSelectionModeEnabled(true);


            alertDialog.setTitle("Navegaci�n de Encuesta");
            alertDialog.setIcon(R.drawable.direction_icon);
            alertDialog.setView(tView.getView());
            alertDialog.setMessage("Seleccione pregunta para ir desplegarla.");
            alertDialog.show();

        }

    }
