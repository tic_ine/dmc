package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.UUID;

public class Seccion_Rural {

	    private UUID sec_id;

	    private int seccion_id;

	    private String localidad;

	    private int act_id;

	    private int id_usuario;

	    private boolean nueva;

		public UUID getSec_id() {
			return sec_id;
		}

		public void setSec_id(UUID sec_id) {
			this.sec_id = sec_id;
		}

		public int getSeccion_id() {
			return seccion_id;
		}

		public void setSeccion_id(int seccion_id) {
			this.seccion_id = seccion_id;
		}

		public String getLocalidad() {
			return localidad;
		}

		public void setLocalidad(String localidad) {
			this.localidad = localidad;
		}

		public int getAct_id() {
			return act_id;
		}

		public void setAct_id(int act_id) {
			this.act_id = act_id;
		}

		public int getId_usuario() {
			return id_usuario;
		}

		public void setId_usuario(int id_usuario) {
			this.id_usuario = id_usuario;
		}

		public boolean isNueva() {
			return nueva;
		}

		public void setNueva(boolean nueva) {
			this.nueva = nueva;
		}
	    

	    
}

