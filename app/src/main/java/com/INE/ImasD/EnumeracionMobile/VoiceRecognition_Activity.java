package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


public class VoiceRecognition_Activity extends Activity {

    ProgressBar progressBar;
    String LOG_TAG = "VoiceRecognition_Activity";
    ImageButton btnAudioGrabar;
    ImageButton btnAudioPlay;
    Button btnAudioConfirmar;
    Button btnAudioCancelar;

    UUID ID_VIVIENDA;
    Boolean TieneAudio = false;

    TextView tvAudioTextoError;
    TextView tvAudioTiempo;
    MediaRecorder recorder;

    MediaPlayer mp;
    String mCurrentAudioPath;
    private Handler mHandler = new Handler();

    long iniciomills = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_recognition_activity);

        this.setFinishOnTouchOutside(true);

        ID_VIVIENDA = UUID.fromString(getIntent().getExtras().getString("ID_VIVIENDA"));


        mCurrentAudioPath = globales.RutaAudioTemp;

        new File(mCurrentAudioPath).delete();



        tvAudioTextoError = (TextView) findViewById(R.id.tvAudioTextoError);
        tvAudioTiempo= (TextView) findViewById(R.id.tvAudioTiempo);
        progressBar = (ProgressBar) findViewById(R.id.pbAudioDecibeles);
        btnAudioGrabar = (ImageButton) findViewById(R.id.btnAudioGrabar);

        btnAudioConfirmar = (Button) findViewById(R.id.btnAudioConfirmar);
        btnAudioCancelar = (Button) findViewById(R.id.btnAudioCancelar);

        CrearArchivoAudioDesdeBD();

        btnAudioConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        btnAudioCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        btnAudioPlay = (ImageButton) findViewById(R.id.btnAudioPlay);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        btnAudioPlay.setEnabled(TieneAudio);


        btnAudioGrabar.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

                long msInit = 0;
                long msFinish = 0;

                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    iniciomills = System.currentTimeMillis();
                    msInit = event.getEventTime();

                    GrabarAudio();

                    btnAudioGrabar.setBackgroundResource(R.drawable.circulo_rojo);

                    //tvAudioTiempo.setText(FormatearMiliSegundos(event.getEventTime() - event.getDownTime()));
                    segundosgrabacionThread.run();
                }



                if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    msFinish = event.getEventTime();

                    if(msFinish - msInit > 800) // long up
                    {
                        DetenerGrabacionAudio();
                        btnAudioGrabar.setBackgroundResource(R.drawable.circulo);
                        tvAudioTextoError.setText("Fin de Grabación");
                        mHandler.removeCallbacks(segundosgrabacionThread);
                    }
                }

                return  true;
            }
        });

        btnAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp==null) {
                    ReproducirAudio(true);
                }
                else
                {
                    if(mp.isPlaying()) {
                        mp.pause();
                    }
                    else
                    {
                        ReproducirAudio(true);
                    }
                }
            }
        });
    }

    private void GrabarAudio()
    {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(globales.RutaAudioTemp);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try
        {
            recorder.prepare();

        }
        catch (Exception e)
        {
            Toast.makeText(VoiceRecognition_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        recorder.start();
    }

    private void DetenerGrabacionAudio()
    {
        recorder.stop();
        recorder.release();
        recorder = null;
        TieneAudio = true;
        btnAudioPlay.setEnabled(TieneAudio);

    }
    private String FormatearMiliSegundos(long ms)
    {
        double segundos = ms / 1000d;

        return String.format("%.2f", segundos) + " segundos.";
    }


    private void ReproducirAudio(boolean reproducir)
    {
        try
        {
            mp = new MediaPlayer();

            FileInputStream fis = new FileInputStream(mCurrentAudioPath);
            mp.setDataSource(fis.getFD());
            fis.close();
            mp.prepare();

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    moveSeekBarThread.run();
                }
            });

            tvAudioTextoError.setText(FormatearMiliSegundos(mp.getDuration()));

        }
        catch (Exception e)
        {
            Toast.makeText(VoiceRecognition_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        if(reproducir)
        {
            mp.start();
        }


    }


private Runnable moveSeekBarThread = new Runnable() {

    public void run() {
        if(mp.isPlaying()){

            int mediaPos_new = mp.getCurrentPosition();
            int mediaMax_new = mp.getDuration();
            progressBar.setMax(mediaMax_new);
            progressBar.setProgress(mediaPos_new);

            mHandler.postDelayed(this, 200);
            btnAudioPlay.setBackgroundResource(R.drawable.circulo_rojo);
            btnAudioPlay.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause));
            // seconds

        }
        else
        {
            progressBar.setProgress(progressBar.getMax());
            btnAudioPlay.setBackgroundResource(R.drawable.circulo);
            btnAudioPlay.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));

        }

    }
};


    private Runnable segundosgrabacionThread = new Runnable() {
        public void run() {
            long secsinicio  = TimeUnit.SECONDS.toSeconds(iniciomills);
            long secfin  = TimeUnit.SECONDS.toSeconds(System.currentTimeMillis());

            tvAudioTiempo.setText("Tiempo:00:" + String.valueOf(Math.round((secfin - secsinicio)/100d)));
            mHandler.postDelayed(this, 100);

        }
    };
    private void CrearArchivoAudioDesdeBD()
    {
        Imagen objaudio;
        DataBaseUtil db = new DataBaseUtil(VoiceRecognition_Activity.this);
        db.open();
        objaudio = db.ObtenerAudio(ID_VIVIENDA);
        db.close();

        if(null != objaudio)
        {
            try
            {
                TieneAudio = true;
                FileOutputStream out = new FileOutputStream(mCurrentAudioPath);
                out.write(objaudio.getIMAGEN());
                out.close();

                tvAudioTextoError.setText("Presione Play para escuchar el audio grabado");

                ReproducirAudio(false);
            }
            catch (Exception e)
            {
                Toast.makeText(VoiceRecognition_Activity.this, "Error al Crear Audio: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            TieneAudio = false;
            tvAudioTextoError.setText("En espera de grabación...");
        }

    }


}