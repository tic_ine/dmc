package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.R;

import java.util.HashMap;
import java.util.List;
 
public class ExpandableList_Adapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<String> _listDataHeader; // header
    private HashMap<String, List<String>> _listDataChild;
    private Context ctxt;
    private List<Integer> _listDataHeaderIconId; // Id Icono header
    
    public ExpandableList_Adapter(Context context, List<String> listDataHeader,
            HashMap<String, List<String>> listChildData,  List<Integer> listDataHeaderIconId) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.ctxt = context;
        this._listDataHeaderIconId = listDataHeaderIconId;
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
  
        
        final String childText = (String) getChild(groupPosition, childPosition);
        
        String color = childText;
        String grupo = getGroup(groupPosition).toString();
        
        
        //if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.descripcion_item, null);
        //}
 
        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
 
        if(grupo=="Colores")
        {
        if(color.equals("AMARILLO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Yellow));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }
        else if (color.equals("VERDE OSCURO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.DarkGreen));
        }
        else if (color.equals("CAF� CLARO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Beige));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }       
        else if (color.equals("CAF�"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Brown));
        }
        else if (color.equals("GRIS"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Gray));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }
        else if (color.equals("BLANCO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.White));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }
        else if (color.equals("NEGRO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Black));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.White));
        }
        else if (color.equals("CELESTE"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.LightBlue));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }
        else if (color.equals("AZUL OSCURO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.DarkBlue));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.White));    	   
        }
        else if (color.equals("AZUL"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Blue));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.White));    	   
        }
        else if (color.equals("VERDE"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Green));
        }
        else if (color.equals("NARANJA"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Orange));
        }
        else if (color.equals("VIOLETA"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Violet));
        }
        else if (color.equals("MORADO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Purple));
        }
        else if (color.equals("VERDE CLARO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.LightGreen));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }
        else if (color.equals("CHOCOLATE"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Chocolate));
        }
        else if (color.equals("VERDE AMARILLENTO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.GreenYellow));
        	txtListChild.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
        }
        else if (color.equals("ROJO"))
        {
        	txtListChild.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Red));
        }
        }
        
        txtListChild.setText(childText);
        
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    
    public int getGroupIconId(int groupPosition) {
        return this._listDataHeaderIconId.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.descripcion_grupo, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        
        //lblListHeader.setCompoundDrawables(null, null, this.ctxt.getResources().getDrawable(getGroupIconId(groupPosition)), null);
        
        lblListHeader.setCompoundDrawablesWithIntrinsicBounds(0,0, getGroupIconId(groupPosition), 0);
        
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}