package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.UUID;

public class Edificio
{
    public UUID id_edif;
    public String nombre_edif;
    public double coord_lat;
    public double coord_lng;
    public int comuna_id;
    public int region_id;
    public boolean nuevo;
    public String calle;
    public String numero;

    
	public UUID getId_edif() {
		return id_edif;
	}
	public void setId_edif(UUID id_edif) {
		this.id_edif = id_edif;
	}
	public String getNombre_edif() {
		return nombre_edif;
	}
	public void setNombre_edif(String nombre_edif) {
		this.nombre_edif = nombre_edif;
	}
	public double getCoord_lat() {
		return coord_lat;
	}
	public void setCoord_lat(double coord_lat) {
		this.coord_lat = coord_lat;
	}
	public double getCoord_lng() {
		return coord_lng;
	}
	public void setCoord_lng(double coord_lng) {
		this.coord_lng = coord_lng;
	}
	public int getComuna_id() {
		return comuna_id;
	}
	public void setComuna_id(int comuna_id) {
		this.comuna_id = comuna_id;
	}
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}
	public boolean isNuevo() {
		return nuevo;
	}
	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}


    
}