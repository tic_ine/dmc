package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Base64;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.POI;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.SSLConnection;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.UUID;

//comienzo enviar
public class AsyncTask_EnviarDatos extends AsyncTask<Void, Void, Integer>   
{

	private Context context;
    private AsyncTaskCompleteListener<Integer> listener;
    ProgressDialog pd;
    

	private int max;
    public AsyncTask_EnviarDatos(Context ctx, AsyncTaskCompleteListener<Integer> listener)
    {
        this.context = ctx;
        this.listener = listener;
    }
    
    
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		pd = new ProgressDialog(context);
		pd.setTitle("Conectando con servidor... Por favor espere.");
		pd.setMessage("Enviando Datos de levantamiento...");       
		pd.setIndeterminate(false);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMax(100);
		pd.setCancelable(false);
		pd.show();
	}

	@Override
	protected Integer doInBackground(Void... params) {


		try
		{

            pd.setMessage("Preparando Base de Datos");
			String nombre = funciones.ComprimirBD();
			SSLConnection.allowAllSSL();

            SubirBD(nombre);
			/*
			if(ObtenerEnviarViviendas())
			{
				ObtenerEnviarDibujosDV();
				ObtenerEnviarRegistrosPosicion();
				ObtenerEnviarEdificios();
				
				ObtenerEnviarImagenes();
				ObtenerEnviarAudios();

				ObtenerPOI();

				return 1;				
			}
			else
			{
				return 0;
			}
			*/
			return 0;

	
		}
		catch(Exception ex)
		{
			return -1;
		}

	}

	
	@Override
	protected void onPostExecute(Integer Result)
	{
		ActualizarTodosDatosEnviados();

		pd.dismiss();
		listener.onTaskCompleteEnvioDatos(Result);
		//btnDescargarViviendas.setEnabled(true);
		//CrearArchivosFeatureServer();
		
	}
	
	
	private boolean ObtenerEnviarViviendas()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String XMLViviendas;
		int i = 0;
		int total;
		boolean ret = false;
		Cursor c;
		String retbh = "";
		String retev = "";
		String reteh = "";
		c = db.ObtenerViviendas(true);
		
		total = c.getCount();
		

		if(total>0)
		{
			pd.setMax(total);
			
			while(c.moveToNext())
			{
				String ID_VIVIENDA = c.getString(c.getColumnIndex("ID_VIVIENDA"));

				XMLViviendas = funciones.SerializarFilaCursor(c, "Viviendas", "Vivienda");
				retbh = BorrarHogaresViviendas(ID_VIVIENDA);
				retev = EnviarViviendas(XMLViviendas);
				reteh = ObtenerEnviarViviendaHogares(ID_VIVIENDA);
				
				ret = (retbh.toLowerCase().equals("ok") && retev.toLowerCase().equals("ok") && reteh.toLowerCase().equals("ok"));
				
				if(ret)
				{
					db.MarcarViviendaComoEnviada(UUID.fromString(ID_VIVIENDA));
					i++;
					pd.setProgress(i);
				}
				else
				{
					break;
				}
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
			//EnviarViviendas(XMLViviendas);
		}
		else
        {
            ret = true;
        }
		
		c.close();
		db.close();
		
		return ret;
		
	}
	/*
	private void ObtenerEnviarHogares()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String HogaresXML;
		int i = 0;
		int total;
		Cursor c;
		
		c = db.ObtenerHogaresParaEnvio();
		
		total = c.getCount();
		

		if(total>0)
		{
			pd.setMax(total);
			
			while(c.moveToNext())
			{
				HogaresXML = funciones.SerializarFilaCursor(c, "Hogares", "Hogar");
				EnviarHogares(HogaresXML);
				i++;
				pd.setProgress(i);
				
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
			//EnviarViviendas(XMLViviendas);
		}
		
		c.close();
		db.close();
		
	}	
	
	*/
	private String ObtenerEnviarViviendaHogares(String ID_VIVIENDA)
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String HogaresXML;
		int i = 0;
		int total;
		Cursor c;
		
		String ret = "ok";
		
		c = db.ObtenerHogaresViviendaParaEnvio(ID_VIVIENDA);
		
		total = c.getCount();
		

		if(total>0)
		{
			pd.setMax(total);
			
			while(c.moveToNext())
			{
				HogaresXML = funciones.SerializarFilaCursor(c, "Hogares", "Hogar");
				ret = EnviarHogares(HogaresXML);
				
				if(ret.equals("1"))
				{
					ret = "ok";
					i++;
					pd.setProgress(i);					
				}
				else
				{
					ret = "error";
					break;
				}

				
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
			//EnviarViviendas(XMLViviendas);
		}
		
		c.close();
		db.close();
		
		return ret;
		
	}	
	
	private void ObtenerEnviarImagenes()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		int i = 0;
		int total;
		Cursor c;
		
		c = db.ObtenerImagenes();
		
		total = c.getCount();
		

		if(total>0)
		{
			pd.setMax(total);
			byte[] b;
			UUID ID_VIVIENDA;
			if(c.moveToFirst())
			{
				do
				{
					
					ID_VIVIENDA = UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA")));
					b = c.getBlob(c.getColumnIndex("IMAGEN"));
					
					EnviarImagenesDb(ID_VIVIENDA, b);
					i++;
					pd.setProgress(i);
					
				}while(c.moveToNext());
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
			//EnviarViviendas(XMLViviendas);
		}
		
		c.close();
		db.close();
		
	}


	private void ObtenerEnviarAudios()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		int i = 0;
		int total;
		Cursor c;

		c = db.ObtenerAudios();

		total = c.getCount();


		if(total>0)
		{
			pd.setMax(total);
			byte[] b;
			UUID ID_VIVIENDA;
			if(c.moveToFirst())
			{
				do
				{

					ID_VIVIENDA = UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA")));
					b = c.getBlob(c.getColumnIndex("AUDIO"));

					EnviarAudiosDb(ID_VIVIENDA, b);
					i++;
					pd.setProgress(i);

				}while(c.moveToNext());
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");
			//EnviarViviendas(XMLViviendas);
		}

		c.close();
		db.close();

	}
	private void ObtenerEnviarEdificios()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String XMLViviendas;
		int i = 0;
		int total;
		Cursor c;
		
		c = db.ObtenerEdificiosCursor();
		
		total = c.getCount();
		

		if(total>0)
		{
			pd.setMax(total);
			
			while(c.moveToNext())
			{
				XMLViviendas = funciones.SerializarFilaCursor(c, "Edificios", "Edificio");
				EnviarEdificios(XMLViviendas);
				i++;
				pd.setProgress(i);
				
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
			//EnviarViviendas(XMLViviendas);
		}
		
		c.close();
		db.close();
		
	}
	private void ObtenerEnviarDibujosDV()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String XMLDibujos;
		int i = 1;
		int total;
		Cursor c;
		
		c = db.ObtenerDibujosCursor(false, true);
		
		total = c.getCount();
		

		if(total>0)
		{
			pd.setMax(total);
			
			if(c.moveToFirst())
			{
				pd.setProgress(0);
				do
				{
					XMLDibujos = funciones.SerializarFilaCursor(c, "Dibujos", "Dibujo");
					EnviarDibujos(XMLDibujos);
					pd.setProgress(i);
					i++;
					
				}
				while(c.moveToNext());
				
			}
			//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
			//EnviarViviendas(XMLViviendas);
		}
		
		c.close();
		db.close();
		
	}		
	
	private String EnviarViviendas(String XMLViviendas)
	{
	

		try
		{
	
			//pd.setTitle("Enviando viviendas nuevas o modificadas...");
			
			String METHOD_NAME = "InsertarModificarViviendaManzana";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
	        request.addProperty("ViviendaXML",  XMLViviendas);
			request.addProperty("key", globales.key);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
	
			
			return envelope.getResponse().toString();
			
		}
		catch (IOException e) {
		    e.printStackTrace();
		    return null;
		} catch (XmlPullParserException e) {
		    e.printStackTrace();
		    return null;
		} catch (Exception e) {
		    return null;
		}
		
	}

	private String BorrarHogaresViviendas(String ID_VIVIENDA)
	{
	
		try
		{
	
			
			String METHOD_NAME = "BorrarHogares";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("id_usuario",  globales.usuario.getId_usuario());
			request.addProperty("act_id",  globales.actualizacion.getAct_id());
	        request.addProperty("ID_VIVIENDA",  ID_VIVIENDA);
			request.addProperty("key", globales.key);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
	
			
			return envelope.getResponse().toString();
			
		}
		catch (IOException e) {
		    e.printStackTrace();
		    return null;
		} catch (XmlPullParserException e) {
		    e.printStackTrace();
		    return null;
		} catch (Exception e) {
		    return null;
		}
		
	}
	private String EnviarHogares(String HogaresXML)
	{
		try
	{

		//pd.setTitle("Enviando viviendas nuevas o modificadas...");
		
		String METHOD_NAME = "InsertarModificarHogares";
		String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

		SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

        request.addProperty("HogaresXML",  HogaresXML);
		request.addProperty("key", globales.key);
        
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);

		//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
		transporte.call(SOAP_ACTION, envelope);

		
		return envelope.getResponse().toString();
		}
		catch (IOException e) {
		    e.printStackTrace();
		    return null;
		} catch (XmlPullParserException e) {
		    e.printStackTrace();
		    return null;
		} catch (Exception e) {
		    return null;
		}
		
	}
	private String EnviarDibujos(String XMLDibujos)
	{
		try
		{

			//pd.setTitle("Enviando viviendas nuevas o modificadas...");
			
			String METHOD_NAME = "InsertarModificarDibujos";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
	
			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
	
	        request.addProperty("DibujosXML",  XMLDibujos);
			request.addProperty("key", globales.key);
	        
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
	
			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
	
			//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
			transporte.call(SOAP_ACTION, envelope);
	
			String retorno =envelope.getResponse().toString(); 
			
			return retorno;
		}
		
		catch (IOException e) 
		{
		    e.printStackTrace();
		    return null;
		} catch (XmlPullParserException e) {
		    e.printStackTrace();
		    return null;
		} catch (Exception e) {
		    return null;
		}
		
	}
	
	private String EnviarEdificios(String EdificiosXML)
	{
		try
	{

		//pd.setTitle("Enviando viviendas nuevas o modificadas...");
		
		String METHOD_NAME = "InsertarEdificios";
		String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

		SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

        request.addProperty("EdificiosXML",  EdificiosXML);
		request.addProperty("key", globales.key);
        
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);

		//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
		transporte.call(SOAP_ACTION, envelope);

		
		return envelope.getResponse().toString();
		}
		catch (IOException e) {
		    e.printStackTrace();
		    return null;
		} catch (XmlPullParserException e) {
		    e.printStackTrace();
		    return null;
		} catch (Exception e) {
		    return null;
		}
		
	}

	private void ActualizarTodosDatosEnviados()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();

        db.ActualizarViviendasEnviadas();
        db.ActualizaHogaresEnviados();
        db.ActualizarEnviadasRegistroPosicion();
        db.ActualizarDibujosEnviados();
        db.ActualizarEdificiosEnviados();
        db.ActualizarTodosPOIEnviados();

		db.close();
		
	}		
	

	/*
	private void EnviarImagenes()
	{
		///pd.setTitle("Enviando imagenes de viviendas...");
		
		File dir = new File(globales.RutaPicsSinImagen);
		Bitmap bm;
		ByteArrayOutputStream baos;
		String encodedImage ;
		
		String METHOD_NAME = "SubirArchivo";
		String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

		SoapObject request;// = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

        SoapSerializationEnvelope envelope;
        
        HttpTransportSE transporte;
		UUID ID_VIVIENDA;
		int i = 1;
		pd.setMax(dir.list().length);
		
		for(File img : dir.listFiles())
		{
			
			ID_VIVIENDA = UUID.fromString(img.getName().replace(".jpg", ""));
			
			bm = BitmapFactory.decodeFile(img.getAbsolutePath());
			baos = new ByteArrayOutputStream();  
			bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object   
			byte[] b = baos.toByteArray(); 
			encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
			
			request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
	        request.addProperty("b",  encodedImage);
	        request.addProperty("ID_VIVIENDA",  ID_VIVIENDA.toString());
	        
			envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			transporte = new HttpTransportSE(globales.WS_URL);

			//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
			try {
				transporte.call(SOAP_ACTION, envelope);
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally
			{
				pd.setProgress(i);
				i++;
				
			}
		}

		
	}

	*/
	private void EnviarImagenesDb(UUID ID_VIVIENDA, byte[] b)
	{
		

		String encodedImage ;
		
		String METHOD_NAME = "SubirArchivo";
		String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
		
		SoapObject request;// = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
		
		SoapSerializationEnvelope envelope;
		
		HttpTransportSE transporte;
		int i = 1;
		
		encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
		
		request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
		request.addProperty("b",  encodedImage);
		request.addProperty("ID_VIVIENDA",  ID_VIVIENDA.toString());
		
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		
		transporte = new HttpTransportSE(globales.WS_URL);
		
		//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
		try {
			transporte.call(SOAP_ACTION, envelope);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			pd.setProgress(i);
			i++;
			
		}
	}

	private void EnviarAudiosDb(UUID ID_VIVIENDA, byte[] b)
	{


		String encodedImage ;

		String METHOD_NAME = "SubirAudio";
		String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

		SoapObject request;// = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

		SoapSerializationEnvelope envelope;

		HttpTransportSE transporte;
		int i = 1;

		encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

		request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
		request.addProperty("b",  encodedImage);
		request.addProperty("ID_VIVIENDA",  ID_VIVIENDA.toString());
		request.addProperty("key",  globales.key);

		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		transporte = new HttpTransportSE(globales.WS_URL);

		//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
		try {
			transporte.call(SOAP_ACTION, envelope);


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			pd.setProgress(i);
			i++;

		}
	}
	
	
	private void ObtenerEnviarRegistrosPosicion()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String XMLDibujos;
		int i = 1;
		int total;
		Cursor c;
		
		try {
			c = db.ObtenerRegistroPosicionEnvio();
			total = c.getCount();
			

			if(total>0)
			{
				pd.setMax(total);
				
				if(c.moveToFirst())
				{
					pd.setProgress(0);
					do
					{
						XMLDibujos = funciones.SerializarFilaCursor(c, "RegistrosPosicion", "RegistroPosicion");
						EnviarRegistroPosicion(XMLDibujos);
						pd.setProgress(i);
						i++;
						
					}while(c.moveToNext());
					
				}
				//XMLViviendas = funciones.SerializarCursor(db.ObtenerViviendas(true), "Viviendas", "Vivienda");		
				//EnviarViviendas(XMLViviendas);
			}
			
			c.close();
			db.close();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}			
	private String EnviarRegistroPosicion(String RegistrosXML)
	{
		try
		{

			//pd.setTitle("Enviando viviendas nuevas o modificadas...");
			
			String METHOD_NAME = "InsertarRegistrosPosicion";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
	
			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
	
	        request.addProperty("RegistrosXML",  RegistrosXML);
			request.addProperty("key", globales.key);
	        
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
	
			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
	
			//envelope.addMapping(NAMESPACE, "Resultado", new Resultado().getClass());
			transporte.call(SOAP_ACTION, envelope);
	
			String retorno =envelope.getResponse().toString(); 
			
			return retorno;
		}
		catch (IOException e) {
		    e.printStackTrace();
		    return null;
		} catch (XmlPullParserException e) {
		    e.printStackTrace();
		    return null;
		} catch (Exception e) {
		    return null;
		}
		
	}


	private void ObtenerPOI()
	{
		DataBaseUtil db = new DataBaseUtil(context);
		db.open();
		String XMLPOI;
		int i = 1;
		int total;


		try {
			Cursor c = db.ObtenerPOISCursor();
			total = c.getCount();


			if(total>0)
			{
				pd.setMax(total);

				if(c.moveToFirst())
				{
					pd.setProgress(0);
					do
					{
						XMLPOI = funciones.SerializarFilaCursor(c, "POIS", "POI");
						String IDPOI = c.getString(c.getColumnIndex("IDPOI")).toString();
						EnviarPOI(XMLPOI, IDPOI);
						pd.setProgress(i);
						i++;

					}while(c.moveToNext());

				}

			}


			db.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}
	private String EnviarPOI(String XMLPOI, String IDPOI)
	{
		try
		{

			String METHOD_NAME = "InsertarPOI";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);

			request.addProperty("key", globales.key);
			request.addProperty("XMLPOI", XMLPOI);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);

			transporte.call(SOAP_ACTION, envelope);

			String retorno =envelope.getResponse().toString();

			if(retorno.equals("OK"))
			{
				ActualizarPOIEnviado(IDPOI);
			}
			return retorno;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			return null;
		}

	}

	private  void ActualizarPOIEnviado(String POI)
	{
		DataBaseUtil db = new DataBaseUtil(this.context);
		db.open();
		db.ActualizarPOIEnviados(POI);
		db.close();

	}

	private String SubirBD(String nombreBDComprimida)
	{
		try
		{

			//pd.setTitle("Enviando viviendas nuevas o modificadas...");

			String METHOD_NAME = "UploadData";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
/*
			String Ruta = Environment.getExternalStorageDirectory()
					+ File.separator + "DV"
					+ File.separator + "dv.db";spatialite
*/

			File archivo = new File(nombreBDComprimida);

			String nombre = archivo.getName();

			long contentLength = archivo.length();

			FileInputStream is = new FileInputStream(archivo);
			int bufferSize = 10 * 1024; // 10k
			byte[] bytesPortion = new byte[bufferSize];
			String retorno = "";

			int progreso = 0;
            int offset = 0;
			long bytessubidos = 0;


            int n = 0;
			while ((n = is.read(bytesPortion, 0, bufferSize)) != -1)
            {
				retorno = SubirChunk(nombre, bytesPortion, offset, contentLength);

				if(retorno.contains("OK"))
				{
                    offset += n;

					progreso = (int)((offset * 100) / contentLength);

					pd.setProgress(progreso);

				}

			}

			//SubirChunk(nombre, bytesPortion, 0, false);

			//SUBIMOS EL ARCHIVO
            if(retorno.equals("OK"))
			{
                is.close();
                boolean borrar;
                borrar = archivo.delete();
                if(borrar) {
                    Toast.makeText(this.context, "Archivo enviado fue eliminado", Toast.LENGTH_SHORT).show();
                }


                archivo = null;

			}
			return retorno;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			return null;
		}

	}

	private String SubirChunk(String nombre, byte[] chunk, int offset, long tamano)
	{
		try {


			String METHOD_NAME = "UploadData";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;

			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);


			request.addProperty("key", globales.key);
			request.addProperty("nombre", nombre);
			request.addProperty("chunk",  Base64.encodeToString(chunk, Base64.DEFAULT));
			request.addProperty("offset", offset);
			request.addProperty("tamano", tamano);
            request.addProperty("act_id", globales.actualizacion.getAct_id());
            request.addProperty("id_usuario", globales.usuario.getId_usuario());

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);


			transporte.call(SOAP_ACTION, envelope);
			String retorno = envelope.getResponse().toString();

			return retorno;

		}
		catch (IOException e) {
			e.printStackTrace();
			return "";
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return "";
		} catch (Exception e) {
			return "";
		}
	}
}