package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Mbecerra on 11-10-2018.
 */

public class POI {
    private UUID IDPOI;
    private int IDCATPOI;
    private String GLOSA;
    private double COORD_X;
    private double COORD_Y;
    private Date FECHACAPTURA;
    private boolean ENVIADO;


    private CategoriaPOI CATEGORIAPOI;

    public UUID getIdpoi() {
        return IDPOI;
    }

    public void setIdpoi(UUID idpoi) {
        this.IDPOI = idpoi;
    }

    public int getIdcatpoi() {
        return IDCATPOI;
    }

    public void setIdcatpoi(int idcatpoi) {
        this.IDCATPOI = idcatpoi;
    }

    public String getGlosa() {
        return GLOSA;
    }

    public void setGlosa(String glosa) {
        this.GLOSA = glosa;
    }

    public double getCoord_x() {
        return COORD_X;
    }

    public void setCoord_x(double coord_x) {
        this.COORD_X = coord_x;
    }

    public double getCoord_y() {
        return COORD_Y;
    }

    public void setCoord_y(double coord_y) {
        this.COORD_Y = coord_y;
    }

    public Date getFechacaptura() {
        return FECHACAPTURA;
    }

    public void setFechacaptura(Date fechacaptura) {
        this.FECHACAPTURA = fechacaptura;
    }


    public CategoriaPOI getCATEGORIAPOI() {
        return CATEGORIAPOI;
    }

    public void setCATEGORIAPOI(CategoriaPOI CATEGORIAPOI) {
        this.CATEGORIAPOI = CATEGORIAPOI;
    }

    public boolean isENVIADO() {
        return ENVIADO;
    }

    public void setENVIADO(boolean ENVIADO) {
        this.ENVIADO = ENVIADO;
    }
}
