package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Regiones;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class Spin_Regiones_Adapter extends BaseAdapter {
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Regiones> values;

    public Spin_Regiones_Adapter(Context context, List<Regiones> regiones) {
        super();
        this.context = context;
        this.values = regiones;
    }

    public int getCount(){
       return values.size();
    }

    public Regiones getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    public View getView(int position, View convertView, ViewGroup parent) {
    	 View v = convertView; 
    	 TextView txt;
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_region, null);
				

	        }
	        txt = (TextView)v.findViewById(R.id.tvNombreRegion);
	        txt.setText(getItem(position).getRegion_glosa());
	        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	
   	 View v = convertView; 
   	 TextView txt;
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_solo_string, null);
	        }
	        txt = (TextView)v.findViewById(R.id.tvitemsolostring);
	        txt.setText(getItem(position).getRegion_glosa());
	        
	        return v;

    }

}
