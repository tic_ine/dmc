package com.INE.ImasD.EnumeracionMobile.Adaptadores;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.OpcionMapa;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;


public class OpcionesMapa_Adapter extends BaseAdapter
{


    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<OpcionMapa> values;

    public OpcionesMapa_Adapter(Context context, List<OpcionMapa> opciones) 
    {
        super();
        this.context = context;
        this.values = opciones;
    }

    public int getCount(){
       return values.size();
    }

    public OpcionMapa getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }

    public void remove(OpcionMapa opc)
	{
    	values.remove(opc);
	}
    
    public List<OpcionMapa> getList()
	{
    	return values;
	}

    public void insert(OpcionMapa opc, int idx)
	{
    	values.add(idx, opc);
	}
    
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; 
        TextView tvOpcionesMapa = null;
        ImageView imgOpcionMapa;
        
        OpcionMapa opc = values.get(position);

        if (convertView == null) { // if it's not recycled, initialize some
                                    // attributes
        LayoutInflater vi = 
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_opciones_mapa , null);
         }
         

        tvOpcionesMapa = (TextView) v.findViewById(R.id.tvOpcionesMapa); 
        //chkOK = (CheckBox) v.findViewById(R.id.chkOk);
        
        imgOpcionMapa = (ImageView)v.findViewById(R.id.imgOpcionMapa);
        tvOpcionesMapa.setText(opc.getGlosa_opcion());
        
        imgOpcionMapa.setImageResource(opc.getId_img());
        
        
        
        return v;

    }

}