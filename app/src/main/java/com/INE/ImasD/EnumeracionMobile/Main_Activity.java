package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTaskCompleteListener;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_DescargarDatos;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_DescargarDesdeArchivo_UsuarioAct;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_EnviarDatos;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.ACCIONES_TABS;

import java.util.ArrayList;
import java.util.List;


public class Main_Activity extends ActivityGroup {
	TabHost tabs;
	private int DESCARGA_UPM_SELECCION = 1000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setTitle("Enumeraci�n de " + globales.usuario.getUsername());
		setContentView(R.layout.main_activity);
		
		Resources res = getResources(); // Resource object to get Drawables
		tabs = (TabHost)findViewById(R.id.tabhost);
	

        tabs.setup(this.getLocalActivityManager());
		Intent intent = new Intent(this, Listado_Activity.class);
		
		tabs.addTab(tabs.newTabSpec("Listado")
				.setIndicator("Listado", res.getDrawable(R.drawable.listado))
				.setContent(intent));

		
		//Intent intentRural = new Intent(this, Listado_Activity.class);
		
		//tabs.addTab(tabs.newTabSpec("Rural")
		//		.setIndicator("Rural", res.getDrawable(R.drawable.listado))
		//		.setContent(intentRural));
		//
		
		Intent intent2 = new Intent(this, Mapa_Activity.class);
		intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		tabs.addTab(tabs
				.newTabSpec("Mapa")
				.setIndicator("Mapa", res.getDrawable(R.drawable.mapa))
				.setContent(intent2));
		
		/*
		Intent intent3 = new Intent(this, Sync_Activity.class);
		tabs.addTab(tabs
				.newTabSpec("Sync")
				.setIndicator("Sync", res.getDrawable(R.drawable.sync))
				.setContent(intent3));
		
		*/

		tabs.setCurrentTab(0);

		// Set tabs Colors
		tabs.setBackgroundColor(Color.BLACK);
		tabs.getTabWidget().setBackgroundColor(Color.BLACK);
		
		globales.accion = ACCIONES_TABS.NINGUNA;
		
		tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
		    @Override
		    public void onTabChanged(String tabId) {
		    	if(tabId=="Mapa")
		    	{
			    	if(globales.accion==ACCIONES_TABS.SETEAR_CENTROIDE)
			    	{
				        Activity currentActivity = getCurrentActivity();
				        if (currentActivity instanceof Mapa_Activity) {
				            ((Mapa_Activity) currentActivity).SetearCentroide();
				        }
			    	}

			        //if (currentActivity instanceof Listado_Activity) { 
			        //    ((Mapa_Activity) currentActivity).aPublicMethodFromClassMessages();
			        //}
		    	}
		    }
		});
	
		getActionBar().setDisplayHomeAsUpEnabled(false);
		getActionBar().setDisplayShowHomeEnabled(true);
		
		if(globales.EsCargaDesdeArchivo)
		{
			new AsyncTask_DescargarDesdeArchivo_UsuarioAct(Main_Activity.this, new AsyncTaskCompleteListener<Integer>() {
				
				@Override
				public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onTaskCompleteManzanas(List<Manzana> manzanas) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onTaskCompleteEnvioDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onTaskCompleteDescargarDatos(Integer resultado) {
					// TODO Auto-generated method stub
					globales.EsCargaDesdeArchivo = false;

				}

				@Override
				public void onTaskCompleteSecciones(List<Seccion> secciones) {
					// TODO Auto-generated method stub
					
				}
			}).execute();
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

		 MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.main_menu, menu);
	 
	        return super.onCreateOptionsMenu(menu);

	}
	
    private void ConfirmarCancelar()
    {
    	
    	new AlertDialog.Builder(Main_Activity.this)
		.setIcon(R.drawable.pregunta)
		.setTitle("Cerrar sesi�n")
		.setMessage("�Est� seguro que desea cerrar sesi�n?") 
		.setPositiveButton("S�", new DialogInterface.OnClickListener() 
		{

			@Override
			public void onClick(DialogInterface dialog, int which) 
			{

				Main_Activity.this.setResult(Activity.RESULT_OK);
				finish();

			}

		})
		.setNegativeButton("No", null)
		.show();
    }
    
    
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionad o
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_activargps: //Grabar
			funciones.MostrarConfigGPS(Main_Activity.this);
			//Toast.makeText(this, "Has pulsado el Item 1 del Action Bar",Toast.LENGTH_SHORT).show();
			return true;

		case R.id.action_cerrarsesion:
//    		setResult(Activity.RESULT_OK);
//    		finish();
			
			ConfirmarCancelar();
			return true;

		case R.id.action_download:
			
			if(!funciones.TieneInternet(Main_Activity.this))
			{
				Toast.makeText(Main_Activity.this, "Necesita conexi�n a Internet para descargar datos.", Toast.LENGTH_SHORT).show();
				return true;
			}
			
			if(ExistenModificadas())
			{
				Toast.makeText(Main_Activity.this, "Debe enviar las modificaciones realizadas antes de descargar datos", Toast.LENGTH_SHORT).show();
				return true;	
			}
			// TODO Auto-generated method stub
			new AlertDialog.Builder(Main_Activity.this)
			.setIcon(R.drawable.download)
			.setTitle("Descargar Datos Remotos")
			.setMessage("�Desea comenzar la descarga de datos remotos?")
			.setPositiveButton("Si", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent i = new Intent(Main_Activity.this, SeleccionUPM_Activity.class);
					startActivityForResult(i, DESCARGA_UPM_SELECCION);
				}
			})
			.setNegativeButton("No", null)
			.show();

			return true;

		case R.id.action_upload:
			
			if(!funciones.TieneInternet(Main_Activity.this))
			{
				Toast.makeText(Main_Activity.this, "Necesita conexi�n a Internet para enviar datos.", Toast.LENGTH_SHORT).show();
				return false;
			}
			
			new AlertDialog.Builder(Main_Activity.this)
			.setIcon(R.drawable.upload)
			.setTitle("Enviar Datos")
			.setMessage("�Desea comenzar el env�o de datos recolectados?")
			.setPositiveButton("Si", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					new AsyncTask_EnviarDatos(Main_Activity.this, new AsyncTaskCompleteListener<Integer>() {
						
						@Override
						public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteManzanas(List<Manzana> manzanas) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteEnvioDatos(Integer resultado) {
							// TODO Auto-generated method stub
							if(resultado==1)
							{
								Toast.makeText(Main_Activity.this, "Env�o exitoso",Toast.LENGTH_SHORT).show();
							}
							
						}

						@Override
						public void onTaskCompleteDescargarDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteSecciones(
								List<Seccion> secciones) {
							// TODO Auto-generated method stub
							
						}

					}).execute();
					
				}

			})
			.setNegativeButton("No", null)
			.show();

			
			return true;
			
		case R.id.action_preferencias:
			
			Intent i = new Intent(this,Preferencias_Activity.class);
			startActivity(i);
			
			return true;
			
		default:
		
			return false;
		
		}
		//return false;
		
	}
	
	public boolean ExistenModificadas()
	{
		 DataBaseUtil db = new DataBaseUtil(Main_Activity.this);
		 db.open();
		 boolean ret;
		 ret = db.ExistenModificadas();
		 db.close();
		 
		 return ret;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == DESCARGA_UPM_SELECCION) {
			if (resultCode == RESULT_OK && data !=null) {// si grabo se agrega el punto a la capa 
				Bundle b = data.getExtras();
				String tipo;
//				if (b != null) {
				
				tipo = b.getString("tipo");


				if (tipo.equals("manzanas"))
				{
					List<Manzana> manzanas; 
					manzanas = (List<Manzana>) b.getSerializable("manzanas");

					if (manzanas.size()>0) {
						new AsyncTask_DescargarDatos(Main_Activity.this, new AsyncTaskCompleteListener<Integer>() {
							
							@Override
							public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onTaskCompleteManzanas(List<Manzana> manzanas) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onTaskCompleteEnvioDatos(Integer resultado) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onTaskCompleteDescargarDatos(Integer resultado) {
								// TODO Auto-generated method stub
								
								
							}
						
							@Override
							public void onTaskCompleteSecciones(List<Seccion> secciones) {
								// TODO Auto-generated method stub
								
							}
						}, manzanas, new ArrayList<Seccion>(), false).execute();
					}
				}
				else if (tipo.equals("secciones"))
				{
					List<Seccion> secciones; 
					secciones= (List<Seccion>) b.getSerializable("secciones");
					if (secciones.size()>0) {
						new AsyncTask_DescargarDatos(Main_Activity.this, new AsyncTaskCompleteListener<Integer>() {
							
							@Override
							public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onTaskCompleteManzanas(List<Manzana> manzanas) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onTaskCompleteEnvioDatos(Integer resultado) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onTaskCompleteDescargarDatos(Integer resultado) {
								// TODO Auto-generated method stub
								
								
							}
						
							@Override
							public void onTaskCompleteSecciones(List<Seccion> secciones) {
								// TODO Auto-generated method stub
								
							}
						}, new ArrayList<Manzana>(), secciones, false).execute();
					}
				}
				else if (tipo.equals("todas"))
				{
				
						
				 new AsyncTask_DescargarDatos(Main_Activity.this, new AsyncTaskCompleteListener<Integer>() {
					
					@Override
					public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteSecciones(List<Seccion> secciones) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteManzanas(List<Manzana> manzanas) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteEnvioDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteDescargarDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}
				},new ArrayList<Manzana>(), new ArrayList<Seccion>(), false).execute();

				}				
			}

		}
	}

}


/*
public class Main_Activity extends Activity {

	// Declaring our tabs and the corresponding fragments.
	ActionBar.Tab TabListado, TabMapa, TabSync;
	Fragment ListadoFragmentTab = new Listado_Activity();
	Fragment MapaFragmentTab = new Mapa_Activity();
	Fragment SyncFragmentTab = new Sync_Activity();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		
		// Asking for the default ActionBar element that our platform supports.
		ActionBar actionBar = getActionBar();
		 
        // Screen handling while hiding ActionBar icon.
        actionBar.setDisplayShowHomeEnabled(true);
 
        // Screen handling while hiding Actionbar title.
        actionBar.setDisplayShowTitleEnabled(true);
 
        // Creating ActionBar tabs.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
 
        // Setting custom tab icons.
        TabListado = actionBar.newTab().setIcon(R.drawable.listado);
        TabMapa = actionBar.newTab().setIcon(R.drawable.mapa);
        TabSync = actionBar.newTab().setIcon(R.drawable.sync);
        
        // Setting tab listeners.
        TabListado.setTabListener(new TabListener(ListadoFragmentTab));
        TabMapa.setTabListener(new TabListener(MapaFragmentTab));
        TabSync.setTabListener(new TabListener(SyncFragmentTab));
       
        ListadoFragmentTab.setRetainInstance(true);
        MapaFragmentTab.setRetainInstance(true);
        SyncFragmentTab.setRetainInstance(true);
        
        // Adding tabs to the ActionBar.
        actionBar.addTab(TabListado);
        actionBar.addTab(TabMapa);
        actionBar.addTab(TabSync);
	}
}

*/

