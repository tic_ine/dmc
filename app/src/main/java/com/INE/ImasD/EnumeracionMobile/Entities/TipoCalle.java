package com.INE.ImasD.EnumeracionMobile.Entities;

public class TipoCalle {
	private String tipocalle_id;
	private String tipocalle_glosa;
	
	public String getTipocalle_id() {
		return tipocalle_id;
	}
	public void setTipocalle_id(String tipocalle_id) {
		this.tipocalle_id = tipocalle_id;
	}
	public String getTipocalle_glosa() {
		return tipocalle_glosa;
	}
	public void setTipocalle_glosa(String tipocalle_glosa) {
		this.tipocalle_glosa = tipocalle_glosa;
	}

	
}
