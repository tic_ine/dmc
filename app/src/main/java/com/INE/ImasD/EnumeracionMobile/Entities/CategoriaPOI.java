package com.INE.ImasD.EnumeracionMobile.Entities;

/**
 * Created by Mbecerra on 11-10-2018.
 */

public class CategoriaPOI {

        private int idcatpoi;
        private String glosa;
        private byte[] icon;
        private boolean selected;

    public int getIdcatpoi() {
        return idcatpoi;
    }

    public void setIdcatpoi(int idcatpoi) {
        this.idcatpoi = idcatpoi;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
