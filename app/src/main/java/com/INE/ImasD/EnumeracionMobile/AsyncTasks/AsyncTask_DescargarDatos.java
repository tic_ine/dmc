package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Area_Trabajo;
import com.INE.ImasD.EnumeracionMobile.Entities.CategoriaPOI;
import com.INE.ImasD.EnumeracionMobile.Entities.Dibujo;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Localidad;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.SSLConnection;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

public class AsyncTask_DescargarDatos extends AsyncTask<Void, Integer, Integer>
{
	private Context context;
    private AsyncTaskCompleteListener<Integer> listener;
    private ProgressDialog pd;
    private DataBaseUtil dbUtil;
    
	private List<Manzana> manzanas;
	private List<Seccion> secciones;
	private boolean Todas;
	private boolean SoloCargaLaboral;

    public AsyncTask_DescargarDatos(Context ctx, AsyncTaskCompleteListener<Integer> listener, List<Manzana> manzanas, List<Seccion> secciones, boolean SoloCargaLaboral)//, boolean todas)
    {
        this.context = ctx;
        this.listener = listener;
        this.manzanas = manzanas;
        this.secciones = secciones;
        this.SoloCargaLaboral = SoloCargaLaboral;
    }

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		pd = new ProgressDialog(context);
		pd.setTitle("Preparando descarga...");
		pd.setMessage("Obteniendo datos de servidor");       
		pd.setIndeterminate(false);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMax(100);
		pd.setCancelable(false);
		pd.show();
	}

	@Override
	protected Integer doInBackground(Void... params) {

		//CrearArchivosFeatureServer();
		try
		{

			SSLConnection.allowAllSSL();
		    if(SoloCargaLaboral)
            {

                if(globales.padres== globales.PADRES.MANZANAS)
                {

                    ObtenerManzanas();
                }
                else
                {
                    ObtenerSecciones();
                }
                return 1;
            }
            else
            {

                String id_padre;
                for (Manzana mz : manzanas) {
                    if (mz.isChecked()) {
                        id_padre = String.valueOf(mz.getIdmanzana());

                        ObtenerViviendasPadre(id_padre);
                        ObtenerHogaresManzana(id_padre);

                    }
                }

                for (Seccion sec : secciones) {
                    if (sec.isChecked()) {
                        id_padre = String.valueOf(sec.getCu_seccion());
                        ObtenerViviendasPadre(id_padre);
                        ObtenerHogaresManzana(id_padre);
                        ObtenerAreasTrabajo(id_padre);
                    }
                }
                //}
                Log.d("SYNC_GEO", "descarga normal de datos: ");


                return ObtenerEdificios() + ObtenerSecciones() + ObtenerLocalidades() + ObtenerManzanas() + ObtenerImagenes() + ObtenerDibujos();
            }


		}
		catch(Exception ex)
		{
			return -1;
		}

	}


	@Override
	protected void onPostExecute(Integer Result)
	{

		pd.dismiss();

        if(globales.padres== globales.PADRES.MANZANAS)
        {

            listener.onTaskCompleteManzanas(null);
        }
        else
        {
            listener.onTaskCompleteSecciones(null);
        }
	}

	private Integer ObtenerViviendas()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			
			String METHOD_NAME = "ObtenerViviendas";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Vivienda> viviendas;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);


			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			viviendas = (List<Vivienda>)funciones.ParsearViviendas(envelope.getResponse().toString());

			total = viviendas.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarViviendasEnviadas();

			//da.BorrarTodosUsuarios();
			for(Vivienda viv : viviendas)
			{

				dbUtil.InsertarVivienda(viv);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	
	private Integer ObtenerViviendasPadre(String ID_PADRE)
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerViviendasManzana";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			
			int total = 0;
			int i = 0;

			List<Vivienda> viviendas;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("idpadre", ID_PADRE);
			request.addProperty("key", globales.key);


			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			viviendas = (List<Vivienda>)funciones.ParsearViviendas(envelope.getResponse().toString());

			total = viviendas.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarViviendasEnviadasManzana(ID_PADRE);

			//da.BorrarTodosUsuarios();
			for(Vivienda viv : viviendas)
			{

				dbUtil.InsertarVivienda(viv);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}

	private Integer ObtenerHogaresManzana(String ID_PADRE)
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			//pd.setTitle("Descargando Hogares de viviendas ...");
			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ListarHogaresManzanaEntidad";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Hogar> hogares;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("idpadre",ID_PADRE);
			request.addProperty("key", globales.key);
			
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			hogares = funciones.ParsearHogares(envelope.getResponse().toString());

			total = hogares.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarHogaresPadre(ID_PADRE);


			for(Hogar hog : hogares)
			{

				hog.setEnviado(true);
				dbUtil.InsertarHogar(hog);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	private Integer ObtenerHogares()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			//pd.setTitle("Descargando Hogares de viviendas ...");
			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ListarHogares";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Hogar> hogares;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			hogares = (List<Hogar>)funciones.ParsearHogares(envelope.getResponse().toString());

			total = hogares.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarHogares();


			for(Hogar hog : hogares)
			{

				hog.setEnviado(true);
				dbUtil.InsertarHogar(hog);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}
	
	private Integer ObtenerDibujos()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			//pd.setTitle("Descargando Hogares de viviendas ...");
			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerDibujos";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;
			boolean ret = false;
			
			List<Dibujo> dibujos;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			dibujos = (List<Dibujo>)funciones.ParsearDibujos(envelope.getResponse().toString());

			total = dibujos.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarDibujosAntiguos();

			for(Dibujo dib : dibujos)
			{
				dib.setNUEVO(false);
				ret = dbUtil.InsertarDibujo(dib);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}		
	private Integer ObtenerManzanas()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			//pd.setTitle("Descargando Manzanas...");
			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerManzanas";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Manzana> manzanas;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			manzanas = (List<Manzana>)funciones.ParsearManzanas(envelope.getResponse().toString());

			total = manzanas.size();
			pd.setMax(total);
			dbUtil.open();

			dbUtil.BorrarManzanas();

			//da.BorrarTodosUsuarios();

			for(Manzana mz : manzanas)
			{
				dbUtil.InsertarManzana(mz);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}


	private Integer ObtenerLocalidades()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{

			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerLocalidades";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Localidad> localidades;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			localidades = (List<Localidad>)funciones.ParsearLocalidades(envelope.getResponse().toString());

			total = localidades.size();
			pd.setMax(total);
			dbUtil.open();
			dbUtil.BorrarLocalidades();
			//da.BorrarTodosUsuarios();

			for(Localidad loc : localidades)
			{
				dbUtil.InsertarLocalidad(loc);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}


	private Integer ObtenerSecciones()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{

			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerSeccionesUsuario";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Seccion> secciones;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
		//	SSLConnection.allowAllSSL();
			transporte.call(SOAP_ACTION, envelope);
			secciones = (List<Seccion>)funciones.ParsearSecciones(envelope.getResponse().toString());

			total = secciones.size();
			pd.setMax(total);
			dbUtil.open();

			dbUtil.BorrarSecciones();

			for(Seccion sec : secciones)
			{
				dbUtil.InsertarSeccion(sec);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}

	private Integer ObtenerEdificios()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{

			//pd.setTitle("Descargando Edificios...");
			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerEdificios";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Edificio> edificios;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("region_id", globales.region.getRegion_id());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new  HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			edificios = (List<Edificio>)funciones.ParsearEdificios(envelope.getResponse().toString());

			total = edificios.size();
			pd.setMax(total);
			dbUtil.open();

			dbUtil.BorrarEdificios();

			//da.BorrarTodosUsuarios();

			for(Edificio edif : edificios)
			{
				
				dbUtil.InsertarEdificio(edif);
				i++;
				pd.setProgress(i);

			}
			dbUtil.close();



			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}

	private Integer ObtenerImagenes()
	{
		dbUtil = new DataBaseUtil(context);

		try
		{
			//String NAMESPACE = globales.WS_NAMESPACE;
			//String URL="http://10.0.2.2/wsEjemplo/wsEjemplo.asmx";
			//String URL="http://10.0.2.2/wsSyncDirectorioViviendas/wsSyncDV.asmx";

			//String URL="http://marcelo_becerra.ine.cl/wsEjemplo/wsEjemplo.asmx";
			String METHOD_NAME = "ObtenerImagenesCargaUsuario";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Imagen> imagenes;


			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
			request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key", globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			imagenes = (List<Imagen>)funciones.ParsearImagenes(envelope.getResponse().toString());

			total = imagenes.size();
			pd.setMax(total);


			dbUtil.open();
			dbUtil.BorrarImagenes();

			for(Imagen img : imagenes)
			{
				try {

					dbUtil.InsertarImagen(img.getID_VIVIENDA(), img.getIMAGEN());

				} catch (Exception e) {
					e.printStackTrace();
				}

				i++;
				pd.setProgress(i);

			}

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}



	private Integer ObtenerAreasTrabajo(String idpadre)
	{
		dbUtil = new DataBaseUtil(context);

		try
		{

			String METHOD_NAME = "ObtenerAreasTrabajoEnumerador";
			String SOAP_ACTION = globales.WS_NAMESPACE + "/" + METHOD_NAME;
			int total = 0;
			int i = 0;

			List<Area_Trabajo> ats;

			SoapObject request = new SoapObject(globales.WS_NAMESPACE, METHOD_NAME);
			request.addProperty("idpadre", idpadre);
			request.addProperty("act_id", globales.actualizacion.getAct_id());
            request.addProperty("id_usuario", globales.usuario.getId_usuario());
			request.addProperty("key"	, globales.key);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE transporte = new HttpTransportSE(globales.WS_URL);
			transporte.call(SOAP_ACTION, envelope);
			ats = funciones.ParsearAreasTrabajo(envelope.getResponse().toString());

			total = ats.size();
			pd.setMax(total);


			dbUtil.open();
			dbUtil.BorrarAreasTrabajo();

			for(Area_Trabajo at : ats)
			{
				try {

					dbUtil.InsertarAreasTrabajo(at);

				} catch (Exception e) {
					e.printStackTrace();
				}

				i++;
				pd.setProgress(i);

			}

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}

	}


}