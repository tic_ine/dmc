package com.INE.ImasD.EnumeracionMobile.Entities;

public class CodigoAnotacion {
	private int codigo_id;
	private String codigo_glosa;
	
	
	public int getCodigo_id() {
		return codigo_id;
	}
	public void setCodigo_id(int codigo_id) {
		this.codigo_id = codigo_id;
	}
	public String getCodigo_glosa() {
		return codigo_glosa;
	}
	public void setCodigo_glosa(String codigo_glosa) {
		this.codigo_glosa = codigo_glosa;
	}

	
	
	
}
