package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Created by Mbecerra on 19-12-2017.
 */

public class Encuesta implements Serializable
{
    private static final long serialVersionUID = 1L;

    private int idencuesta;
    private int act_id;
    private String nombreencuesta;
    private String tipoencuesta;
    private String json;
    private List<Seccion_Encuesta> secciones;


    public int getIdencuesta() {
        return idencuesta;
    }

    public void setIdencuesta(int idencuesta) {
        this.idencuesta = idencuesta;
    }

    public int getAct_id() {
        return act_id;
    }

    public void setAct_id(int act_id) {
        this.act_id = act_id;
    }

    public String getNombreencuesta() {
        return nombreencuesta;
    }

    public void setNombreencuesta(String nombreencuesta) {
        this.nombreencuesta = nombreencuesta;
    }

    public String getTipoencuesta() {
        return tipoencuesta;
    }

    public void setTipoencuesta(String tipoencuesta) {
        this.tipoencuesta = tipoencuesta;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }


    public List<Seccion_Encuesta> getSecciones() {
        return secciones;
    }

    public void setSecciones(List<Seccion_Encuesta> secciones) {
        this.secciones = secciones;
    }

    public int getTotalPreguntasEncuesta()
    {
        int total = 0;

        for (Seccion_Encuesta sec : getSecciones()) {
            total +=  sec.getTotalPreguntas();
        }

        return  total;
    }

    public int getTotalPreguntasRespondidasEncuesta()
    {
        int total = 0;

        for (Seccion_Encuesta sec : getSecciones()) {
            total += sec.getTotalRespondidas();
        }

        return  total;
    }


}
