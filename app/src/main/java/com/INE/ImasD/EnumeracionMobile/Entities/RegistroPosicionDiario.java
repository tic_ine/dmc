package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.Date;


public class RegistroPosicionDiario 
{

	private String dia;
	private int total_registros;
	private Date mindate;
	private Date maxdate;
	
	public String getDia() {
		return dia;
	}
	
	public void setDia(String dia) {
		this.dia = dia;
	}
	
	
	public int getTotal_registros() {
		return total_registros;
	}

	public void setTotal_registros(int total_registros) {
		this.total_registros = total_registros;
	}

	public Date getMindate() {
		return mindate;
	}
	public void setMindate(Date mindate) {
		this.mindate = mindate;
	}
	public Date getMaxdate() {
		return maxdate;
	}
	public void setMaxdate(Date maxdate) {
		this.maxdate = maxdate;
	}
	
	
}
