package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;
import java.util.UUID;

public class Spin_Edificios_Adapter extends BaseAdapter {

    private Context context;
    private List<Edificio> values;

    public Spin_Edificios_Adapter(Context context, List<Edificio> values) {
        super();
        this.context = context;
        this.values = values;
    }

    public int getCount(){
       return values.size();
    }

    public Edificio getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }

    public void remove(String opc)
	{
    	values.remove(opc);
	}
    
    public int getPositionById(UUID id)
    {
    	int retorno = -1;
    	for(int i = 0; i< values.size();i++)
    	{
    		if(values.get(i).getId_edif().equals(id))
    		{
    			retorno = i;
    			break;
    		}
    	}
		return retorno;
    	
    }
    
    public int getPosition(String str)
    {
    	return values.lastIndexOf(str);
    }
    
    public List<Edificio> getList()
	{
    	return values;
	}

    public void insert(Edificio opc, int idx)
	{
    	values.add(idx, opc);
	}
    
    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    public View getView(int position, View convertView, ViewGroup parent) {
    	 View v = convertView; 
    	 TextView txt;
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_edificio, null);
	        }
	        
	        Edificio edif = getItem(position);
	        txt = (TextView)v.findViewById(R.id.tvNombreEdificio);
	        
	        if(edif.getComuna_id()==0)
	        {
	        	txt.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.cancelar), null, null, null);
	        	txt.setText(edif.getNombre_edif());
	        }
	        else
	        {
	        	txt.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.edificio), null, null, null);
	        	
	        	txt.setText("Edificio " + edif.getNombre_edif());
	        }
	        notifyDataSetChanged();
	        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	
   	 View v = convertView; 
   	 TextView tvIENombreEdificio;
   	 TextView tvIEDireccion;
   	 ImageView imgIEDEdificio;
   	 
    if (convertView == null) 
    { // if it's not recycled, initialize some
        // attributes
		LayoutInflater vi = 
		(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = vi.inflate(R.layout.item_edificio_dropdown, null);
    }
    
    Edificio edif = getItem(position);
    tvIENombreEdificio = (TextView)v.findViewById(R.id.tvIENombreEdificio);
    tvIEDireccion = (TextView)v.findViewById(R.id.tvIEDireccion);
    imgIEDEdificio = (ImageView)v.findViewById(R.id.imgIEDEdificio);
    if(edif.getComuna_id()==0)
    {
    	imgIEDEdificio.setImageResource(R.drawable.cancelar);
    	tvIENombreEdificio.setText(edif.getNombre_edif());
    	tvIEDireccion.setText("N/A");
    }
    else
    {
    	imgIEDEdificio.setImageResource(R.drawable.edificio);
        tvIENombreEdificio.setText("Edificio " + edif.getNombre_edif());
        tvIEDireccion.setText(edif.getCalle() + " N�" + edif.getNumero());
    }
    notifyDataSetChanged();
    return v;

    }
}
