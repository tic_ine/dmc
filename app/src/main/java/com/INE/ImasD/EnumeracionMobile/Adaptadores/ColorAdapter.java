package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;


public class ColorAdapter extends ArrayAdapter<String> {
	Context ctxt;	
	
    public ColorAdapter(Context context, List<String> colors) {
       super(context, 0, colors);
       this.ctxt = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // Get the data item for this position
       String color = getItem(position);    
       // Check if an existing view is being reused, otherwise inflate the view
       if (convertView == null) {
          convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_solo_string, parent, false);
       }

       TextView tvcolor = (TextView) convertView.findViewById(R.id.tvitemsolostring);

       if(color.equals("AMARILLO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Yellow));
       	   tvcolor.setTextColor(this.ctxt.getResources().getColor(R.color.Black));
       }
       else if (color.equals("VERDE OSCURO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.DarkGreen));
       }
       else if (color.equals("CAF� CLARO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Beige));
       }       
       else if (color.equals("CAF�"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Brown));
       }
       else if (color.equals("GRIS"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Gray));
       }
       else if (color.equals("BLANCO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.White));
       }
       else if (color.equals("NEGRO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Black));
    	   tvcolor.setTextColor(this.ctxt.getResources().getColor(R.color.White));
       }
       else if (color.equals("CELESTE"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.LightBlue));
       }
       else if (color.equals("AZUL OSCURO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.DarkBlue));
       	   tvcolor.setTextColor(this.ctxt.getResources().getColor(R.color.White));    	   
       }
       else if (color.equals("AZUL"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Blue));
       	   tvcolor.setTextColor(this.ctxt.getResources().getColor(R.color.White));    	   
       }
       else if (color.equals("VERDE"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Green));
       }
       else if (color.equals("NARANJA"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Orange));
       }
       else if (color.equals("VIOLETA"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Violet));
       }
       else if (color.equals("MORADO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Purple));
       }
       else if (color.equals("VERDE CLARO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.LightGreen));
       }
       else if (color.equals("CHOCOLATE"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Chocolate));
       }
       else if (color.equals("VERDE AMARILLENTO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.GreenYellow));
       }
       else if (color.equals("ROJO"))
       {
    	   tvcolor.setBackgroundColor(this.ctxt.getResources().getColor(R.color.Red));
       }
       
	   tvcolor.setText(color);
	   
	   
       return convertView;
   }
}