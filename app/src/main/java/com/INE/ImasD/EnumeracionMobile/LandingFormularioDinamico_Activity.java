package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Encuesta;
import com.INE.ImasD.EnumeracionMobile.Entities.Opcion_Pregunta;
import com.INE.ImasD.EnumeracionMobile.Entities.Pregunta;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion_Encuesta;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LandingFormularioDinamico_Activity extends Activity {
    Encuesta encuesta;
    TextView tvLFD_Encuesta;
    Button btnLFD_IrEncuesta;
    private int REQUEST_NUEVA_ENCUESTA = 2000;

    int TotalPreguntas = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_formulario_dinamico_activity);

        tvLFD_Encuesta = (TextView)findViewById(R.id.tvLFD_Encuesta);
        btnLFD_IrEncuesta = (Button) findViewById(R.id.btnLFD_IrEncuesta );


        btnLFD_IrEncuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                funciones.CrearCopiaEncuesta(LandingFormularioDinamico_Activity.this, encuesta, UUID.randomUUID(), 1,1);
                Intent i = new Intent(LandingFormularioDinamico_Activity.this, Formulario_Dinamico_Activity.class);
                i.putExtra("encuesta", encuesta);
                i.putExtra("TotalPreguntas", TotalPreguntas);
                startActivityForResult(i, REQUEST_NUEVA_ENCUESTA);
            }
        });

        String json = LeerTxt();//ObtenerJSON();


        tvLFD_Encuesta.setText(json);

        try {
            JSONObject jObject = new JSONObject(json);
            encuesta = funciones.ParsearEncuesta(jObject);

        }
        catch (Exception ex)
        {
            Toast.makeText(LandingFormularioDinamico_Activity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String LeerTxt()
    {

        File sdcard = Environment.getExternalStorageDirectory();

        File file = new File(sdcard,"token.txt");

        StringBuilder text = new StringBuilder();

        try
        {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null)
            {
                text.append(line);
                text.append('\n');
            }
            br.close();

            return  text.toString();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
            return  "";
        }
    }

    /*
    private void ParsearEncuesta(JSONObject obj)
    {
        try
        {

            encuesta = new Encuesta();
            encuesta.setIdencuesta(obj.getInt("IdEncuesta"));
            encuesta.setNombreencuesta(obj.getString("NombreEncuesta"));
            encuesta.setTipoencuesta(obj.getString("TipoEncuesta"));

            encuesta.setAct_id(obj.getInt("Act_Id"));

            JSONArray jArray = obj.getJSONArray("Secciones");

            encuesta.setSecciones(ParsearSeccion(encuesta, jArray));
            encuesta.setJson(obj.toString(4));

        }
        catch(Exception ex)
        {

        }
    }



    private List<Seccion_Encuesta> ParsearSeccion(Encuesta enc, JSONArray jsecciones)
    {

        List<Seccion_Encuesta> seccion_encuestas = new ArrayList<Seccion_Encuesta>();
        Seccion_Encuesta sec;

        try
        {
            for(int n = 0; n < jsecciones.length(); n++)
            {
                sec = new Seccion_Encuesta();
                JSONObject object = jsecciones.getJSONObject(n);

                sec.setNombreseccion(object.getString("NombreSeccion"));
                sec.setAyudaseccion(object.getString("AyudaSeccion"));

                JSONArray jPreguntas = object.getJSONArray("Preguntas");

                sec.setPreguntas(ParsearPreguntas(sec, jPreguntas));

                TotalPreguntas = TotalPreguntas + jPreguntas.length();
                seccion_encuestas.add(sec);
            }
        }
        catch(Exception ex)
        {

        }
        return seccion_encuestas;
    }

    private List<Pregunta> ParsearPreguntas(Seccion_Encuesta sec, JSONArray jpreguntas)
    {

        List<Pregunta> preguntas = new ArrayList<Pregunta>();
        Pregunta preg;

        try
        {
            for(int n = 0; n < jpreguntas.length(); n++)
            {
                preg = new Pregunta();
                JSONObject object = jpreguntas.getJSONObject(n);

                preg.setCodigoPregunta(object.getInt("Codigo"));
                preg.setDescripcion(object.getString("Descripcion"));
                preg.setAyuda(object.getString("Ayuda"));
                preg.setRegex(object.getString("Regex"));
                preg.setEstilo(object.getString("Estilo"));

                preg.setTipo(object.getString("TipoElemento"));

                preg.setModificable(object.getString("Modificable").contains("Si") ? true: false);
                preg.setRequerido(object.getString("Requerido").contains("Si") ? true: false);

                preg.setLargomaximo(object.getInt("Largo"));
                preg.setValorminimo(object.getInt("ValorMinimo"));
                preg.setValormaximo(object.getInt("ValorMaximo"));

                JSONArray jValores = object.getJSONArray("Valores");
                preg.setValores(ParsearValores(preg, jValores));

                JSONArray jOpciones = object.getJSONArray("Opciones");
                preg.setOpciones(ParsearOpciones(preg, jOpciones));

                preg.setRespondida(object.getString("Respondida").contains("Si") ? true: false);
                preg.setOpcionOtro(object.getString("OpcionOtro").contains("Si") ? true: false);
                preg.setTextoOtro(object.getString("TextoOtro"));

                preg.setHintTexto(object.getString("HintTexto"));

                preguntas.add(preg);

            }

            sec.setPreguntas(preguntas);


        }
        catch(Exception ex)
        {

        }
        return preguntas;
    }

    private List<String> ParsearValores(Pregunta preg, JSONArray jpreguntas)
    {

        List<String> valores = new ArrayList<String>();

        try
        {
            for(int n = 0; n < jpreguntas.length(); n++)
            {

                valores.add(jpreguntas.get(n).toString());
            }

            return valores;
        }
        catch(Exception ex)
        {
            return valores;
        }
    }


    private List<Opcion_Pregunta>  ParsearOpciones(Pregunta preg, JSONArray jOpciones)
    {

        List<Opcion_Pregunta> opciones = new ArrayList<Opcion_Pregunta>();

        Opcion_Pregunta opc;

        try
        {

            for(int n = 0; n < jOpciones.length(); n++)
            {

                opc = new Opcion_Pregunta();
                JSONObject object = jOpciones.getJSONObject(n);
                opc.setCodigo_pregunta(preg.getCodigoPregunta());
                opc.setCodigo_opcion(object.getString("Codigo"));
                opc.setGlosa_opcion(object.getString("Valor"));
                opciones.add(opc);
            }

            return opciones;

        }
        catch(Exception ex)
        {
            return opciones;
        }
    }

    */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if (requestCode == REQUEST_NUEVA_ENCUESTA && resultCode==RESULT_OK) {

/*
            encuesta = (Encuesta)data.getSerializableExtra("encuesta");
            tvLFD_Encuesta.setText(funciones.EncuestaToJSON(encuesta));
            */


        }
    }
}
