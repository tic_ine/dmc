package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Created by Mbecerra on 04-01-2018.
 */

public class EncuestaMovil implements Serializable
{
    private static final long serialVersionUID = 1L;

    private int idencuesta;
    private int act_id;
    private int id_usuario;
    private String nombreencuesta;
    private String tipoencuesta;
    private String idpadre;
    private UUID id_vivienda;
    private int hogar;
    private int persona;
    private List<Seccion_Encuesta> secciones;
    private String json;
    private boolean original;
    private UUID idEncuestaMovil;

    public int getIdencuesta() {
        return idencuesta;
    }

    public void setIdencuesta(int idencuesta) {
        this.idencuesta = idencuesta;
    }

    public int getAct_id() {
        return act_id;
    }

    public void setAct_id(int act_id) {
        this.act_id = act_id;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombreencuesta() {
        return nombreencuesta;
    }

    public void setNombreencuesta(String nombreencuesta) {
        this.nombreencuesta = nombreencuesta;
    }

    public String getTipoencuesta() {
        return tipoencuesta;
    }

    public void setTipoencuesta(String tipoencuesta) {
        this.tipoencuesta = tipoencuesta;
    }

    public String getIdpadre() {
        return idpadre;
    }

    public void setIdpadre(String idpadre) {
        this.idpadre = idpadre;
    }

    public UUID getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(UUID id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public int getHogar() {
        return hogar;
    }

    public void setHogar(int hogar) {
        this.hogar = hogar;
    }

    public int getPersona() {
        return persona;
    }

    public void setPersona(int persona) {
        this.persona = persona;
    }

    public List<Seccion_Encuesta> getSecciones() {
        return secciones;
    }

    public void setSecciones(List<Seccion_Encuesta> secciones) {
        this.secciones = secciones;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public boolean isOriginal() {
        return original;
    }

    public void setOriginal(boolean original) {
        this.original = original;
    }

    public UUID getIdEncuestaMovil() {
        return idEncuestaMovil;
    }

    public void setIdEncuestaMovil(UUID idEncuestaMovil) {
        this.idEncuestaMovil = idEncuestaMovil;
    }
}
