package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Opcion_Pregunta;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Mbecerra on 20-12-2017.
 */

public class OpcionPregunta_Adapter extends ArrayAdapter<Opcion_Pregunta> {

    Context context;
    List<Opcion_Pregunta> mList, mListAll;

    public OpcionPregunta_Adapter(Context context,int resource,
                                  List<Opcion_Pregunta> mList) {
        super(context, 0 , mList);
        this.context = context;
        this.mList = mList;
        mListAll = mList;
    }

    @Override
    public Opcion_Pregunta getItem(int position) {

        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }


    public int getPositionById(String id)
    {
        int retorno = -1;
        for(int i = 0; i< mList.size();i++)
        {
            if(id.equals(String.valueOf(mList.get(i).getCodigo_opcion())))
            {
                retorno = i;
                break;
            }
        }
        return retorno;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.item_solo_string, parent, false);
            TextView textView = (TextView) view.findViewById(R.id.tvitemsolostring);

            textView.setTextColor(Color.BLACK);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);

            textView.setText(mList.get(position).getGlosa_opcion());
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        TextView txt;
        if (convertView == null)
        { // if it's not recycled, initialize some
            // attributes
            LayoutInflater vi =
                    (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_solo_string, null);
        }
        txt = (TextView)v.findViewById(R.id.tvitemsolostring);
        txt.setText(getItem(position).getGlosa_opcion());

        return v;

    }
}