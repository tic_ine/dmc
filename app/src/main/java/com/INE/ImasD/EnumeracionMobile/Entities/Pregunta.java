package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mbecerra on 19-12-2017.
 */

public class Pregunta implements Serializable {
    private static final long serialVersionUID = 1L;

    private int idencuesta;
    private int act_id;
    private int id_usuario;

    private int codigo_pregunta;
    private String descripcion;
    private String ayuda;
    private String regex;
    private String estilo;
    private List<String> valores;
    private String tipo;
    private List<Opcion_Pregunta> opciones;
    private boolean modificable;
    private boolean requerido;

    private int largomaximo;
    private int valorminimo;
    private int valormaximo;

    private boolean opcionOtro;
    private boolean respondida;
    private String textoOtro;

    private String hintTexto;

    public int getCodigoPregunta() {
        return codigo_pregunta;
    }

    public void setCodigoPregunta(int codigo) {
        this.codigo_pregunta = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAyuda() {
        return ayuda;
    }

    public void setAyuda(String ayuda) {
        this.ayuda = ayuda;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public List<String> getValores() {
        return valores;
    }

    public void setValores(List<String> valores) {
        this.valores = valores;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<Opcion_Pregunta> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<Opcion_Pregunta> opciones) {
        this.opciones = opciones;
    }

    public boolean isModificable() {
        return modificable;
    }

    public void setModificable(boolean modificable) {
        this.modificable = modificable;
    }

    public boolean isRequerido() {
        return requerido;
    }

    public void setRequerido(boolean requerido) {
        this.requerido = requerido;
    }


    public int getLargomaximo() {
        return largomaximo;
    }

    public void setLargomaximo(int largomaximo) {
        this.largomaximo = largomaximo;
    }

    public int getValorminimo() {
        return valorminimo;
    }

    public void setValorminimo(int valorminimo) {
        this.valorminimo = valorminimo;
    }

    public int getValormaximo() {
        return valormaximo;
    }

    public void setValormaximo(int valormaximo) {
        this.valormaximo = valormaximo;
    }


    public boolean isOpcionOtro() {
        return opcionOtro;
    }

    public void setOpcionOtro(boolean opcionOtro) {
        this.opcionOtro = opcionOtro;
    }

    public boolean isRespondida() {
        return respondida;
    }

    public void setRespondida(boolean respondida) {
        this.respondida = respondida;
    }

    public String getOtro() {
        return textoOtro;
    }

    public void setTextoOtro(String textoOtro) {
        this.textoOtro = textoOtro;
    }

    public String getHintTexto() {
        return hintTexto;
    }

    public void setHintTexto(String hintTexto) {
        this.hintTexto = hintTexto;
    }
}