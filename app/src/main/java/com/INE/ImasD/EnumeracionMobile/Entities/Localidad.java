package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.Date;
import java.util.UUID;

public class Localidad {

	/*
	 ENTIDAD_ID
	 COMUNA_ID
	 DISTRITO
	 NOMBRELOCALIDAD
	 NOMBREENTIDAD
	 CAT_ID
	 CAT_GLOSA
	 ACT_ID
	 ID_USUARIO
	 FECHA_CREACION
	 CENTROIDE_LAT
	 CENTROIDE_LNG
	 
	 * */
	
	private UUID loc_id;
	private String loc_glosa;	
	private int comuna_id;
	private int distrito;
	private double centroide_lat;
	private double centroide_lng;	

	private int act_id;
	private int id_usuario;
	private Date fecha_creacion;
	private int nueva;
	
	
	public UUID getLoc_id() {
		return loc_id;
	}
	public void setLoc_id(UUID loc_id) {
		this.loc_id = loc_id;
	}
	public String getLoc_glosa() {
		return loc_glosa;
	}
	public void setLoc_glosa(String loc_glosa) {
		this.loc_glosa = loc_glosa;
	}
	public int getComuna_id() {
		return comuna_id;
	}
	public void setComuna_id(int comuna_id) {
		this.comuna_id = comuna_id;
	}
	public int getDistrito() {
		return distrito;
	}
	public void setDistrito(int distrito) {
		this.distrito = distrito;
	}
	public double getCentroide_lat() {
		return centroide_lat;
	}
	public void setCentroide_lat(double centroide_lat) {
		this.centroide_lat = centroide_lat;
	}
	public double getCentroide_lng() {
		return centroide_lng;
	}
	public void setCentroide_lng(double centroide_lng) {
		this.centroide_lng = centroide_lng;
	}
	public int getAct_id() {
		return act_id;
	}
	public void setAct_id(int act_id) {
		this.act_id = act_id;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Date getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	public int getNueva() {
		return nueva;
	}
	public void setNueva(int nueva) {
		this.nueva = nueva;
	}	
	
	
	
}
