package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class DescargaUPM_Secciones_Adapter extends BaseAdapter{


	    private Context context;
	    // Your custom values for the spinner (User)
	    public List<Seccion> values;
	    private boolean IsAllChecked;
	    
	    
	    public DescargaUPM_Secciones_Adapter(Context context, List<Seccion> secciones) {
	        super();
	        this.context = context;
	        this.values = secciones;
	    }
	    
	    public int getCount(){
	        return values.size();
	     }

	     public Seccion getItem(int position){
	        return values.get(position);
	     }

	     public long getItemId(int position){
	        return position;
	     }

	     public void remove(Manzana mz)
	 	{
	     	values.remove(mz);
	 	}
	     
	     public List<Seccion> getList()
	 	{
	     	return values;
	 	}

	     public void insert(Seccion sec, int idx)
	 	{
	     	values.add(idx, sec);
	 	}
	     
	     public void CheckearTodos(boolean checkear)
	     {
	    	 	IsAllChecked = checkear;
	    	 	
	    	 	for(Seccion sec : values)
	    	 	{
	    	 		sec.setChecked(checkear);
	    	 	}
	    	 	
				notifyDataSetChanged();
	     }

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		   	View v = convertView; 
		   	TextView txt1;
		   	TextView txt2;
		   	TextView tvDUPM_GlosaAreaUPM;
		   	CheckBox chkDUPM;
		   	final int pos = position;
		   	
		   	Seccion sec = getItem(position);
		   	
			        if (convertView == null) 
			        { // if it's not recycled, initialize some
			            // attributes
						LayoutInflater vi = 
						(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						v = vi.inflate(R.layout.item_upm_descarga, null);
						
			        }
			        
			        tvDUPM_GlosaAreaUPM = (TextView)v.findViewById(R.id.tvDUPM_GlosaAreaUPM);
			        txt1 = (TextView)v.findViewById(R.id.tvDUPM_PadreInfoGeografica1);
			        txt2 = (TextView)v.findViewById(R.id.tvDUPM_PadreInfoGeografica2);
			        
			        chkDUPM = (CheckBox)v.findViewById(R.id.chkDUPM);
			        chkDUPM.setChecked(sec.isChecked());
			        
		        	v.setBackgroundResource(R.drawable.rounded_corner_seccion);
		        	tvDUPM_GlosaAreaUPM.setText("CU_SEC:" + sec.getCu_seccion());
			        txt1.setText(sec.getComuna_glosa()  +  " - RPC:" + sec.getCut() + " - Distritos: " + sec.getCod_distrito());
			        txt2.setText("Estrato Geo:" + sec.getEstrato_geo() + " - Cod Sec: " + sec.getCodigo_seccion());
			        
			        
	 		        chkDUPM.setOnCheckedChangeListener(new OnCheckedChangeListener() {
						
						@Override
						public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
							// TODO Auto-generated method stub
							   	Seccion sec = getItem(pos);
							   	sec.setChecked(isChecked);
							   	//notifyDataSetChanged();
						   	
						}
					});
			        return v;

		    }
	}

