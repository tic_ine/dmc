package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.Date;
import java.util.UUID;

public class Dibujo {

	private UUID ID_DIBUJO;
	private String DIB_TEXT;
	private int ACT_ID;
	private int ID_USUARIO;
	private String IDPADRE;
	private String DIB_GLOSA;
	private boolean NULO;
	private Date FECHA_CREACION;
	private String DIB_TIPO;
	private int DIB_ESTADO;
	private boolean NUEVO;
	
	public UUID getID_DIBUJO() {
		return ID_DIBUJO;
	}
	public void setID_DIBUJO(UUID iD_DIBUJO) {
		ID_DIBUJO = iD_DIBUJO;
	}
	public String getDIB_TEXT() {
		return DIB_TEXT;
	}
	public void setDIB_TEXT(String dIB_TEXT) {
		DIB_TEXT = dIB_TEXT;
	}
	public int getACT_ID() {
		return ACT_ID;
	}
	public void setACT_ID(int aCT_ID) {
		ACT_ID = aCT_ID;
	}
	public int getID_USUARIO() {
		return ID_USUARIO;
	}
	public void setID_USUARIO(int iD_USUARIO) {
		ID_USUARIO = iD_USUARIO;
	}

	public String getIDPADRE() {
		return IDPADRE;
	}
	public void setIDPADRE(String iDPADRE) {
		IDPADRE = iDPADRE;
	}
	public String getDIB_GLOSA() {
		return DIB_GLOSA;
	}
	public void setDIB_GLOSA(String dIB_GLOSA) {
		DIB_GLOSA = dIB_GLOSA;
	}
	public boolean isNULO() {
		return NULO;
	}
	public void setNULO(boolean nULO) {
		NULO = nULO;
	}
	public Date getFECHA_CREACION() {
		return FECHA_CREACION;
	}
	public void setFECHA_CREACION(Date fECHA_CREACION) {
		FECHA_CREACION = fECHA_CREACION;
	}
	public boolean isNUEVO() {
		return NUEVO;
	}
	public void setNUEVO(boolean nUEVO) {
		NUEVO = nUEVO;
	}
	public String getDIB_TIPO() {
		return DIB_TIPO;
	}
	public void setDIB_TIPO(String dIB_TIPO) {
		DIB_TIPO = dIB_TIPO;
	}
	public int getDIB_ESTADO() {
		return DIB_ESTADO;
	}
	public void setDIB_ESTADO(int dIB_ESTADO) {
		DIB_ESTADO = dIB_ESTADO;
	}
	
	
	
	
}
