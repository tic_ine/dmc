package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.RegistroPosicionDiario;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.R;

import org.joda.time.DateTime;

import java.util.List;

public class Spin_RegistroPosicion_Adapter extends BaseAdapter{
	//Your sent context
	private Context context;
	//Your custom values for the spinner (User)
	private List<RegistroPosicionDiario> values;


	public Spin_RegistroPosicion_Adapter(Context context, List<RegistroPosicionDiario> registros) {
		super();
		this.context = context;
		this.values = registros;
	}

	public int getCount(){
		return values.size();
	}

	public RegistroPosicionDiario getItem(int position){
		return values.get(position);
	}

	public long getItemId(int position){
		return position;
	}



    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    public View getView(int position, View convertView, ViewGroup parent) {
    	/*
    	 View v = convertView; 
    	 TextView txt;
    	 RegistroPosicionDiario reg = getItem(position);
    	   	
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_dia_registro_posicion_passive, null);
	        }
	        
	        txt = (TextView)v.findViewById(R.id.tvDia_RegPos);
	        //txt.setCompoundDrawables(context.getResources().getDrawable(R.drawable.bluepushpin32), null, null, null);

		        txt.setText(reg.getDia());

	        return v;
	        */
    	
      	 View v = convertView; 
        	TextView tvDia_RegPos;
       	TextView tvTotalPuntos_RegPos;
       	TextView tvFechaDesde_RegPos;
       	TextView tvFechaHasta_RegPos;
        	
        		RegistroPosicionDiario reg = getItem(position);
        	
             if (convertView == null) 
             { // if it's not recycled, initialize some
                 // attributes
     			LayoutInflater vi = 
     			(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
     			v = vi.inflate(R.layout.item_dia_registro_posicion_dropdown, null);
     			
             }
             
             tvDia_RegPos = (TextView)v.findViewById(R.id.tvDia_RegPos);
             tvTotalPuntos_RegPos = (TextView)v.findViewById(R.id.tvTotalReg_RegPos);
             tvFechaDesde_RegPos = (TextView)v.findViewById(R.id.tvFechaDesdeRegPos);
             tvFechaHasta_RegPos = (TextView)v.findViewById(R.id.tvFechaHastaRegPos);
             
             tvDia_RegPos.setText("D�a: " + reg.getDia());
             tvTotalPuntos_RegPos.setText(String.valueOf(reg.getTotal_registros()));
             tvFechaDesde_RegPos.setText("Desde: " + funciones.FormatearFecha(new DateTime(reg.getMindate().getTime())));
             tvFechaHasta_RegPos.setText("Hasta: " + funciones.FormatearFecha(new DateTime(reg.getMaxdate().getTime())));

             return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    /*
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	
   	 View v = convertView; 
   	TextView tvDia_RegPos;
  	TextView tvTotalPuntos_RegPos;
  	TextView tvFechaDesde_RegPos;
  	TextView tvFechaHasta_RegPos;
   	
   		RegistroPosicionDiario reg = getItem(position);
   	
        if (convertView == null) 
        { // if it's not recycled, initialize some
            // attributes
			LayoutInflater vi = 
			(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.item_dia_registro_posicion_dropdown, null);
			
        }
        
        tvDia_RegPos = (TextView)v.findViewById(R.id.tvDia_RegPos);
        tvTotalPuntos_RegPos = (TextView)v.findViewById(R.id.tvTotalReg_RegPos);
        tvFechaDesde_RegPos = (TextView)v.findViewById(R.id.tvFechaDesde_RegPos);
        tvFechaHasta_RegPos = (TextView)v.findViewById(R.id.tvFechaHasta_RegPos);
        
        tvDia_RegPos.setText("D�a: " + reg.getDia());
        tvTotalPuntos_RegPos.setText(String.valueOf(reg.getTotal_registros()));
        tvFechaDesde_RegPos.setText("Desde: " + funciones.FormatearFecha(new DateTime(reg.getMindate().getTime())));
        tvFechaHasta_RegPos.setText("Hasta: " + funciones.FormatearFecha(new DateTime(reg.getMaxdate().getTime())));

        return v;

    }
 */   
}
