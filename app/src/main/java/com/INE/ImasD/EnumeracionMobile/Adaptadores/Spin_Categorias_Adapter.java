package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class Spin_Categorias_Adapter  extends BaseAdapter {

    private Context context;
    private List<Categoria> values;

    public Spin_Categorias_Adapter(Context context, List<Categoria> categorias) {
        super();
        this.context = context;
        this.values = categorias;
    }

    public int getCount(){
       return values.size();
    }

    public Categoria getItem(int position){
       return values.get(position);
    }

    public long getItemId(int position){
       return position;
    }

    public int getPositionById(int id)
    {
    	int retorno = -1;
    	for(int i = 0; i< values.size();i++)
    	{
    		if(values.get(i).getCat_id()==id)
    		{
    			retorno = i;
    			break;
    		}
    	}
		return retorno;
    	
    }

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    public View getView(int position, View convertView, ViewGroup parent) {
    	 View v = convertView; 
    	 TextView txt;
	        if (convertView == null) 
	        { 
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_codigoanotacion, null);
				

	        }
	        txt = (TextView)v.findViewById(R.id.tvTextoCodigoAnotacion);
	        txt.setText(getItem(position).getCat_glosa());
	        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	
   	 View v = convertView; 
   	 TextView txt;
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_solo_string, null);
	        }
	        txt = (TextView)v.findViewById(R.id.tvitemsolostring);
	        txt.setText(getItem(position).getCat_glosa());
	        
	        return v;

    }
    
}