package com.INE.ImasD.EnumeracionMobile.Entities;

public class Categoria {
	private int cat_id;
	private String cat_glosa;
	
	
	public int getCat_id() {
		return cat_id;
	}
	public void setCat_id(int cat_id) {
		this.cat_id = cat_id;
	}
	public String getCat_glosa() {
		return cat_glosa;
	}
	public void setCat_glosa(String cat_glosa) {
		this.cat_glosa = cat_glosa;
	}

	
	
}
