package com.INE.ImasD.EnumeracionMobile.OtrasClases;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Actualizacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Area_Trabajo;
import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.Entities.CategoriaPOI;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Dibujo;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Encuesta;
import com.INE.ImasD.EnumeracionMobile.Entities.EncuestaMovil;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Localidad;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Opcion_Pregunta;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.Pregunta;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion_Encuesta;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Usuario;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.File_Browser_Activity;
import com.INE.ImasD.EnumeracionMobile.R;
import com.esri.core.geometry.Point;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.lang.String;



public class funciones
{


	public static boolean isNumeric(String s) {
		return s != null && s.matches("[-+]?\\d*\\.?\\d+");
	}
	public static List<Usuario> ParsearUsuarios(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Usuarios");
		List<Usuario> misusuarios = new ArrayList<Usuario>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Usuario");

			Usuario usu = new Usuario(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("ID_USUARIO"))
					usu.setId_usuario(Integer.valueOf(text));

				else if (name.equals("USERNAME"))
					usu.setUsername(text);

				else if (name.equals("CONTRASENA"))
					usu.setContrasena(text);

				else if (name.equals("NOMBRES"))
					usu.setNombres(text);

				else if (name.equals("APELLIDOS"))
					usu.setApellidos(text);

				else if (name.equals("EMAIL"))
					usu.setEmail(text);

				else if (name.equals("VIGENTE"))
					usu.setVigente(Integer.valueOf(text));                    
				else if (name.equals("PERFIL_ID"))
					usu.setPerfil_id(Integer.valueOf(text));      
			}
			i +=1;
			misusuarios.add(usu);
		}
		return misusuarios;
	}

	public static List<Actualizacion> ParsearActualizaciones(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Actualizaciones");
		List<Actualizacion> misactualizaciones = new ArrayList<Actualizacion>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Actualizacion");

			Actualizacion act = new Actualizacion(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("ACT_ID"))
					act.setAct_id(Integer.valueOf(text));

				else if (name.equals("ACT_GLOSA"))
					act.setAct_glosa(text);
				else if (name.equals("ACTIVA"))
					act.setActiva(Boolean.valueOf(text));
				else if (name.equals("FECHAHORA"))
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					java.util.Date fecha = new java.util.Date();
					fecha = sdf.parse(text);
					act.setFechahora(fecha);
				}
				else if (name.equals("REGION_ID"))
					act.setRegion_id(Integer.valueOf(text));				

			}
			i +=1;
			misactualizaciones.add(act);
		}
		return misactualizaciones;
	}


	public static List<Vivienda> ParsearViviendas( String XMLDocument) throws Exception

	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Viviendas");
		List<Vivienda> viviendas = new ArrayList<Vivienda>();



		try{
			while(parser.nextTag() != XmlPullParser.END_TAG)
			{
				parser.require(XmlPullParser.START_TAG, null, "Vivienda");

				Vivienda viv = new Vivienda();

				while (parser.nextTag() != XmlPullParser.END_TAG)
				{
					String name = parser.getName();

					String text = parser.nextText();

					if (i == 11 && name.equals("ID_VIVIENDA") ){
						//Log.d("asd",name);
						Log.d( "asdasd",text);
					}
					/*
					if (i >249 && name.equals("AT_ID") ){
						//Log.d("asd",name);
						Log.d( "AT ERROR",text);
					}
					*/
					if (name.equals("ACT_ID"))
						viv.setACT_ID(Integer.valueOf(text));


					else  if (name.equals("ID_USUARIO"))
						viv.setID_USUARIO(Integer.valueOf(text));

					else if (name.equals("IDPADRE"))
						viv.setIDPADRE(text);

					else if (name.equals("ID_VIVIENDA"))
						viv.setID_VIVIENDA(UUID.fromString(text));

					else if (name.equals("TIPOCALLE_GLOSA"))
						viv.setTIPOCALLE_GLOSA(text);

					else if (name.equals("NOMBRE_CALLE_CAMINO"))
						viv.setNOMBRE_CALLE_CAMINO(text);

					else if (name.equals("N_DOMICILIO"))
						viv.setN_DOMICILIO(text);

					else if (name.equals("N_LETRA_BLOCK"))
						viv.setN_LETRA_BLOCK(text);

					else if (name.equals("N_PISO"))
						viv.setN_PISO(text);

					else if (name.equals("N_LETRA_DEPTO"))
						viv.setN_LETRA_DEPTO(text);

					else if (name.equals("ID_ESE"))
						viv.setID_ESE(Integer.valueOf(text));

					else if (name.equals("ID_USODESTINO"))
						viv.setID_USODESTINO(Integer.valueOf(text));

					else if (name.equals("NULO"))
						viv.setNULO(Boolean.valueOf(text));

					else if (name.equals("FECHAINGRESO"))
					{

						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						java.util.Date fecha = new java.util.Date();
						fecha = sdf.parse(text);
						viv.setFECHAINGRESO(fecha);

					}

					else if (name.equals("COORD_X"))
						viv.setCOORD_X(Double.valueOf(text));

					else if (name.equals("COORD_Y"))
						viv.setCOORD_Y(Double.valueOf(text));


					else if (name.equals("DESCRIPCION")){
						viv.setDESCRIPCION(text);
					}


					else if (name.equals("ID_ORIGEN"))
						viv.setID_ORIGEN(Integer.valueOf(text));


					else if (name.equals("FECHAMODIFICACION"))
					{
						if (text != "")
						{
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							java.util.Date fecha = new java.util.Date();
							fecha = sdf.parse(text);
							viv.setFECHAINGRESO(fecha);
						}
					}

					else if (name.equals("FECHAELIMINACION"))
					{
						if (text != "")
						{
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							java.util.Date fecha = new java.util.Date();
							fecha = sdf.parse(text);
							viv.setFECHAINGRESO(fecha);
						}
					}

					else if (name.equals("USUARIO_ID_MODIFICACION"))
					{
						if (text != "")
						{
							viv.setUSUARIO_ID_MODIFICACION(Integer.valueOf(text));
						}
					}
					else if (name.equals("USUARIO_ID_ELIMINACION"))
					{
						if (text != "")
						{
							viv.setUSUARIO_ID_ELIMINACION(Integer.valueOf(text));
						}
					}

					else if (name.equals("USUARIO_ID_INGRESO"))
						viv.setUSUARIO_ID_INGRESO(Integer.valueOf(text));

					else if (name.equals("ORDEN"))
						viv.setORDEN(Integer.valueOf(text));

					else if (name.equals("ORDEN_VIV"))
						viv.setORDEN_VIV(Integer.valueOf(text));

					else if (name.equals("TIPOCALLE_ID"))
						viv.setTIPOCALLE_ID(text);

					else if (name.equals("CODIGO_ID"))
						viv.setCODIGO_ID(Integer.valueOf(text));

					else if (name.equals("POSICION"))
						viv.setPOSICION(Integer.valueOf(text));
				/*
				+ " VIVTEMP_ID integer null,"
				+ " VIVTEMP_GLOSA text null,"
				+ " KILOMETRO text null,"
				+ " LOTE text null,"
				+ " SITIO text null,"
				+ " LETRAPARCELA text null,"
				+ " ORDENEDIFICACION integer null,"
				+ " UBICACIONHORIZONTAL text null,"
				+ " UBICACIONVERTICAL text null,"
				 * */

					else if (name.equals("VIVTEMP_ID"))
						viv.setVIVTEMP_ID(Integer.valueOf(text));
					else if (name.equals("VIVTEMP_GLOSA"))
						viv.setVIVTEMP_GLOSA(text);
					else if (name.equals("KILOMETRO"))
						viv.setKILOMETRO(text);
					else if (name.equals("LOTE"))
						viv.setLOTE(text);
					else if (name.equals("SITIO"))
						viv.setSITIO(text);
					else if (name.equals("LETRAPARCELA"))
						viv.setLETRAPARCELA(text);
					else if (name.equals("ORDENEDIFICACION"))
						viv.setORDENEDIFICACION(Integer.valueOf(text));
					else if (name.equals("UBICACIONHORIZONTAL"))
						viv.setUBICACIONHORIZONTAL(text);
					else if (name.equals("UBICACIONVERTICAL"))
						viv.setUBICACIONVERTICAL(text);
					else if (name.equals("ID_EDIF"))
						viv.setID_EDIF(UUID.fromString(text));

					else if (name.equals("NOMBRELOCALIDAD"))
						viv.setNOMBRELOCALIDAD(text);
					else if (name.equals("NOMBREENTIDAD"))
						viv.setNOMBREENTIDAD(text);

					else if (name.equals("CAT_ID"))
					{
						if (text != "")
						{
							viv.setCAT_ID(Integer.valueOf(text));
						}
					}
					else if (name.equals("AT_ID")){
						if(text != "")
						{
							viv.setAT_ID(Integer.valueOf(text));
						}else{
							viv.setAT_ID(0);
						}
					}



				}
				i +=1;

				viv.setOK(false);
				viv.setNUEVA(false);
				viv.setMODIFICADA(false);
				viviendas.add(viv);
			}
		}catch (Exception ex)
		{

			//Toast.makeText(funciones.this.getActivity(), "Error autenticaci�n.", Toast.LENGTH_LONG).show();
			//Log.e("I shouldn't be here");
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			//return ;
			return viviendas;
		}


		return viviendas;
	}


	public static List<Hogar> ParsearHogares(String XMLDocument) throws Exception

	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Hogares");
		List<Hogar> hogares = new ArrayList<Hogar>();



		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Hogar");

			Hogar hog = new Hogar(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();


				if (name.equals("ACT_ID"))
					hog.setAct_id(Integer.valueOf(text));

				else if (name.equals("ID_USUARIO"))
					hog.setId_usuario(Integer.valueOf(text));

				else if (name.equals("ID_VIVIENDA"))
					hog.setId_vivienda(UUID.fromString(text));

				else if (name.equals("ORDENHOGAR"))
					hog.setOrdenhogar(Integer.valueOf(text));

				else if (name.equals("NUMPERSONAS"))
					hog.setNumpersonas(Integer.valueOf(text));
				else if (name.equals("JEFE"))
					hog.setJefe(text);



			}
			i +=1;

			hogares.add(hog);
		}
		return hogares;
	}

	public static void MostrarConfigGPS(final Context ctxt)
	{

        final LocationManager manager = (LocationManager) ctxt.getSystemService ( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            AlertDialog.Builder adb = new AlertDialog.Builder(ctxt);
            adb.setIcon(R.drawable.activargps);
            adb.setTitle("GPS No Habilitado");
            adb.setMessage("�Desea activar el GPS para obtener posici�n actual?");
            adb.setCancelable(false);
            adb.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    ctxt.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
            adb.setNegativeButton("No", new AlertDialog.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {

                    //NADA
                }
            });
            adb.show();
        }
        else
        {
            ctxt.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }



	}


    /**
     * Checking whether network is connected
     * @param context Context to get {@link ConnectivityManager}
     * @return true if Network is connected, else false
     */
	public static boolean TieneInternet(Context context) {
		boolean connected = true;
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo ni = cm.getActiveNetworkInfo();

		if(ni != null)
		{
			if (ni.getType() == ConnectivityManager.TYPE_MOBILE || ni.getType() == ConnectivityManager.TYPE_WIFI)
			{

				try {
					/*	 COMENTADO PARA SOLO VERIFICAR SI ESTA CONECTADO A WIFI
					URL url = new URL(globales.WS_URL);
					String host = url.getHost();


					Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 " + host);
					int returnVal = p1.waitFor();
					boolean reachable = (returnVal==0);
					return reachable;

					*/
					return true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				connected = true;
			}
			else
            {
				connected = false;
			}
		}
		else
		{
			connected = false;
		}



		return connected;
	}



	public static List<Manzana> ParsearManzanas(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Manzanas");
		List<Manzana> mismanzanas = new ArrayList<Manzana>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Manzana");

			Manzana mz = new Manzana(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("IDMANZANA"))
					mz.setId_manzana(Integer.valueOf(text));
				else if (name.equals("COMUNA_ID"))
					mz.setComuna_id(Integer.valueOf(text));
				else if (name.equals("URBANO_ID"))
					mz.setUrbano_id(Integer.valueOf(text));
				else if (name.equals("URBANO_NOMBRE"))
					mz.setUrbano_nombre(text);
				else if (name.equals("DISTRITO"))
					mz.setDistrito(Integer.valueOf(text));
				else if (name.equals("ZONA"))
					mz.setZona(Integer.valueOf(text));
				else if (name.equals("MANZANA"))
					mz.setManzana(Integer.valueOf(text));
				else if (name.equals("GEOCODIGO"))
					mz.setGeocodigo(Long.valueOf(text));
				else if (name.equals("CENTROIDE_LAT"))
					mz.setCentroide_lat(Double.valueOf(text));
				else if (name.equals("CENTROIDE_LNG"))
					mz.setCentroide_lng(Double.valueOf(text));
				else if (name.equals("ORIGEN"))
					mz.setOrigen(Integer.valueOf(text));
				else if (name.equals("ACT_ID"))
					mz.setAct_id(Integer.valueOf(text));
				else if (name.equals("ID_USUARIO"))
					mz.setId_usuario(Integer.valueOf(text));				

				/*
				else if (name.equals("FECHAHORA"))
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					java.util.Date fecha = new java.util.Date();
					fecha = sdf.parse(text);
					mz.setFechahora(fecha);
				}
				 */

			}
			i +=1;

			mismanzanas.add(mz);
		}
		return mismanzanas;
	}

	public static List<Localidad> ParsearLocalidades(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Localidades");
		List<Localidad> mislocalidades = new ArrayList<Localidad>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Entidad");

			Localidad loc = new Localidad(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("LOC_ID"))
					loc.setLoc_id(UUID.fromString(text));
				else if (name.equals("LOC_GLOSA"))
					loc.setLoc_glosa(text);				
				else if (name.equals("COMUNA_ID"))
					loc.setComuna_id(Integer.valueOf(text));
				else if (name.equals("DISTRITO"))
					loc.setDistrito(Integer.valueOf(text));
				else if (name.equals("ACT_ID"))
					loc.setAct_id(Integer.valueOf(text));
				else if (name.equals("ID_USUARIO"))
					loc.setId_usuario(Integer.valueOf(text));
				else if (name.equals("FECHA_CREACION"))
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					java.util.Date fecha = new java.util.Date();
					fecha = sdf.parse(text);
					loc.setFecha_creacion(fecha);
				}				
				else if (name.equals("CENTROIDE_LAT"))
					loc.setCentroide_lat(Double.valueOf(text));
				else if (name.equals("CENTROIDE_LNG"))
					loc.setCentroide_lng(Double.valueOf(text));

			}
			i +=1;
			mislocalidades.add(loc);
		}
		return mislocalidades;
	}

	public static List<Seccion> ParsearSecciones(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Secciones");
		List<Seccion> missecciones = new ArrayList<Seccion>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Seccion");

			Seccion sec = new Seccion(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("CU_SECCION"))
					sec.setCu_seccion(Long.valueOf(text));
				
				else if (name.equals("CUT"))
					sec.setCut(Integer.valueOf(text));

				else if (name.equals("COMUNA_GLOSA"))
					sec.setComuna_glosa(text);
				
				else if (name.equals("DISTRITO"))
					sec.setCod_distrito(text);			

				else if (name.equals("COD_CARTO"))
					sec.setCod_carto(Integer.valueOf(text));

				else if (name.equals("ESTRATO_MUESTRAL"))
					sec.setEstrato_muestral(Integer.valueOf(text));
				
				else if (name.equals("ESTRATO_GEO"))
					sec.setEstrato_geo(Integer.valueOf(text));
				
				else if (name.equals("ESTRATO_ENE"))
					sec.setEstrato_ene(Integer.valueOf(text));

				else if (name.equals("TIPO_ESTRATO"))
					sec.setTipo_est(Integer.valueOf(text));

				else if (name.equals("CODIGO_SECCION"))
					sec.setCodigo_seccion(Integer.valueOf(text));				

				else if (name.equals("ACT_ID"))
					sec.setAct_id(Integer.valueOf(text));

				else if (name.equals("ID_USUARIO"))
					sec.setId_usuario(Integer.valueOf(text));				

			}
			i +=1;

			missecciones.add(sec);
		}
		return missecciones;
	}


	public static List<TipoCalle> ParsearTipoCalle(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "TiposCalle");
		List<TipoCalle> tipos = new ArrayList<TipoCalle>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "TipoCalle");

			TipoCalle tc = new TipoCalle(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("TIPOCALLE_ID"))
					tc.setTipocalle_id(text);

				else if (name.equals("TIPOCALLE_GLOSA"))
					tc.setTipocalle_glosa(text);

			}
			i +=1;
			tipos.add(tc);
		}
		return tipos;
	}

	public static List<UsoDestino> ParsearUsoDestino(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "UsosDestino");
		List<UsoDestino> usos = new ArrayList<UsoDestino>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "UsoDestino");

			UsoDestino tc = new UsoDestino(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("ID_USODESTINO"))
					tc.setId_usodestino(Integer.valueOf(text));

				else if (name.equals("GLOSA_USODESTINO"))
					tc.setGlosa_usodestino(text);

			}
			i +=1;
			usos.add(tc);
		}
		return usos;
	}


	public static List<Categoria> ParsearCategoria(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Categorias");
		List<Categoria> cats = new ArrayList<Categoria>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Categoria");

			Categoria cat = new Categoria(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("CAT_ID"))
					cat.setCat_id(Integer.valueOf(text));

				else if (name.equals("CAT_GLOSA"))
					cat.setCat_glosa(text);

			}
			i +=1;

			cats.add(cat);
		}
		return cats;
	}

	public static List<TipoViviendaTemporada> ParsearTiposViviendaTemporada(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "TiposViviendasTemporada");
		List<TipoViviendaTemporada> tipos = new ArrayList<TipoViviendaTemporada>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "TipoViviendasTemporada");

			TipoViviendaTemporada tipo = new TipoViviendaTemporada(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("VIVTEMP_ID"))
					tipo.setVivtemp_id(Integer.valueOf(text));

				else if (name.equals("VIVTEMP_GLOSA"))
					tipo.setVivtemp_glosa(text);

			}
			i +=1;

			tipos.add(tipo);
		}
		return tipos;
	}

	public static List<CodigoAnotacion> ParsearCodigoAnotacion(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Codigos");
		List<CodigoAnotacion> codigos = new ArrayList<CodigoAnotacion>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Codigo");

			CodigoAnotacion codigo = new CodigoAnotacion(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("CODIGO_ID"))
					codigo.setCodigo_id(Integer.valueOf(text));

				else if (name.equals("CODIGO_GLOSA"))
					codigo.setCodigo_glosa(text);

			}
			i +=1;

			codigos.add(codigo);
		}
		return codigos;
	}


	public static List<Parametro> ParsearParametros(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Parametros");
		List<Parametro> params = new ArrayList<Parametro>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Parametro");

			Parametro param = new Parametro(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("KEY"))
					param.setKey(text);
				else if (name.equals("VALOR"))
					param.setValor(text);

			}
			i +=1;

			params.add(param);
		}
		return params;
	}


	public static List<Imagen> ParsearImagenes(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "MisImagenes");
		List<Imagen> imagenes = new ArrayList<Imagen>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "MiImagen");

			Imagen img = new Imagen(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("ID_VIVIENDA"))
					img.setID_VIVIENDA(UUID.fromString(text));

				else if (name.equals("IMAGEN"))
				{
					byte[] encodedImage = Base64.decode(text, Base64.DEFAULT);
					img.setIMAGEN(encodedImage);
				}


			}
			i +=1;
			imagenes.add(img);
		}
		return imagenes;
	}


	public static List<Dibujo> ParsearDibujos(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Dibujos");
		List<Dibujo> dibujos = new ArrayList<Dibujo>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Dibujo");

			Dibujo dib = new Dibujo(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				dib.setNUEVO(false);

				if (name.equals("ACT_ID"))
					dib.setACT_ID(Integer.valueOf(text));

				else if (name.equals("ID_USUARIO"))
					dib.setID_USUARIO(Integer.valueOf(text));

				else if (name.equals("ID_DIBUJO"))
					dib.setID_DIBUJO(UUID.fromString(text));
				
				else if (name.equals("IDPADRE"))
					dib.setIDPADRE(text);				

				else if (name.equals("DIB_GLOSA"))
					dib.setDIB_GLOSA(text);

				else if (name.equals("DIB_TEXT"))
					dib.setDIB_TEXT(text);
				
				else if (name.equals("DIB_TIPO"))
					dib.setDIB_TIPO(text);
				
				else if (name.equals("DIB_ESTADO"))
					dib.setDIB_ESTADO(Integer.valueOf(text));				

				else if (name.equals("FECHA_CREACION"))
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					java.util.Date fecha = new java.util.Date();
					fecha = sdf.parse(text);
					dib.setFECHA_CREACION(fecha);			
				}

			}
			i +=1;

			dib.setNUEVO(false);

			dibujos.add(dib);
		}
		return dibujos;
	}
	public static String SerializarCursor(Cursor c, String NombreColeccion, String NombreItem)
	{
		String xml;
		String campo;
		String valor;

		xml = "<" + NombreColeccion + ">";
		if(c.moveToFirst())
		{

			do
			{
				xml += "<" + NombreItem + ">";
				for(int col = 0;col<=c.getColumnCount()-1;col++)
				{
					campo = c.getColumnName(col).toUpperCase(Locale.US);

					xml += "<" + campo + ">";
					try
					{
						valor = c.getString(c.getColumnIndex(campo));	
						if(valor==null)
						{
							valor = "";
						}



					}
					catch(Exception ex)
					{
						valor = "";
					}
					xml += valor;

					xml += "</" + campo + ">";


				}
				xml += "</" + NombreItem + ">";
			}while(c.moveToNext());


		}

		c.close();

		xml +="</" + NombreColeccion + ">"; 
		return xml;
	}

	public static String SerializarFilaCursor(Cursor c, String NombreColeccion, String NombreItem)
	{
		String xml;
		String campo;
		String strvalor ;
		float fvalor;
		int ivalor;
		long lvalor;
		byte[] bvalor ;


		xml = "<" + NombreColeccion + ">";
		xml += "<" + NombreItem + ">";
		for(int col = 0;col<=c.getColumnCount()-1;col++)
		{
			campo = c.getColumnName(col).toUpperCase(Locale.US);

			xml += "<" + campo + ">";
			try
			{

				if(c.getType(col) == Cursor.FIELD_TYPE_STRING)
				{
					strvalor = c.getString(c.getColumnIndex(campo));	
				}
				else if (c.getType(col) == Cursor.FIELD_TYPE_FLOAT)
				{
					fvalor = c.getFloat(c.getColumnIndex(campo));
					strvalor = String.valueOf(fvalor);
				}
				else if (c.getType(col) == Cursor.FIELD_TYPE_INTEGER)
				{
					if(c.getColumnName(col).toString().contains("FECHA"))
					{
						lvalor = c.getLong(c.getColumnIndex(campo));
						strvalor = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(lvalor)).toString();
					}
					else
					{
						ivalor = c.getInt(c.getColumnIndex(campo));
						strvalor = String.valueOf(ivalor);						
					}


				}
				else if (c.getType(col) == Cursor.FIELD_TYPE_NULL)
				{
					ivalor = c.getInt(c.getColumnIndex(campo));
					strvalor = "";
				}				
				else if (c.getType(col) == Cursor.FIELD_TYPE_BLOB)
				{
					bvalor = c.getBlob(c.getColumnIndex(campo));

					strvalor = Base64.encodeToString(bvalor, Base64.DEFAULT);

				}		
				else
				{
					strvalor = "";
				}




			}
			catch(Exception ex)
			{
				strvalor = "";
			}
			xml += strvalor;

			xml += "</" + campo + ">";


		}
		xml += "</" + NombreItem + ">";





		//c.close();

		xml +="</" + NombreColeccion + ">"; 
		return xml;
	}

	public static List<Edificio> ParsearEdificios(String XMLDocument) throws Exception
	{

		Integer i = 1; 
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "Edificios");
		List<Edificio> edificios = new ArrayList<Edificio>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "Edificio");

			Edificio edif = new Edificio(); 
			while (parser.nextTag() != XmlPullParser.END_TAG) 
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("ID_EDIF"))
					edif.setId_edif(UUID.fromString(text));

				else if (name.equals("NOMBRE_EDIF"))
					edif.setNombre_edif(text);

				else if (name.equals("COORD_LAT"))
					edif.setCoord_lat(Double.valueOf(text));

				else if (name.equals("COORD_LNG"))
					edif.setCoord_lng(Double.valueOf(text));

				else if (name.equals("COMUNA_ID"))
					edif.setComuna_id(Integer.valueOf(text));

				else if (name.equals("REGION_ID"))
					edif.setRegion_id(Integer.valueOf(text));

				else if (name.equals("CALLE"))
					edif.setCalle(text);

				else if (name.equals("NUMERO"))
					edif.setNumero(text);

			}
			i +=1;
			edif.setNuevo(false);

			edificios.add(edif);
		}
		return edificios;
	}

	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}

	public static String FormatearFecha(DateTime dateTime) {



		//DateTime dateTime = DateTimeFormat.forPattern("EEE hh:mma MMM d, yyyy");

		DateTime today = new DateTime();
		DateTime yesterday = today.minusDays(1);
		DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mma");
		int diff = Days.daysBetween(dateTime, today).getDays();

		if (dateTime.toLocalDate().equals(today.toLocalDate())) {
			return "Hoy " + timeFormatter.print(dateTime);
		} else if (dateTime.toLocalDate().equals(yesterday.toLocalDate())) {
			return "Ayer " + timeFormatter.print(dateTime);
		} /*else if (diff >1 && diff<7){
        	return "Hace "+ diff + " d?as " + timeFormatter.print(dateTime);
        } */else {
        	return dateTime.toLocalDateTime().toString("EEE dd MMM yyyy HH:mma");
        }
	}

	public static void ResizeAndSaveImage(Context ctxt, Bitmap bm, int width, int height,int calidad, UUID ID_VIVIENDA) throws IOException {
		Bitmap photo;

		photo = Bitmap.createScaledBitmap(bm, width, height, false);
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, calidad, bytes);

		/*
		File f = new File(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
		f.createNewFile();
		FileOutputStream fo = new FileOutputStream(f);
		fo.write(bytes.toByteArray());
		fo.flush();
		fo.close();
*/
		//String nombrejpg = ID_VIVIENDA.toString() + ".jpg";
		//File to = new File(globales.RutaPicsSinImagen  + nombrejpg);
		//f.renameTo(to);
		//f.delete();

		DataBaseUtil db = new DataBaseUtil(ctxt);
		db.open();
		db.BorrarImagen(ID_VIVIENDA);
		db.InsertarImagen(ID_VIVIENDA, bytes.toByteArray());
		db.close();

		photo.recycle();

	}

	public static void setForceShowIcon(PopupMenu popupMenu) {
		try {
			Field[] fields = popupMenu.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ("mPopup".equals(field.getName())) {
					field.setAccessible(true);
					Object menuPopupHelper = field.get(popupMenu);
					Class<?> classPopupHelper = Class.forName(menuPopupHelper
							.getClass().getName());
					Method setForceIcons = classPopupHelper.getMethod(
							"setForceShowIcon", boolean.class);
					setForceIcons.invoke(menuPopupHelper, true);
					break;
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static String formatDate(long milliSeconds, String dateFormat)
	{

		DateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in milliseconds to date. 
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	public static String ComprimirBD()
	{
		try {
			//This is name and path of zip file to be created
			String fecha = new SimpleDateFormat("yyyyMMdd_HHmmss_ms").format(new Date());

			String rutaBD = Environment.getExternalStorageDirectory() + File.separator + "DV" + File.separator + "dv.db";
			String rutaBDZip = Environment.getExternalStorageDirectory()  + File.separator + "DV_BKP" + File.separator + "dv_" + fecha + ".mmhzip";


			ZipFile zipFile = new ZipFile(rutaBDZip);

			//Add files to be archived into zip file
			ArrayList<File> filesToAdd = new ArrayList<File>();
			filesToAdd.add(new File(rutaBD));

			//Initiate Zip Parameters which define various properties
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); // set compression method to deflate compression

			//DEFLATE_LEVEL_FASTEST     - Lowest compression level but higher speed of compression
			//DEFLATE_LEVEL_FAST        - Low compression level but higher speed of compression
			//DEFLATE_LEVEL_NORMAL  - Optimal balance between compression level/speed
			//DEFLATE_LEVEL_MAXIMUM     - High compression level with a compromise of speed
			//DEFLATE_LEVEL_ULTRA       - Highest compression level but low speed
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL); 

			//Set the encryption flag to true
			parameters.setEncryptFiles(true);

			//Set the encryption method to AES Zip Encryption
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);

			//AES_STRENGTH_128 - For both encryption and decryption
			//AES_STRENGTH_192 - For decryption only
			//AES_STRENGTH_256 - For both encryption and decryption
			//Key strength 192 cannot be used for encryption. But if a zip file already has a
			//file encrypted with key strength of 192, then Zip4j can decrypt this file
			parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_128);

			//Set password
			parameters.setPassword("123456");

			//Now add files to the zip file
			zipFile.addFiles(filesToAdd, parameters);

			return rutaBDZip;
		} 
		catch (ZipException e) 
		{
			return "";
		}
	}

	public static String DescomprimirBD(Context ctxt, String rutaBDZip)
	{
		try {
			//This is name and path of zip file to be created

			File dir = ctxt.getDir("DV", ctxt.MODE_PRIVATE);
			String rutaBDDir = Environment.getExternalStorageDirectory() + File.separator + "DV";
			File rutaBDTemp = new File(Environment.getExternalStorageDirectory() + File.separator + "UNZIPPED");

			rutaBDTemp.mkdir();

			ZipFile zipFile = new ZipFile(rutaBDZip);

			if(zipFile.isEncrypted())
			{
				zipFile.setPassword("123456");
			}

			zipFile.extractAll(rutaBDTemp.getAbsolutePath());

			return "ok";
		} 
		catch (ZipException e) 
		{
			return "";
		}
	}

	public static void CopiarArchivo(File src, File dst) throws IOException {
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(dst);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
	}
	
	public static boolean LLenarSpinnerSN(Context ctxt, final AutoCompleteTextView ac, boolean MostrarOpciones_OnFocus)
	{
		List<String> numdom = new ArrayList<String>(); 
		numdom.add(0, "SN");
		
    	ArrayAdapter<String> adapterNumDom =  new ArrayAdapter<String>(ctxt, R.layout.item_solo_string, numdom);
    	
    	ac.setThreshold(1);
    	ac.setAdapter(adapterNumDom);
    	
    	adapterNumDom.setNotifyOnChange(true);
    	
    	if(MostrarOpciones_OnFocus)
    	{
	    	ac.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
				
					if(hasFocus)
					{
						ac.showDropDown();
						
					}
				}
			});
    	}
		return true;
	}
	
	
	public static void CrearCarpetasRequeridas(Context ctxt)
	{
		new File(Environment.getExternalStorageDirectory () + "/MapaBase/").mkdirs();
		new File(Environment.getExternalStorageDirectory () + "/Spatialite/").mkdirs();
		new File(Environment.getExternalStorageDirectory () + "/DV_BKP/").mkdirs();
		new File(Environment.getExternalStorageDirectory () + "/EnumVivPics/").mkdirs();
	
		
	}
	
	public static void LlenarParametroWS(Context ctxt)
	{
    	String item = "";
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ctxt);
    	dbutil.open();
    	List<Parametro> params =  dbutil.ObtenerParametrosMapa("WEBSERVICE", "VALOR");
    	dbutil.close();

    	if(params.size()==0)
    	{
    		Toast.makeText(ctxt, "Error en par�metro de Web Service. Descargue nuevamente los datos desde formulario de autenticaci�n.", Toast.LENGTH_LONG).show();
    	}
    	else
    	{
        	item = params.get(0).getValor();

    		globales.WS_URL = item;    		
    	}

	}


	public static boolean isExternalStorage() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	public static String EncuestaToJSON(EncuestaMovil enc)
    {
        StringBuilder sb = new StringBuilder();

        try
        {

            JSONObject jsonObj = new JSONObject();

            jsonObj.put("IdEncuesta", enc.getIdencuesta());
            jsonObj.put("NombreEncuesta", enc.getNombreencuesta());
            jsonObj.put("TipoEncuesta", enc.getTipoencuesta());
            jsonObj.put("Act_Id", enc.getAct_id());
            jsonObj.put("IdPadre", enc.getIdpadre());
            jsonObj.put("Id_Vivienda", enc.getId_vivienda().toString());
            jsonObj.put("Hogar", enc.getHogar());
            jsonObj.put("Persona", enc.getPersona());

            JSONArray jsecciones = null;
            JSONArray jpreguntas= null;
            JSONArray jopciones= null;
            JSONArray jvalores= null;

            JSONObject jsecc = null;
            JSONObject jpreg= null;
            JSONObject jopc = null;
            JSONObject jval = null;

            for(Seccion_Encuesta sec : enc.getSecciones())
            {
                jsecciones = new JSONArray();
                jsecc = new JSONObject();

                jsecc.put("NombreSeccion", sec.getNombreseccion());
                jsecc.put("AyudaSeccion", sec.getAyudaseccion());

                jpreguntas = new JSONArray();

                for (Pregunta preg : sec.getPreguntas())
                {
                    jpreg = new JSONObject();

                    jopciones = new JSONArray();
                    jvalores = new JSONArray();

                    jpreg.put("Codigo", preg.getCodigoPregunta());
                    jpreg.put("Descripcion", preg.getDescripcion());
                    jpreg.put("Ayuda", preg.getAyuda());
                    jpreg.put("Regex", preg.getRegex());
                    jpreg.put("Estilo", preg.getEstilo());
                    jpreg.put("TipoElemento",preg.getTipo());

                    jpreg.put("Largo",preg.getLargomaximo());
                    jpreg.put("ValorMinimo",preg.getValorminimo());
                    jpreg.put("ValorMaximo",preg.getValormaximo());

                    jpreg.put("Modificable", preg.isModificable() ? "Si" : "No");

                    jpreg.put("Requerido", preg.isModificable() ? "Si" : "No");

                    jpreg.put("Respondida", preg.isModificable() ? "Si" : "No");
                    jpreg.put("OpcionOtro", preg.isOpcionOtro() ? "Si" : "No");
                    jpreg.put("TextoOtro", preg.getOtro());
                    jpreg.put("HintTexto", preg.getHintTexto());


                    for (String valor : preg.getValores())
                    {
                        jval = new JSONObject();
                        jval.put("Valor", valor);
                        jvalores.put(jval);
                    }


                    for (Opcion_Pregunta opc : preg.getOpciones())
                    {
                        jopc = new JSONObject();
                        jopc.put("Codigo", opc.getCodigo_opcion());
                        jopc.put("Valor", opc.getGlosa_opcion());
                        jopciones.put(jopc);
                    }

                    jpreg.put("Valores", jvalores);
                    jpreg.put("Opciones", jopciones);

                    jpreguntas.put(jpreg);
                }

                jsecciones.put(jpreguntas);
            }

            jsonObj.put("Secciones", jsecciones);

            return  jsonObj.toString();

        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
            return  "";
        }
    }

	public static void MostrarMensaje(Context ctxt, int icono, String mensaje, String titulo, String TextPositivo, String TextNegativo, DialogInterface.OnClickListener listenerPositivo, DialogInterface.OnClickListener listenerNegativo)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctxt);

		alertDialog.setTitle(titulo);
		alertDialog.setIcon(icono);
		alertDialog.setMessage(mensaje);
		alertDialog.setPositiveButton(TextPositivo, listenerPositivo);
        alertDialog.setNegativeButton(TextNegativo, listenerNegativo);
        alertDialog.show();
	}

    public static List<Encuesta> ParsearEncuestas(String XMLDocument) throws Exception
{

    Integer i = 1;
    KXmlParser parser = new KXmlParser();
    ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
    InputStreamReader isr = new InputStreamReader(BAIS);
    parser.setInput(isr);

    parser.nextTag();
    parser.require(XmlPullParser.START_TAG, null, "Encuestas");
    List<Encuesta> encuestas = new ArrayList<Encuesta>();

    while(parser.nextTag() != XmlPullParser.END_TAG)
    {
        parser.require(XmlPullParser.START_TAG, null, "Encuesta");

        Encuesta enc = new Encuesta();
        while (parser.nextTag() != XmlPullParser.END_TAG)
        {

            String name = parser.getName();
            String text = parser.nextText();

            if (name.equals("IDENCUESTA"))
                enc.setIdencuesta(Integer.valueOf(text));
            else if (name.equals("NOMBREENCUESTA"))
                enc.setNombreencuesta(text);
            else if (name.equals("ACT_ID"))
                enc.setAct_id(Integer.valueOf(text));
            else if (name.equals("TIPOENCUESTA"))
                enc.setTipoencuesta(text);
            else if (name.equals("JSON"))
                enc.setJson(text);

        }

        i +=1;

        encuestas.add(enc);
    }
    return encuestas;
}

    public static List<EncuestaMovil> ParsearEncuestasMovil(String XMLDocument) throws Exception
    {

        Integer i = 1;
        KXmlParser parser = new KXmlParser();
        ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
        InputStreamReader isr = new InputStreamReader(BAIS);
        parser.setInput(isr);

        parser.nextTag();
        parser.require(XmlPullParser.START_TAG, null, "Encuestas");
        List<EncuestaMovil> encuestas = new ArrayList<EncuestaMovil>();

        while(parser.nextTag() != XmlPullParser.END_TAG)
        {
            parser.require(XmlPullParser.START_TAG, null, "Encuesta");

            EncuestaMovil enc = new EncuestaMovil();
            while (parser.nextTag() != XmlPullParser.END_TAG)
            {

                String name = parser.getName();
                String text = parser.nextText();

                if (name.equals("IDENCUESTA"))
                    enc.setIdencuesta(Integer.valueOf(text));
                else if (name.equals("NOMBREENCUESTA"))
                    enc.setNombreencuesta(text);
                else if (name.equals("ACT_ID"))
                    enc.setAct_id(Integer.valueOf(text));
                else if (name.equals("ID_USUARIO"))
                    enc.setId_usuario(Integer.valueOf(text));
                else if (name.equals("TIPOENCUESTA"))
                    enc.setTipoencuesta(text);
                else if (name.equals("IDPADRE"))
                    enc.setIdpadre(text);
                else if (name.equals("ID_VIVIENDA"))
                    enc.setId_vivienda(UUID.fromString(text));
                else if (name.equals("HOGAR"))
                    enc.setHogar(Integer.valueOf(text));
                else if (name.equals("PERSONA"))
                    enc.setPersona(Integer.valueOf(text));
                else if (name.equals("ORIGINAL"))
                    enc.setOriginal(Boolean.valueOf(text));
                else if (name.equals("JSON"))
                    enc.setJson(text);

            }

            i +=1;

            encuestas.add(enc);
        }
        return encuestas;
    }

    public static EncuestaMovil CrearCopiaEncuesta(Context ctx, Encuesta enc, UUID ID_VIVIENDA, int Hogar, int Persona)
	{
	    String tipo = enc.getTipoencuesta();
		EncuestaMovil encuesta_movil = new EncuestaMovil();

		encuesta_movil.setIdencuesta(enc.getIdencuesta());
		encuesta_movil.setNombreencuesta(enc.getNombreencuesta());
		encuesta_movil.setTipoencuesta(enc.getTipoencuesta());
		encuesta_movil.setAct_id(enc.getAct_id());

	    if(tipo.equals("UPM") || tipo.equals("MANZANA") || tipo.equals("SECCION"))
        {

			encuesta_movil.setIdpadre(globales.IDPADRE);
			encuesta_movil.setId_usuario(globales.usuario.getId_usuario());
        }

        if(tipo.equals("VIVIENDA"))
        {
			encuesta_movil.setIdpadre(globales.IDPADRE);
			encuesta_movil.setId_usuario(globales.usuario.getId_usuario());
			encuesta_movil.setId_vivienda(ID_VIVIENDA);
        }

        if(tipo.equals("HOGAR"))
        {
			encuesta_movil.setIdpadre(globales.IDPADRE);
			encuesta_movil.setId_usuario(globales.usuario.getId_usuario());
			encuesta_movil.setId_vivienda(ID_VIVIENDA);
			encuesta_movil.setHogar(Hogar);
        }

        if(tipo.equals("PERSONA"))
        {
			encuesta_movil.setIdpadre(globales.IDPADRE);
			encuesta_movil.setId_usuario(globales.usuario.getId_usuario());
			encuesta_movil.setId_vivienda(ID_VIVIENDA);
			encuesta_movil.setHogar(Hogar);
			encuesta_movil.setPersona(Persona);
        }

		encuesta_movil.setIdEncuestaMovil(UUID.randomUUID());

        DataBaseUtil db = new DataBaseUtil(ctx);
	    db.open();
	    boolean ret = db.InsertarEncuesta(enc);
	    db.close();

	    return encuesta_movil;

	}


	public static Encuesta ParsearEncuesta(JSONObject obj)
	{

		Encuesta encuesta = null;
		try
		{

			encuesta = new Encuesta();
			encuesta.setIdencuesta(obj.getInt("IdEncuesta"));
			encuesta.setNombreencuesta(obj.getString("NombreEncuesta"));
			encuesta.setTipoencuesta(obj.getString("TipoEncuesta"));

			encuesta.setAct_id(obj.getInt("Act_Id"));

			JSONArray jArray = obj.getJSONArray("Secciones");

			encuesta.setSecciones(ParsearSeccion(encuesta, jArray));
			encuesta.setJson(obj.toString(4));
		}
		catch(Exception ex)
		{

		}

        return encuesta;

	}



	private static List<Seccion_Encuesta> ParsearSeccion(Encuesta enc, JSONArray jsecciones)
	{

		List<Seccion_Encuesta> seccion_encuestas = new ArrayList<Seccion_Encuesta>();
		Seccion_Encuesta sec;

		try
		{
			for(int n = 0; n < jsecciones.length(); n++)
			{
				sec = new Seccion_Encuesta();
				JSONObject object = jsecciones.getJSONObject(n);

				sec.setNombreseccion(object.getString("NombreSeccion"));
				sec.setAyudaseccion(object.getString("AyudaSeccion"));

				JSONArray jPreguntas = object.getJSONArray("Preguntas");

				sec.setPreguntas(ParsearPreguntas(sec, jPreguntas));

				seccion_encuestas.add(sec);
			}
		}
		catch(Exception ex)
		{

		}
		return seccion_encuestas;
	}

	private static List<Pregunta> ParsearPreguntas(Seccion_Encuesta sec, JSONArray jpreguntas)
	{

		List<Pregunta> preguntas = new ArrayList<Pregunta>();
		Pregunta preg;

		try
		{
			for(int n = 0; n < jpreguntas.length(); n++)
			{
				preg = new Pregunta();
				JSONObject object = jpreguntas.getJSONObject(n);

				preg.setCodigoPregunta(object.getInt("Codigo"));
				preg.setDescripcion(object.getString("Descripcion"));
				preg.setAyuda(object.getString("Ayuda"));
				preg.setRegex(object.getString("Regex"));
				preg.setEstilo(object.getString("Estilo"));

				preg.setTipo(object.getString("TipoElemento"));

				preg.setModificable(object.getString("Modificable").contains("Si") ? true: false);
				preg.setRequerido(object.getString("Requerido").contains("Si") ? true: false);

				preg.setLargomaximo(object.getInt("Largo"));
				preg.setValorminimo(object.getInt("ValorMinimo"));
				preg.setValormaximo(object.getInt("ValorMaximo"));

				JSONArray jValores = object.getJSONArray("Valores");
				preg.setValores(ParsearValores(preg, jValores));

				JSONArray jOpciones = object.getJSONArray("Opciones");
				preg.setOpciones(ParsearOpciones(preg, jOpciones));

				preg.setRespondida(object.getString("Respondida").contains("Si") ? true: false);
				preg.setOpcionOtro(object.getString("OpcionOtro").contains("Si") ? true: false);
				preg.setTextoOtro(object.getString("TextoOtro"));

				preg.setHintTexto(object.getString("HintTexto"));

				preguntas.add(preg);

			}

			sec.setPreguntas(preguntas);


		}
		catch(Exception ex)
		{

		}
		return preguntas;
	}

	private static List<String> ParsearValores(Pregunta preg, JSONArray jpreguntas)
	{

		List<String> valores = new ArrayList<String>();

		try
		{
			for(int n = 0; n < jpreguntas.length(); n++)
			{

				valores.add(jpreguntas.get(n).toString());
			}

			return valores;
		}
		catch(Exception ex)
		{
			return valores;
		}
	}


	private static List<Opcion_Pregunta>  ParsearOpciones(Pregunta preg, JSONArray jOpciones)
	{

		List<Opcion_Pregunta> opciones = new ArrayList<Opcion_Pregunta>();

		Opcion_Pregunta opc;

		try
		{

			for(int n = 0; n < jOpciones.length(); n++)
			{

				opc = new Opcion_Pregunta();
				JSONObject object = jOpciones.getJSONObject(n);
				opc.setCodigo_pregunta(preg.getCodigoPregunta());
				opc.setCodigo_opcion(object.getString("Codigo"));
				opc.setGlosa_opcion(object.getString("Valor"));
				opciones.add(opc);
			}

			return opciones;

		}
		catch(Exception ex)
		{
			return opciones;
		}
	}

	public static List<CategoriaPOI> ParsearCategoriasPOI(String XMLDocument) throws Exception
	{

		Integer i = 1;
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "CategoriasPOI");
		List<CategoriaPOI> cats = new ArrayList<CategoriaPOI>();


		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "CategoriaPOI");

			CategoriaPOI cat = new CategoriaPOI();
			while (parser.nextTag() != XmlPullParser.END_TAG)
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("IDCATPOI"))
					cat.setIdcatpoi(Integer.valueOf(text));
				else if (name.equals("GLOSA"))
					cat.setGlosa(text);
                else if (name.equals("ICON"))
                    cat.setIcon( Base64.decode(text, Base64.DEFAULT));

			}
			i +=1;

            cats.add(cat);
		}
		return cats;
	}
	public static void PlaySound(Context context, int ResId)
	{
		MediaPlayer player;

		player= MediaPlayer.create(context,ResId);
		player.start();
	}

	public static List<Area_Trabajo> ParsearAreasTrabajo(String XMLDocument) throws Exception

	{

		Integer i = 1;
		KXmlParser parser = new KXmlParser();
		ByteArrayInputStream BAIS = new ByteArrayInputStream( XMLDocument.getBytes());
		InputStreamReader isr = new InputStreamReader(BAIS);
		parser.setInput(isr);

		parser.nextTag();
		parser.require(XmlPullParser.START_TAG, null, "AreasTrabajo");
		List<Area_Trabajo> ats = new ArrayList<Area_Trabajo>();



		while(parser.nextTag() != XmlPullParser.END_TAG)
		{
			parser.require(XmlPullParser.START_TAG, null, "AreaTrabajo");

			Area_Trabajo at = new Area_Trabajo();
			while (parser.nextTag() != XmlPullParser.END_TAG)
			{
				String name = parser.getName();
				String text = parser.nextText();

				if (name.equals("AT_ID"))
				at.setAt_id(Integer.valueOf(text));

				else if (name.equals("CU_SECCION"))
					at.setCu_seccion(Long.valueOf(text));

				else if (name.equals("ACT_ID"))
					at.setAct_id(Integer.valueOf(text));

				else if (name.equals("ID_USUARIO"))
					at.setId_usuario(Integer.valueOf(text));

				else if (name.equals("ID_USUARIO_SUP"))
					at.setId_usuario_sup(Integer.valueOf(text));

				else if (name.equals("NUMERO_AT"))
					at.setNumero_at(Integer.valueOf(text));

				else if (name.equals("FECHACREACION"))
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					java.util.Date fecha = new java.util.Date();
					fecha = sdf.parse(text);
					at.setFechacreacion(fecha);
				}

				else if (name.equals("WKT"))
					at.setWkt(text);
			}
			i +=1;

			ats.add(at);
		}
		return ats;
	}



}
