package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.Date;


public class Area_Trabajo
{
    private int at_id;
    private long cu_seccion;
    private int act_id;
    private int id_usuario;
    private int id_usuario_sup;
    private Date fechacreacion;
    private int numero_at;
    private String wkt;

    public int getAt_id() {
        return at_id;
    }

    public void setAt_id(int at_id) {
        this.at_id = at_id;
    }

    public long getCu_seccion() {
        return cu_seccion;
    }

    public void setCu_seccion(long cu_seccion) {
        this.cu_seccion = cu_seccion;
    }

    public int getAct_id() {
        return act_id;
    }

    public void setAct_id(int act_id) {
        this.act_id = act_id;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_usuario_sup() {
        return id_usuario_sup;
    }

    public void setId_usuario_sup(int id_usuario_sup) {
        this.id_usuario_sup = id_usuario_sup;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public int getNumero_at() {
        return numero_at;
    }

    public void setNumero_at(int numero_at) {
        this.numero_at = numero_at;
    }

    public String getWkt() {
        return wkt;
    }

    public void setWkt(String wkt) {
        this.wkt = wkt;
    }
}
