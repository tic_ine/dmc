package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

/**
 * Created by Mbecerra on 25-07-2018.
 */

public class AsyncTask_DownloadNewVersion extends AsyncTask<String,Integer,Boolean> {

    Context ctxt;
    ProgressDialog bar;
    public AsyncResponse delegate = null;

    public AsyncTask_DownloadNewVersion(Context ctxt, AsyncResponse delegate)
    {
        this.ctxt = ctxt;
        this.delegate = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        bar = new ProgressDialog(ctxt);
        bar.setCancelable(false);

        bar.setMessage("Descargando...");

        bar.setIndeterminate(true);
        bar.setCanceledOnTouchOutside(false);
        bar.show();

    }

    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);

        bar.setIndeterminate(false  );
        bar.setMax(100);
        bar.setProgress(progress[0]);
        String msg = "";
        if(progress[0]>99){

            msg="Finalizando... ";

        }else {

            msg="Descargando... "+progress[0]+"%";
        }
        bar.setMessage(msg);

    }
    @Override
    protected void onPostExecute(Boolean result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

        bar.dismiss();

        if(result){

            Toast.makeText(ctxt,"Actualización realizada",
                    Toast.LENGTH_SHORT).show();

        }else{

            Toast.makeText(ctxt,"Error: Intente nuevamente",
                    Toast.LENGTH_SHORT).show();

        }

        delegate.processFinish(result);

    }

    @Override
    protected Boolean doInBackground(String... arg0) {
        Boolean flag = false;

        try {

            URL u = new URL(globales.URLAPK);

            URLConnection connection = u.openConnection();
            connection.connect();
            // getting file length
            int lengthOfFile = connection.getContentLength();


            InputStream is = u.openStream();

            DataInputStream  dis = new DataInputStream(is);

            byte[] buffer = new byte[1024];
            int length;

            File outputFile = new File(Environment.getExternalStorageDirectory() + "/Download/","app.apk");

            if(outputFile.exists()){
                outputFile.delete();
            }

            FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() +"/Download/app.apk"));

            int per = 0;
            int downloaded=0;

            while ((length = dis.read(buffer))>0) {
                fos.write(buffer, 0, length);
                downloaded +=length;
                per = (int) (downloaded * 100 / lengthOfFile);
                publishProgress(per);
            }
            fos.flush();
            fos.close();

            //OpenNewVersion(PATH);

            flag = true;
        } catch (Exception e) {
            Log.e("Download", "Error de Actualización: " + e.getMessage());
            flag = false;
        }
        return flag;
    }

    public interface AsyncResponse {
        void processFinish(boolean retorno);
    }




}

