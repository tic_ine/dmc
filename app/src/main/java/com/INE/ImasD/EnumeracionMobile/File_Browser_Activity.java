package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTaskCompleteListener;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_DescargarDesdeArchivo;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.io.File;
import java.util.List;
 
public class File_Browser_Activity extends Activity implements OnClickListener {
 
    private static final int REQUEST_PICK_FILE = 1;
	private TextView filePath;
    private Button Browse;
    private File selectedFile;
    
    Button btnFB_CargarArchivoDatos;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filebrowser_activity);
 
        filePath = (TextView)findViewById(R.id.file_path);
        Browse = (Button)findViewById(R.id.browse);
        Browse.setOnClickListener(this);      
        
        btnFB_CargarArchivoDatos = (Button)findViewById(R.id.btnFB_CargarArchivoDatos);
        
        btnFB_CargarArchivoDatos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				globales.Ruta_MMHDB = filePath.getText().toString();
				
				new AsyncTask_DescargarDesdeArchivo(File_Browser_Activity.this, new AsyncTaskCompleteListener<Integer>() {
					
					@Override
					public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteManzanas(List<Manzana> manzanas) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteEnvioDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onTaskCompleteDescargarDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
						if(resultado>0)
						{
							Toast.makeText(File_Browser_Activity.this, "Carga Completa", Toast.LENGTH_LONG).show();
							globales.EsCargaDesdeArchivo = true;
						}
						else
						{
							Toast.makeText(File_Browser_Activity.this, "Error al cargar datos", Toast.LENGTH_LONG).show();
						}
					}

					@Override
					public void onTaskCompleteSecciones(List<Seccion> secciones) {
						// TODO Auto-generated method stub
						
					}
				}).execute();
			}
		});
    }
 
    public void onClick(View v) {
    	
        switch(v.getId()) {
        
        case R.id.browse:
            Intent intent = new Intent(this, FilePicker_Activity.class);
            intent.putExtra("accepted_file_extensions", getIntent().getStringArrayExtra("accepted_file_extensions"));
            startActivityForResult(intent, REQUEST_PICK_FILE);
            break;
        }
    }
 
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
        if(resultCode == RESULT_OK) {
        	
            switch(requestCode) {
            
            case REQUEST_PICK_FILE:
            	
                if(data.hasExtra(FilePicker_Activity.EXTRA_FILE_PATH)) {
                	
                    selectedFile = new File
                    		(data.getStringExtra(FilePicker_Activity.EXTRA_FILE_PATH));
                    filePath.setText(selectedFile.getPath());  
                }
                break;
            }
        }
    }
    

	
	
}