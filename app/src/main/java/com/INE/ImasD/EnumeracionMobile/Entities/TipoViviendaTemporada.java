package com.INE.ImasD.EnumeracionMobile.Entities;

public class TipoViviendaTemporada {
	
	private int vivtemp_id;
	private String vivtemp_glosa;
	
	
	public int getVivtemp_id() {
		return vivtemp_id;
	}
	public void setVivtemp_id(int vivtemp_id) {
		this.vivtemp_id = vivtemp_id;
	}
	public String getVivtemp_glosa() {
		return vivtemp_glosa;
	}
	public void setVivtemp_glosa(String vivtemp_glosa) {
		this.vivtemp_glosa = vivtemp_glosa;
	}
	
	

}
