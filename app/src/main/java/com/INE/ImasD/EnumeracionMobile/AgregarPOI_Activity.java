package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ImageAdapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.CategoriaPOI;
import com.INE.ImasD.EnumeracionMobile.Entities.POI;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AgregarPOI_Activity extends Activity
{
    GridView grvCategoriasPOI;
    TextView tvLatitudPOI;
    TextView tvLongitudPOI;
    EditText etvGlosaPOI;
    Point pt;
    Point PtProyectado;
    CategoriaPOI cat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agregar_poi_activity);

        grvCategoriasPOI = (GridView) findViewById(R.id.grvCategoriasPOI);
        tvLatitudPOI = (TextView) findViewById(R.id.tvLatitudPOI);
        tvLongitudPOI = (TextView) findViewById(R.id.tvLongitudPOI);

        etvGlosaPOI = (EditText) findViewById(R.id.etvGlosaPOI);
        pt = (com.esri.core.geometry.Point)getIntent().getSerializableExtra("POI");

        PtProyectado = (Point) GeometryEngine.project(pt, SpatialReference.create(globales.SRID_WGS84), SpatialReference.create(globales.SRID_WebMercator_Spherical));
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        tvLatitudPOI.setText(String.valueOf(PtProyectado.getX()));
        tvLongitudPOI.setText(String.valueOf(PtProyectado.getY()));

        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetterOrDigit(source.charAt(i)) && !Character.isSpaceChar(source.charAt(i))) {
                        etvGlosaPOI.setError("No se permite \"" +source.charAt(i) + "\"");
                        return "";
                    }
                }
                return null;
            }
        };
        etvGlosaPOI.setFilters(new InputFilter[] { filter });

        CargarCategoriasPOI();


        grvCategoriasPOI.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cat = ((ImageAdapter)grvCategoriasPOI.getAdapter()).selectedCat;
      }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        CrearMenu(menu);
        return true;


    }

    private void CrearMenu(Menu menu) {
        MenuItem item1 = menu.add(0, 0, 0, "Grabar");
        {
            // --Copio las imagenes que van en cada item
            item1.setIcon(R.drawable.grabar);
            item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
        {
            item2.setIcon(R.drawable.cancelar);
            item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // --Llamamos al metodo que sabe que itema fue seleccionado
        return MenuSelecciona(item);
    }

    private boolean MenuSelecciona(MenuItem item) {
        switch (item.getItemId()) {

            case 0: //Grabar
                cat = ((ImageAdapter)grvCategoriasPOI.getAdapter()).selectedCat;

            if(cat==null)
            {
                Toast.makeText(AgregarPOI_Activity.this , "Seleccione una Categor�a", Toast.LENGTH_SHORT).show();
                return  true;
            }
                InsertarPOI();
                //Toast.makeText(this, "Has pulsado el Item 1 del Action Bar",Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
                return true;

            case 1://Cancelar

                setResult(Activity.RESULT_CANCELED);

                finish();
                return true;
        }
        return false;
    }

    private void CargarCategoriasPOI()
    {
        DataBaseUtil db = new DataBaseUtil(AgregarPOI_Activity.this);
        db.open();
        List<CategoriaPOI> cats = db.ObtenerCategoriasPOI();
        db.close();

        ImageAdapter adapter = new ImageAdapter(AgregarPOI_Activity.this, cats);
        grvCategoriasPOI.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    private void InsertarPOI()
    {

        POI poi = new POI();
        poi.setCoord_x(PtProyectado.getX());
        poi.setCoord_y(PtProyectado.getY());
        poi.setFechacaptura(new Date());
        poi.setGlosa(etvGlosaPOI.getEditableText().toString().trim());
        poi.setCATEGORIAPOI(cat);
        poi.setIdcatpoi(cat.getIdcatpoi());

        DataBaseUtil db = new DataBaseUtil(AgregarPOI_Activity.this);
        db.open();
        db.InsertarPOI(poi);
        db.close();

    }
}
