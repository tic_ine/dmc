package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;

/**
 * Created by Mbecerra on 19-12-2017.
 */

public class Opcion_Pregunta implements Serializable
{
    private static final long serialVersionUID = 1L;

    private  int codigo_pregunta;
    private  String codigo_opcion;
    private  String glosa_opcion;
    private  String ayuda_opcion;


    public int getCodigo_pregunta() {
        return codigo_pregunta;
    }

    public void setCodigo_pregunta(int codigo_pregunta) {
        this.codigo_pregunta = codigo_pregunta;
    }

    public String getCodigo_opcion() {
        return codigo_opcion;
    }

    public void setCodigo_opcion(String codigo_opcion) {
        this.codigo_opcion = codigo_opcion;
    }

    public String getGlosa_opcion() {
        return glosa_opcion;
    }

    public void setGlosa_opcion(String glosa_opcion) {
        this.glosa_opcion = glosa_opcion;
    }

    public String getAyuda_opcion() {
        return ayuda_opcion;
    }

    public void setAyuda_opcion(String ayuda_opcion) {
        this.ayuda_opcion = ayuda_opcion;
    }
}
