package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;

public class Manzana  implements Serializable {
	/*
	 COMUNA_ID
	 URBANO_ID
	 URBANO_NOMBRE
	 DISTRITO
	 ZONA
	 MANZANA
	 GEOCODIGO
	 CENTROIDE_LAT
	 CENTROIDE_LNG
	 ORIGEN
	 
	 * */
	
	private int idmanzana;
	private int comuna_id;
	private int urbano_id;
	private String urbano_nombre;
	private int distrito;
	private int zona;
	private int manzana;
	private long geocodigo;
	private double centroide_lat;
	private double centroide_lng;
	private int origen;
	
	private int act_id;
	private int id_usuario;
	
	private boolean checked = false;
	

	public int getIdmanzana() {
		return idmanzana;
	}
	public void setId_manzana(int idmanzana) {
		this.idmanzana = idmanzana;
	}
	public int getComuna_id() {
		return comuna_id;
	}
	public void setComuna_id(int comuna_id) {
		this.comuna_id = comuna_id;
	}
	public int getUrbano_id() {
		return urbano_id;
	}
	public void setUrbano_id(int urbano_id) {
		this.urbano_id = urbano_id;
	}
	public String getUrbano_nombre() {
		return urbano_nombre;
	}
	public void setUrbano_nombre(String urbano_nombre) {
		this.urbano_nombre = urbano_nombre;
	}
	public int getDistrito() {
		return distrito;
	}
	public void setDistrito(int distrito) {
		this.distrito = distrito;
	}
	public int getZona() {
		return zona;
	}
	public void setZona(int zona) {
		this.zona = zona;
	}
	public int getManzana() {
		return manzana;
	}
	public void setManzana(int manzana) {
		this.manzana = manzana;
	}
	public long getGeocodigo() {
		return geocodigo;
	}
	public void setGeocodigo(long geocodigo) {
		this.geocodigo = geocodigo;
	}
	public double getCentroide_lat() {
		return centroide_lat;
	}
	public void setCentroide_lat(double centroide_lat) {
		this.centroide_lat = centroide_lat;
	}
	public double getCentroide_lng() {
		return centroide_lng;
	}
	public void setCentroide_lng(double centroide_lng) {
		this.centroide_lng = centroide_lng;
	}
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	public int getAct_id() {
		return act_id;
	}
	public void setAct_id(int act_id) {
		this.act_id = act_id;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	
	
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	
}
	