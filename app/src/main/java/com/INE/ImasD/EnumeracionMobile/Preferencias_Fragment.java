package com.INE.ImasD.EnumeracionMobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.io.File;

public class Preferencias_Fragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{
    final static int REQUEST_PATH = 1000;

	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.preferencias);
        
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        
        findPreference("prefMetrosBusquedaCalles").setTitle(preferences.getString("prefMetrosBusquedaCalles", "0.005") + " " + getString(R.string.pref_MetrosBusquedaCalles));
        
        String mostrarorden = preferences.getString("prefMostrarOrdenes", "1");
        
        findPreference("prefMostrarOrdenes").setTitle((mostrarorden.equals("1") ? "Orden de Vivienda " : "Orden Correlativo de Listado ") + "se mostrar� como orden en Mapa" );
         
        preferences.registerOnSharedPreferenceChangeListener(this);

        String distancia_regpos = preferences.getString("prefDistanciaRegistroPosicion", "10");
        
        findPreference("prefDistanciaRegistroPosicion").setTitle("Distancia de " + distancia_regpos + " " + getString(R.string.pref_distancia_registroposicion));

        findPreference("prefRutaMapaBase").setDefaultValue(globales.Ruta_TPK);

		Preference myPref = findPreference("prefRutaMapaBase");
		myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				//open browser or intent here
                Intent i = new Intent( getActivity(), FilePicker_Activity.class);
                i.putExtra("accepted_file_extensions", new String[] {".asdasd"});
                startActivityForResult(i, REQUEST_PATH);
				return true;
			}
		});
    }

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		// TODO Auto-generated method stub

			if(this.isAdded()) {
				if (key.equals("prefMetrosBusquedaCalles")) {

					findPreference(key).setTitle(sharedPreferences.getString("prefMetrosBusquedaCalles", "0.005") + " " + getString(R.string.pref_MetrosBusquedaCalles));
				}

				if (key.equals("prefMostrarOrdenes")) {
					String mostrarorden = sharedPreferences.getString("prefMostrarOrdenes", "1");
					findPreference("prefMostrarOrdenes").setTitle((mostrarorden.equals("1") ? "Orden de Vivienda " : "Orden Correlativo de Listado ") + "se mostrar� como orden en Mapa");
				}

				if (key.equals("prefDistanciaRegistroPosicion")) {
					String distancia_regpos = sharedPreferences.getString("prefDistanciaRegistroPosicion", "10");
					findPreference("prefDistanciaRegistroPosicion").setTitle("Distancia de " + distancia_regpos + " " + getString(R.string.pref_distancia_registroposicion));
				}

				if (key.equals("prefMostrarOrdenes")) {
					globales.ActualizarCapas = true;
				}

				if (key.equals("prefRutaMapaBase")) {
					String ruta_mapabase = sharedPreferences.getString("prefRutaMapaBase", globales.Ruta_TPK);
					findPreference("prefRutaMapaBase").setSummary("Ruta actual:" + ruta_mapabase);
				}

			}
	}
	public void OnDestroy()
	{

		super.onDestroy();
		
	}

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == REQUEST_PATH) {

            String ruta = data.getStringExtra("file_path");
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("prefRutaMapaBase", ruta);
            editor.apply();

            globales.Ruta_TPK = "file://" + ruta +  File.separator;

        }
    }

    
    
}