package com.INE.ImasD.EnumeracionMobile.Entities;


public class Usuario
{
    private int id_usuario;
    private String username;
    private String contrasena;
    private String nombres;
    private String apellidos;
    private String email;
    private int vigente;
    private int perfil_id;
    
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getVigencia() {
		return vigente;
	}
	public void setVigente(int vigente) {
		this.vigente = vigente;
	}
	public int getPerfil_id() {
		return perfil_id;
	}
	public void setPerfil_id(int perfil_id) {
		this.perfil_id = perfil_id;
	}

    
}
