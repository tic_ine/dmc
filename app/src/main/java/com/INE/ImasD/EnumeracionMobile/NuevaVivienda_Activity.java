package com.INE.ImasD.EnumeracionMobile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ColorAdapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_CodigosAnotacion_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Edificios_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_SoloString_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_TipoCalle_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_UsoDestino_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_ViviendaTemporada_Adapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.Mapa_Simple_DialogFragment.AccionesMapaSimple;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;
import com.INE.ImasD.EnumeracionMobile.R.id;
import com.esri.core.geometry.Point;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;



public class NuevaVivienda_Activity extends Activity
{
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int AGREGAREDIFICIO = 2;  // The request code
	static final int SUGERENCIAS = 3;  // The request code
	
	private static final String TAG = "" + NuevaVivienda_Activity.class;
			
	double lat;
	double lng;

	double latEdificio;
	double lngEdificio;
	
	TextView lblLatitud;
	TextView lblLongitud;
	
	/*CONTROLES FORM*/
	ImageView imgNVFoto;
	Spinner spNVEdificios;
	Spinner spNVTiposCalle;
	Spinner spNVViviendaTemporada;
	Spinner spNVUsoDestinoEdificacion;
	Spinner spNVCodigoAnotacion;
	Spinner spNVUbicacionHorizontal;
	Spinner spNVUbicacionVertical;
	
	ImageButton btnAddHogar;
	
	/*
	Button btnNVMateriales;
	Button btnNVColores;
	Button btnNVOtrosTextos;
	Button btnNVOtrosUsos;
	Button btnNVObjetos;
	Button btnNVOrdenVivSitio;
	*/
	
	Button btnNVVerCoordenada;
	Button btnNVSugerencia;
	Button btnNVGrabarAudio;

	Spinner spNVOrdenEdificacion ;
	AutoCompleteTextView acNVNombreCalle;
	EditText etvNVNumeroDomicilio;
	EditText etvNVBlock;
	EditText etvNVPiso;
	EditText etvNVDepto;
	EditText etvNVDescripcion;
	RadioButton rbNVIzquierda;
	RadioButton rbNVDerecha;

	/*FIN CONTROLES FORM*/
	UUID ID_VIVIENDA;
	
	LinearLayout loNVHogares;
	public String mCurrentPhotoPath = "";
	
	TipoCalle tipocalle;
	TipoViviendaTemporada tipoviviendatemporada;
	UsoDestino usodestino;
	CodigoAnotacion codigoanotacion;
	
	String UbicacionHorizontal;
	String UbicacionVertical;
	
	int OrdenEdif;
	
	ProgressDialog pd;
	
	Button btnMVGrabarAudio;
	ImageButton imgbtnNVNuevoEdificio;
	ImageView imgNVRotar;
	
	TextView tvNVOrdenVivienda;
    protected static final int RESULT_SPEECH = 1;
    
    boolean HayFoto = false;


    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.nuevaviv_activity);


	    
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		
	    //mCurrentPhotoPath = "";

	    ID_VIVIENDA = UUID.randomUUID();

	    loNVHogares =  (LinearLayout)findViewById(R.id.loNVHogaresVivienda);
    	
	    imgNVFoto = (ImageView)findViewById(R.id.imgNVFoto);
	    
	    btnAddHogar = (ImageButton)findViewById(R.id.btnNVAddHog);

		btnNVGrabarAudio = (Button) findViewById(id.btnNVGrabarAudio);
	    /*
	    btnNVMateriales = (Button)findViewById(R.id.btnNVMateriales);
	    btnNVColores = (Button)findViewById(R.id.btnNVColores);
	    btnNVOtrosTextos = (Button)findViewById(R.id.btnNVOtrosTextos);
	    btnNVOtrosUsos = (Button)findViewById(R.id.btnNVOtrosUsos);
	    btnNVOrdenVivSitio = (Button)findViewById(R.id.btnNVOrdenVivSitio);
	    btnNVObjetos = (Button)findViewById(R.id.btnNVObjetos);
	    */
	    btnNVVerCoordenada = (Button)findViewById(R.id.btnNVVerCoordenada);
	    
	    
	    
	    
	    spNVEdificios= (Spinner)findViewById(R.id.spNVEdificios);
	    spNVTiposCalle = (Spinner)findViewById(R.id.spNVTiposCalle);
	    spNVViviendaTemporada = (Spinner)findViewById(R.id.spNVViviendaTemporada);
	    spNVUsoDestinoEdificacion = (Spinner)findViewById(R.id.spNVUsoDestinoEdificacion);
	    spNVCodigoAnotacion = (Spinner)findViewById(R.id.spNVCodigoAnotacion);
	    
	    spNVUbicacionHorizontal = (Spinner)findViewById(R.id.spNVUbicacionHorizontal);
	    spNVUbicacionVertical = (Spinner)findViewById(R.id.spNVUbicacionVertical);
	    
	    
	    spNVOrdenEdificacion = (Spinner)findViewById(id.spNVOrdenEdificacion);
	    tvNVOrdenVivienda = (TextView)findViewById(id.tvNVOrdenVivienda);
	    acNVNombreCalle = (AutoCompleteTextView)findViewById(id.acNVNombreCalle);
	    etvNVNumeroDomicilio = (EditText)findViewById(id.etvNVNumeroDomicilio);
	    etvNVBlock = (EditText)findViewById(id.etvNVBlock);
	    etvNVPiso = (EditText)findViewById(id.etvNVPiso);
	    etvNVDepto = (EditText)findViewById(id.etvNVDepto);
	    etvNVDescripcion = (EditText)findViewById(id.etvNVDescripcion);
	    
	    rbNVIzquierda = (RadioButton)findViewById(id.rbNVIzquierda);
	    rbNVDerecha = (RadioButton)findViewById(id.rbNVDerecha);
	    
	    imgbtnNVNuevoEdificio = (ImageButton)findViewById(R.id.imgbtnNVNuevoEdificio);
	    
	    imgNVRotar = (ImageView)findViewById(R.id.imgNVRotar);
	    
	    btnNVSugerencia = (Button)findViewById(R.id.btnNVSugerencia);
	    
	    imgNVFoto.setOnClickListener(new OnClickListener() {
	    	
			@Override
			public void onClick(View arg0) {
				SacarFoto();
			}
		});
	    
	    btnAddHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AgregarNuevoHogar();
				
			}
		});


		btnNVGrabarAudio.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(NuevaVivienda_Activity.this, VoiceRecognition_Activity.class);
				startActivity(i);
			}
		});

	    spNVTiposCalle.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				tipocalle =  (TipoCalle)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNVUsoDestinoEdificacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				usodestino =  (UsoDestino)parent.getItemAtPosition(position);
				
				if(usodestino.getId_usodestino()>4)
				{
					tvNVOrdenVivienda.setText("0");
				}
				else
				{
					tvNVOrdenVivienda.setText(String.valueOf(ObtenerMaxOrdenViv()+1));
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNVCodigoAnotacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				codigoanotacion =  (CodigoAnotacion)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    spNVViviendaTemporada.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				tipoviviendatemporada =  (TipoViviendaTemporada)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNVUbicacionHorizontal.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionHorizontal = (String)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spNVUbicacionVertical.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionVertical= (String)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    acNVNombreCalle.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				acNVNombreCalle.showDropDown();
	            acNVNombreCalle.requestFocus();
	            return false;
			}
		});
	    
	    
	    imgbtnNVNuevoEdificio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ine = new Intent(NuevaVivienda_Activity.this, NuevoEdificio_Activity.class);
				ine.putExtra("lat", lat);
				ine.putExtra("lng", lng);
				startActivityForResult(ine, AGREGAREDIFICIO);
				
			}
		}); 
	    
	    Intent i = getIntent();
	    
	    lat = i.getDoubleExtra("lat", -33.9999);
	    lng = i.getDoubleExtra("lng", -70.9999);
	    
	    //editor.putString("coordenada", String.valueOf(lat) + ", " +String.valueOf(lng));
	    
	    
	    lblLatitud = (TextView)findViewById(R.id.tvNVLatitud);
	    lblLongitud = (TextView)findViewById(R.id.tvNVLongitud);
	    
	    lblLatitud.setText(String.valueOf(lat));
	    lblLongitud.setText(String.valueOf(lng));
	    
	    CargarEdificios();
	    CargarTiposCalle();
	    
	    new TaskCargarSugerenciasCalles().execute();
	    
	    CargarViviendasTemporada();
	    CargarUsoDestino();
	    CargarCodigosAnotacion();
	    
	    CargarUbicacionHorizontal();
	    CargarUbicacionVertical();
	    
	    CargarOrdenEdificacion();



		tvNVOrdenVivienda.setText(String.valueOf(ObtenerMaxOrdenViv()+1));
		
		/*
		btnNVMateriales.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("MATERIALES", "materiales");
				
			}
		});
		
		btnNVColores.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("COLORES", "Colores");
				
			}
		});
		
		btnNVOtrosTextos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("OTROS", "texto");
				
			}
		});		
		
		btnNVOtrosUsos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OTRO_USO", " ejemplos Otros Usos");
			}
		});
		
		btnNVObjetos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OBJETO", " objeto a describir");
			}
		});
		
		btnNVOrdenVivSitio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("VIVIENDA", "Orden Vivienda dentro de Sitio");
			}
		});
		
		 */
		spNVEdificios.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			
				Edificio edif = (Edificio)spNVEdificios.getItemAtPosition(position);
				//"00000000-0000-0000-0000-000000000000"
				if(position==0)
				{
					
					lblLatitud.setText(String.valueOf(lat));
					lblLongitud.setText(String.valueOf(lng));
					acNVNombreCalle.setText("");
					etvNVNumeroDomicilio.setText("");
					acNVNombreCalle.setEnabled(true);
					etvNVNumeroDomicilio.setEnabled(true);
					Toast.makeText(NuevaVivienda_Activity.this, "Se asumir� coordenada original", Toast.LENGTH_SHORT).show();
				}
				else
				{
					lblLatitud.setText(String.valueOf(edif.getCoord_lat()));
					lblLongitud.setText(String.valueOf(edif.getCoord_lng()));
					acNVNombreCalle.setText(edif.getCalle());
					etvNVNumeroDomicilio.setText(edif.getNumero());

					acNVNombreCalle.setEnabled(false);
					etvNVNumeroDomicilio.setEnabled(false);
					
					latEdificio = edif.getCoord_lat();
					lngEdificio= edif.getCoord_lng();
					
					Toast.makeText(NuevaVivienda_Activity.this, "Se asumir� coordenada de edificio", Toast.LENGTH_SHORT).show();
					
				}
				
	
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		btnNVVerCoordenada.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				//Point pt= (Point) GeometryEngine.project(new Point(viv.getCOORD_X(), viv.getCOORD_Y()) ,SpatialReference.create(4674), SpatialReference.create(102100));
				Point pt;
				if(((Edificio)spNVEdificios.getSelectedItem()).getId_edif().equals(globales.UUID_CERO))
				{
					pt= new Point(lng, lat);	
				}
				else
				{
					pt= new Point(lngEdificio, latEdificio);
				}
				
				FragmentManager fm = ((Activity)NuevaVivienda_Activity.this).getFragmentManager();
				
				Mapa_Simple_DialogFragment dialogo =  Mapa_Simple_DialogFragment.newInstance("Punto Vivienda", AccionesMapaSimple.PUNTO_VIVIENDA, ID_VIVIENDA, true);
				dialogo.geometria = pt;
				dialogo.show(fm, AccionesMapaSimple.PUNTO_VIVIENDA.toString());
			}
		});
		
		
		imgNVRotar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(HayFoto)
				{
					// TODO Auto-generated method stub
					Bitmap bm = ((BitmapDrawable)imgNVFoto.getDrawable()).getBitmap();
					bm = funciones.RotateBitmap(bm, 90);
					imgNVFoto.setImageBitmap(bm);
				}
			
			
				
			}
		});
		
		btnNVSugerencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(NuevaVivienda_Activity.this, Descripcion_Activity.class);
				i.putExtra("DESCRIPCION", etvNVDescripcion.getEditableText().toString());

				
				startActivityForResult(i, SUGERENCIAS);
			}
		});
		
		
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	CrearMenu(menu);
    	return true;
    	
    	
    }
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Grabar");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.grabar);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
		{
			item2.setIcon(R.drawable.cancelar);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}


	}
	
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionado
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item) {
		switch (item.getItemId()) {

		case 0: //Grabar
			//Toast.makeText(this, "Has pulsado el Item 1 del Action Bar",Toast.LENGTH_SHORT).show();
			GrabarNuevaVivienda();
			return true;

		case 1://Cancelar
			
    		//setResult(Activity.RESULT_CANCELED);
    		
    		//finish();
			
			ConfirmarCancelar();
			
			return true;

		case android.R.id.home:
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		}
		return false;
	}
	
	
	@Override
	public void onBackPressed()
	{
		//super.onBackPressed();
		ConfirmarCancelar();
			
			
		  // optional depending on your needs
	}
	
    private void ConfirmarCancelar()
    {
    	
    	new AlertDialog.Builder(NuevaVivienda_Activity.this)
		.setIcon(R.drawable.pregunta)
		.setTitle("Cerrar formulario de Ingreso")
		.setMessage("�Est� seguro que desea cancelar el ingreso del registro?") 
		.setPositiveButton("S�", new DialogInterface.OnClickListener() 
		{

			@Override
			public void onClick(DialogInterface dialog, int which) 
			{

				NuevaVivienda_Activity.this.setResult(Activity.RESULT_CANCELED);
				finish();

			}

		})
		.setNegativeButton("No", null)
		.show();
    }
    
    private void CargarEdificios()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	List<Edificio> edificios =  dbutil.ObtenerEdificios();
    	dbutil.close();
    	
    	Spin_Edificios_Adapter adapter = new Spin_Edificios_Adapter(NuevaVivienda_Activity.this, edificios);
    	spNVEdificios.setPrompt("Seleccione Edificio si corresponde");
    	spNVEdificios.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    private void CargarSugerenciasDescripcion(String TipoSugerencia, String Titulo)
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	List<Parametro> params =  dbutil.ObtenerParametrosMapa(TipoSugerencia, "VALOR");
    	dbutil.close();
    	
    	final List<String> items = new ArrayList<String>();
    	
    	
    	for(Parametro param : params)
    	{
    		items.add(param.getValor());
    	}



    	final ArrayAdapter<String> adapter = new ColorAdapter(this, items);

    	AlertDialog.Builder builder3=new AlertDialog.Builder(NuevaVivienda_Activity.this);
    	builder3.setAdapter(adapter, new DialogInterface.OnClickListener() {

    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			// TODO Auto-generated method stub

    			etvNVDescripcion.getText().insert(etvNVDescripcion.getSelectionStart(),adapter.getItem(which) + " ");
    			etvNVDescripcion.setSelection(etvNVDescripcion.getText().length());
    		}
    	});

    		
    		  builder3.setTitle("Seleccione " + Titulo);

    		  builder3.show();


    }
    
    
    private void CargarTiposCalle()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	List<TipoCalle> tiposcalle =  dbutil.ObtenerTiposCalle();
    	dbutil.close();
    	
    	Spin_TipoCalle_Adapter adapter = new Spin_TipoCalle_Adapter(NuevaVivienda_Activity.this, tiposcalle);
    	spNVTiposCalle.setPrompt("Seleccione Tipo de Calle");
    	spNVTiposCalle.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
    	
    	
    }
    
    private void CargarViviendasTemporada()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	List<TipoViviendaTemporada> vivtemp =  dbutil.ObtenerTiposViviendaTemporada();
    	dbutil.close();
    	
    	Spin_ViviendaTemporada_Adapter adapter = new Spin_ViviendaTemporada_Adapter(NuevaVivienda_Activity.this, vivtemp);
    	spNVViviendaTemporada.setPrompt("Tipo de Vivienda de Temporada");
    	spNVViviendaTemporada.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
   
    
    private void CargarUsoDestino()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	List<UsoDestino> usos =  dbutil.ObtenerUsosDestino();
    	dbutil.close();
    	
    	Spin_UsoDestino_Adapter adapter = new Spin_UsoDestino_Adapter(NuevaVivienda_Activity.this, usos);
    	spNVUsoDestinoEdificacion.setPrompt("Uso o Destino Edificaci�n");
    	spNVUsoDestinoEdificacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
   }

    private void CargarCodigosAnotacion()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	List<CodigoAnotacion> usos =  dbutil.ObtenerCodigosAnotacion();
    	dbutil.close();
    	
    	Spin_CodigosAnotacion_Adapter adapter = new Spin_CodigosAnotacion_Adapter(NuevaVivienda_Activity.this, usos);
    	spNVCodigoAnotacion.setPrompt("C�digos de Anotaci�n");
    	spNVCodigoAnotacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    
    private void CargarUbicacionHorizontal()
    {
    
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionhorizontal);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spNVUbicacionHorizontal.setPrompt("Ubicaci�n horizontal dentro del sitio");
    	spNVUbicacionHorizontal.setAdapter(adapter);
    	
    	adapter.notifyDataSetChanged();
    }
    
    private void CargarUbicacionVertical()
    {
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionvertical);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spNVUbicacionVertical.setPrompt("Ubicaci�n vertical dentro del sitio");
    	spNVUbicacionVertical.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
    
    
    private void CargarOrdenEdificacion()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(NuevaVivienda_Activity.this);
    	dbutil.open();
    	int maxedif =  dbutil.ObtenerMaxOrdenEdificio(globales.IDPADRE);
    	dbutil.close();
    	
    	
    	Integer[] ints = new Integer[maxedif+2];
    	
    	for(int i = 0;i<=maxedif+1;i++)
    	{
    		ints[i] = i;
    	}
    	
    
    	ArrayAdapter<Integer> adaptermaxedif =  new ArrayAdapter<Integer>(NuevaVivienda_Activity.this, R.layout.item_solo_string, ints);
    	spNVOrdenEdificacion.setPrompt("Orden de Edificaci�n");
    	spNVOrdenEdificacion.setAdapter(adaptermaxedif);
    	adaptermaxedif.notifyDataSetChanged();
    	spNVOrdenEdificacion.setSelection(maxedif);
    	
    }
    private void AgregarNuevoHogar()
    {
    	ImageButton imgBtnBorrarHogar;
    	TextView txtNumHogar;
    	EditText etvNumPersonasHogar;
		EditText etvJefeHogar;
    	

    	
    	int total =loNVHogares.getChildCount();
    	if(total>0)
    	{
	    	View v;
			v = loNVHogares.getChildAt(total-1);
			etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
			etvJefeHogar = (EditText)v.findViewById(id.etvJefeHogar);
			if(etvNumPersonasHogar.getText().toString().equals(""))
			{
				Toast.makeText(NuevaVivienda_Activity.this, "Ingrese Personas al hogar anterior para agregar otro", Toast.LENGTH_SHORT).show();
				return;
			}
    	
    	}
        	
        
    	final View child = getLayoutInflater().inflate(R.layout.item_hogar, null);
    	
    	txtNumHogar = (TextView)child.findViewById(R.id.lblNumeroHogar);
    	etvNumPersonasHogar = (EditText)child.findViewById(R.id.etvNumPersonasHogar);
    	
    	imgBtnBorrarHogar = (ImageButton)child.findViewById(R.id.imgBtnBorrarHog);

    	imgBtnBorrarHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loNVHogares.removeView(child);
				RenumerarHogares();
				
			}
		});
    	
    	
    	loNVHogares.addView(child);
    	
    	//etvNumPersonasHogar.requestFocus();
    	
    	RenumerarHogares();
    	
    	
    }
    
    private void RenumerarHogares()
    {
    	TextView txtNumHogar;
    	
    	int total =loNVHogares.getChildCount();
    	View v;
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		v = loNVHogares.getChildAt(i);
        	txtNumHogar = (TextView)v.findViewById(R.id.lblNumeroHogar);
        	txtNumHogar.setText(String.valueOf(i+1));
    	}
    	
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (requestCode ==RESULT_SPEECH && resultCode == RESULT_OK && null != data) {

			ArrayList<String> text = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

			etvNVDescripcion.setText(text.get(0));
		}
    	
    	if(requestCode == AGREGAREDIFICIO && resultCode == RESULT_OK)
    	{
    		 UUID ID_EDIF = (UUID) data.getExtras().get("ID_EDIF");
    		 UUID ID_E;
    		 CargarEdificios();
    		 for (int i = 0; i < spNVEdificios.getCount(); i++) {
    			 ID_E = ((Edificio)spNVEdificios.getItemAtPosition(i)).getId_edif();
    		        if (ID_E.equals(ID_EDIF)) {
    		        	spNVEdificios.setSelection(i);
    		            break;
    		        }
    		    }
    		 
    		 
    	}
    	
    	if (requestCode ==SUGERENCIAS && resultCode == RESULT_OK && null != data) {

    		 String DESCRIPCION = data.getExtras().get("DESCRIPCION").toString();
			etvNVDescripcion.setText(DESCRIPCION);
		}
    	
    	
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) 
        {
        	
        	BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

        	try {

        		bm = BitmapFactory.decodeFile(mCurrentPhotoPath);
        		if(bm != null)
        		{
        			
    				resizeImage(bm,320, 240);
    		      	imgNVFoto.setImageBitmap(bm);     
    		      	HayFoto = true;
        		}
        		else
        		{
        			Toast.makeText(NuevaVivienda_Activity.this, "Error al asociar foto", Toast.LENGTH_SHORT).show();
        		}
        		

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
                    
       	
        	Toast.makeText(NuevaVivienda_Activity.this, "Foto creada", Toast.LENGTH_SHORT).show();
            //Bundle extras = data.getExtras();

        }
        else
        {
        	HayFoto = false;
        }
    }
  
    private void resizeImage(Bitmap bm, int width, int height) throws IOException {
    	Bitmap photo;
    	
    	photo = Bitmap.createScaledBitmap(bm, width, height, false);
    	ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    	photo.compress(Bitmap.CompressFormat.JPEG, 80, bytes);

    	File f = new File(mCurrentPhotoPath);
    	f.createNewFile();
    	FileOutputStream fo = new FileOutputStream(f);
    	fo.write(bytes.toByteArray());
    	fo.close();
    	
    	photo.recycle();
    	
    }
    
    private void SacarFoto() {
    	
   	
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            	Toast.makeText(NuevaVivienda_Activity.this, "Error", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                
                mCurrentPhotoPath = photoFile.getAbsolutePath();
                
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {

        File storageDir = new File(globales.RutaPicsSinImagen);
        
        storageDir.mkdirs();
        File image = new File(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        
        return image;
    }
   
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
    private void GrabarNuevaVivienda()
    {
    	if(ValidarDatos())
    	{
    		
    		Vivienda viv = new Vivienda();
    		viv.setACT_ID(globales.actualizacion.getAct_id());
    		viv.setID_USUARIO(globales.usuario.getId_usuario());
    		viv.setID_VIVIENDA(ID_VIVIENDA);
    		viv.setID_ORIGEN(1);
    		viv.setIDPADRE(globales.IDPADRE);

    		viv.setNOMBRE_CALLE_CAMINO(acNVNombreCalle.getEditableText().toString());
    		viv.setN_DOMICILIO(etvNVNumeroDomicilio.getEditableText().toString());
    		viv.setN_LETRA_BLOCK(etvNVBlock.getEditableText().toString());
    		viv.setN_PISO(etvNVPiso.getEditableText().toString());
    		viv.setN_LETRA_DEPTO(etvNVDepto.getEditableText().toString());
    		viv.setFECHAINGRESO(new Date());
    		
    		viv.setTIPOCALLE_ID(tipocalle.getTipocalle_id());
    		viv.setTIPOCALLE_GLOSA(tipocalle.getTipocalle_glosa());
    		
    		viv.setID_USODESTINO(usodestino.getId_usodestino());
    		viv.setCODIGO_ID(codigoanotacion.getCodigo_id());

    		viv.setDESCRIPCION(etvNVDescripcion.getEditableText().toString());
    		
    		viv.setPOSICION(rbNVIzquierda.isChecked()? 1 : 2);

    		viv.setID_ESE(0);
    		viv.setNULO(false);
    		viv.setNUEVA(true);
    		viv.setMODIFICADA(false);
    		
    		viv.setUSUARIO_ID_INGRESO(globales.usuario.getId_usuario());
    		viv.setORDEN(ObtenerCorrelativoPadre()+1);
    		
    		
    		if(usodestino.getId_usodestino()>4)
    		{
    			viv.setORDEN_VIV(0);	
    		}
    		else
    		{
    			viv.setORDEN_VIV(ObtenerMaxOrdenViv()+1);
    		}
    		
    		
    		viv.setUBICACIONHORIZONTAL(UbicacionHorizontal);
    		viv.setUBICACIONVERTICAL(UbicacionVertical);
    		
    		
    		viv.setORDENEDIFICACION((int)spNVOrdenEdificacion.getSelectedItemId());
    		
    		Edificio myedif =(Edificio)spNVEdificios.getSelectedItem();
    		viv.setID_EDIF(myedif.getId_edif());
    		
    		if(myedif.getId_edif().equals(globales.UUID_CERO))
    		{
        		viv.setCOORD_X(lng);
        		viv.setCOORD_Y(lat);
    		}
    		else
    		{
        		viv.setCOORD_X(lngEdificio);
        		viv.setCOORD_Y(latEdificio);
    		}
    		
    		
    		
    		DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Activity.this);
    		db.open();
    		
    		db.InsertarVivienda(viv);
    		
    		db.close();
    		db = null;
    		
    		Toast.makeText(NuevaVivienda_Activity.this, "Vivienda Grabada", Toast.LENGTH_SHORT).show();
    		
    		GrabarHogares();

    		GrabarImagen();
    		
    		Intent resultIntent = new Intent();	
    		resultIntent.putExtra("NUEVA_VIVIENDA", viv);
    		setResult(Activity.RESULT_OK, resultIntent);
    		 
    		finish();
    		
    	}

    }
    
    private boolean ValidarDatos()
    {
    	boolean retorno;
    	Toast toast = Toast.makeText(NuevaVivienda_Activity.this, "", Toast.LENGTH_SHORT);
    
    	if(spNVOrdenEdificacion.getSelectedItem()==null)
    	{
    		toast.setText("Debe ingresar Orden de Edificaci�n.");
    		retorno = false;
    	}
    	else if(acNVNombreCalle.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Calle.");
    		retorno = false;
    	}
    	else if(etvNVNumeroDomicilio.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar N�mero de Domicilio. Si  no tiene digite SN");
    		retorno = false;
    	}    	
    	else
    	{
    		retorno = true;
    	}
    	
    	if(!retorno)
    	{
    		toast.show();
    	}
    	
		return retorno;
    	
    }
    
    private void GrabarHogares()
    {
    	EditText etvNumPersonasHogar;

		EditText etvJefeHogar;
    	Hogar hog;
    	int total =loNVHogares.getChildCount();
    	View v;
    	
    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Activity.this);
    	db.open();
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		hog = new Hogar();
    		
    		v = loNVHogares.getChildAt(i);
    		etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
			etvJefeHogar= (EditText)v.findViewById(id.etvJefeHogar);

        	hog.setAct_id(globales.actualizacion.getAct_id());
        	hog.setId_usuario(globales.usuario.getId_usuario());
        	hog.setId_vivienda(ID_VIVIENDA);
        	hog.setOrdenhogar(i+1);
        	hog.setNumpersonas(Integer.valueOf(etvNumPersonasHogar.getEditableText().toString()));
        	hog.setJefe(etvJefeHogar.getText().toString());
        	db.InsertarHogar(hog);
        	
    	}
    	
    	db.close();
    	
    }
    
    private void GrabarImagen()
    {
    	if(HayFoto)
    	{
    		BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;

        	try 
        	{
        		bm = BitmapFactory.decodeFile(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
        		
        		bm = funciones.RotateBitmap(bm, 90);
        		if(bm != null)
        		{
    				funciones.ResizeAndSaveImage(NuevaVivienda_Activity.this, bm,320, 240, 100, ID_VIVIENDA);
        		}
        	}
    		catch (Exception e) 
    		{
    			
    		}
					// TODO: handle exception
    	}
    }
    
    private int ObtenerCorrelativoPadre()
    {
    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Activity.this);
    	db.open();
    	int ret = db.ObtenerCorrelativoVivienda(globales.IDPADRE);
    	db.close();
		return ret;
    	
    }
    
    private int ObtenerMaxOrdenViv()
    {
    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Activity.this);
    	db.open();
    	int ret = db.ObtenerOrdenViviendaMaximo(globales.IDPADRE);
    	db.close();
		return ret;
    	
    }
    

    
		private class TaskCargarSugerenciasCalles extends AsyncTask<Void, Void, List<String>>
		{
			

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				pd = new ProgressDialog(NuevaVivienda_Activity.this);
				pd.setTitle("Cargando Sugerencias Calles..");
				pd.setMessage("Por favor espere...");       
				pd.setIndeterminate(true);
				pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//				pd.setMax(100);
				pd.show();
			}
			
			@Override
			protected List<String> doInBackground(Void... args) 
			{
				
				try 
				{
					return CargarSugerencias();
					
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}

			}
			
		    private List<String> CargarSugerencias()
		    {
		    	DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Activity.this);
		    	db.open();
		    	List<String> calles = db.ObtenerSugerenciasCalle(globales.IDPADRE);
		    	db.close();
		    	
		    	try {
					calles = AgregarSugerenciasCallesMapa(calles);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	
		    	return calles;

		    }
		    
			 public List<String> AgregarSugerenciasCallesMapa(List<String> calles) throws Exception 
			 {
				 
				 try 
				 {
					 if(globales.padres==PADRES.MANZANAS)
					 {
						 DataBaseUtil db = new DataBaseUtil(NuevaVivienda_Activity.this);
						 db.open();
						 Manzana mz = db.ObtenerManzana(globales.IDPADRE,0);
						 db.close();
						 new DataBaseSpatialite().ObtenerCallesPadre(String.valueOf(mz.getGeocodigo()), calles);
					 }
					 
					 if(globales.padres==PADRES.SECCIONES)
					 {
						 new DataBaseSpatialite().ObtenerCallesPadre(globales.IDPADRE, calles);
					 }					 
					 
					 //return sldb.AgregarSugerenciasCalles(calles, lat, lng);
					 new DataBaseSpatialite().AgregarSugerenciasCalles(calles, lat, lng);
					 
					 return calles;
		
				 } 
				 catch (Exception e) 
				 {
			            e.printStackTrace();
			            Log.d("EnumeracionViviendas","open database"  +  e.getMessage().toString());
			            return calles; 
			      }
			    }


			
			@Override 
			protected void onPostExecute(List<String> calles) 
			{
				
				if(calles !=null)
				{
					Collections.sort(calles);
					calles.add(0, "SN");
					
			    	ArrayAdapter<String> adapterCalles =  new ArrayAdapter<String>(NuevaVivienda_Activity.this, R.layout.item_solo_string, calles);
			    	
			    	acNVNombreCalle.setThreshold(1);
			    	acNVNombreCalle.setAdapter(adapterCalles);
			    	adapterCalles.setNotifyOnChange(true);
			    	
					if (pd != null) {
						pd.dismiss();
					}
					//new TaskCargarViviendas().execute();		
					/*
					if(padres.size()>0)
					{
						new TaskCargarViviendas().execute();				
					}
					*/
				}
			}
		}

		
}
