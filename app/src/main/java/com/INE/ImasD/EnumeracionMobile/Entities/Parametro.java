package com.INE.ImasD.EnumeracionMobile.Entities;

public class Parametro {

	private String key;
	private String valor;
	
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
