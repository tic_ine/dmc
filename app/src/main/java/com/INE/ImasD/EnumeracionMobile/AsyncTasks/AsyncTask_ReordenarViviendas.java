package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;

import java.util.List;

public class AsyncTask_ReordenarViviendas extends AsyncTask<List<Vivienda>, Void, List<Vivienda>>
{
	private Context context;
    private AsyncTaskCompleteListener<List<Vivienda>> listener;
    ProgressDialog pd;
    
    
	private int max;
	private int inicio;
    public AsyncTask_ReordenarViviendas(Context ctx, AsyncTaskCompleteListener<List<Vivienda>> listener, int inicio, int max)
    {
        this.context = ctx;
        this.listener = listener;
        this.max = max;
        this.inicio = inicio;
    }

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		
		super.onPreExecute();
		pd = new ProgressDialog(this.context);
		pd.setTitle("Reordenando Viviendas");
		pd.setMessage("Por favor espere...");       
		pd.setIndeterminate(false);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMax(max);
		pd.show();
	}
	
	@Override
	protected List<Vivienda> doInBackground(List<Vivienda>... listas)
	{
		return OrdenarViviendas(listas[0]);
	}
	
	private List<Vivienda> OrdenarViviendas(List<Vivienda> viviendas)
	{

		DataBaseUtil dbUtil = new DataBaseUtil(this.context);
		dbUtil.open();
		int i = this.inicio;
		int ordenviv = this.inicio;
		for(Vivienda viv : viviendas)
		{
			
			
			if(viv.getID_USODESTINO() == 0 || viv.getID_USODESTINO() == 1 || viv.isNULO())
			{
				viv.setORDEN_VIV(0);
			}
			else
			{
				viv.setORDEN_VIV(ordenviv);
				ordenviv = ordenviv + 1;
			}
			viv.setORDEN(i);
			
			dbUtil.ReordenarVivienda(viv);
			
			pd.setProgress(i);
			i++;
			
			
		}
		
		dbUtil.close();
		
		return viviendas;
	
	}
	
	@Override 
	protected void onPostExecute(List<Vivienda> viviendas) 
	{
		super.onPostExecute(viviendas);
		if (pd != null) {
			pd.dismiss();
			listener.onTaskCompleteViviendas(viviendas);
			//gva.notifyDataSetChanged();
		}
		//new TaskCargarViviendas().execute();
		


	}
}