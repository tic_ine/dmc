package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil_MMHDB;
import com.INE.ImasD.EnumeracionMobile.Entities.Actualizacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Usuario;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;

import java.util.List;

public class AsyncTask_DescargarDesdeArchivo extends AsyncTask<Void, Integer, Integer> 
{
	ProgressDialog prog;
	private Context context;
    private AsyncTaskCompleteListener<Integer> listener;
    DataBaseUtil dbUtil;
	DataBaseUtil_MMHDB dbUtilMMHDB;

    public AsyncTask_DescargarDesdeArchivo(Context ctx, AsyncTaskCompleteListener<Integer> listener)
    {
        this.context = ctx;
        this.listener = listener;
    }
    
	@Override
	protected void onPreExecute() 
	{
		prog = new ProgressDialog(context);
		prog.setTitle("Cargando datos desde archivo");
		prog.setMessage("Leyendo datos, por favor espere...");       
		prog.setIndeterminate(false);
		prog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		prog.setMax(100);
		prog.show();

	}


	@Override
	protected Integer doInBackground(Void... params) 
	{
		return 	ObtenerActualizaciones() + ObtenerUsuarios() + ObtenerTiposCalle() + ObtenerUsoDestino() + 
				ObtenerCategorias() + ObtenerTiposViviendaTemporada() + 
				ObtenerCodigosAnotacion() + ObtenerParametros();
				
		

	}
	private int ObtenerActualizaciones()
	{
		

		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		
		int i = 0;
		
		try
		{
			
			prog.setProgress(0);

			List<Actualizacion> misactualizaciones;

			dbUtilMMHDB.open();
			misactualizaciones = dbUtilMMHDB.ObtenerActualizaciones();
			dbUtilMMHDB.close();

			int total = misactualizaciones.size();

			prog.setMax(total);
			dbUtil.open();
			dbUtil.BorrarTodosActualizaciones();
			//da.BorrarTodosUsuarios();

			for(Actualizacion act : misactualizaciones)
			{
				dbUtil.InsertarActualizaciones(act);
				i++;
				prog.setProgress(i);

			}

			dbUtil.close();

			/**********FIN ACTUALIZACIONES***********/

			return 1;

		}
		catch (Exception e)
		{
			//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
			return 0;
		}
	}
private int ObtenerUsuarios()
{
	dbUtil = new DataBaseUtil(context);
	dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
	
	try
	{

		int total = 0;
		int i = 0;

		List<Usuario> misusuarios;
		
		dbUtilMMHDB.open();
		misusuarios = dbUtilMMHDB.ObtenerUsuarios();
		dbUtilMMHDB.close();

		total = misusuarios.size();
		prog.setMax(total);

		dbUtil = new DataBaseUtil(context);
		dbUtil.open();
		dbUtil.BorrarTodosUsuarios();


		for(Usuario usuario : misusuarios)
		{
			dbUtil.InsertarUsuario(usuario);
			i++;
			prog.setProgress(i);

		}
		dbUtil.close();


		return 1;

	}
	catch (Exception e)
	{
		//Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
		return 0;
	}

}
	private Integer ObtenerUsoDestino()
	{
		dbUtil = new DataBaseUtil(context);
    	  
        try
        {
    		int total = 0;
    		int i = 0;
    		
    		dbUtilMMHDB.open();
    		List<UsoDestino> usos = dbUtilMMHDB.ObtenerUsosDestino();
    		dbUtilMMHDB.close();

    		total = usos.size();
    		prog.setMax(total);


    		dbUtil = new DataBaseUtil(context);
    		dbUtil.open();
            dbUtil.BorrarUsoDestino();
      	 //da.BorrarTodosUsuarios();
      	 
      	 for(UsoDestino uso : usos)
      	 {
      		 dbUtil.InsertarUsosDestino(uso);
      		i++;
      		prog.setProgress(i);

      	 }
      	 dbUtil.close();
      	 
      	
      	 
            return 1;
            
         }
        catch (Exception e)
        {
            //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
            return 0;
        }

    }

	private Integer ObtenerTiposCalle()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
        try
        {
        	          	  
            int total = 0;
            int i = 0;

            List<TipoCalle> tipos;

            dbUtilMMHDB.open();
            tipos = dbUtilMMHDB.ObtenerTiposCalle();
            dbUtilMMHDB.close();
            
            total = tipos.size();
            prog.setMax(total);
            dbUtil.open();
            dbUtil.BorrarTiposCalle();

      	 
      	 for(TipoCalle tc : tipos)
      	 {
      		 dbUtil.InsertarTiposCalle(tc);
      		i++;
      		prog.setProgress(i);

      	 }
      	 dbUtil.close();
      	 
      	
      	 
            return 1;
            
         }
        catch (Exception e)
        {
            //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
            return 0;
        }

    }
	
	private Integer ObtenerCategorias()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
        try
        {

            int total = 0;
            int i = 0;

            List<Categoria> cats;
            dbUtilMMHDB.open();
            

            cats = dbUtilMMHDB.ObtenerCategoria();
            dbUtilMMHDB.close();
            
            total = cats.size();
            prog.setMax(total);
            dbUtil.open();
            dbUtil.BorrarCategoria();

      	 
      	 for(Categoria cat : cats)
      	 {
      		 dbUtil.InsertarCategoria(cat);
      		i++;
      		prog.setProgress(i);

      	 }
      	 dbUtil.close();
      	 
      	
      	 
            return 1;
            
         }
        catch (Exception e)
        {
            //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
            return 0;
        }

    }	
	
	private Integer ObtenerTiposViviendaTemporada()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
		
        try
        {

            int total = 0;
            int i = 0;

            List<TipoViviendaTemporada> tipos;
            dbUtilMMHDB.open();
            tipos = dbUtilMMHDB.ObtenerTiposViviendaTemporada();
            dbUtilMMHDB.close();
            
            total = tipos.size();
            prog.setMax(total);
            dbUtil.open();
            dbUtil.BorrarTipoViviendaTemporada();
      	 //da.BorrarTodosUsuarios();
      	 
      	 for(TipoViviendaTemporada tipo : tipos)
      	 {
      		 dbUtil.InsertarTipoViviendaTemporada(tipo);
      		i++;
      		prog.setProgress(i);

      	 }
      	 dbUtil.close();
      	 
      	
      	 
            return 1;
            
         }
        catch (Exception e)
        {
            //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
            return 0;
        }

    }
	
	
	private Integer ObtenerCodigosAnotacion()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
        try
        {

            int total = 0;
            int i = 0;

            List<CodigoAnotacion> codigos;


            dbUtilMMHDB.open();
            codigos = dbUtilMMHDB.ObtenerCodigosAnotacion();
            dbUtilMMHDB.close();
            
            total = codigos.size();
            prog.setMax(total);
            dbUtil.open();
            dbUtil.BorrarCodigosAnotacion();
      	 //da.BorrarTodosUsuarios();
      	 
      	 for(CodigoAnotacion codigo : codigos)
      	 {
      		 dbUtil.InsertarCodigosAnotacion(codigo);
      		i++;
      		prog.setProgress(i);

      	 }
      	 dbUtil.close();
      	 
      	
      	 
            return 1;
            
         }
        catch (Exception e)
        {
            //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
            return 0;
        }

    }
	
	
	private Integer ObtenerParametros()
	{
		dbUtil = new DataBaseUtil(context);
		dbUtilMMHDB = new DataBaseUtil_MMHDB(context, globales.Ruta_MMHDB);
        try
        {

            int total = 0;
            int i = 0;

            List<Parametro> params;
            dbUtilMMHDB.open();
            params = dbUtilMMHDB.ObtenerParametros();
            dbUtilMMHDB.close();
            
            
            total = params.size();
            prog.setMax(total);
            dbUtil.open();
            dbUtil.BorrarParametros();
      	 //da.BorrarTodosUsuarios();
      	 
      	 for(Parametro param : params)
      	 {
      		 dbUtil.InsertarParametro(param);
      		i++;
      		prog.setProgress(i);

      	 }
      	 dbUtil.close();
      	 
      	
      	 
            return 1;
            
         }
        catch (Exception e)
        {
            //Toast.makeText(LoginActivity.this, "Error al obtener Usuarios", Toast.LENGTH_LONG).show();
            return 0;
        }

    }
	@Override
	protected void onPostExecute(Integer result) 
	{
		super.onPostExecute(result);
		
		listener.onTaskCompleteDescargarDatos(result);
		
		int duration = Toast.LENGTH_SHORT;

		if(result>0)
		{
			
			Toast.makeText(context, "Datos Obtenidos correctamente", duration).show();
		}
		else
		{
			Toast.makeText(context, "Error al Obtener Datos", Toast.LENGTH_SHORT).show();

		}
		
		prog.dismiss();

	}
}