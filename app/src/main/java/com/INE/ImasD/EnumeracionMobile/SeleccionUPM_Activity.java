package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.DescargaUPM_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.DescargaUPM_Secciones_Adapter;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTaskCompleteListener;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_CargarManzanas;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_CargarSecciones;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_DescargarDatos;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;

import java.util.ArrayList;
import java.util.List;

public class SeleccionUPM_Activity extends Activity {
	
	ListView lstDescargaUPM;
	Button btnDescargaUPM_Aceptar;
	CheckBox chkDescargar_UPM_Todas;
	boolean todas = false;
    boolean SoloCargaLaboral;
    List<Manzana> mzs;
    List<Seccion> secs;


	@Override
	 public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seleccion_upm_descarga_activity);
		
		
		lstDescargaUPM = (ListView)findViewById(R.id.lstDescargaUPM);
		btnDescargaUPM_Aceptar = (Button)findViewById(R.id.btnDescargaUPM_Aceptar);

		chkDescargar_UPM_Todas = (CheckBox)findViewById(R.id.chkDescargar_UPM_Todas);

		getActionBar().setDisplayShowHomeEnabled(true);

		if (globales.padres==PADRES.MANZANAS)
		{
			new AsyncTask_CargarManzanas(SeleccionUPM_Activity.this, new AsyncTaskCompleteListener<List<Manzana>>() {
				
				@Override
				public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onTaskCompleteManzanas(List<Manzana> manzanas) {
					// TODO Auto-generated method stub
					CargarManzanas(manzanas);
                    mzs = manzanas;
				}
				
				@Override
				public void onTaskCompleteEnvioDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onTaskCompleteDescargarDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}
	
				@Override
				public void onTaskCompleteSecciones(List<Seccion> secciones) {
					// TODO Auto-generated method stub
					
				}
			}).execute();
		
		}
		else if (globales.padres == PADRES.SECCIONES) {
            new AsyncTask_CargarSecciones(SeleccionUPM_Activity.this, new AsyncTaskCompleteListener<List<Seccion>>() {

                @Override
                public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTaskCompleteSecciones(List<Seccion> secciones) {
                    // TODO Auto-generated method stub
                    CargarSecciones(secciones);
                    secs = secciones;
                }

                @Override
                public void onTaskCompleteManzanas(List<Manzana> manzanas) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTaskCompleteEnvioDatos(Integer resultado) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTaskCompleteDescargarDatos(Integer resultado) {
                    // TODO Auto-generated method stub

                }
            }).execute();

		}
		btnDescargaUPM_Aceptar.setOnClickListener(new OnClickListener() {

                                                      @Override
                                                      public void onClick(View v) {
                                                          // TODO Auto-generated method stub

                                                          SoloCargaLaboral = true;
                                                          new AsyncTask_DescargarDatos(SeleccionUPM_Activity.this, new AsyncTaskCompleteListener<Integer>() {
                                                              @Override
                                                              public void onTaskCompleteViviendas(List<Vivienda> viviendas)
                                                              {

                                                              }

                                                              @Override
                                                              public void onTaskCompleteManzanas(List<Manzana> manzanas)
                                                              {
                                                                  CargarManzanas(manzanas);
                                                              }

                                                              @Override
                                                              public void onTaskCompleteSecciones(List<Seccion> secciones)
                                                              {

                                                                  DataBaseUtil db = new DataBaseUtil(SeleccionUPM_Activity.this);
                                                                  db.open();
                                                                  secciones = db.ObtenerSecciones();
                                                                  db.close();

                                                                  CargarSecciones(secciones);

                                                              }

                                                              @Override
                                                              public void onTaskCompleteEnvioDatos(Integer resultado) {

                                                              }

                                                              @Override
                                                              public void onTaskCompleteDescargarDatos(Integer resultado) {

                                                              }
                                                          }, new ArrayList<Manzana>(), new ArrayList<Seccion>(), true).execute();

                                                      }
                                                  });
		chkDescargar_UPM_Todas.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
		
				todas = isChecked;
				
				if(lstDescargaUPM.getAdapter()!=null)
				{
				
					if(globales.padres==PADRES.MANZANAS)
					{
							((DescargaUPM_Adapter)lstDescargaUPM.getAdapter()).CheckearTodos(isChecked);
					}
					else if (globales.padres==PADRES.SECCIONES)
					{
						((DescargaUPM_Secciones_Adapter)lstDescargaUPM.getAdapter()).CheckearTodos(isChecked);
					}
				}
				else
				{
					CerrarVentanaTodas();
				}
					
					
				
			}
		});
		
	}

	private void Descargar()
    {

        if(todas)
        {
            if(lstDescargaUPM.getAdapter() == null)
            {
                CerrarVentanaTodas();
            }
            else
            {
                if(globales.padres==PADRES.MANZANAS)
                {
                    CerrarVentanaSeleccionManzanas(((DescargaUPM_Adapter)lstDescargaUPM.getAdapter()).getList());
                }
                else if (globales.padres==PADRES.SECCIONES)
                {
                    CerrarVentanaSeleccionSecciones(((DescargaUPM_Secciones_Adapter)lstDescargaUPM.getAdapter()).getList());
                }
            }

        }
        else
        {
            if(lstDescargaUPM.getAdapter() != null)
            {
                if(globales.padres==PADRES.MANZANAS)
                {
                    CerrarVentanaSeleccionManzanas(((DescargaUPM_Adapter)lstDescargaUPM.getAdapter()).getList());
                }
                else if (globales.padres==PADRES.SECCIONES)
                {
                    CerrarVentanaSeleccionSecciones(((DescargaUPM_Secciones_Adapter)lstDescargaUPM.getAdapter()).getList());
                }
            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        CrearMenu(menu);
        return true;
    }

    private void CrearMenu(Menu menu)
    {
        MenuItem item1 = menu.add(0, 0, 0, "Grabar");
        {
            // --Copio las imagenes que van en cada item
            item1.setIcon(R.drawable.confirmado_menu);
            item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
        {
            item2.setIcon(R.drawable.cancelar);
            item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return MenuSelecciona(item);
    }

    private boolean MenuSelecciona(MenuItem item)
    {
        switch (item.getItemId())
        {

            case 0: //Grabar


                Descargar();

                return true;

            case 1://Cancelar

                this.setResult(Activity.RESULT_CANCELED);
                finish();

                return true;

            default:
                return false;
        }
    }
	private void CargarManzanas(List<Manzana> padres)
	{
		
		if(padres.size()==0)
		{
			CerrarVentanaSeleccionManzanas(padres);
		}
		else
		{
			DescargaUPM_Adapter adapter = new DescargaUPM_Adapter(SeleccionUPM_Activity.this, padres);
			lstDescargaUPM.setAdapter(adapter);
			adapter.notifyDataSetChanged();
		}
	}
	
	private void CargarSecciones(List<Seccion> padres)
	{
		
		if(padres.size()==0)
		{
			CerrarVentanaSeleccionSecciones(padres);
		}
		else
		{
		    if(SoloCargaLaboral)
            {
                int total = 0;
                DataBaseUtil db = new DataBaseUtil(SeleccionUPM_Activity.this);
                db.open();

                for(Seccion sec : padres) // lo que tengo
                {
                    total = db.ObtenerTotalSoloPadre(String.valueOf(sec.getCu_seccion()));

                    if(total==0) {
                        sec.setChecked(true);
                    }
                }

            }

			DescargaUPM_Secciones_Adapter adapter = new DescargaUPM_Secciones_Adapter(SeleccionUPM_Activity.this, padres);
			lstDescargaUPM.setAdapter(adapter);
			adapter.notifyDataSetChanged();
		}
	}

	private void CerrarVentanaSeleccionManzanas(List<Manzana> padres)
	{
		boolean HayChecked = false;
		if (padres.size()>0)
		{

			for(Manzana mz : padres)
			{
				if(mz.isChecked())
				{
					HayChecked = true;
					break;
				}
			}
			
			if(HayChecked)
			{
				Intent i = new Intent();
				Bundle bundle = new Bundle();
				bundle.putString("tipo", "manzanas");
				bundle.putSerializable("manzanas", (ArrayList<Manzana>)padres);
                bundle.putBoolean("SoloCargaLaboral", SoloCargaLaboral);
				i.putExtras(bundle);
				
				setResult(Activity.RESULT_OK, i);
				finish();
			}
			else
			{
				Toast.makeText(SeleccionUPM_Activity.this, "No ha seleccionado ninguna manzana", Toast.LENGTH_LONG).show();
			}			
		}

	}
	
	private void CerrarVentanaSeleccionSecciones(List<Seccion> padres)
	{
		boolean HayChecked = false;
		if (padres.size()>0)
		{

			for(Seccion sec : padres)
			{
				if(sec.isChecked())
				{
					HayChecked = true;
					break;
				}
			}
			
			if(HayChecked)
			{
				Intent i = new Intent();
				Bundle bundle = new Bundle();
				bundle.putString("tipo", "secciones");
				bundle.putSerializable("secciones", (ArrayList<Seccion>)padres);
                bundle.putBoolean("SoloCargaLaboral", SoloCargaLaboral);
				i.putExtras(bundle);
				
				setResult(Activity.RESULT_OK, i);
				finish();
			}
			else
			{
				Toast.makeText(SeleccionUPM_Activity.this, "No ha seleccionado ninguna manzana", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	private void CerrarVentanaTodas()
	{

			Intent i = new Intent();
			Bundle bundle = new Bundle();
			bundle.putString("tipo", "todas");
            bundle.putBoolean("SoloCargaLaboral", SoloCargaLaboral);
			i.putExtras(bundle);
			
			setResult(Activity.RESULT_OK, i);
			finish();
		
	}
}

