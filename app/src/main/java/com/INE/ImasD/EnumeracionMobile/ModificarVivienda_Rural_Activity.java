package com.INE.ImasD.EnumeracionMobile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ColorAdapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Categorias_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_CodigosAnotacion_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Edificios_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_SoloString_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_TipoCalle_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_UsoDestino_Adapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.Mapa_Simple_DialogFragment.AccionesMapaSimple;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.R.id;
import com.esri.core.geometry.Point;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;



public class ModificarVivienda_Rural_Activity extends Activity
{
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int AGREGAREDIFICIO = 2;  // The request code
	private static final String TAG = "" + ModificarVivienda_Activity.class;
	static final int SUGERENCIAS = 3;  // The request code

	static final int GRABAR_AUDIO = 4;
			
	double lat;
	double lng;
	TextView lblLatitud;
	TextView lblLongitud;

	boolean HayAudio = false;
	
	//double latOriginal;
	//double lngOriginal;
	
	boolean primeracarga = true;
	
	/*CONTROLES FORM*/
	
	Spinner spMV_R_Categoria;
	
	ImageView imgMV_R_FotoViv;
	Spinner spMV_R_Edificios;
	Spinner spMV_R_TiposCalle;
	//Spinner spMV_R_ViviendaTemporada;
	Spinner spMV_R_UsoDestinoEdificacion;
	Spinner spMV_R_CodigoAnotacion;
	Spinner spMV_R_UbicacionHorizontal;
	Spinner spMV_R_UbicacionVertical;
	
	ImageButton btnMV_R_AddHogar;
	
	/*
	Button btnMV_R_Materiales;
	Button btnMV_R_Colores;
	Button btnMV_R_OtrosTextos;
	Button btnMV_R_OtrosUsos;
	Button btnMV_R_Objetos;
	Button btnMV_R_OrdenVivSitio;
	
	*/
	Button btnMV_R_VerCoordenada;
	
	
	
	Spinner spMV_R_OrdenEdificacion ;
	AutoCompleteTextView acMV_R_NombreCalle;
	AutoCompleteTextView acMV_R_NumeroDomicilio;
	EditText etvMV_R_Block;
	EditText etvMV_R_Piso;
	EditText etvMV_R_Depto;
	EditText etvMV_R_Descripcion;
	RadioButton rbMV_R_Izquierda;
	RadioButton rbMV_R_Derecha;
	
	ImageButton imgbtnMV_R_NuevoEdificio;

	AutoCompleteTextView acMV_R_NombreEntidad;
	AutoCompleteTextView acMV_R_Localidad;
	
	TextView tvMV_R_OrdenVivienda;
	/*FIN CONTROLES FORM*/
	UUID ID_VIVIENDA;
	
	LinearLayout loMV_R_Hogares;
	public String mCurrentPhotoPath = "";
	
	TipoCalle tipocalle;
	TipoViviendaTemporada tipoviviendatemporada;
	UsoDestino usodestino;
	CodigoAnotacion codigoanotacion;
	
	String UbicacionHorizontal;
	String UbicacionVertical;
	
	ImageView imgMV_R_Rotar;
	Button btnMV_R_Sugerencia;
	
	ProgressDialog pd;
	
	Button btnSpeak;

	ImageButton btnMV_R_GrabarAudio;
	ImageButton btnMV_R_PlayAudio;


	Vivienda viv;
    protected static final int RESULT_SPEECH = 1;
    boolean HayFoto = false;
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.modificarviv_r_activity);


		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
	    //mCurrentPhotoPath = "";

	    //ID_VIVIENDA = UUID.randomUUID();

	    loMV_R_Hogares =  (LinearLayout)findViewById(R.id.loMV_R_HogaresVivienda);
    	
	    imgMV_R_FotoViv = (ImageView)findViewById(R.id.imgMV_R_FotoViv);
	    
	    btnMV_R_AddHogar = (ImageButton)findViewById(R.id.btnMV_R_AddHog);



	    /*
	    btnMV_R_Materiales = (Button)findViewById(R.id.btnMV_R_Materiales);
	    btnMV_R_Colores = (Button)findViewById(R.id.btnMV_R_Colores);
	    btnMV_R_OtrosTextos = (Button)findViewById(R.id.btnMV_R_OtrosTextos);
	    btnMV_R_OtrosUsos = (Button)findViewById(R.id.btnMV_R_OtrosUsos);
	    btnMV_R_Objetos = (Button)findViewById(R.id.btnMV_R_ObjetosDesc);
	    btnMV_R_OrdenVivSitio = (Button)findViewById(R.id.btnMV_R_OrdenVivSitio);
	    */

		acMV_R_Localidad = (AutoCompleteTextView) findViewById(R.id.acMV_R_Localidad);
	    spMV_R_Categoria = (Spinner)findViewById(R.id.spMV_R_Categorias);
	    
	    btnMV_R_VerCoordenada = (Button)findViewById(R.id.btnMV_R_VerCoordenada);
	    
	    spMV_R_Edificios= (Spinner)findViewById(R.id.spMV_R_Edificios);
	    spMV_R_TiposCalle = (Spinner)findViewById(R.id.spMV_R_TiposCalle);
	    spMV_R_UsoDestinoEdificacion = (Spinner)findViewById(R.id.spMV_R_UsoDestinoEdificacion);
	    spMV_R_CodigoAnotacion = (Spinner)findViewById(R.id.spMV_R_CodigoAnotacion);
	    
	    spMV_R_UbicacionHorizontal = (Spinner)findViewById(R.id.spMV_R_UbicacionHorizontal);
	    spMV_R_UbicacionVertical = (Spinner)findViewById(R.id.spMV_R_UbicacionVertical);
	    
	    
	    spMV_R_OrdenEdificacion = (Spinner)findViewById(id.spMV_R_OrdenEdificacion);
	    acMV_R_NombreCalle = (AutoCompleteTextView)findViewById(id.acMV_R_NombreCalle);
	    acMV_R_NumeroDomicilio = (AutoCompleteTextView)findViewById(id.acMV_R_NumeroDomicilio);
	    etvMV_R_Block = (EditText)findViewById(R.id.etvMV_R_Block);
	    etvMV_R_Piso = (EditText)findViewById(id.etvMV_R_Piso);
	    etvMV_R_Depto = (EditText)findViewById(id.etvMV_R_Depto);
	    etvMV_R_Descripcion = (EditText)findViewById(id.etvMV_R_Descripcion);
	    
	    acMV_R_NombreEntidad = (AutoCompleteTextView)findViewById(id.acMV_R_NombreEntidad);
	    
	    tvMV_R_OrdenVivienda = (TextView)findViewById(R.id.tvMV_R_OrdenVivienda);
	    
	    rbMV_R_Izquierda = (RadioButton)findViewById(id.rbMV_R_Izquierda);
	    rbMV_R_Derecha = (RadioButton)findViewById(id.rbMV_R_Derecha);
	    
	    imgMV_R_Rotar = (ImageView)findViewById(R.id.imgMV_R_Rotar);
	    btnMV_R_Sugerencia = (Button)findViewById(R.id.btnMV_R_Sugerencia);
	    
	    imgbtnMV_R_NuevoEdificio = (ImageButton)findViewById(R.id.imgbtnMV_R_NuevoEdificio);
	    
	    imgMV_R_FotoViv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				SacarFoto();
			}
		});
	    
	    btnMV_R_AddHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AgregarNuevoHogar();
				
			}
		});


		btnMV_R_GrabarAudio= (ImageButton) findViewById(id.btnMV_R_GrabarAudio);

		btnMV_R_PlayAudio= (ImageButton) findViewById(id.btnMV_R_PlayAudio);


		acMV_R_Localidad.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
		acMV_R_NombreEntidad.setFilters(new InputFilter[] {new InputFilter.AllCaps()});


		btnMV_R_GrabarAudio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v)
			{

				Intent i = new Intent(ModificarVivienda_Rural_Activity.this, VoiceRecognition_Activity.class);
				i.putExtra("ID_VIVIENDA", ID_VIVIENDA.toString());
				startActivityForResult(i, GRABAR_AUDIO);

			}

		});

		btnMV_R_PlayAudio.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				if(HayAudio)
				{
					ReproducirAudio();
				}

			}
		});

	    spMV_R_TiposCalle.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				tipocalle =  (TipoCalle)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMV_R_UsoDestinoEdificacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				usodestino =  (UsoDestino)parent.getItemAtPosition(position);
			
				if(usodestino.getId_usodestino()<=1)
				{
					tvMV_R_OrdenVivienda.setText("0");
				}
				else
				{
					Toast.makeText(ModificarVivienda_Rural_Activity.this, "El orden de vivienda definitivo se dar� al reordenar el listado", Toast.LENGTH_LONG).show();
					tvMV_R_OrdenVivienda.setText(String.valueOf(viv.getORDEN_VIV()));
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMV_R_CodigoAnotacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				codigoanotacion =  (CodigoAnotacion)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
/*	    
	    spMV_R_ViviendaTemporada.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				tipoviviendatemporada =  (TipoViviendaTemporada)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
*/	    
	    spMV_R_UbicacionHorizontal.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionHorizontal = (String)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMV_R_UbicacionVertical.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionVertical= (String)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    acMV_R_NombreCalle.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				acMV_R_NombreCalle.showDropDown();
	            acMV_R_NombreCalle.requestFocus();
	            return false;
			}
		});


		acMV_R_Localidad.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*
                if(!globales.LOCALIDAD.equals(s.toString().trim()))
                {
                    tvMV_R_OrdenVivienda.setText("1");
                    spMV_R_OrdenEdificacion.setSelection(1);

                }
                else
                {
                    */
                    globales.LOCALIDAD =  acMV_R_Localidad.getText().toString();

                    tvMV_R_OrdenVivienda.setText(String.valueOf(ObtenerMaxOrdenViv()+1));
                    spMV_R_OrdenEdificacion.setSelection(spMV_R_OrdenEdificacion.getCount() - 1);
                //}

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


		acMV_R_Localidad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				globales.LOCALIDAD = parent.getAdapter().getItem(position).toString();
			}
		});

	    Intent i = getIntent();
	    
	    lblLatitud = (TextView)findViewById(R.id.tvMV_R_Latitud);
	    lblLongitud = (TextView)findViewById(R.id.tvMV_R_Longitud);
	    

	    CargarCategorias();
	    
	    CargarEdificios();
	    CargarTiposCalle();
	    
	    
	    
	    //CargarViviendasTemporada();
	    CargarUsoDestino();
	    CargarCodigosAnotacion();
	    
	    CargarUbicacionHorizontal();
	    CargarUbicacionVertical();
	    
	    CargarOrdenEdificacion();
	 
	    funciones.LLenarSpinnerSN(ModificarVivienda_Rural_Activity.this, acMV_R_NumeroDomicilio, true);
	    
		btnSpeak = (Button) findViewById(R.id.btnSpeak);
/*
		btnSpeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Intent i = new Intent(ModificarVivienda_Rural_Activity.this, VoiceRecognition_Activity.class);

				startActivityForResult(i, RESULT_SPEECH); 
				
			}
 
		});
		
*/
		
	    ID_VIVIENDA =UUID.fromString(i.getStringExtra("ID_VIVIENDA"));
	    ObtenerVivienda();

		CrearArchivoAudioDesdeBD();
	    
	    lat = viv.getCOORD_Y();
	    lng = viv.getCOORD_X();
	    
	    //latOriginal = viv.getCOORD_Y();
	    //lngOriginal = viv.getCOORD_X();
	    
	    
	    new TaskCargarSugerenciasCalles().execute();

		new TaskCargarLocalidades().execute();

	    new TaskCargarEntidades().execute();
	    
	    lblLatitud.setText(String.format("%.2f", lat));
	    lblLongitud.setText(String.format("%.2f", lng));
	    
	    acMV_R_NombreCalle.setText(viv.getNOMBRE_CALLE_CAMINO());
	    acMV_R_NumeroDomicilio.setText(viv.getN_DOMICILIO());	    	
	    

	    etvMV_R_Block.setText(viv.getN_LETRA_BLOCK());
	    etvMV_R_Piso.setText(viv.getN_PISO());
	    etvMV_R_Depto.setText(viv.getN_LETRA_DEPTO());
	    
	    
	    spMV_R_OrdenEdificacion.setSelection(viv.getORDENEDIFICACION()-1);
	    tvMV_R_OrdenVivienda.setText(String.valueOf(viv.getORDEN_VIV()));
	    
	    spMV_R_UsoDestinoEdificacion.setSelection(((Spin_UsoDestino_Adapter)spMV_R_UsoDestinoEdificacion.getAdapter()).getPositionById(viv.getID_USODESTINO()));
	    
	    spMV_R_TiposCalle.setSelection(((Spin_TipoCalle_Adapter)spMV_R_TiposCalle.getAdapter()).getPositionById(viv.getTIPOCALLE_ID()));
	    spMV_R_CodigoAnotacion.setSelection(((Spin_CodigosAnotacion_Adapter)spMV_R_CodigoAnotacion.getAdapter()).getPositionById(viv.getCODIGO_ID()));
	    //spMV_R_ViviendaTemporada.setSelection(((Spin_ViviendaTemporada_Adapter)spMV_R_ViviendaTemporada.getAdapter()).getPositionById(viv.getVIVTEMP_ID()));
	    spMV_R_UbicacionHorizontal.setSelection(((Spin_SoloString_Adapter) spMV_R_UbicacionHorizontal.getAdapter()).getPosition(viv.getUBICACIONHORIZONTAL()));
	    spMV_R_UbicacionVertical.setSelection(((Spin_SoloString_Adapter) spMV_R_UbicacionVertical.getAdapter()).getPosition(viv.getUBICACIONVERTICAL()));
	    spMV_R_Edificios.setSelection(((Spin_Edificios_Adapter)spMV_R_Edificios.getAdapter()).getPositionById(viv.getID_EDIF()));	    
	    
	    if(viv.getPOSICION() == 1) {rbMV_R_Izquierda.setChecked(true);} else { rbMV_R_Derecha.setChecked(true);} 
	    
	    etvMV_R_Descripcion.setText(viv.getDESCRIPCION().toUpperCase());
	    
	    acMV_R_NombreEntidad.setText(viv.getNOMBREENTIDAD());
		acMV_R_Localidad.setText(viv.getNOMBRELOCALIDAD());
	    spMV_R_Categoria.setSelection(((Spin_Categorias_Adapter)spMV_R_Categoria.getAdapter()).getPositionById(viv.getCAT_ID()));
	    

	    ObteneryAgregarHogares();
	    
	    CargarFotoViv(); 
       
        
        spMV_R_Edificios.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				primeracarga = false;
				return false;
			}
		});
        spMV_R_Edificios.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			    if(!primeracarga)
			    {
					Edificio edif = (Edificio)spMV_R_Edificios.getItemAtPosition(position);
					//"00000000-0000-0000-0000-000000000000"
					if(position==0)
					{
						lat = viv.getCOORD_Y();
						lng = viv.getCOORD_X();
						//lblLongitud.setText(String.valueOf(viv.getCOORD_X()));
						
						
						lblLatitud.setText(String.format("%.2f", viv.getCOORD_Y()));
						lblLongitud.setText(String.format("%.2f", viv.getCOORD_X()));
						acMV_R_NombreCalle.setText("");
						acMV_R_NumeroDomicilio.setText("");
						acMV_R_NombreCalle.setEnabled(true);
						acMV_R_NumeroDomicilio.setEnabled(true);
						Toast.makeText(ModificarVivienda_Rural_Activity.this, "Se asumir� coordenada original", Toast.LENGTH_SHORT).show();
					}
					else
					{
						lblLatitud.setText(String.format("%.2f", edif.getCoord_lat()));
						lblLongitud.setText(String.format("%.2f", edif.getCoord_lng()));
						acMV_R_NombreCalle.setText(edif.getCalle());
						acMV_R_NumeroDomicilio.setText(edif.getNumero());
						
						acMV_R_NombreCalle.setEnabled(false);
						acMV_R_NumeroDomicilio.setEnabled(false);
						
						lat= edif.getCoord_lat();
						lng= edif.getCoord_lng();
						
						Toast.makeText(ModificarVivienda_Rural_Activity.this, "Se asumir� coordenada de edificio", Toast.LENGTH_SHORT).show();
						
					}
					
			    }
			    
				
			/*
				if(position==0)
				{
					
					lblLatitud.setText(String.valueOf(viv.getCOORD_Y()));
					lblLongitud.setText(String.valueOf(viv.getCOORD_X()));
					Toast.makeText(ModificarVivienda_Rural_Activity.this, "Se asumir� coordenada original", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Edificio edif = (Edificio)spMV_R_Edificios.getItemAtPosition(position);
					lblLatitud.setText(String.valueOf(edif.getCoord_lat()));
					lblLongitud.setText(String.valueOf(edif.getCoord_lng()));
					Toast.makeText(ModificarVivienda_Rural_Activity.this, "Se asumir� coordenada de edificio", Toast.LENGTH_SHORT).show();
					
				}
				
			 */
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
        
        
        
        /*
		btnMV_R_Materiales.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("MATERIALES", "materiales");
				
			}
		});
		
		btnMV_R_Colores.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("COLORES", "Colores");
				
			}
		});
		
		btnMV_R_OtrosTextos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("OTROS", "texto");
				
			}
		});		
		
		btnMV_R_OtrosUsos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OTRO_USO", " ejemplos Otros Usos");
			}
		});
		
		btnMV_R_Objetos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OBJETO", " objeto a describir");
			}
		});
		
		btnMV_R_OrdenVivSitio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("VIVIENDA", " Orden Viviendas en Sitio seg�n recorrido");
			}
		});
		*/
        
        
		btnMV_R_VerCoordenada.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Point pt;
				
				if(((Edificio)spMV_R_Edificios.getSelectedItem()).getId_edif().equals(globales.UUID_CERO))
				{
					pt= new Point(viv.getCOORD_X(), viv.getCOORD_Y());	
				}
				else
				{
					pt= new Point(lng, lat);
				}
				
				//pt= new Point(viv.getCOORD_X(), viv.getCOORD_Y());
				FragmentManager fm = ((Activity)ModificarVivienda_Rural_Activity.this).getFragmentManager();
				
				Mapa_Simple_DialogFragment dialogo = Mapa_Simple_DialogFragment.newInstance ("Punto Vivienda", AccionesMapaSimple.PUNTO_VIVIENDA, viv.getID_VIVIENDA(), false);
				dialogo.geometria = pt;
				dialogo.show(fm, AccionesMapaSimple.PUNTO_VIVIENDA.toString());
			}
		});
		
		imgMV_R_Rotar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(HayFoto)
				{
					// TODO Auto-generated method stub
					Bitmap bm = ((BitmapDrawable)imgMV_R_FotoViv.getDrawable()).getBitmap();
					bm = funciones.RotateBitmap(bm, 90);
					imgMV_R_FotoViv.setImageBitmap(bm);
				}
			
			
				
			}
		});
		
		btnMV_R_Sugerencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ModificarVivienda_Rural_Activity.this, Descripcion_Activity.class);
				i.putExtra("DESCRIPCION", etvMV_R_Descripcion.getEditableText().toString());

				
				startActivityForResult(i, SUGERENCIAS);
			}
		});
		
		
		imgbtnMV_R_NuevoEdificio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ine = new Intent(ModificarVivienda_Rural_Activity.this, NuevoEdificio_Activity.class);
				ine.putExtra("lat", lat);
				ine.putExtra("lng", lng);
				startActivityForResult(ine, AGREGAREDIFICIO);
				
			}
		});
    }
    

    private void ObtenerVivienda()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	viv =  dbutil.ObtenerVivienda(ID_VIVIENDA);
    	dbutil.close();
    	
    	
    }
    
    private void ObteneryAgregarHogares()
    {
      	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Hogar> hogares =  dbutil.ObtenerHogares(ID_VIVIENDA);
    	dbutil.close();
    	
    	for(Hogar hog : hogares)
    	{
    		AgregarNuevoHogar(hog);
    	}
    	
    }
    
    
    private void CargarSugerenciasDescripcion(String TipoSugerencia, String Titulo)
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Parametro> params =  dbutil.ObtenerParametrosMapa(TipoSugerencia, "VALOR");
    	dbutil.close();
    	
    	final List<String> items = new ArrayList<String>();
    	
    	
    	for(Parametro param : params)
    	{
    		items.add(param.getValor());
    	}



    	final ArrayAdapter<String> adapter = new ColorAdapter(this, items);

    	AlertDialog.Builder builder3=new AlertDialog.Builder(ModificarVivienda_Rural_Activity.this);
    	builder3.setAdapter(adapter, new DialogInterface.OnClickListener() {

    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			// TODO Auto-generated method stub

    			etvMV_R_Descripcion.getText().insert(etvMV_R_Descripcion.getSelectionStart(),adapter.getItem(which) + " ");
    			etvMV_R_Descripcion.setSelection(etvMV_R_Descripcion.getText().length());
    		}
    	});

    		
    		  builder3.setTitle("Seleccione " + Titulo);

    		  builder3.show();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	CrearMenu(menu);
    	return true;
    	
    	
    }
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Grabar");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.grabar);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
		{
			item2.setIcon(R.drawable.cancelar);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}


	}
	
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionado
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item) {
		switch (item.getItemId()) {

		case 0: //Grabar
			//Toast.makeText(this, "Has pulsado el Item 1 del Action Bar",Toast.LENGTH_SHORT).show();
			GrabarModificarVivienda();
	
			return true;

		case 1://Cancelar
			
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		case android.R.id.home:
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		}
		return false;
	}
	
    private void CargarEdificios()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Edificio> edificios =  dbutil.ObtenerEdificios();
    	dbutil.close();
    	
    	Spin_Edificios_Adapter adapter = new Spin_Edificios_Adapter(ModificarVivienda_Rural_Activity.this, edificios);
    	spMV_R_Edificios.setPrompt("Seleccione Edificio si corresponde");
    	spMV_R_Edificios.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    private void CargarTiposCalle()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<TipoCalle> tiposcalle =  dbutil.ObtenerTiposCalle();
    	dbutil.close();
    	
    	Spin_TipoCalle_Adapter adapter = new Spin_TipoCalle_Adapter(ModificarVivienda_Rural_Activity.this, tiposcalle);
    	spMV_R_TiposCalle.setPrompt("Seleccione Tipo de Calle");
    	spMV_R_TiposCalle.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
    	
    	
    }
    
    /*
    private void CargarViviendasTemporada()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<TipoViviendaTemporada> vivtemp =  dbutil.ObtenerTiposViviendaTemporada();
    	dbutil.close();
    	
    	
    	Spin_ViviendaTemporada_Adapter adapter = new Spin_ViviendaTemporada_Adapter(ModificarVivienda_Rural_Activity.this, vivtemp);
    	spMV_R_ViviendaTemporada.setPrompt("Tipo de Vivienda de Temporada");
    	spMV_R_ViviendaTemporada.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
    }
    */
   
    
    private void CargarUsoDestino()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<UsoDestino> usos =  dbutil.ObtenerUsosDestino();
    	dbutil.close();
    	
    	Spin_UsoDestino_Adapter adapter = new Spin_UsoDestino_Adapter(ModificarVivienda_Rural_Activity.this, usos);
    	spMV_R_UsoDestinoEdificacion.setPrompt("Uso o Destino Edificaci�n");
    	spMV_R_UsoDestinoEdificacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
   }

    private void CargarCategorias()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<Categoria> categorias =  dbutil.ObtenerCategoria();
    	dbutil.close();
    	
    	Spin_Categorias_Adapter adapter = new Spin_Categorias_Adapter(ModificarVivienda_Rural_Activity.this, categorias);
    	spMV_R_Categoria.setPrompt("Seleccione Categor�a");
    	spMV_R_Categoria.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
    
    private void CargarCodigosAnotacion()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	List<CodigoAnotacion> usos =  dbutil.ObtenerCodigosAnotacion();
    	dbutil.close();
    	
    	Spin_CodigosAnotacion_Adapter adapter = new Spin_CodigosAnotacion_Adapter(ModificarVivienda_Rural_Activity.this, usos);
    	spMV_R_CodigoAnotacion.setPrompt("C�digos de Anotaci�n");
    	spMV_R_CodigoAnotacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    private void CargarUbicacionHorizontal()
    {
    
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionhorizontal);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spMV_R_UbicacionHorizontal.setPrompt("Ubicaci�n horizontal dentro del sitio");
    	spMV_R_UbicacionHorizontal.setAdapter(adapter);
    	
    	adapter.notifyDataSetChanged();
    }
    
    private void CargarUbicacionVertical()
    {
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionvertical);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spMV_R_UbicacionVertical.setPrompt("Ubicaci�n vertical dentro del sitio");
    	spMV_R_UbicacionVertical.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
    
    
    private void CargarOrdenEdificacion()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	dbutil.open();
    	int maxedif =  dbutil.ObtenerMaxOrdenEdificio(globales.IDPADRE);
    	dbutil.close();
    	
    	
    	Integer[] ints = new Integer[maxedif+1];
    	
    	for(int i = 1;i<=maxedif+1;i++)
    	{
    		ints[i-1] = i;
    	}
    	
    
    	ArrayAdapter<Integer> adaptermaxedif =  new ArrayAdapter<Integer>(ModificarVivienda_Rural_Activity.this, R.layout.item_solo_string, ints);
    	spMV_R_OrdenEdificacion.setPrompt("Orden de Edificaci�n");
    	spMV_R_OrdenEdificacion.setAdapter(adaptermaxedif);
    	adaptermaxedif.notifyDataSetChanged();
    	spMV_R_OrdenEdificacion.setSelection(maxedif);
    	
    }
    private void AgregarNuevoHogar()
    {
    	ImageButton imgBtnBorrarHogar;
    	TextView txtNumHogar;
    	EditText etvNumPersonasHogar;
        EditText etvJefeHogar;

        int total =loMV_R_Hogares.getChildCount();

        if(total>0)
        {
            View v;
            v = loMV_R_Hogares.getChildAt(total-1);
            etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
            etvJefeHogar= (EditText)v.findViewById(id.etvJefeHogar);

            if(etvNumPersonasHogar.getText().toString().equals(""))
            {
                Toast.makeText(ModificarVivienda_Rural_Activity.this, "Ingrese Personas al hogar anterior para agregar otro", Toast.LENGTH_SHORT).show();
                return;
            }
            else if(etvJefeHogar.getText().toString().equals(""))
            {
                Toast.makeText(ModificarVivienda_Rural_Activity.this, "Ingrese Nombre de Jefe de Hogar o SN si corresponde", Toast.LENGTH_SHORT).show();
                return;
            }

        }
    	final View child = getLayoutInflater().inflate(R.layout.item_hogar, null);
    	
    	txtNumHogar = (TextView)child.findViewById(R.id.lblNumeroHogar);
    	etvNumPersonasHogar = (EditText)child.findViewById(R.id.etvNumPersonasHogar);
    	
    	imgBtnBorrarHogar = (ImageButton)child.findViewById(R.id.imgBtnBorrarHog);

    	imgBtnBorrarHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loMV_R_Hogares.removeView(child);
				RenumerarHogares();
				
			}
		});
    	
    	
    	loMV_R_Hogares.addView(child);
    	
    	etvNumPersonasHogar.requestFocus();
    	
    	RenumerarHogares();
    	
    	
    }
    
    private void RenumerarHogares()
    {
    	TextView txtNumHogar;
    	
    	int total =loMV_R_Hogares.getChildCount();
    	View v;
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		v = loMV_R_Hogares.getChildAt(i);
        	txtNumHogar = (TextView)v.findViewById(R.id.lblNumeroHogar);
        	txtNumHogar.setText(String.valueOf(i+1));
    	}
    	
    }
    
    
    private void AgregarNuevoHogar(Hogar hog)
    {
    	ImageButton imgBtnBorrarHogar;
    	TextView txtNumHogar;
		EditText etvJefeHogar;
    	EditText etvNumPersonasHogar;
    	

    	final View child = getLayoutInflater().inflate(R.layout.item_hogar, null);
   	
    	imgBtnBorrarHogar = (ImageButton)child.findViewById(R.id.imgBtnBorrarHog);

    	imgBtnBorrarHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loMV_R_Hogares.removeView(child);
				RenumerarHogares();
				
			}
		});
    	
    	txtNumHogar = (TextView)child.findViewById(R.id.lblNumeroHogar);
		etvJefeHogar = (EditText)child.findViewById(id.etvJefeHogar);
    	etvNumPersonasHogar = (EditText)child.findViewById(R.id.etvNumPersonasHogar);
    	loMV_R_Hogares.addView(child);
    	

    	txtNumHogar.setText(String.valueOf(hog.getOrdenhogar()));
    	etvNumPersonasHogar.setText(String.valueOf(hog.getNumpersonas()));
		etvJefeHogar.setText(hog.getJefe());
    	//RenumerarHogares();
    	
    	
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (requestCode ==RESULT_SPEECH && resultCode == RESULT_OK && null != data) {

			ArrayList<String> text = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

			etvMV_R_Descripcion.setText(text.get(0));
		}
    	
    	
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) 
        {
        	
        	BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

        	try {
        		
        		bm = BitmapFactory.decodeFile(mCurrentPhotoPath);
        		
        		bm = funciones.RotateBitmap(bm, 90);
        		if(bm != null)
        		{
    		      	HayFoto = true;
    				funciones.ResizeAndSaveImage(ModificarVivienda_Rural_Activity.this, bm,320, 240, 80, ID_VIVIENDA);


    		      	imgMV_R_FotoViv.setImageBitmap(bm);        			
        		}
        		else
        		{
    		      	HayFoto = false;
        			Toast.makeText(ModificarVivienda_Rural_Activity.this, "Error al asociar foto", Toast.LENGTH_SHORT).show();
        		}
        		

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
                    
       	
        	Toast.makeText(ModificarVivienda_Rural_Activity.this, "Foto creada", Toast.LENGTH_SHORT).show();
            //Bundle extras = data.getExtras();

        }
        
    	if(requestCode == AGREGAREDIFICIO && resultCode == RESULT_OK)
    	{
    		 UUID ID_EDIF = (UUID) data.getExtras().get("ID_EDIF");
    		 UUID ID_E;
    		 CargarEdificios();
    		 for (int i = 0; i < spMV_R_Edificios.getCount(); i++) {
    			 ID_E = ((Edificio)spMV_R_Edificios.getItemAtPosition(i)).getId_edif();
    		        if (ID_E.equals(ID_EDIF)) {
    		        	spMV_R_Edificios.setSelection(i);
    		            break;
    		        }
    		    }
    		 
    	}  
        
    	if (requestCode ==SUGERENCIAS && resultCode == RESULT_OK && null != data) {

   		 String DESCRIPCION = data.getExtras().get("DESCRIPCION").toString();
			etvMV_R_Descripcion.setText(DESCRIPCION);
		}

		if (requestCode == GRABAR_AUDIO)
		{
			if (resultCode == RESULT_OK) {
				GrabarAudio();
			} else
			{
				HayAudio = false;
			}
		}
    }

	private void GrabarAudio()
	{

		String mCurrentAudioPath =  globales.RutaAudioTemp;

		byte[] getBytes = {};
		try
		{
			File file = new File(mCurrentAudioPath);
			getBytes = new byte[(int) file.length()];
			InputStream is = new FileInputStream(file);
			is.read(getBytes);
			is.close();
			//file.delete();

			DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
			db.open();
			db.BorrarAudio(ID_VIVIENDA);
			db.InsertarAudio(ID_VIVIENDA, getBytes);
			db.close();



		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

    private void SacarFoto() {
    	
   	
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            	Toast.makeText(ModificarVivienda_Rural_Activity.this, "Error", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                
//                mCurrentPhotoPath = photoFile.getAbsolutePath();
                
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private int ObtenerMaxOrdenViv()
    {
        DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
        db.open();
        int ret = db.ObtenerOrdenViviendaMaximo(globales.IDPADRE);
        db.close();
        return ret;

    }

    private File createImageFile() throws IOException {

        File storageDir = new File(globales.RutaPicsSinImagen);
        
        storageDir.mkdirs();
        File image = new File(storageDir.getAbsolutePath() + globales.NombreFotoTemp);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        
        return image;
    }
   
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
    private void GrabarModificarVivienda()
    {
    	if(ValidarDatos() && ValidarHogares())
    	{

    		//viv = new Vivienda();
    		viv.setACT_ID(globales.actualizacion.getAct_id());
    		viv.setID_USUARIO(globales.usuario.getId_usuario());
    		viv.setID_VIVIENDA(ID_VIVIENDA);
    		viv.setID_ORIGEN(2);
    		viv.setIDPADRE(globales.IDPADRE);

    		viv.setNOMBRE_CALLE_CAMINO(acMV_R_NombreCalle.getEditableText().toString());
    		viv.setN_DOMICILIO(acMV_R_NumeroDomicilio.getEditableText().toString());
    		viv.setN_LETRA_BLOCK(etvMV_R_Block.getEditableText().toString());
    		viv.setN_PISO(etvMV_R_Piso.getEditableText().toString());
    		viv.setN_LETRA_DEPTO(etvMV_R_Depto.getEditableText().toString());

    		viv.setTIPOCALLE_ID(tipocalle.getTipocalle_id());
    		viv.setTIPOCALLE_GLOSA(tipocalle.getTipocalle_glosa());

    		viv.setID_USODESTINO(usodestino.getId_usodestino());
    		viv.setCODIGO_ID(codigoanotacion.getCodigo_id());

    		viv.setDESCRIPCION(etvMV_R_Descripcion.getEditableText().toString());

    		viv.setPOSICION(rbMV_R_Izquierda.isChecked()? 1 : 2);

    		viv.setID_ESE(0);
    		viv.setNULO(false);
    		viv.setNUEVA(false);
    		viv.setMODIFICADA(true);

    		viv.setFECHAMODIFICACION(new Date());

    		//viv.setID_EDIF( ((Edificio)spMV_R_Edificios.getSelectedItem()).getId_edif());
    		//viv.setORDEN(ObtenerCorrelativoPadre()+1);

			if(!globales.LOCALIDAD.equals(acMV_R_Localidad.getText().toString().trim()))
			{
				viv.setORDEN(1);
			}


    		Edificio myedif =(Edificio)spMV_R_Edificios.getSelectedItem();
    		viv.setID_EDIF(myedif.getId_edif());

    		if(myedif.getId_edif().equals(globales.UUID_CERO))
    		{
        		viv.setCOORD_X(viv.getCOORD_X());
        		viv.setCOORD_Y(viv.getCOORD_Y());
    		}
    		else
    		{
        		viv.setCOORD_X(lng);
        		viv.setCOORD_Y(lat);
    		}


    		if(usodestino.getId_usodestino()<=1)
    		{
    			viv.setORDEN_VIV(0);
    		}


    		viv.setUBICACIONHORIZONTAL(UbicacionHorizontal);
    		viv.setUBICACIONVERTICAL(UbicacionVertical);

    		viv.setORDENEDIFICACION((int)spMV_R_OrdenEdificacion.getSelectedItemId() + 1);

    		viv.setNOMBRELOCALIDAD(acMV_R_Localidad.getText().toString().trim().toUpperCase());
    		viv.setNOMBREENTIDAD(acMV_R_NombreEntidad.getEditableText().toString().trim().toUpperCase());
    		Categoria myCat = (Categoria)spMV_R_Categoria.getSelectedItem();

    		viv.setCAT_ID(myCat.getCat_id());

    		DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    		db.open();

    		boolean ret = db.ModificarVivienda(viv);

    		db.BorrarHogaresVivienda(viv);

    		db.close();
    		db = null;

    		Toast.makeText(ModificarVivienda_Rural_Activity.this, "Vivienda Grabada", Toast.LENGTH_SHORT).show();

    		GrabarHogares();

    		GrabarImagen();


            Intent resultIntent = getIntent();

    		if(acMV_R_Localidad.getText().toString().equals(globales.LOCALIDAD))
            {
                getIntent().putExtra("CAMBIO_LOC", false);
            }
            else
            {
                getIntent().putExtra("CAMBIO_LOC", true);
            }

            globales.LOCALIDAD = acMV_R_Localidad.getText().toString().toUpperCase();

    		resultIntent.putExtra("MODIFICAR_VIVIENDA", viv);

    		setResult(Activity.RESULT_OK,resultIntent);
    		
    		finish();

    		
    	}
    	
    	
    	
    }
    
    private boolean ValidarDatos()
    {
    	boolean retorno;
    	Toast toast = Toast.makeText(ModificarVivienda_Rural_Activity.this, "", Toast.LENGTH_SHORT);
    
    	/*
    	if(etvMV_R_Localidad.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Localidad. Si no tiene digite SN");
    		retorno = false;
    	}
    	else if(acMV_R_NombreEntidad.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Entidad. Si no tiene digite SN");
    		retorno = false;
    	} 
    	else 
    	*/
    	if(spMV_R_OrdenEdificacion.getSelectedItem()==null)
    	{
    		toast.setText("Debe ingresar Orden de Edificaci�n.");
    		retorno = false;
    	}
    	else if(acMV_R_NombreCalle.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Calle.");
    		retorno = false;
    	}
    	else if(acMV_R_NumeroDomicilio.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar N�mero de Domicilio. Si  no tiene digite SN");
    		retorno = false;
    	}   
     	
    	else
    	{
    		retorno = true;
    	}
    	
    	if(!retorno)
    	{
    		toast.show();
    	}
    	
		return retorno;
    	
    }
  
    private boolean ValidarHogares()
    {
    	EditText etvNumPersonasHogar;
        EditText etvJefeHogar;
    	int total =loMV_R_Hogares.getChildCount();
    	View v;
    	boolean ret = true;
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		
    		v = loMV_R_Hogares.getChildAt(i);
    		etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
            etvJefeHogar = (EditText)v.findViewById(id.etvJefeHogar);

        	if(etvNumPersonasHogar.getEditableText().toString().equals(""))
        	{
        	
        		Toast.makeText(ModificarVivienda_Rural_Activity.this, "Debe ingresar N� de Personas en el Hogar " + (i+1), Toast.LENGTH_SHORT).show();
        		ret = false;
        		break;
        	}
        	else if (etvJefeHogar.getEditableText().toString().equals(""))
            {

                Toast.makeText(ModificarVivienda_Rural_Activity.this, "Debe ingresar Nombre de Jefe de Hogar o SN si no fue entregado ", Toast.LENGTH_SHORT).show();
                ret = false;
                break;
            }

        			

    	}
    	
    	return ret;
    	
    	
    }
    
    private void GrabarHogares()
    {
    	EditText etvNumPersonasHogar;
        EditText etvJefeHogar;
    	Hogar hog;
    	int total =loMV_R_Hogares.getChildCount();
    	View v;
    	
    	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
    	db.open();
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		hog = new Hogar();
    		
    		v = loMV_R_Hogares.getChildAt(i);
    		etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
            etvJefeHogar= (EditText)v.findViewById(id.etvJefeHogar);

        	hog.setAct_id(globales.actualizacion.getAct_id());
        	hog.setId_usuario(globales.usuario.getId_usuario());
        	hog.setId_vivienda(ID_VIVIENDA);
        	hog.setOrdenhogar(i+1);
        	hog.setNumpersonas(Integer.valueOf(etvNumPersonasHogar.getEditableText().toString()));
        	hog.setJefe(etvJefeHogar.getText().toString());
        	db.InsertarHogar(hog);
    	}
    	
    	db.close();
    	
    }
    
    private void GrabarImagen()
    {
    	if(HayFoto)
    	{
    		BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;

        	try 
        	{
        		bm = ((BitmapDrawable)imgMV_R_FotoViv.getDrawable()).getBitmap();
        		
        		if(bm != null)
        		{
    				funciones.ResizeAndSaveImage(ModificarVivienda_Rural_Activity.this, bm,320, 240, 100, ID_VIVIENDA);
        		}
        	}
    		catch (Exception e) 
    		{
    			
    		}
					// TODO: handle exception
    	}
    }
    
    private void CargarFotoViv()
    {
       
        	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
        	db.open();
        	Imagen img = db.ObtenerImagen(viv.getID_VIVIENDA());
        	db.close();

        	if(img!=null)
        	{
        		HayFoto = true;
	 	        BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        
	        	Bitmap bm = BitmapFactory.decodeByteArray(img.getIMAGEN(), 0, img.getIMAGEN().length, options);
	
	        	imgMV_R_FotoViv.setImageBitmap(bm);
        	}
        	
    }
	private class TaskCargarSugerenciasCalles extends AsyncTask<Void, Void, List<String>>
	{
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(ModificarVivienda_Rural_Activity.this);
			pd.setTitle("Cargando Sugerencias Calles..");
			pd.setMessage("Por favor espere...");       
			pd.setIndeterminate(true);
			pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//			pd.setMax(100);
			pd.show();
		}
		
		@Override
		protected List<String> doInBackground(Void... args) 
		{
			
			try {
				return CargarSugerencias();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}
		
	    private List<String> CargarSugerencias()
	    {
	    	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
	    	db.open();
	    	List<String> calles = db.ObtenerSugerenciasCalle(globales.IDPADRE);
	    	db.close();
	    	
	    	try {
				//calles = AgregarSugerenciasCallesMapa(calles);
	    		calles = AgregarSugerenciasCallesMapa(calles);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	return calles;
	    	
	    	
	    	


	    }
	    
		 @SuppressWarnings("finally")
		public List<String> AgregarSugerenciasCallesMapa(List<String> calles) throws Exception {
			 try 
			 {	     
				 DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
				 db.open();
				 Manzana mz = db.ObtenerManzana(globales.IDPADRE,0);
				 
				 DataBaseSpatialite sldb = new DataBaseSpatialite();
				 if(mz!=null)
				 {
					 sldb.ObtenerCallesPadre(String.valueOf(mz.getGeocodigo()), calles);
				 }
				 else
				 {
					 Seccion sec = db.ObtenerSeccion(globales.IDPADRE);
					 sldb.ObtenerCallesPadre(String.valueOf(sec.getCu_seccion()), calles);
				 }
				 
					 
				 db.close();

			 } 
			 catch (Exception e) 
			 {
		            e.printStackTrace();
		            Log.d("EnumeracionViviendas","open database"  +  e.getMessage().toString());
		     }
			 finally
			 {
				   return calles;
			 }
		    }


		
		@Override 
		protected void onPostExecute(List<String> calles) 
		{
			
			Collections.sort(calles);
			calles.remove("SN");
			calles.add(0, "SN");	
			
			
	    	ArrayAdapter<String> adapterCalles =  new ArrayAdapter<String>(ModificarVivienda_Rural_Activity.this, R.layout.item_solo_string, calles);
	    	
	    	acMV_R_NombreCalle.setThreshold(1);
	    	acMV_R_NombreCalle.setAdapter(adapterCalles);
	    	adapterCalles.setNotifyOnChange(true);
	    	
			if (pd != null) {
				pd.dismiss();
			}

		}
	}

	private class TaskCargarLocalidades extends AsyncTask<Void, Void, List<String>>
	{


		@Override
		protected List<String> doInBackground(Void... args)
		{

			try
			{
				return CargarLocalidades();

			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}

		private List<String> CargarLocalidades()
		{

			List<String> locs = null;
			try {


				DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
				db.open();
				locs = db.ObtenerLocalidadesPadre(globales.IDPADRE);
				db.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return locs;

		}




		@Override
		protected void onPostExecute(List<String> locs)
		{

			if(locs !=null)
			{

				Collections.sort(locs);

				ArrayAdapter<String> adapterLocalidades=  new ArrayAdapter<String>(ModificarVivienda_Rural_Activity.this, R.layout.item_solo_string, locs);

				acMV_R_Localidad.setThreshold(1);
				acMV_R_Localidad.setAdapter(adapterLocalidades);
				adapterLocalidades.setNotifyOnChange(true);

				acMV_R_Localidad.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if(hasFocus)
						{
							acMV_R_Localidad.showDropDown();
						}
					}
				});

			}
		}
	}


	private class TaskCargarEntidades extends AsyncTask<Void, Void, List<String>>
	{
		

		@Override
		protected List<String> doInBackground(Void... args) 
		{
			
			try 
			{
				return CargarEntidades();
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				return null;
			}

		}
		
	    private List<String> CargarEntidades()
	    {
	    	
	    	List<String> entidades = null;
	    	try {
	    		
	    		
	    	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
	    	db.open();
	    	entidades = db.ObtenerEntidadesPadre(globales.IDPADRE);
	    	db.close();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	return entidades;

	    }
	    


		
		@Override 
		protected void onPostExecute(List<String> entidades) 
		{
			
			if(entidades !=null)
			{
				Collections.sort(entidades);
				
		    	ArrayAdapter<String> adapterEntidades=  new ArrayAdapter<String>(ModificarVivienda_Rural_Activity.this, R.layout.item_solo_string, entidades);
		    	
		    	acMV_R_NombreEntidad.setThreshold(1);
		    	acMV_R_NombreEntidad.setAdapter(adapterEntidades);
		    	adapterEntidades.setNotifyOnChange(true);
		    	
		    	acMV_R_NombreEntidad.setOnFocusChangeListener(new OnFocusChangeListener() {
					
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if(hasFocus)
						{
							acMV_R_NombreEntidad.showDropDown();
						}
					}
				});

			}
		}
	}

	private void ReproducirAudio()
	{
		MediaPlayer mp = new MediaPlayer();
		try
		{
			FileInputStream fis = new FileInputStream(globales.RutaAudioTemp);
			mp.setDataSource(fis.getFD());
			fis.close();
			mp.prepare();
		}
		catch (Exception e)
		{
			Toast.makeText(ModificarVivienda_Rural_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}

		mp.start();

	}



	private void CrearArchivoAudioDesdeBD()
	{
		Imagen objaudio;
		DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Rural_Activity.this);
		db.open();
		objaudio = db.ObtenerAudio(ID_VIVIENDA);
		db.close();

		if(null != objaudio)
		{
			try {
				HayAudio  = true;
				FileOutputStream out = new FileOutputStream(globales.RutaAudioTemp);
				out.write(objaudio.getIMAGEN());
				out.close();

				btnMV_R_PlayAudio.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				Toast.makeText(ModificarVivienda_Rural_Activity.this, "Error al Crear Audio: " + e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}
		else
		{
			HayAudio = false;
			btnMV_R_PlayAudio.setVisibility(View.INVISIBLE);
		}

	}
}
