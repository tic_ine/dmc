package com.INE.ImasD.EnumeracionMobile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.ColorAdapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_CodigosAnotacion_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Edificios_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_SoloString_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_TipoCalle_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_UsoDestino_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_ViviendaTemporada_Adapter;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.Mapa_Simple_DialogFragment.AccionesMapaSimple;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.R.id;
import com.esri.core.geometry.Point;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;



public class ModificarVivienda_Activity extends Activity
{
	static final int REQUEST_IMAGE_CAPTURE = 1;
	private static final String TAG = "" + ModificarVivienda_Activity.class;
	static final int SUGERENCIAS = 3;  // The request code
			
	double lat;
	double lng;
	TextView lblLatitud;
	TextView lblLongitud;
	
	double latEdificio;
	double lngEdificio;
	
	boolean primeracarga = true;
	
	/*CONTROLES FORM*/
	ImageView imgMVFoto;
	Spinner spMVEdificios;
	Spinner spMVTiposCalle;
	Spinner spMVViviendaTemporada;
	Spinner spMVUsoDestinoEdificacion;
	Spinner spMVCodigoAnotacion;
	Spinner spMVUbicacionHorizontal;
	Spinner spMVUbicacionVertical;
	
	ImageButton btnMVAddHogar;
	
	/*
	Button btnMVMateriales;
	Button btnMVColores;
	Button btnMVOtrosTextos;
	Button btnMVOtrosUsos;
	Button btnMVObjetos;
	Button btnMVOrdenVivSitio;
	
	*/
	Button btnMVVerCoordenada;
	
	
	
	Spinner spMVOrdenEdificacion ;
	AutoCompleteTextView acMVNombreCalle;
	EditText etvMVNumeroDomicilio;
	EditText etvMVBlock;
	EditText etvMVPiso;
	EditText etvMVDepto;
	EditText etvMVDescripcion;
	RadioButton rbMVIzquierda;
	RadioButton rbMVDerecha;

	TextView tvMVOrdenVivienda;
	/*FIN CONTROLES FORM*/
	UUID ID_VIVIENDA;
	
	LinearLayout loMVHogares;
	public String mCurrentPhotoPath = "";
	
	TipoCalle tipocalle;
	TipoViviendaTemporada tipoviviendatemporada;
	UsoDestino usodestino;
	CodigoAnotacion codigoanotacion;
	
	String UbicacionHorizontal;
	String UbicacionVertical;
	
	ImageView imgMVRotar;
	Button btnMVSugerencia;
	
	ProgressDialog pd;
	
	Button btnSpeak;
	
	Vivienda viv;
    protected static final int RESULT_SPEECH = 1;
    boolean HayFoto = false;
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.modificarviv_activity);


		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
	    //mCurrentPhotoPath = "";

	    //ID_VIVIENDA = UUID.randomUUID();

	    loMVHogares =  (LinearLayout)findViewById(R.id.loMVHogaresVivienda);
    	
	    imgMVFoto = (ImageView)findViewById(R.id.imgMVFoto);
	    
	    btnMVAddHogar = (ImageButton)findViewById(R.id.btnMVAddHog);
	    
	    /*
	    btnMVMateriales = (Button)findViewById(R.id.btnMVMateriales);
	    btnMVColores = (Button)findViewById(R.id.btnMVColores);
	    btnMVOtrosTextos = (Button)findViewById(R.id.btnMVOtrosTextos);
	    btnMVOtrosUsos = (Button)findViewById(R.id.btnMVOtrosUsos);
	    btnMVObjetos = (Button)findViewById(R.id.btnMVObjetosDesc);
	    btnMVOrdenVivSitio = (Button)findViewById(R.id.btnMVOrdenVivSitio);
	    */
	    btnMVVerCoordenada = (Button)findViewById(R.id.btnMVVerCoordenada);
	    
	    spMVEdificios= (Spinner)findViewById(R.id.spMVEdificios);
	    spMVTiposCalle = (Spinner)findViewById(R.id.spMVTiposCalle);
	    spMVViviendaTemporada = (Spinner)findViewById(R.id.spMVViviendaTemporada);
	    spMVUsoDestinoEdificacion = (Spinner)findViewById(R.id.spMVUsoDestinoEdificacion);
	    spMVCodigoAnotacion = (Spinner)findViewById(R.id.spMVCodigoAnotacion);
	    
	    spMVUbicacionHorizontal = (Spinner)findViewById(R.id.spMVUbicacionHorizontal);
	    spMVUbicacionVertical = (Spinner)findViewById(R.id.spMVUbicacionVertical);
	    
	    
	    spMVOrdenEdificacion = (Spinner)findViewById(id.spMVOrdenEdificacion);
	    acMVNombreCalle = (AutoCompleteTextView)findViewById(id.acMVNombreCalle);
	    etvMVNumeroDomicilio = (EditText)findViewById(id.etvMVNumeroDomicilio);
	    etvMVBlock = (EditText)findViewById(R.id.etvMVBlock);
	    etvMVPiso = (EditText)findViewById(id.etvMVPiso);
	    etvMVDepto = (EditText)findViewById(id.etvMVDepto);
	    etvMVDescripcion = (EditText)findViewById(id.etvMVDescripcion);
	    
	    
	    tvMVOrdenVivienda = (TextView)findViewById(R.id.tvMVOrdenVivienda);
	    
	    rbMVIzquierda = (RadioButton)findViewById(id.rbMVIzquierda);
	    rbMVDerecha = (RadioButton)findViewById(id.rbMVDerecha);
	    
	    imgMVRotar = (ImageView)findViewById(R.id.imgMVRotar);
	    btnMVSugerencia = (Button)findViewById(R.id.btnMVSugerencia);
	    
	    
	    imgMVFoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				SacarFoto();
			}
		});
	    
	    btnMVAddHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AgregarNuevoHogar();
				
			}
		});
	    
    
	    spMVTiposCalle.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				tipocalle =  (TipoCalle)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMVUsoDestinoEdificacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				usodestino =  (UsoDestino)parent.getItemAtPosition(position);
			
				if(usodestino.getId_usodestino()<=1)
				{
					tvMVOrdenVivienda.setText("0");
				}
				else
				{
					Toast.makeText(ModificarVivienda_Activity.this, "El orden de vivienda definitivo se dar� al reordenar el listado", Toast.LENGTH_LONG).show();
					tvMVOrdenVivienda.setText(String.valueOf(viv.getORDEN_VIV()));
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMVCodigoAnotacion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				codigoanotacion =  (CodigoAnotacion)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    spMVViviendaTemporada.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				tipoviviendatemporada =  (TipoViviendaTemporada)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMVUbicacionHorizontal.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionHorizontal = (String)parent.getItemAtPosition(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spMVUbicacionVertical.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				UbicacionVertical= (String)parent.getItemAtPosition(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    
	    acMVNombreCalle.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				acMVNombreCalle.showDropDown();
	            acMVNombreCalle.requestFocus();
	            return false;
			}
		});
	    
	    Intent i = getIntent();
	    
	    lblLatitud = (TextView)findViewById(R.id.tvMVLatitud);
	    lblLongitud = (TextView)findViewById(R.id.tvMVLongitud);
	    

	    CargarEdificios();
	    CargarTiposCalle();
	    
	    
	    
	    CargarViviendasTemporada();
	    CargarUsoDestino();
	    CargarCodigosAnotacion();
	    
	    CargarUbicacionHorizontal();
	    CargarUbicacionVertical();
	    
	    CargarOrdenEdificacion();
	 
	    
		btnSpeak = (Button) findViewById(R.id.btnSpeak);
/*
		btnSpeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Intent i = new Intent(ModificarVivienda_Activity.this, VoiceRecognition_Activity.class);

				startActivityForResult(i, RESULT_SPEECH); 
				
			}
 
		});
		*/
		
	    ID_VIVIENDA =UUID.fromString(i.getStringExtra("ID_VIVIENDA"));
	    ObtenerVivienda();
	    
	    lat = viv.getCOORD_Y();
	    lng = viv.getCOORD_X();
	    
	    new TaskCargarSugerenciasCalles().execute();
	    
	    
	    lblLatitud.setText(String.valueOf(lat));
	    lblLongitud.setText(String.valueOf(lng));
	    
	    acMVNombreCalle.setText(viv.getNOMBRE_CALLE_CAMINO());
		    etvMVNumeroDomicilio.setText(viv.getN_DOMICILIO());	    	
	    

	    etvMVBlock.setText(viv.getN_LETRA_BLOCK());
	    etvMVPiso.setText(viv.getN_PISO());
	    etvMVDepto.setText(viv.getN_LETRA_DEPTO());
	    
	    
	    spMVOrdenEdificacion.setSelection(viv.getORDENEDIFICACION()-1);
	    tvMVOrdenVivienda.setText(String.valueOf(viv.getORDEN_VIV()));
	    
	    spMVUsoDestinoEdificacion.setSelection(((Spin_UsoDestino_Adapter)spMVUsoDestinoEdificacion.getAdapter()).getPositionById(viv.getID_USODESTINO()));
	    
	    spMVTiposCalle.setSelection(((Spin_TipoCalle_Adapter)spMVTiposCalle.getAdapter()).getPositionById(viv.getTIPOCALLE_ID()));
	    spMVCodigoAnotacion.setSelection(((Spin_CodigosAnotacion_Adapter)spMVCodigoAnotacion.getAdapter()).getPositionById(viv.getCODIGO_ID()));
	    spMVViviendaTemporada.setSelection(((Spin_ViviendaTemporada_Adapter)spMVViviendaTemporada.getAdapter()).getPositionById(viv.getVIVTEMP_ID()));
	    spMVUbicacionHorizontal.setSelection(((Spin_SoloString_Adapter) spMVUbicacionHorizontal.getAdapter()).getPosition(viv.getUBICACIONHORIZONTAL()));
	    spMVUbicacionVertical.setSelection(((Spin_SoloString_Adapter) spMVUbicacionVertical.getAdapter()).getPosition(viv.getUBICACIONVERTICAL()));
	    spMVEdificios.setSelection(((Spin_Edificios_Adapter)spMVEdificios.getAdapter()).getPositionById(viv.getID_EDIF()));	    
	    
	    if(viv.getPOSICION() == 1) {rbMVIzquierda.setChecked(true);} else { rbMVDerecha.setChecked(true);} 
	    
	    etvMVDescripcion.setText(viv.getDESCRIPCION());
	    
	    ObteneryAgregarHogares();
	    
	    CargarFotoViv(); 
       
        
        spMVEdificios.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				primeracarga = false;
				return false;
			}
		});
        spMVEdificios.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			    if(!primeracarga)
			    {
					Edificio edif = (Edificio)spMVEdificios.getItemAtPosition(position);
					//"00000000-0000-0000-0000-000000000000"
					if(position==0)
					{
						lat = viv.getCOORD_Y();
						lng = viv.getCOORD_X();
						lblLongitud.setText(String.valueOf(viv.getCOORD_X()));
						
						lblLatitud.setText(String.valueOf(viv.getCOORD_Y()));
						lblLongitud.setText(String.valueOf(viv.getCOORD_X()));
						acMVNombreCalle.setText("");
						etvMVNumeroDomicilio.setText("");
						acMVNombreCalle.setEnabled(true);
						etvMVNumeroDomicilio.setEnabled(true);
						Toast.makeText(ModificarVivienda_Activity.this, "Se asumir� coordenada original", Toast.LENGTH_SHORT).show();
					}
					else
					{
						lblLatitud.setText(String.valueOf(edif.getCoord_lat()));
						lblLongitud.setText(String.valueOf(edif.getCoord_lng()));
						acMVNombreCalle.setText(edif.getCalle());
						etvMVNumeroDomicilio.setText(edif.getNumero());
						
						acMVNombreCalle.setEnabled(false);
						etvMVNumeroDomicilio.setEnabled(false);
						
						latEdificio = edif.getCoord_lat();
						lngEdificio= edif.getCoord_lng();
						
						Toast.makeText(ModificarVivienda_Activity.this, "Se asumir� coordenada de edificio", Toast.LENGTH_SHORT).show();
						
					}
					
			    }
			    
				
			/*
				if(position==0)
				{
					
					lblLatitud.setText(String.valueOf(viv.getCOORD_Y()));
					lblLongitud.setText(String.valueOf(viv.getCOORD_X()));
					Toast.makeText(ModificarVivienda_Activity.this, "Se asumir� coordenada original", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Edificio edif = (Edificio)spMVEdificios.getItemAtPosition(position);
					lblLatitud.setText(String.valueOf(edif.getCoord_lat()));
					lblLongitud.setText(String.valueOf(edif.getCoord_lng()));
					Toast.makeText(ModificarVivienda_Activity.this, "Se asumir� coordenada de edificio", Toast.LENGTH_SHORT).show();
					
				}
				
	*/
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
        
        
        
        /*
		btnMVMateriales.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("MATERIALES", "materiales");
				
			}
		});
		
		btnMVColores.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("COLORES", "Colores");
				
			}
		});
		
		btnMVOtrosTextos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				CargarSugerenciasDescripcion("OTROS", "texto");
				
			}
		});		
		
		btnMVOtrosUsos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OTRO_USO", " ejemplos Otros Usos");
			}
		});
		
		btnMVObjetos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("OBJETO", " objeto a describir");
			}
		});
		
		btnMVOrdenVivSitio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CargarSugerenciasDescripcion("VIVIENDA", " Orden Viviendas en Sitio seg�n recorrido");
			}
		});
		*/
        
        
		btnMVVerCoordenada.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Point pt;
				
				if(((Edificio)spMVEdificios.getSelectedItem()).getId_edif().equals(globales.UUID_CERO))
				{
					pt= new Point(viv.getCOORD_X(), viv.getCOORD_Y());	
				}
				else
				{
					pt= new Point(lngEdificio, latEdificio);
				}
				
				//pt= new Point(viv.getCOORD_X(), viv.getCOORD_Y());
				FragmentManager fm = ((Activity)ModificarVivienda_Activity.this).getFragmentManager();
				
				Mapa_Simple_DialogFragment dialogo = Mapa_Simple_DialogFragment.newInstance ("Punto Vivienda", AccionesMapaSimple.PUNTO_VIVIENDA, viv.getID_VIVIENDA(), false);
				dialogo.geometria = pt;
				dialogo.show(fm, AccionesMapaSimple.PUNTO_VIVIENDA.toString());
			}
		});
		
		imgMVRotar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(HayFoto)
				{
					// TODO Auto-generated method stub
					Bitmap bm = ((BitmapDrawable)imgMVFoto.getDrawable()).getBitmap();
					bm = funciones.RotateBitmap(bm, 90);
					imgMVFoto.setImageBitmap(bm);
				}
			
			
				
			}
		});
		
		btnMVSugerencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ModificarVivienda_Activity.this, Descripcion_Activity.class);
				i.putExtra("DESCRIPCION", etvMVDescripcion.getEditableText().toString());

				
				startActivityForResult(i, SUGERENCIAS);
			}
		});
		
    }
    

    private void ObtenerVivienda()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	viv =  dbutil.ObtenerVivienda(ID_VIVIENDA);
    	dbutil.close();
    	
    	
    }
    
    private void ObteneryAgregarHogares()
    {
      	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<Hogar> hogares =  dbutil.ObtenerHogares(ID_VIVIENDA);
    	dbutil.close();
    	
    	for(Hogar hog : hogares)
    	{
    		AgregarNuevoHogar(hog);
    	}
    	
    }
    
    
    private void CargarSugerenciasDescripcion(String TipoSugerencia, String Titulo)
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<Parametro> params =  dbutil.ObtenerParametrosMapa(TipoSugerencia, "VALOR");
    	dbutil.close();
    	
    	final List<String> items = new ArrayList<String>();
    	
    	
    	for(Parametro param : params)
    	{
    		items.add(param.getValor());
    	}



    	final ArrayAdapter<String> adapter = new ColorAdapter(this, items);

    	AlertDialog.Builder builder3=new AlertDialog.Builder(ModificarVivienda_Activity.this);
    	builder3.setAdapter(adapter, new DialogInterface.OnClickListener() {

    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			// TODO Auto-generated method stub

    			etvMVDescripcion.getText().insert(etvMVDescripcion.getSelectionStart(),adapter.getItem(which) + " ");
    			etvMVDescripcion.setSelection(etvMVDescripcion.getText().length());
    		}
    	});

    		
    		  builder3.setTitle("Seleccione " + Titulo);

    		  builder3.show();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	CrearMenu(menu);
    	return true;
    	
    	
    }
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Grabar");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.grabar);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
		{
			item2.setIcon(R.drawable.cancelar);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}


	}
	
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionado
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item) {
		switch (item.getItemId()) {

		case 0: //Grabar
			//Toast.makeText(this, "Has pulsado el Item 1 del Action Bar",Toast.LENGTH_SHORT).show();
			GrabarModificarVivienda();
    		setResult(Activity.RESULT_OK);
    		finish();			
			return true;

		case 1://Cancelar
			
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		case android.R.id.home:
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		}
		return false;
	}
	
    private void CargarEdificios()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<Edificio> edificios =  dbutil.ObtenerEdificios();
    	dbutil.close();
    	
    	Spin_Edificios_Adapter adapter = new Spin_Edificios_Adapter(ModificarVivienda_Activity.this, edificios);
    	spMVEdificios.setPrompt("Seleccione Edificio si corresponde");
    	spMVEdificios.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    private void CargarTiposCalle()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<TipoCalle> tiposcalle =  dbutil.ObtenerTiposCalle();
    	dbutil.close();
    	
    	Spin_TipoCalle_Adapter adapter = new Spin_TipoCalle_Adapter(ModificarVivienda_Activity.this, tiposcalle);
    	spMVTiposCalle.setPrompt("Seleccione Tipo de Calle");
    	spMVTiposCalle.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
    	
    	
    }
    
    private void CargarViviendasTemporada()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<TipoViviendaTemporada> vivtemp =  dbutil.ObtenerTiposViviendaTemporada();
    	dbutil.close();
    	
    	Spin_ViviendaTemporada_Adapter adapter = new Spin_ViviendaTemporada_Adapter(ModificarVivienda_Activity.this, vivtemp);
    	spMVViviendaTemporada.setPrompt("Tipo de Vivienda de Temporada");
    	spMVViviendaTemporada.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
   
    
    private void CargarUsoDestino()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<UsoDestino> usos =  dbutil.ObtenerUsosDestino();
    	dbutil.close();
    	
    	Spin_UsoDestino_Adapter adapter = new Spin_UsoDestino_Adapter(ModificarVivienda_Activity.this, usos);
    	spMVUsoDestinoEdificacion.setPrompt("Uso o Destino Edificaci�n");
    	spMVUsoDestinoEdificacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
   }

    private void CargarCodigosAnotacion()
    {
    	
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	List<CodigoAnotacion> usos =  dbutil.ObtenerCodigosAnotacion();
    	dbutil.close();
    	
    	Spin_CodigosAnotacion_Adapter adapter = new Spin_CodigosAnotacion_Adapter(ModificarVivienda_Activity.this, usos);
    	spMVCodigoAnotacion.setPrompt("C�digos de Anotaci�n");
    	spMVCodigoAnotacion.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	
   }
    
    private void CargarUbicacionHorizontal()
    {
    
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionhorizontal);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spMVUbicacionHorizontal.setPrompt("Ubicaci�n horizontal dentro del sitio");
    	spMVUbicacionHorizontal.setAdapter(adapter);
    	
    	adapter.notifyDataSetChanged();
    }
    
    private void CargarUbicacionVertical()
    {
    	String[] myResArray = getResources().getStringArray(R.array.ubicacionvertical);
    	List<String> myResArrayList = Arrays.asList(myResArray);
    	
    	Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(this,myResArrayList);
    	spMVUbicacionVertical.setPrompt("Ubicaci�n vertical dentro del sitio");
    	spMVUbicacionVertical.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    }
    
    
    private void CargarOrdenEdificacion()
    {
    	DataBaseUtil dbutil = new DataBaseUtil(ModificarVivienda_Activity.this);
    	dbutil.open();
    	int maxedif =  dbutil.ObtenerMaxOrdenEdificio(globales.IDPADRE);
    	dbutil.close();
    	
    	
    	Integer[] ints = new Integer[maxedif+1];
    	
    	for(int i = 1;i<=maxedif+1;i++)
    	{
    		ints[i-1] = i;
    	}
    	
    
    	ArrayAdapter<Integer> adaptermaxedif =  new ArrayAdapter<Integer>(ModificarVivienda_Activity.this, R.layout.item_solo_string, ints);
    	spMVOrdenEdificacion.setPrompt("Orden de Edificaci�n");
    	spMVOrdenEdificacion.setAdapter(adaptermaxedif);
    	adaptermaxedif.notifyDataSetChanged();
    	spMVOrdenEdificacion.setSelection(maxedif);
    	
    }
    private void AgregarNuevoHogar()
    {
    	ImageButton imgBtnBorrarHogar;
    	TextView txtNumHogar;
    	EditText etvNumPersonasHogar;
    	

    	final View child = getLayoutInflater().inflate(R.layout.item_hogar, null);
    	
    	txtNumHogar = (TextView)child.findViewById(R.id.lblNumeroHogar);
    	etvNumPersonasHogar = (EditText)child.findViewById(R.id.etvNumPersonasHogar);
    	
    	imgBtnBorrarHogar = (ImageButton)child.findViewById(R.id.imgBtnBorrarHog);

    	imgBtnBorrarHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loMVHogares.removeView(child);
				RenumerarHogares();
				
			}
		});
    	
    	
    	loMVHogares.addView(child);
    	
    	etvNumPersonasHogar.requestFocus();
    	
    	RenumerarHogares();
    	
    	
    }
    
    private void RenumerarHogares()
    {
    	TextView txtNumHogar;
    	
    	int total =loMVHogares.getChildCount();
    	View v;
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		v = loMVHogares.getChildAt(i);
        	txtNumHogar = (TextView)v.findViewById(R.id.lblNumeroHogar);
        	txtNumHogar.setText(String.valueOf(i+1));
    	}
    	
    }
    
    
    private void AgregarNuevoHogar(Hogar hog)
    {
    	ImageButton imgBtnBorrarHogar;
    	TextView txtNumHogar;
    	EditText etvNumPersonasHogar;
    	

    	final View child = getLayoutInflater().inflate(R.layout.item_hogar, null);
   	
    	imgBtnBorrarHogar = (ImageButton)child.findViewById(R.id.imgBtnBorrarHog);

    	imgBtnBorrarHogar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loMVHogares.removeView(child);
				RenumerarHogares();
				
			}
		});
    	
    	txtNumHogar = (TextView)child.findViewById(R.id.lblNumeroHogar);
    	etvNumPersonasHogar = (EditText)child.findViewById(R.id.etvNumPersonasHogar);
    	loMVHogares.addView(child);
    	

    	txtNumHogar.setText(String.valueOf(hog.getOrdenhogar()));
    	etvNumPersonasHogar.setText(String.valueOf(hog.getNumpersonas()));
    	
    	//RenumerarHogares();
    	
    	
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (requestCode ==RESULT_SPEECH && resultCode == RESULT_OK && null != data) {

			ArrayList<String> text = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

			etvMVDescripcion.setText(text.get(0));
		}
    	
    	
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) 
        {
        	
        	BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

        	try {
        		
        		bm = BitmapFactory.decodeFile(mCurrentPhotoPath);
        		
        		bm = funciones.RotateBitmap(bm, 90);
        		if(bm != null)
        		{
    		      	HayFoto = true;
    				funciones.ResizeAndSaveImage(ModificarVivienda_Activity.this, bm,320, 240, 80, ID_VIVIENDA);


    		      	imgMVFoto.setImageBitmap(bm);        			
        		}
        		else
        		{
    		      	HayFoto = false;
        			Toast.makeText(ModificarVivienda_Activity.this, "Error al asociar foto", Toast.LENGTH_SHORT).show();
        		}
        		

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
                    
       	
        	Toast.makeText(ModificarVivienda_Activity.this, "Foto creada", Toast.LENGTH_SHORT).show();
            //Bundle extras = data.getExtras();

        }
        
    	if (requestCode ==SUGERENCIAS && resultCode == RESULT_OK && null != data) {

   		 String DESCRIPCION = data.getExtras().get("DESCRIPCION").toString();
			etvMVDescripcion.setText(DESCRIPCION);
		}
    }
  /*
    private void resizeImage(Bitmap bm, int width, int height) throws IOException {
    	Bitmap photo;
    	
    	photo = Bitmap.createScaledBitmap(bm, width, height, false);
    	ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    	photo.compress(Bitmap.CompressFormat.JPEG, 80, bytes);

    	File f = new File(mCurrentPhotoPath);
    	f.createNewFile();
    	FileOutputStream fo = new FileOutputStream(f);
    	fo.write(bytes.toByteArray());
    	fo.flush();
    	fo.close();
    	
		String nombrejpg = ID_VIVIENDA.toString() + ".jpg";
		File to = new File(Environment.getExternalStorageDirectory().getPath() + "/EnumVivPics/" + nombrejpg);
		f.renameTo(to);
		f.delete();
		
    	photo.recycle();
    	
    }
    */
    private void SacarFoto() {
    	
   	
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            	Toast.makeText(ModificarVivienda_Activity.this, "Error", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                
//                mCurrentPhotoPath = photoFile.getAbsolutePath();
                
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {

        File storageDir = new File(globales.RutaPicsSinImagen);
        
        storageDir.mkdirs();
        File image = new File(storageDir + globales.NombreFotoTemp);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        
        return image;
    }
   
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
    private void GrabarModificarVivienda()
    {
    	if(ValidarDatos())
    	{
    		
    		//viv = new Vivienda();
    		viv.setACT_ID(globales.actualizacion.getAct_id());
    		viv.setID_USUARIO(globales.usuario.getId_usuario());
    		viv.setID_VIVIENDA(ID_VIVIENDA);
    		viv.setID_ORIGEN(1);
    		viv.setIDPADRE(globales.IDPADRE);

    		viv.setNOMBRE_CALLE_CAMINO(acMVNombreCalle.getEditableText().toString());
    		viv.setN_DOMICILIO(etvMVNumeroDomicilio.getEditableText().toString());
    		viv.setN_LETRA_BLOCK(etvMVBlock.getEditableText().toString());
    		viv.setN_PISO(etvMVPiso.getEditableText().toString());
    		viv.setN_LETRA_DEPTO(etvMVDepto.getEditableText().toString());
    		
    		viv.setTIPOCALLE_ID(tipocalle.getTipocalle_id());
    		viv.setTIPOCALLE_GLOSA(tipocalle.getTipocalle_glosa());
    		
    		viv.setID_USODESTINO(usodestino.getId_usodestino());
    		viv.setCODIGO_ID(codigoanotacion.getCodigo_id());

    		viv.setDESCRIPCION(etvMVDescripcion.getEditableText().toString());
    		
    		viv.setPOSICION(rbMVIzquierda.isChecked()? 1 : 2);

    		viv.setID_ESE(0);
    		viv.setNULO(false);
    		viv.setNUEVA(false);
    		viv.setMODIFICADA(true);
    		
    		viv.setFECHAMODIFICACION(new Date());
    		
    		//viv.setID_EDIF( ((Edificio)spMVEdificios.getSelectedItem()).getId_edif());
    		//viv.setORDEN(ObtenerCorrelativoPadre()+1);
    		
    		Edificio myedif =(Edificio)spMVEdificios.getSelectedItem();
    		viv.setID_EDIF(myedif.getId_edif());
    		
    		if(myedif.getId_edif().equals(globales.UUID_CERO))
    		{
        		viv.setCOORD_X(viv.getCOORD_X());
        		viv.setCOORD_Y(viv.getCOORD_Y());
    		}
    		else
    		{
        		viv.setCOORD_X(lngEdificio);
        		viv.setCOORD_Y(latEdificio);
    		}
    		
    		
    		if(usodestino.getId_usodestino()<=1)
    		{
    			viv.setORDEN_VIV(0);	
    		}
    		
    		
    		viv.setUBICACIONHORIZONTAL(UbicacionHorizontal);
    		viv.setUBICACIONVERTICAL(UbicacionVertical);
    		
    		viv.setORDENEDIFICACION((int)spMVOrdenEdificacion.getSelectedItemId() + 1);
    		
    		DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Activity.this);
    		db.open();
    		
    		boolean ret = db.ModificarVivienda(viv);
    		
    		db.BorrarHogaresVivienda(viv);
    		
    		db.close();
    		db = null;
    		
    		Toast.makeText(ModificarVivienda_Activity.this, "Vivienda Grabada", Toast.LENGTH_SHORT).show();
    		
    		
    		GrabarHogares();

    		GrabarImagen();
    		Intent resultIntent = new Intent();
    		resultIntent.putExtra("MODIFICAR_VIVIENDA", viv);
    		setResult(Activity.RESULT_OK, resultIntent);
    		
    		finish();

    		
    	}
    	
    	
    	
    }
    
    private boolean ValidarDatos()
    {
    	boolean retorno;
    	Toast toast = Toast.makeText(ModificarVivienda_Activity.this, "", Toast.LENGTH_SHORT);
    
    	if(spMVOrdenEdificacion.getSelectedItem()==null)
    	{
    		toast.setText("Debe ingresar Orden de Edificaci�n.");
    		retorno = false;
    	}
    	else if(acMVNombreCalle.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar Nombre de Calle.");
    		retorno = false;
    	}
    	else if(etvMVNumeroDomicilio.getEditableText().toString().equals(""))
    	{
    		toast.setText("Debe ingresar N�mero de Domicilio. Si  no tiene digite SN");
    		retorno = false;
    	}    	
    	else
    	{
    		retorno = true;
    	}
    	
    	if(!retorno)
    	{
    		toast.show();
    	}
    	
		return retorno;
    	
    }
    
    private void GrabarHogares()
    {
    	EditText etvNumPersonasHogar;
    	Hogar hog;
    	int total =loMVHogares.getChildCount();
    	View v;
    	
    	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Activity.this);
    	db.open();
    	
    	for(int i = 0; i<=total-1;i++)
    	{
    		hog = new Hogar();
    		
    		v = loMVHogares.getChildAt(i);
    		etvNumPersonasHogar = (EditText)v.findViewById(R.id.etvNumPersonasHogar);
        	
        	hog.setAct_id(globales.actualizacion.getAct_id());
        	hog.setId_usuario(globales.usuario.getId_usuario());
        	hog.setId_vivienda(ID_VIVIENDA);
        	hog.setOrdenhogar(i+1);
        	hog.setNumpersonas(Integer.valueOf(etvNumPersonasHogar.getEditableText().toString()));
        	//hog.setJefe("");
        	db.InsertarHogar(hog);
        	
    	}
    	
    	db.close();
    	
    }
    
    private void GrabarImagen()
    {
    	if(HayFoto)
    	{
    		BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            
        	Bitmap bm;

        	try 
        	{
        		bm = ((BitmapDrawable)imgMVFoto.getDrawable()).getBitmap();
        		
        		if(bm != null)
        		{
    				funciones.ResizeAndSaveImage(ModificarVivienda_Activity.this, bm,320, 240, 100, ID_VIVIENDA);
        		}
        	}
    		catch (Exception e) 
    		{
    			
    		}
					// TODO: handle exception
    	}
    }
  
    

    
    private void CargarFotoViv()
    {
       
        	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Activity.this);
        	db.open();
        	Imagen img = db.ObtenerImagen(viv.getID_VIVIENDA());
        	db.close();

        	if(img!=null)
        	{
        		HayFoto = true;
	 	        BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        
	        	Bitmap bm = BitmapFactory.decodeByteArray(img.getIMAGEN(), 0, img.getIMAGEN().length, options);
	
	        	imgMVFoto.setImageBitmap(bm);
        	}
        	
    }
	private class TaskCargarSugerenciasCalles extends AsyncTask<Void, Void, List<String>>
	{
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(ModificarVivienda_Activity.this);
			pd.setTitle("Cargando Sugerencias Calles..");
			pd.setMessage("Por favor espere...");       
			pd.setIndeterminate(true);
			pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//			pd.setMax(100);
			pd.show();
		}
		
		@Override
		protected List<String> doInBackground(Void... args) 
		{
			
			try {
				return CargarSugerencias();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}
		
	    private List<String> CargarSugerencias()
	    {
	    	DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Activity.this);
	    	db.open();
	    	List<String> calles = db.ObtenerSugerenciasCalle(globales.IDPADRE);
	    	db.close();
	    	
	    	try {
				//calles = AgregarSugerenciasCallesMapa(calles);
	    		calles = AgregarSugerenciasCallesMapa(calles);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	return calles;
	    	
	    	
	    	


	    }
	    
		 @SuppressWarnings("finally")
		public List<String> AgregarSugerenciasCallesMapa(List<String> calles) throws Exception {
			 try 
			 {	     
				 DataBaseUtil db = new DataBaseUtil(ModificarVivienda_Activity.this);
				 db.open();
				 Manzana mz = db.ObtenerManzana(globales.IDPADRE,0);
				 
				 DataBaseSpatialite sldb = new DataBaseSpatialite();
				 if(mz!=null)
				 {
					 sldb.ObtenerCallesPadre(String.valueOf(mz.getGeocodigo()), calles);
				 }
				 else
				 {
					 Seccion sec = db.ObtenerSeccion(globales.IDPADRE);
					 sldb.ObtenerCallesPadre(String.valueOf(sec.getCu_seccion()), calles);
				 }
				 
					 
				 db.close();

				
				 
				 
	
			 } 
			 catch (Exception e) 
			 {
		            e.printStackTrace();
		            Log.d("EnumeracionViviendas","open database"  +  e.getMessage().toString());
		     }
			 finally
			 {
				   return calles;
			 }
		    }


		
		@Override 
		protected void onPostExecute(List<String> calles) 
		{
			
			Collections.sort(calles);
			calles.remove("SN");
			calles.add(0, "SN");	
			
			
	    	ArrayAdapter<String> adapterCalles =  new ArrayAdapter<String>(ModificarVivienda_Activity.this, R.layout.item_solo_string, calles);
	    	
	    	acMVNombreCalle.setThreshold(1);
	    	acMVNombreCalle.setAdapter(adapterCalles);
	    	adapterCalles.setNotifyOnChange(true);
	    	
			if (pd != null) {
				pd.dismiss();
			}
			//new TaskCargarViviendas().execute();		
			/*
			if(padres.size()>0)
			{
				new TaskCargarViviendas().execute();				
			}
			*/

		}
	}

}
