package com.INE.ImasD.EnumeracionMobile.Entities;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mbecerra on 19-12-2017.
 */

public class Seccion_Encuesta implements Serializable
{
    private static final long serialVersionUID = 1L;

    private int idencuesta;
    private int act_id;
    private int id_usuario;
    private String nombreseccion;
    private String ayudaseccion;
    private List<Pregunta> preguntas;

    private  int totalpreg;

    public String getNombreseccion() {
        return nombreseccion;
    }

    public void setNombreseccion(String nombreseccion) {
        this.nombreseccion = nombreseccion;
    }

    public String getAyudaseccion() {
        return ayudaseccion;
    }

    public void setAyudaseccion(String ayudaseccion) {
        this.ayudaseccion = ayudaseccion;
    }

    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
        this.totalpreg = preguntas.size();
    }

    public int getTotalRespondidas()
    {
        int total = 0;
        for(Pregunta preg : preguntas)
        {
            if(preg.isRespondida())
            {
                total++;
            }

        }
        return total;
    }

    public int getTotalPreguntas()
    {
        return totalpreg;
    }
}
