package com.INE.ImasD.EnumeracionMobile;

/**
 * Created by Mbecerra on 21-11-2018.
 */

/*******************************************************************************
 * Copyright 2015 Esri
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 ******************************************************************************/

        import android.app.DialogFragment;
        import android.os.Bundle;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.ViewStub;
        import android.widget.BaseAdapter;
        import android.widget.Button;
        import android.widget.CheckBox;
        import android.widget.CompoundButton;
        import android.widget.LinearLayout;
        import android.widget.ListView;

        import com.INE.ImasD.EnumeracionMobile.OtrasClases.CapaGrafica;
        import com.esri.android.map.FeatureLayer;
        import com.esri.android.map.Layer;

/**
 * A dialog that allows the user to turn layer visibility on and off and also zoom to layers.
 */
public class LayersDialogFragment extends DialogFragment {

    private static final String TAG = LayersDialogFragment.class.getSimpleName();

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_layers, container, false);
        getDialog().setTitle("Capas");

        if (getActivity() instanceof Mapa_Activity) {
            final ListView listView_layers = (ListView) v.findViewById(R.id.listView_layers);
            final Mapa_Activity mapActivity = (Mapa_Activity) getActivity();

            listView_layers.setAdapter(new BaseAdapter() {
                @Override
                public int getCount() {
                    return mapActivity.map.getLayers().length;
                }

                @Override
                public Object getItem(int position) {
                    return getLayer(position);
                }

                private Layer  getLayer(int position) {
                    Layer[] layers = mapActivity.map.getLayers();
                    return layers[layers.length - (position + 1)];
                }

                @Override
                public long getItemId(int position) {
                    return getLayer(position).getID();
                }

                @Override
                public View getView(final int position, View v, ViewGroup parent)
                {

                    //if(v==null) {
                        v = View.inflate(getActivity(), R.layout.layer_list_item, null);
                    //}


                    final Layer l = getLayer(position);

                    boolean mostrar;
                    if(l instanceof CapaGrafica)
                    {
                        mostrar = ((CapaGrafica) l).isShowable();

                    }
                    else
                    {
                        mostrar = true;
                    }

                    if (mostrar)
                    {
                        CheckBox checkbox = (CheckBox) v.findViewById(R.id.checkbox_visible);


                        checkbox.setText(position + ".-" + l.getName());
                        checkbox.setChecked(l.isVisible());
                        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                l.setVisible(isChecked);
                            }
                        });
                        Button zoomToButton = (Button) v.findViewById(R.id.button_zoomTo);
                        zoomToButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mapActivity.map.setExtent(l.getFullExtent());
                            }
                        });

                    }
                    else
                    {

                        v = new ViewStub(getActivity());
                    }
                    return v;


                }
            });
        }

        return v;
    }

}