package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;

import java.util.List;

public class AsyncTask_CargarManzanas extends AsyncTask<Integer, Void, List<Manzana>>
{
	private Context context;
    private AsyncTaskCompleteListener<List<Manzana>> listener;
    ProgressDialog pd;

    
    public AsyncTask_CargarManzanas(Context ctx, AsyncTaskCompleteListener<List<Manzana>> listener)
    {
        this.context = ctx;
        this.listener = listener;
    }
    
    
	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();
		
		// TODO Auto-generated method stub
		pd = new ProgressDialog(context);
		pd.setTitle("Cargando Manzanas");
		pd.setMessage("Por favor espere...");       
		pd.setIndeterminate(true);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//		pd.setMax(100);
		pd.show();
	}
	
	@Override
	protected List<Manzana> doInBackground(Integer... args) 
	{
		//int area = args[0];
		
		return ObtenerManzanas();

	}
	
	@Override 
	protected void onPostExecute(List<Manzana> manzanas) 
	{
		super.onPostExecute(manzanas);
		listener.onTaskCompleteManzanas(manzanas);
		pd.dismiss();
	}
	
	private List<Manzana> ObtenerManzanas()
	{
		DataBaseUtil dbUtil = new DataBaseUtil(context);
		dbUtil.open();
		List<Manzana> manzanas = dbUtil.ObtenerManzanas();
		dbUtil.close();
		
		return manzanas;

	}
}



