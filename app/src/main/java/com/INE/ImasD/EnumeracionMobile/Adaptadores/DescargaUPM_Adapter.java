package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class DescargaUPM_Adapter extends BaseAdapter
	{

    private Context context;
    // Your custom values for the spinner (User)
    public List<Manzana> values;
    private boolean IsAllChecked;
    
    
    public DescargaUPM_Adapter(Context context, List<Manzana> manzanas) {
        super();
        this.context = context;
        this.values = manzanas;
    }
    
    public int getCount(){
        return values.size();
     }

     public Manzana getItem(int position){
        return values.get(position);
     }

     public long getItemId(int position){
        return position;
     }

     public void remove(Manzana mz)
 	{
     	values.remove(mz);
 	}
     
     public List<Manzana> getList()
 	{
     	return values;
 	}

     public void insert(Manzana mz, int idx)
 	{
     	values.add(idx, mz);
 	}
     
     public void CheckearTodos(boolean checkear)
     {
    	 	IsAllChecked = checkear;
			notifyDataSetChanged();
     }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	   	View v = convertView; 
	   	TextView txt1;
	   	TextView txt2;
	   	TextView tvDUPM_GlosaAreaUPM;
	   	CheckBox chkDUPM;
	   	final int pos = position;
	   	
	   	Manzana mz = getItem(position);
	   	
		        if (convertView == null) 
		        { // if it's not recycled, initialize some
		            // attributes
					LayoutInflater vi = 
					(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = vi.inflate(R.layout.item_upm_descarga, null);
					
		        }
		        
		        tvDUPM_GlosaAreaUPM = (TextView)v.findViewById(R.id.tvDUPM_GlosaAreaUPM);
		        txt1 = (TextView)v.findViewById(R.id.tvDUPM_PadreInfoGeografica1);
		        txt2 = (TextView)v.findViewById(R.id.tvDUPM_PadreInfoGeografica2);
		        chkDUPM = (CheckBox)v.findViewById(R.id.chkDUPM);
		        
		        
		        
		        if(mz.getOrigen()==1)
		        {
		        	//v.setBackgroundResource(R.color.LightBlue);
		        	tvDUPM_GlosaAreaUPM.setText("Urbano de:" + mz.getUrbano_nombre());
			        txt1.setText("RPC: " + mz.getComuna_id() + "-" + "Distrito: " + mz.getDistrito());
			        txt2.setText("Zona: " + mz.getZona() + " - Manzana: " + mz.getManzana());
			        
			        chkDUPM.setChecked(IsAllChecked);
			        chkDUPM.setEnabled(!IsAllChecked);
		        }
		        
		        
 		        chkDUPM.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub
						   	Manzana mz = getItem(pos);
						   	mz.setChecked(isChecked);
						   	//notifyDataSetChanged();
					   	
					}
				});
		        return v;

	    }
}
