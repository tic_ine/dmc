package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;



public class Spin_Manzanas_Adapter  extends BaseAdapter{
	//Your sent context
	private Context context;
	//Your custom values for the spinner (User)
	private List<Manzana> values;


	public Spin_Manzanas_Adapter(Context context, List<Manzana> padres) {
		super();
		this.context = context;
		this.values = padres;
	}

	public int getCount(){
		return values.size();
	}

	public Manzana getItem(int position){
		return values.get(position);
	}

	public long getItemId(int position){
		return position;
	}

    public int getPositionById(int id)
    {
    	int retorno = -1;
    	for(int i = 0; i< values.size();i++)
    	{
    		if(values.get(i).getIdmanzana()==id)
    		{
    			retorno = i;
    			break;
    		}
    	}
		return retorno;
    	
    }

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    public View getView(int position, View convertView, ViewGroup parent) {
    	 View v = convertView; 
    	 TextView txt;
    	 Manzana mz = getItem(position);
    	   	
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_padre_passive , null);
	        }
	        
	        txt = (TextView)v.findViewById(R.id.tvitem_Padre);
	        //txt.setCompoundDrawables(context.getResources().getDrawable(R.drawable.bluepushpin32), null, null, null);
	        
	        if(mz.getOrigen()==1)
	        {
		        txt.setText(mz.getUrbano_nombre() + " Distrito:" + mz.getDistrito() + " Zona:" + mz.getZona() + " Manzana:" + mz.getManzana());
	        }
	        /*
	        else
	        {
		        txt.setText(padre.getComuna_id() + "-Dis:" + padre.getDistrito() + "-" + padre.getNombrelocalidad() + "-" + padre.getNombreentidad() + "-" + padre.getNombrecategoria());
	        }
	        */
	        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	
   	 View v = convertView; 
   	 TextView txt1;
   	TextView txt2;
   	TextView txt3;
   	TextView txt4;
   	TextView txtPadreOrigen;
   	Manzana mz = getItem(position);
   	
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_padre , null);
				
	        }
	        // txtPadreInfoGeografica1
	        
	        txtPadreOrigen = (TextView)v.findViewById(R.id.tvGlosaAreaPadre);
	        txt1 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica1);
	        txt2 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica2);
	        txt3 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica3);
	        txt4 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica4);
	        
	        if(mz.getOrigen()==1)
	        {
	        	//v.setBackgroundResource(R.color.LightBlue);
	            txtPadreOrigen.setText("Urbano de:" + mz.getUrbano_nombre());
		        txt1.setText("RPC: " + mz.getComuna_id() + "-" + "Distrito: " + mz.getDistrito());
		        txt2.setText("Zona: " + mz.getZona() + " - Manzana: " + mz.getManzana());
		        txt3.setVisibility(View.GONE);
		        txt4.setVisibility(View.GONE);
	        }
	        /*
	        else
	        {
	        
		        txt3.setVisibility(View.VISIBLE);
		        txt4.setVisibility(View.VISIBLE);
	        	v.setBackgroundResource(R.color.AntiqueWhite);		        
	            txtPadreOrigen.setText("Rural. Comuna:" + padre.getComuna_nombre());
		        txt1.setText("RPC:" + padre.getComuna_id() + " - Distrito:" + padre.getDistrito());
		        txt2.setText("Localidad:" + padre.getNombrelocalidad());
		        txt3.setText("Entidad: " + padre.getNombreentidad());
		        txt4.setText("Categor�a: " + padre.getNombrecategoria());
	        }
	    */
	        return v;

    }
    
}

