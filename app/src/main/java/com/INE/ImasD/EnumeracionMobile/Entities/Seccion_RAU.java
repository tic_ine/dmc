package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.UUID;

public class Seccion_RAU {

	    private UUID sec_id;

	    private int seccion_id;

	    private int manzana;

	    private int act_id;

	    private int id_usuario;

	    private boolean nueva;

		public UUID getSec_id() {
			return sec_id;
		}

		public void setSec_id(UUID sec_id) {
			this.sec_id = sec_id;
		}

		public int getSeccion_id() {
			return seccion_id;
		}

		public void setSeccion_id(int seccion_id) {
			this.seccion_id = seccion_id;
		}

		public int getManzana() {
			return manzana;
		}

		public void setManzana(int manzana) {
			this.manzana = manzana;
		}

		public int getAct_id() {
			return act_id;
		}

		public void setAct_id(int act_id) {
			this.act_id = act_id;
		}

		public int getId_usuario() {
			return id_usuario;
		}

		public void setId_usuario(int id_usuario) {
			this.id_usuario = id_usuario;
		}

		public boolean isNueva() {
			return nueva;
		}

		public void setNueva(boolean nueva) {
			this.nueva = nueva;
		}
	    

	    
}

