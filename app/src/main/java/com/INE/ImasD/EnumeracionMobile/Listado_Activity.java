package com.INE.ImasD.EnumeracionMobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.Adaptadores.GV_Listado_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_AreaTrabajo_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Manzanas_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_Secciones_Adapter;
import com.INE.ImasD.EnumeracionMobile.Adaptadores.Spin_SoloString_Adapter;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTaskCompleteListener;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_CargarManzanas;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_CargarSecciones;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_CargarViviendas;
import com.INE.ImasD.EnumeracionMobile.AsyncTasks.AsyncTask_ReordenarViviendas;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Area_Trabajo;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.ACCIONES_TABS;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;
import com.mobeta.android.dslv.DragSortListView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import jsqlite.Exception;

//import android.support.v4.widget.SwipeRefreshLayout;
//import android.util.Log;

public class Listado_Activity extends Activity{
	ProgressDialog pd; 
	DragSortListView lstViviendas;
	Spinner spPadres;
	Spinner spLocalidades;

    Spinner spAreasTrabajo;
	Button btnGrabarListado;
	
	Button btnLV_PadresRefresh;
	
	Button btnRefrescarViviendasListado;
	//private DataBaseUtil dbUtil;
	//String IDPADRE;
	public GV_Listado_Adapter gva;
	//SwipeRefreshLayout swipeLayout;
	RadioGroup rbgArea;
	
	TextView tvVivConfirmadas;
	TextView tvVivPorConfirmar;
	TextView tvTotalRegistros;
	
	Boolean userIsInteracting = false;
	
	Boolean primeracargaviv = true;
	
	final private int ASYNC_CARGA_VIVIENDAS = 1000;
	final private int REQUEST_IMAGE_CAPTURE = 1;
	

	@Override
	 public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listado_activity);

		rbgArea = (RadioGroup)findViewById(R.id.rbgAreaViviendas);
		
		
		lstViviendas = (DragSortListView) findViewById( R.id.lstDragSortViviendas);
		
		
		lstViviendas.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        
		
        lstViviendas.setDropListener(new DragSortListView.DropListener() {
            @Override
            public void drop(int from, int to) {
                if (from != to) {
                    //DragSortListView list = lstViviendas;
                    Vivienda item = gva.getItem(from);
                    gva.remove(item);
                    gva.insert(item, to);
                    gva.notifyDataSetChanged();
                }
            }
        });
        
        lstViviendas.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) 
			{
				// TODO Auto-generated method stub

				 final int lastItem = firstVisibleItem + visibleItemCount;
				 if(lastItem == totalItemCount)
				    {
					 	globales.intPosicionListado = lastItem;
				    }
			 
				 //Log.d("SCROLL", String.valueOf(lastItem));
			}
		});
        
        tvVivConfirmadas = (TextView)findViewById(R.id.tvVivConfirmadas);
    	tvVivPorConfirmar = (TextView)findViewById(R.id.tvVivPorConfirmar);
    	tvTotalRegistros = (TextView)findViewById(R.id.tvTotalRegistrosListado);
    	
        //lstViviendas.setRemoveListener(onRemove);
        //lstViviendas.setDragScrollProfile(ssProfile);
        
		spPadres  = (Spinner)findViewById(R.id.spPadres);
		spPadres.setPrompt("Selecciona U.P.M.");

		spLocalidades = (Spinner)findViewById(R.id.spListado_Localidades);
		spLocalidades.setPrompt("Selecciona Localidades");

        spAreasTrabajo= (Spinner)findViewById(R.id.spAreasTrabajo);
        spAreasTrabajo.setPrompt("Selecciona �rea de Trabajo");


		btnGrabarListado = (Button)findViewById(R.id.btnGrabarOrdenListado);
		
		
		btnGrabarListado.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    switch (v.getId()) {
		        case R.id.btnGrabarOrdenListado:
		            //switchFragment(HelpFragment.TAG);
		              AlertDialog.Builder ad = new AlertDialog.Builder(Listado_Activity.this);
		              ad.setTitle("Reordenar viviendas");
		              ad.setIcon(R.drawable.mensajes32);
		              ad.setMessage("�Est� seguro que las viviendas sean reordenadas seg�n el orden actual?\nEsto no se podr� deshacer.");
		              ad.setPositiveButton("Grabar nuevo orden", new OnClickListener() {
						
						@SuppressWarnings("unchecked")
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
			                List<Vivienda> lst = gva.getList();
				            //  TaskReordenarViviendas task = new TaskReordenarViviendas(lst.size());
				            //task.execute(lst);

			                new AsyncTask_ReordenarViviendas(Listado_Activity.this, new AsyncTaskCompleteListener<List<Vivienda>>() {
								
								@Override
								public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
									// TODO Auto-generated method stub
									
									new AsyncTask_CargarViviendas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Vivienda>>() {
										
										@Override
										public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
											// TODO Auto-generated method stub
											CargarViviendas(viviendas);
											Toast.makeText(Listado_Activity.this, "Viviendas desde button refresh", Toast.LENGTH_LONG).show();
										}
										
										@Override
										public void onTaskCompleteManzanas(List<Manzana> manzanas) {
											// TODO Auto-generated method stub
											
										}

										@Override
										public void onTaskCompleteEnvioDatos(Integer resultado) {
											// TODO Auto-generated method stub
											
										}

										@Override
										public void onTaskCompleteDescargarDatos(Integer resultado) {
											// TODO Auto-generated method stub
											
										}

										@Override
										public void onTaskCompleteSecciones(
												List<Seccion> secciones) {
											// TODO Auto-generated method stub
											
										}
									}).execute();


								}
								
								@Override
								public void onTaskCompleteManzanas(List<Manzana> manzanas) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onTaskCompleteEnvioDatos(
										Integer resultado) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onTaskCompleteDescargarDatos(Integer resultado) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onTaskCompleteSecciones(
										List<Seccion> secciones) {
									// TODO Auto-generated method stub
									
								}
							},1,lst.size()).execute(lst);
						}
					});
		              
		              ad.setNegativeButton("Cancelar", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
						}
					});
		              ad.show();		        	
		            break;
	/*	        case R.id.textView_settings:
		            switchFragment(SettingsFragment.TAG);
		            break;
		            */
		    }
			}
		});
				
		//Toast.makeText(Listado_Activity.this, "CLick vivienda:" + position, Toast.LENGTH_SHORT).show();

		spPadres.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				if(!userIsInteracting && !primeracargaviv )
				{
					return;
				}
				
				primeracargaviv = false; 
				
				if(rbgArea.getCheckedRadioButtonId()==R.id.rbManzanas)
				{
					
					Manzana mz = (Manzana)parent.getItemAtPosition(position);
					globales.IDPADRE = Integer.toString(mz.getIdmanzana());
					
					globales.ConCentroide = true;
					globales.RPC = mz.getComuna_id();

					ObtenerConteos();
					
					try {
						DataBaseSpatialite sldb = new DataBaseSpatialite();
						globales.Polygon_UPM = sldb.ObtenerGeometriaManzana(globales.IDPADRE);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						//globales.Centroide_Proyectado = new Point(mz.getCentroide_lng(), mz.getCentroide_lat());
						e.printStackTrace();
					}
				}
				
				
				if(rbgArea.getCheckedRadioButtonId()==R.id.rbSecciones)
				{
					
					globales.ActualizarCapas = true;
					Seccion sec = (Seccion)parent.getItemAtPosition(position);
					globales.IDPADRE = Long.toString(sec.getCu_seccion());
					globales.RPC = sec.getCut();
					globales.ConCentroide = true;
					
					ObtenerConteos();
					
					try {
						globales.Polygon_UPM = new DataBaseSpatialite().ObtenerGeometriaSeccion(globales.IDPADRE);
						//globales.Polygon_Localidad = new DataBaseSpatialite().ObtenerLocalidadActual();
						globales.Polygons_AreaLevantamiento = new DataBaseSpatialite().ObtenerGeometriaAreasLevantamiento(sec);
						CargarLocalidades();
						CargarAreasTrabajo();
						globales.ActualizarCapas = true;	
						
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//globales.Centroide_Proyectado = new Point(mz.getCentroide_lng(), mz.getCentroide_lat());
						e.printStackTrace();
					}
				}				
				

				globales.accion = ACCIONES_TABS.SETEAR_CENTROIDE;
				//SetearCentroide();
				
				
				new AsyncTask_CargarViviendas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Vivienda>>() {
					
					@Override
					public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
						// TODO Auto-generated method stub
						CargarViviendas(viviendas);

						Toast.makeText(Listado_Activity.this, "Viviendas", Toast.LENGTH_LONG).show();
					}
					
					@Override
					public void onTaskCompleteManzanas(List<Manzana> manzanas) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteEnvioDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteDescargarDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteSecciones(List<Seccion> secciones) {
						// TODO Auto-generated method stub
						
					}
				}).execute();

				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		
		spLocalidades.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String strlocalidad = (String) parent.getItemAtPosition(position);
				globales.LOCALIDAD = strlocalidad;


				try {
					globales.Polygons_Localidades = new DataBaseSpatialite().ObtenerLocalidadesActual();
					globales.ActualizarCapas = true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
				
				globales.accion = ACCIONES_TABS.SETEAR_CENTROIDE;
				//SetearCentroide();
				
				
				new AsyncTask_CargarViviendas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Vivienda>>() {
					
					@Override
					public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
						// TODO Auto-generated method stub
						CargarViviendas(viviendas);

						Toast.makeText(Listado_Activity.this, "Viviendas", Toast.LENGTH_LONG).show();
					}
					
					@Override
					public void onTaskCompleteManzanas(List<Manzana> manzanas) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteEnvioDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteDescargarDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteSecciones(List<Seccion> secciones) {
						// TODO Auto-generated method stub
						
					}
				}).execute();
			
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		rbgArea.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				
				if(checkedId==R.id.rbManzanas)
				{
					spLocalidades.setVisibility(View.GONE);
					globales.padres = PADRES.MANZANAS;
					new AsyncTask_CargarManzanas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Manzana>>() {
						
						@Override
						public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteManzanas(List<Manzana> manzanas) {
							
							CargarManzanas(manzanas);
							// TODO Auto-generated method stub
							Toast.makeText(Listado_Activity.this, "Manzanas desde rgb", Toast.LENGTH_LONG).show();
	
						}
	
						@Override
						public void onTaskCompleteEnvioDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
	
						@Override
						public void onTaskCompleteDescargarDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
	
						@Override
						public void onTaskCompleteSecciones(List<Seccion> secciones) {
							// TODO Auto-generated method stub
							
						}
					}).execute();
				
				}

				
				else if(checkedId==R.id.rbSecciones)
				{
					
					spLocalidades.setVisibility(View.VISIBLE);
					globales.padres = PADRES.SECCIONES;
					new AsyncTask_CargarSecciones(Listado_Activity.this, new AsyncTaskCompleteListener<List<Seccion>>() {

						@Override
						public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteManzanas(List<Manzana> manzanas) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteSecciones(List<Seccion> secciones) {
							// TODO Auto-generated method stub
							CargarSecciones(secciones);
							CargarLocalidades();
						}

						@Override
						public void onTaskCompleteEnvioDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteDescargarDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
					}).execute();
				}
				else
				{
					globales.padres = PADRES.UPM;
				}
				
			}
		});
		
		rbgArea.check(R.id.rbSecciones);

    spAreasTrabajo.setOnItemSelectedListener(new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Area_Trabajo at = (Area_Trabajo) parent.getItemAtPosition(position);
            globales.AT_ID= at.getAt_id();

            new AsyncTask_CargarViviendas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Vivienda>>() {

                @Override
                public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
                    // TODO Auto-generated method stub
                    CargarViviendas(viviendas);
                    Toast.makeText(Listado_Activity.this, "Viviendas desde button refresh", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onTaskCompleteManzanas(List<Manzana> manzanas) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTaskCompleteEnvioDatos(Integer resultado) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTaskCompleteDescargarDatos(Integer resultado) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTaskCompleteSecciones(List<Seccion> secciones) {
                    // TODO Auto-generated method stub

                }
            }).execute();
        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    });

		btnLV_PadresRefresh = (Button)findViewById(R.id.btnLV_PadresRefresh);
		
		btnLV_PadresRefresh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(rbgArea.getCheckedRadioButtonId()==R.id.rbManzanas)
				{
					new AsyncTask_CargarManzanas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Manzana>>() {
						
						@Override
						public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteManzanas(List<Manzana> manzanas) {
							// TODO Auto-generated method stub
							CargarManzanas(manzanas);
	
						}
	
						@Override
						public void onTaskCompleteEnvioDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
	
						@Override
						public void onTaskCompleteDescargarDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
	
						@Override
						public void onTaskCompleteSecciones(List<Seccion> secciones) {
							// TODO Auto-generated method stub
							
						}
					}).execute();
				}
				
				
				if(rbgArea.getCheckedRadioButtonId()==R.id.rbSecciones)
				{

					new AsyncTask_CargarSecciones(Listado_Activity.this, new AsyncTaskCompleteListener<List<Seccion>>() {
						
						@Override
						public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteSecciones(List<Seccion> secciones) {
							// TODO Auto-generated method stub
							CargarSecciones(secciones);
							CargarLocalidades();
							CargarAreasTrabajo();
						}
						
						@Override
						public void onTaskCompleteManzanas(List<Manzana> manzanas) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteEnvioDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onTaskCompleteDescargarDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}
					}).execute();
				}

				
			}
		});
		
		btnRefrescarViviendasListado = (Button)findViewById(R.id.btnRefrescarViviendasListado);
    
		btnRefrescarViviendasListado.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new AsyncTask_CargarViviendas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Vivienda>>() {
					
					@Override
					public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
						// TODO Auto-generated method stub
						CargarViviendas(viviendas);
						Toast.makeText(Listado_Activity.this, "Viviendas desde button refresh", Toast.LENGTH_LONG).show();
					}
					
					@Override
					public void onTaskCompleteManzanas(List<Manzana> manzanas) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteEnvioDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteDescargarDatos(Integer resultado) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onTaskCompleteSecciones(List<Seccion> secciones) {
						// TODO Auto-generated method stub
						
					}
				}).execute();
			}
		});
		
	}


	public void ObtenerConteos()
	{
		int confirmadas = 0;
		int porconfirmar = 0;
		int totalreg = 0;
		DataBaseUtil db = new DataBaseUtil(Listado_Activity.this);
		db.open();
		confirmadas = db.ObtenerTotalOKPadre(globales.IDPADRE, true);
		porconfirmar= db.ObtenerTotalOKPadre(globales.IDPADRE, false);
		totalreg = db.ObtenerTotalRegistrosPadre(globales.IDPADRE);
		
		db.close();
		
		tvTotalRegistros.setText(String.valueOf(totalreg));
		tvVivConfirmadas.setText(String.valueOf(confirmadas));
		tvVivPorConfirmar.setText(String.valueOf(porconfirmar));

	}
	
	@Override
	public void onBackPressed()
	{
		// code here to show dialog
		
		Toast.makeText(Listado_Activity.this, "Recuerde enviar sus datos antes de salir de la aplicaci�n presionando el bot�n Enviar en el men� superior.\n\nSi desea salir, presione Cerrar Sesi�n en el men� superior", Toast.LENGTH_SHORT).show();
		
		//super.onBackPressed();  // optional depending on your needs
	}
	
	
	
	
	@Override
	public void onUserInteraction() {
	     super.onUserInteraction();
	     userIsInteracting = true;
	}
	  
	@Override
	public void onPause() {
	    super.onPause();

	    if(pd != null)
	    	pd.dismiss();
	    pd = null;
	}
	
	
	@Override
	public void onResume() {
	    super.onResume();
	    
	    //if(((Spin_Manzanas_Adapter)spPadres.getAdapter()).getCount()>0)
	    if(spPadres.getAdapter()!=null)
	    {
	    	
			new AsyncTask_CargarViviendas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Vivienda>>() {
				
				@Override
				public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
					// TODO Auto-generated method stub
					
					CargarViviendas(viviendas);
					
					//Toast.makeText(Listado_Activity.this, "Viviendas", Toast.LENGTH_LONG).show();
				}
				
				@Override
				public void onTaskCompleteManzanas(List<Manzana> manzanas) {
					// TODO Auto-generated method stub
					
				}
	
				@Override
				public void onTaskCompleteEnvioDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}
	
				@Override
				public void onTaskCompleteDescargarDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onTaskCompleteSecciones(List<Seccion> secciones) {
					// TODO Auto-generated method stub
					
				}
			}).execute();
	    }
	    
	    ConfigurarActivity();
	}
	
	
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
        
    	super.onCreateContextMenu(menu, v, menuInfo);
        
    	AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
    	int position = info.position;
    	
    	
      if (v.getId()==R.id.lstDragSortViviendas) {

    	  Vivienda viv= ((Vivienda)((ListView)v).getAdapter().getItem(position));
    	  
    	  
            menu.setHeaderTitle("�Qu� desea hacer con la vivienda?");
            menu.add(Menu.NONE, 1,  1, "Modificar Vivienda");
            if(viv.isNULO())
            {
            	menu.add(Menu.NONE ,2 ,2 , "Restablecer Vivienda");	
            }
            else
            {
            	menu.add(Menu.NONE ,2 ,2 , "Anular Vivienda");
            }
            
            menu.add(Menu.NONE, 3 ,3 , "Ver en Mapa");
      }
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) 
    {
    	super.onContextItemSelected(item);
    	
    	  AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    	  
    	  final Vivienda viv = (Vivienda) lstViviendas.getAdapter().getItem(info.position);
    	  
      int menuItemIndex = item.getItemId();
      //classAmigo amigo =classAmigosAdapter.MisId.get(info.position);
      if(menuItemIndex==1)
      {

      }
      else if(menuItemIndex==2)
      {
    	  DialogInterface.OnClickListener listener = new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
					//AnularVivienda(viv.getID_VIVIENDA(), !viv.isNULO());
			}
		}; 

		if(viv.isNULO())
		{
			dialog_message("�Deseas restablecer la vivienda anulada?","Restablecer Vivienda", "Si", "No",listener);
		}
		else
		{
			dialog_message("�Deseas anular la vivienda?","Anular Vivienda", "Si", "No",listener);
		}
               
      }
      
     return true;
    }
    

    
    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	CrearMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_listado, menu);
        return true;
    }    
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Sync Usuarios");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.download);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

	}
	*/
	
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	ListView lstViviendas = (ListView) findViewById( R.id.lstDragSortViviendas);
    	lstViviendas.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    	
        switch (item.getItemId()) {

        case R.id.MnuConfigGPS:
        	funciones.MostrarConfigGPS(Listado_Activity.this);
            return true;
        
        case R.id.MnuActualizar:
        	// TODO Auto-generated method stub
			new AsyncTask_CargarManzanas(Listado_Activity.this,  new AsyncTaskCompleteListener<List<Manzana>>() {
				
				@Override
				public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
					// TODO Auto-generated method stub
					
					//Toast.makeText(Listado_Activity.this, "Viviendas desde button refresh", Toast.LENGTH_LONG).show();
				}
				
				@Override
				public void onTaskCompleteManzanas(List<Manzana> manzanas) {
					
					CargarManzanas(manzanas);
					// TODO Auto-generated method stub
					//Toast.makeText(Listado_Activity.this, "Manzanas desde button refresh", Toast.LENGTH_LONG).show();
					
					
				}

				@Override
				public void onTaskCompleteEnvioDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onTaskCompleteDescargarDatos(Integer resultado) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onTaskCompleteSecciones(List<Seccion> secciones) {
					// TODO Auto-generated method stub
					
				}
			}).execute();
			
        return true;
        
        case R.id.MnuCerrarSesion:
        return true;

        
        default:
            return super.onOptionsItemSelected(item);

                    
        }
    }
    

    public void dialog_message(String mensaje,String titulo, String positivo, String negativo, 
    		DialogInterface.OnClickListener listenerPositivo) 
    {
          final Builder alertDialog = new AlertDialog.Builder(Listado_Activity.this);

              alertDialog.setTitle(titulo);
              alertDialog.setIcon(R.drawable.mensajes32);
              alertDialog.setMessage(mensaje);

              alertDialog.setPositiveButton(positivo, listenerPositivo);

              alertDialog.setNegativeButton(negativo, new DialogInterface.OnClickListener() 
              {

               public void onClick(DialogInterface dialog, int which) 
               {       
                   

               }
              });

              alertDialog.show();

    }
	private void CargarViviendas(List<Vivienda> viviendas)
	  {
		
		
		  gva = new GV_Listado_Adapter(Listado_Activity.this, viviendas);	  
		  //lstViviendas = (DragSortListView) findViewById(R.id.lstViviendas);
		  lstViviendas.setAdapter(gva);
		  gva.notifyDataSetChanged();
		  //lstViviendas.invalidateViews();
		  //registerForContextMenu(lstViviendas);

		 
		 if(globales.IDPADRE!=null)
		 {
			 ObtenerConteos();	 
		 }
		 
		
			lstViviendas.setSelection(globales.intPosicionListado);
	  }
	
	
	/*
	public class AsyncTask_CargarViviendas extends AsyncTask<Void, Integer, List<Vivienda>>
	{
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			if (pd == null) {
				pd = new ProgressDialog(Listado_Activity.this);
			}
			
			pd.setTitle("Cargando Viviendas");
			pd.setMessage("Por favor espere...");       
			pd.setIndeterminate(true);
			pd.setMax(100);
			pd.show();
		}

		@Override
		protected List<Vivienda> doInBackground(Void... params) {
			return ObtenerViviendas();
		}
		

		@Override 
		protected void onPostExecute(List<Vivienda> viviendas) 
		   {
				CargarViviendas(viviendas);
				
				
				if (pd != null) { 
			        pd.dismiss();
			   } 
				if(viviendas.size()>0)
				{
					Toast.makeText(Listado_Activity.this, "Viviendas Cargadas", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(Listado_Activity.this, "No se encontraron viviendas", Toast.LENGTH_SHORT).show();
				}
				//new TaskCargarViviendas().execute();
				
				
		     }

		private List<Vivienda> ObtenerViviendas()
		{
			if(globales.IDPADRE!=null)
			{
				DataBaseUtil dbUtil = new DataBaseUtil(Listado_Activity.this);
				dbUtil.open();
				List<Vivienda> viviendas = dbUtil.ObtenerViviendas(globales.IDPADRE);
				dbUtil.close();
				return viviendas;
			}
			else
			{
				return new ArrayList<Vivienda>();	
			}
			
		}

		
		}
*/	
	private void CargarManzanas(List<Manzana> padres)
	{
		
		Spin_Manzanas_Adapter adapter = new Spin_Manzanas_Adapter(Listado_Activity.this, padres);
		spPadres.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		
		if(padres.size()>0 && globales.IDPADRE.equals(""))
		{
			globales.IDPADRE = String.valueOf(padres.get(0).getIdmanzana());
			globales.RPC = padres.get(0).getComuna_id();
		}
	}
	
	private void CargarSecciones(List<Seccion> padres)
	{
		
		Spin_Secciones_Adapter adapter = new Spin_Secciones_Adapter(Listado_Activity.this, padres);
		spPadres.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		
		if(padres.size()>0 && globales.IDPADRE.equals(""))
		{
			globales.IDPADRE = String.valueOf(padres.get(0).getCu_seccion());
			globales.RPC = padres.get(0).getCut();
		}
	}
	
	private void CargarLocalidades()
	{
		spLocalidades.setVisibility(View.VISIBLE);
		
		DataBaseUtil db = new DataBaseUtil(Listado_Activity.this);
		db.open();
		List<String> localidades = db.ObtenerLocalidadesPadre(globales.IDPADRE);
		db.close();
		
		Spin_SoloString_Adapter adapter = new Spin_SoloString_Adapter(Listado_Activity.this, localidades);
		spLocalidades.setAdapter(adapter);
		adapter.notifyDataSetChanged();


        if(globales.LOCALIDAD  != "")
        {
            spLocalidades.setSelection(((Spin_SoloString_Adapter)spLocalidades.getAdapter()).getPosition(globales.LOCALIDAD));
        }
		else if(localidades.size()>0)
		{
			globales.LOCALIDAD = localidades.get(0);
		}
		else
		{
			globales.LOCALIDAD  = "";
		}
	}

	private void CargarAreasTrabajo()
	{
		spLocalidades.setVisibility(View.VISIBLE);

		DataBaseUtil db = new DataBaseUtil(Listado_Activity.this);
		db.open();
		List<Area_Trabajo> ats = db.ObtenerAreasTrabajo(true);
		db.close();

		Spin_AreaTrabajo_Adapter adapter = new Spin_AreaTrabajo_Adapter(Listado_Activity.this, ats);
		spAreasTrabajo.setAdapter(adapter);
		adapter.notifyDataSetChanged();


		if(globales.AT_ID  != 0)
		{
            spAreasTrabajo.setSelection(((Spin_AreaTrabajo_Adapter)spAreasTrabajo.getAdapter()).getPositionById(globales.AT_ID));
		}
		else if(ats.size()>0)
		{
			globales.AT_ID  = ats.get(0).getAt_id();
		}
		else
		{
			globales.AT_ID  = 0;
		}
	}

	/*
	private void CargarLocalidades(List<Localidad> padres)
	{
		
		Spin_Manzanas_Adapter adapter = new Spin_Manzanas_Adapter(Listado_Activity.this, padres);
		spPadres.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	*/
	
	
/*
	private class TaskReordenarViviendas extends AsyncTask<List<Vivienda>, Void, Integer>
	{
		
		private int max;
		  public TaskReordenarViviendas(int max){
		        this.max= max;

		    }
		  
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(Listado_Activity.this);
			pd.setTitle("Reordenando Viviendas");
			pd.setMessage("Por favor espere...");       
			pd.setIndeterminate(false);
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pd.setMax(max);
			pd.show();
		}
		
		@Override
		protected Integer doInBackground(List<Vivienda>... listas) 
		{
			
			return OrdenarViviendas(listas[0]);

		}
		
		private int OrdenarViviendas(List<Vivienda> viviendas)
		{
			DataBaseUtil dbUtil = new DataBaseUtil(Listado_Activity.this);
			dbUtil.open();
			int i = 1;
			int ordenviv = 1;
			for(Vivienda viv : viviendas)
			{
				
				
				if(viv.getID_USODESTINO() == 5 || viv.getID_USODESTINO() == 6 || viv.isNULO())
				{
					viv.setORDEN_VIV(0);
				}
				else
				{
					viv.setORDEN_VIV(ordenviv);
					ordenviv = ordenviv + 1;
				}
				viv.setORDEN(i);
				
				dbUtil.ReordenarVivienda(viv);
				
				pd.setProgress(i);
				i++;
				
				
			}
			
			dbUtil.close();
			
			return 1;
		
		}
		
		@Override 
		protected void onPostExecute(Integer result) 
		{
			
			if (pd != null) {
				pd.dismiss();
				gva.notifyDataSetChanged();
			}
			//new TaskCargarViviendas().execute();
		}
	}
	*/
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (requestCode == ASYNC_CARGA_VIVIENDAS && resultCode == RESULT_OK) {

    		if(data.getExtras().getBoolean("CAMBIO_LOC"))
			{
				CargarLocalidades();
                new AsyncTask_CargarViviendas(Listado_Activity.this, new AsyncTaskCompleteListener<List<Vivienda>>() {

                    @Override
                    public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
                        // TODO Auto-generated method stub
                        CargarViviendas(viviendas);
                        btnGrabarListado.performClick();
                        Toast.makeText(Listado_Activity.this, "Viviendas desde men� modificar ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onTaskCompleteManzanas(List<Manzana> manzanas) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onTaskCompleteEnvioDatos(Integer resultado) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onTaskCompleteDescargarDatos(Integer resultado) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onTaskCompleteSecciones(List<Seccion> secciones) {
                        // TODO Auto-generated method stub

                    }
                }).execute();



			}
			else
            {
				new AsyncTask_CargarViviendas(Listado_Activity.this, new AsyncTaskCompleteListener<List<Vivienda>>() {

					@Override
					public void onTaskCompleteViviendas(List<Vivienda> viviendas)
                    {
						// TODO Auto-generated method stub
						CargarViviendas(viviendas);

						Toast.makeText(Listado_Activity.this, "Viviendas desde menu modificar ", Toast.LENGTH_LONG).show();
					}

					@Override
					public void onTaskCompleteManzanas(List<Manzana> manzanas) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onTaskCompleteEnvioDatos(Integer resultado) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onTaskCompleteDescargarDatos(Integer resultado) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onTaskCompleteSecciones(List<Seccion> secciones) {
						// TODO Auto-generated method stub

					}
				}).execute();
			}
		}
    	else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK ) 
    	{
    		//String PhotoPath = data.getExtras().getString("PATH_PHOTO");
    		//UUID ID_VIVIENDA = (UUID)data.getExtras().get("ID_VIVIENDA");
    		
    		
    		BitmapFactory.Options options = new BitmapFactory.Options();
        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        	Bitmap bm;// = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

        	try {
        		
        		bm = BitmapFactory.decodeFile(globales.RutaPicsSinImagen + globales.NombreFotoTemp);
        		
        		bm = funciones.RotateBitmap(bm, 90);
        		if(bm != null)
        		{
    				funciones.ResizeAndSaveImage(Listado_Activity.this, bm,320, 240, 100, globales.ID_VIVIENDA_LISTADO);

    				DataBaseUtil db = new DataBaseUtil(Listado_Activity.this);
    				db.open();
    				db.MarcarViviendaComoActualizada(globales.ID_VIVIENDA_LISTADO);
    				db.close();
    				
   					new AsyncTask_CargarViviendas(Listado_Activity.this, new AsyncTaskCompleteListener<List<Vivienda>>() {
						@Override
						public void onTaskCompleteViviendas(List<Vivienda> viviendas) {
							// TODO Auto-generated method stub
							
							CargarViviendas(viviendas);
						}
						
						@Override
						public void onTaskCompleteManzanas(List<Manzana> manzanas) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteEnvioDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteDescargarDatos(Integer resultado) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTaskCompleteSecciones(
								List<Seccion> secciones) {
							// TODO Auto-generated method stub
							
						}
					}).execute();			
        		}
        		else
        		{
        			Toast.makeText(Listado_Activity.this, "Error al asociar foto", Toast.LENGTH_SHORT).show();
        		}
        		

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
                    
       	
        	Toast.makeText(Listado_Activity.this, "Foto creada", Toast.LENGTH_SHORT).show();
            //Bundle extras = data.getExtras();
    	}
    }
    
    
    private void ConfigurarActivity()
    {

	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Listado_Activity.this);

	    //Toast.makeText(Listado_Activity.this, String.valueOf(preferences.getBoolean("prefOcultarNulos", false)) , Toast.LENGTH_SHORT).show() ;


		globales.boolOcultarNulos = preferences.getBoolean("prefOcultarNulos", false);
		globales.boolMostrarDescripciones = preferences.getBoolean("prefMostrarDescripciones", false);
		globales.boolMostrarNombreEdificio= preferences.getBoolean("prefMostrarNombreEdificio", true);
		globales.strMetrosBusquedaCalles = preferences.getString("prefMetrosBusquedaCalles", "0.005");
		globales.strMostrarOrdenes = preferences.getString("prefMostrarOrdenes", "1");
		globales.strDistanciaRegistroPosicion = preferences.getString("prefDistanciaRegistroPosicion", "10");
		globales.boolCentrarPosicion= preferences.getBoolean("prefCentrarPosicion", true);
		globales.boolHabilitarRegistroPosicion= preferences.getBoolean("prefHabilitarRegistroPosicion", true);

    }
    
}
	


