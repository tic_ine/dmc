package com.INE.ImasD.EnumeracionMobile.Adaptadores;

/**
 * Created by Mbecerra on 18-10-2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.CategoriaPOI;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class ImageAdapter extends BaseAdapter {
    private Context Context;
    private List<CategoriaPOI> values;
    private boolean IsSelected = false;
    public CategoriaPOI selectedCat;
    // Constructor
    public ImageAdapter(Context c, List<CategoriaPOI> values) {
        this.Context = c;
        this.values = values;
    }

    public int getCount() {
        return values.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public  void DesSeleccionarTodos()
    {
        for(CategoriaPOI cat : values)
        {
            cat.setSelected(false);
        }

        notifyDataSetChanged();
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        final CategoriaPOI cat = values.get(position);

        if (convertView == null) { // if it's not recycled, initialize some attributes
            LayoutInflater vi =
                    (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_categoriapoi , null);
        }

        final ImageView imgbtnIconCategoriaPOI = (ImageView)v.findViewById(R.id.imgbtnIconCategoriaPOI);

        final TextView tvGlosaCategoriaPOI = (TextView) v.findViewById(R.id.tvGlosaCategoriaPOI);
        imgbtnIconCategoriaPOI.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imgbtnIconCategoriaPOI.setPadding(8, 8, 8, 8);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        final Bitmap bitmap = BitmapFactory.decodeByteArray(cat.getIcon(), 0, cat.getIcon().length);
        imgbtnIconCategoriaPOI.setImageBitmap(bitmap);

        tvGlosaCategoriaPOI.setText(cat.getGlosa());
        tvGlosaCategoriaPOI.setTextColor(Color.BLACK);

        if (cat.isSelected()) {
            imgbtnIconCategoriaPOI.setBackgroundResource(R.drawable.gradienteazul);
        } else {

            imgbtnIconCategoriaPOI.setBackgroundResource(R.drawable.gradienteamarillo);
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DesSeleccionarTodos();
                boolean isSelected=cat.isSelected();
                cat.setSelected(!isSelected);

                if (cat.isSelected()) {
                    selectedCat = cat;
                } else {

                    selectedCat = null;
                }

                notifyDataSetChanged();

            }
        });

        imgbtnIconCategoriaPOI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DesSeleccionarTodos();
                boolean isSelected=cat.isSelected();
                cat.setSelected(!isSelected);

                if (cat.isSelected()) {
                    selectedCat = cat;
                } else {

                    selectedCat = null;
                }

                notifyDataSetChanged();
            }
        });

        return v;
    }

}