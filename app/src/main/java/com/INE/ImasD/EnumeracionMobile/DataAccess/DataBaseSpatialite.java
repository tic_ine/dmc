package com.INE.ImasD.EnumeracionMobile.DataAccess;

import android.util.Log;

import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.WKT_Utilities;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jsqlite.Constants;
import jsqlite.Exception;
import jsqlite.Stmt;

public class DataBaseSpatialite 
{
	jsqlite.Database db; 
	File spatialDbFile;
	public DataBaseSpatialite() throws Exception
	{
		spatialDbFile  = new File(globales.Ruta_SPATIALITE);
		db = new jsqlite.Database();
		db.open(spatialDbFile.getAbsolutePath(),
                Constants.SQLITE_OPEN_READWRITE | Constants.SQLITE_OPEN_CREATE );
		db.spatialite_create();
	}

	public void CerrarBD() throws Exception
	{
		db.close();
	}

	public List<String> AgregarSugerenciasCalles(List<String> calles , double lat, double lng)
	{

		Stmt stmt=null;
		try 
		{
			/*
			stmt = db.prepare("SELECT ASTEXT(BuildCircleMbr(" + lng + "," + lat + ",0.00053))");

			if(stmt.step())
			{
				stmt.column_string(0).trim();
			}
*/
			//StringBuilder sb = new StringBuilder();

			
			String query = "SELECT DISTINCT NOMBRE FROM Eje_Vial " + 
					"WHERE ST_Intersects(Geometry,BuildCircleMbr(" + lng + "," + lat + " , " + globales.strMetrosBusquedaCalles + ")) = 1 "+
					"AND ROWID IN ( " +
					"SELECT ROWID " + 
					"FROM SpatialIndex " + 
					" WHERE f_table_name = 'Eje_Vial' " +
					"AND search_frame = BuildCircleMbr(" + lng + "," + lat + " ," + globales.strMetrosBusquedaCalles + "))" ;

			
			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", query);

			String calle = "";
			while( stmt.step() ) {

				calle = stmt.column_string(0).trim().toUpperCase();
				//Log.d("enumeracionviviendas", "stmt");

				//String geomStr = stmt.column_string(0);
				//String substring = geomStr;
				if (calle.length() > 1){
					//  substring = geomStr.substring(0, 40);
					// sb.append("\t").append(stmt.column_string(0)).append(" - \n");
					Log.d("EnumeracionViviendas"," salida sql " + " "+ calle + "...\n");
					if(!calles.contains(calle))
					{
						calles.add(calle);	
					}

				}

			}		        


		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return calles;

	}

	public Polygon ObtenerGeometriaManzana(double lat, double lng)
	{

		String WKT="";
		Stmt stmt=null;
		String circulo = "";
		
		Polygon manzanapolygono = null;
		try 
		{
			stmt = db.prepare("SELECT ASTEXT(BuildCircleMbr(" + lng + "," + lat + ",0.00053))");

			if(stmt.step())
			{
				circulo = stmt.column_string(0).trim();
			}

			StringBuilder sb = new StringBuilder();

			String query = "SELECT ASTEXT(GEOMETRY) FROM MARCO_MANZANAS " + 
					"WHERE ST_Intersects(Geometry,GeomFromText('"+ circulo + "')) = 1 "+
					"AND ROWID IN ( " +
					"SELECT ROWID " + 	
					"FROM SpatialIndex " + 
					" WHERE f_table_name = 'MARCO_MANZANAS' " +
					"AND search_frame = MAKEPOINT(" + lng + ", " + lat + "))" ;

			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			if( stmt.step() ) 
			{

				if (stmt.column_string(0).trim().length() > 1){
					WKT = stmt.column_string(0).trim();
				
					manzanapolygono = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
					
					Log.d("EnumeracionViviendas"," salida wkt "+ WKT + "...\n");
				}
			}

			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return manzanapolygono;


	}

	
	public Polygon ObtenerGeometriaManzana(String IDPADRE)
	{
	
		String WKT="";
		Stmt stmt=null;
		Polygon manzanapolygono = null;
		try 
		{
	
			StringBuilder sb = new StringBuilder();
	
			String query = "SELECT ASTEXT(GEOMETRY) FROM MARCO_MANZANAS WHERE MANZENT = '" + IDPADRE + "'";
	
			stmt = db.prepare(query);
	
	
			Log.d("enumeracionviviendas", sb.toString());
	
			if( stmt.step() ) 
			{
	
				if (stmt.column_string(0).trim().length() > 1){
					WKT = stmt.column_string(0).trim();
					
					manzanapolygono = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
					
					Log.d("EnumeracionViviendas"," salida wkt "+ WKT + "...\n");
				}
			}
	
			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();
	
			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{
	
			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return manzanapolygono;
	
	
	}

	public Polygon ObtenerGeometriaSeccion(double lat, double lng)
	{

		String WKT="";
		Stmt stmt=null;
		Polygon seccionpolygono = null;
		String circulo = "";
		
		try 
		{
			stmt = db.prepare("SELECT ASTEXT(BuildCircleMbr(" + lng + "," + lat + ",0.00053))");

			if(stmt.step())
			{
				circulo = stmt.column_string(0).trim();
			}

			StringBuilder sb = new StringBuilder();

			String query = "SELECT ASTEXT(Transform(GEOMETRY, " + globales.SRID_WGS84 +  ")) FROM MARCO_SECCIONES " +
					"WHERE ST_Intersects(Geometry,GeomFromText('"+ circulo + "')) = 1 "+
					"AND ROWID IN ( " +
					"SELECT ROWID " + 	
					"FROM SpatialIndex " + 
					" WHERE f_table_name = 'MARCO_SECCIONES' " +
					"AND search_frame = MAKEPOINT(" + lng + ", " + lat + "))" ;

			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			if( stmt.step() ) 
			{

				if (stmt.column_string(0).trim().length() > 1){
					WKT = stmt.column_string(0).trim();
				
					seccionpolygono = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
					
					Log.d("EnumeracionViviendas"," salida wkt "+ WKT + "...\n");
				}
			}
		}
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return seccionpolygono;


	}	
	
	public Polygon ObtenerGeometriaSeccion(String IDPADRE)
	{

		String WKT="";
		Stmt stmt=null;
		Polygon seccionpolygono = null;
		try 
		{

			StringBuilder sb = new StringBuilder();

			String query = "SELECT ASTEXT(Transform(GEOMETRY, " + globales.SRID_WGS84 +  ")) FROM MARCO_SECCIONES WHERE CU_SECCION= " + IDPADRE + "";

			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			if( stmt.step() ) 
			{

				if (stmt.column_string(0).trim().length() > 1){
					WKT = stmt.column_string(0).trim();
					
					seccionpolygono = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
					
					//Log.d("EnumeracionViviendas"," salida wkt "+ WKT + "...\n");
				}
			}

			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return seccionpolygono;


	}

	public List<Polygon> ObtenerGeometriasPseudoManzana()
	{

		String WKT="";
		Stmt stmt=null;
		Polygon polygono_pseudo = null;
		List<Polygon> pseudomanzanas = new ArrayList<Polygon>();
		try
		{

			StringBuilder sb = new StringBuilder();

			String query = "SELECT ASTEXT(Transform(GEOMETRY, " + globales.SRID_WGS84 + ")) FROM Pseudo_Manzanas WHERE CU_SECCION = " +  globales.IDPADRE + "";
			stmt = db.prepare(query);
			if(stmt.step())
			{
				do
				{
					if (stmt.column_string(0).trim().length() > 1){
						WKT = stmt.column_string(0).trim();
						polygono_pseudo = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
						pseudomanzanas.add(polygono_pseudo);
						Log.d("EnumeracionViviendas"," PSEUDO MZ: "+ WKT + "...\n");
					}
				}
				while( stmt.step());
			}

		}
		catch ( jsqlite.Exception  e)
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return pseudomanzanas;


	}

	public List<Polygon> ObtenerGeometriaAreasLevantamiento(Seccion sec)
	{

		String WKT="";
		Stmt stmt=null;
		Polygon seccionpolygono = null;
		List<Polygon> seccionespoly = new ArrayList<Polygon>();
		try 
		{

			StringBuilder sb = new StringBuilder();

			//String query = "SELECT ASTEXT(Transform(GEOMETRY, " + globales.SRID_WGS84 + ")) FROM AREA_LEV WHERE CU_SECCION= " + sec.getCu_seccion() + "";
			 String query = "SELECT ASTEXT(Transform(GEOMETRY, " + globales.SRID_WGS84 + ")) FROM Area_Levantamiento WHERE CU_SECCION= " + sec.getCu_seccion() + "";


			stmt = db.prepare(query);


			//Log.d("enumeracionviviendas", sb.toString());

			if(stmt.step())
			{
			 do
			{

				if (stmt.column_string(0).trim().length() > 1){
					WKT = stmt.column_string(0).trim();
					
					seccionpolygono = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
					
					seccionespoly.add(seccionpolygono);
					Log.d("EnumeracionViviendas"," AREA LEV: "+ WKT + "...\n");
				}
			}
			while( stmt.step());
			}
			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return seccionespoly;


	}		
	
	public List<Polygon> ObtenerLocalidadesActual()
	{

		String WKT="";
		Stmt stmt=null;
		Polygon localidadpolygono = null;
		List<Polygon>  localidades =  new ArrayList<Polygon>();
		
		try 
		{

			StringBuilder sb = new StringBuilder();

			String query = "SELECT ASTEXT(Transform(GEOMETRY, " + globales.SRID_WGS84 + ")) FROM Localidades WHERE NOMBRE_LOC = '" + globales.LOCALIDAD + "'";

			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			while( stmt.step() ) 
			{

				if (stmt.column_string(0).trim().length() > 1){
					//globales.intCUT =stmt.column_int(0);
					
					WKT = stmt.column_string(0).trim();
					
					localidadpolygono  = (Polygon)WKT_Utilities.wkt2Geometry(WKT);
					
					localidades.add(localidadpolygono);
					Log.d("EnumeracionViviendas"," salida wkt "+ WKT + "...\n");
				}
			}

			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return localidades;


	}	
	public List<String> ObtenerCallesPadre(String IDPADRE, List<String> calles )
	{

		Stmt stmt=null;
		String query;
		
		try 
		{
		
			query =  "SELECT DISTINCT NOMBRE " +
							"FROM Eje_Vial where ROWID IN ( " + 
							"SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'Eje_Vial' " + 
							"AND search_frame = " + 
							" (SELECT geometry FROM MARCO_MANZANAS m WHERE m.MANZENT='" + IDPADRE + "'" +
							" UNION ALL" +
							" SELECT geometry FROM MARCO_SECCIONES s WHERE s.CU_SECCION=" + IDPADRE + ")" +
							") ";
			
			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", query);
			String calle = "";
			while( stmt.step() ) {

				calle = stmt.column_string(0).trim().toUpperCase();
				if (calle.length() > 1){
					Log.d("EnumeracionViviendas"," salida sql " + " "+ calle + "...\n");
					if(!calles.contains(calle))
					{
						calles.add(calle);	
					}
				}
			}
		}
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{
			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return calles;


	}	
	
	
	public long ObtenerGeocodigoManzana(double lat, double lng)
	{

		long geocodigo= 0;
		Stmt stmt=null;
		String circulo = "";

		try 
		{
			stmt = db.prepare("SELECT ASTEXT(BuildCircleMbr(" + lng + "," + lat + "," + globales.strMetrosBusquedaCalles + "))");

			if(stmt.step() && stmt.column_string(0) != null)
			{
				circulo = stmt.column_string(0).trim();
			}
			else
			{
				return 0;
			}

			StringBuilder sb = new StringBuilder();

			String query = "SELECT MANZENT FROM MARCO_MANZANAS " + 
					"WHERE ST_Intersects(Geometry,GeomFromText('"+ circulo + "')) = 1 "+
					"AND ROWID IN ( " +
					"SELECT ROWID " + 
					"FROM SpatialIndex " + 
					" WHERE f_table_name = 'MARCO_MANZANAS' " +
					"AND search_frame = MAKEPOINT(" + lng + ", " + lat + "))" ;

			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			if( stmt.step() ) 
			{

				if (stmt.column_string(0).trim().length() > 1){
					geocodigo = stmt.column_long(0);
					
					
					Log.d("EnumeracionViviendas"," salida geocodigo "+ geocodigo + "...\n");
				}
			}

			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return geocodigo;


	}
	
	public long ObtenerCU_Seccion(double lat, double lng)
	{

		long cu_seccion= 0;
		Stmt stmt=null;
		String circulo = "";

		//Point pt = (Point) GeometryEngine.project(new Point(lng, lat), SpatialReference.create(globales.SRID_WebMercator_Spherical), SpatialReference.create(globales.SRID_WGS84));


		try 
		{
			stmt = db.prepare("SELECT ASTEXT(BuildCircleMbr(" + lng + "," + lat + ",0.005))");

			if(stmt.step())
			{
				circulo = stmt.column_string(0).trim();
			}

			StringBuilder sb = new StringBuilder();

			String query = "SELECT CU_SECCION FROM MARCO_SECCIONES " + 
					"WHERE ST_Intersects(Geometry,GeomFromText('"+ circulo + "')) = 1 "+
					"AND ROWID IN ( " +
					"SELECT ROWID " + 
					"FROM SpatialIndex " + 
					" WHERE f_table_name = 'MARCO_SECCIONES' " +
					"AND search_frame = MAKEPOINT(" + lng + ", " + lat + "))" ;

			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			if( stmt.step() ) 
			{

				if (stmt.column_string(0).trim().length() > 1){
					cu_seccion = stmt.column_long(0);
					
					
					Log.d("EnumeracionViviendas"," salida geocodigo "+ cu_seccion + "...\n");
				}
			}

			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return cu_seccion;


	}
	public Manzana ObtenerManzana(long Geocodigo)
	{

		long geocodigo= 0;
		Stmt stmt=null;
		Manzana mz = null;

		try 
		{

			StringBuilder sb = new StringBuilder();

			String query = "SELECT COMUNA, COD_DISTRI, COD_ZONA, COD_MANZAN, MANZENT FROM MARCO_MANZANAS " + 
					"WHERE MANZENT = '" + Geocodigo + "'"; 


			stmt = db.prepare(query);


			Log.d("enumeracionviviendas", sb.toString());

			if( stmt.step() ) 
			{
				mz = new Manzana();
				
				if (stmt.column_string(0).trim().length() > 1){
					mz.setComuna_id(stmt.column_int(0));
					mz.setDistrito(stmt.column_int(1));
					mz.setZona(stmt.column_int(2));
					mz.setManzana(stmt.column_int(3));
					mz.setGeocodigo(stmt.column_long(4));
					
					
					Log.d("EnumeracionViviendas"," salida geocodigo "+ geocodigo + "...\n");
				}
			}

			
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mz;


	}
	
	/*
	 * SELECT CUT,	Geometry
FROM Marco_Manzanas
where ROWID IN (
			SELECT ROWID 
			FROM SpatialIndex 
			WHERE f_table_name = 'MARCO_MANZANAS' 
			AND search_frame = MAKEPOINT(-7865022,-3955064)
		)
	 * */
	
	public int[] ObtenerRegionCUT(double lng, double lat)
	{

		Stmt stmt=null;
		int[] DATA = new int[2];
		

		try 
		{

			new StringBuilder();

			String query =  
					" SELECT REGION, CUT,	Geometry  " + 
					" FROM Marco_Manzanas " + 
					" where ROWID IN ( " +
						" SELECT ROWID FROM SpatialIndex " +
						" WHERE f_table_name = 'MARCO_MANZANAS' " +  
						" AND search_frame = MAKEPOINT(" + lng + ", " + lat + ") " +
						")";

			stmt = db.prepare(query);


			while( stmt.step() ) 
			{
				if (stmt.column_string(0).trim().length() > 0){
					DATA[0] = stmt.column_int(0);
					DATA[1] = stmt.column_int(1);
				}
			}
		} 
		catch ( jsqlite.Exception  e) 
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{
			try {
				stmt.close();
				CerrarBD();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return DATA;


	}


	public long InsertarSecciones()
	{

		long cu_seccion= 0;
		Stmt stmt=null;
		String circulo = "";

		try
		{
		    String wkt = "";
            StringBuilder sb = new StringBuilder();

            Map<String, Object> attrs;


		    for (Graphic gp: globales.Graphics_SeccionesService)
            {
                wkt = WKT_Utilities.Geometry2WKT(gp.getGeometry());

                attrs =  gp.getAttributes();
                String carto = ("00000" + attrs.get("COD_CARTO").toString());

                String query = " INSERT INTO MARCO_SECCIONES(CUT, COMUNA, URBANO, COD_DISTRI, COD_CARTO, EST_MUESTR, EST_GEOGRA, EST_ENE, TIPO_EST, COD_SECCIO, CU_SECCION, GEOMETRY) " +
                        " VALUES" +
                       " (" + Integer.valueOf(attrs.get("CUT").toString()) +
                        ", '" + attrs.get("COMUNA").toString() + "'" +
                        ", '" + attrs.get("URBANO").toString()  + "'    " +
                        ", '" + attrs.get("COD_DISTRITO").toString() + "'" +
                        ", " + Integer.valueOf(attrs.get("COD_CARTO").toString()) +
                        ", " + Integer.valueOf(attrs.get("EST_MUESTRAL").toString()) +
                        ", " + Integer.valueOf(attrs.get("EST_GEOGRAFICO").toString()) +
                        ", " + Integer.valueOf(attrs.get("EST_ENE").toString()) +
                        ", " + Integer.valueOf(attrs.get("TIPO_EST").toString()) +
                        ", " + Integer.valueOf(attrs.get("COD_SECCION").toString()) +
                        //", " + Double.valueOf(attrs.get("CU_SECCION").toString()) +
						", " + attrs.get("CUT").toString() + attrs.get("TIPO_EST").toString() + carto.substring(carto.length() - 5, carto.length()) +
                        ", GeomFromText('" + wkt + "', " + globales.SRID_WGS84 + "))" ;

                stmt = db.prepare(query);


                //db.exec(query, null);

                boolean ret;

                if( stmt.step() )
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }

            }
		}
		catch ( jsqlite.Exception  e)
		{
			e.printStackTrace();

			Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
		}
		finally
		{

			try {
				stmt.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return cu_seccion;


	}

	public void BorrarSecciones()
	{
		Stmt stmt=null;
		try {


			String query = "delete from marco_secciones";

            stmt = db.prepare(query);
            stmt.step();
		}
        catch ( jsqlite.Exception  e)
        {
            e.printStackTrace();

            Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
        }
        finally
        {

            try {
                stmt.close();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
	}

	public void CrearSpatialIndex(String table, String campoGeom)
	{
		Stmt stmt=null;
		try {

            String query = "SELECT CreateSpatialIndex('" + table + "', '" + campoGeom + "');";
            stmt = db.prepare(query);
            stmt.step();


			}
        catch ( jsqlite.Exception  e)
        {
            e.printStackTrace();

            Log.d("INEAPP","open database"  +  e.getLocalizedMessage().toString());
        }
        finally
        {

            try {
                stmt.close();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
	}


}
