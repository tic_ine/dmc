package com.INE.ImasD.EnumeracionMobile.Entities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;
public class Actualizacion {
    private int act_id;
    private String act_glosa;
    private Date fechahora;
    private Boolean activa;
    private int region_id;
    
	public int getAct_id() {
		return act_id;
	}
	public void setAct_id(int act_id) {
		this.act_id = act_id;
	}
	public String getAct_glosa() {
		return act_glosa;
	}
	public void setAct_glosa(String act_glosa) {
		this.act_glosa = act_glosa;
	}
	public java.util.Date getFechahora() {
		return fechahora;
	}
	
	@SuppressLint("SimpleDateFormat")
	public Integer getFechahoraComoNumero() {

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
			return Integer.valueOf(sdf.format(fechahora));			
		
	}
	
	public void setFechahora(Date fechahora) {
		this.fechahora = fechahora;
	}
	public Boolean getActiva() {
		return activa;
	}
	public void setActiva(Boolean activa) {
		this.activa = activa;
	}
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}



}
