package com.INE.ImasD.EnumeracionMobile;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.Point;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.PictureMarkerSymbol;

import java.util.UUID;

public class Mapa_Simple_DialogFragment extends DialogFragment 
{

    public static Mapa_Simple_DialogFragment newInstance(String Titulo,AccionesMapaSimple accion, UUID ID_VIVIENDA, boolean IsNueva) {
        Mapa_Simple_DialogFragment myFragment = new Mapa_Simple_DialogFragment();

        Bundle args = new Bundle();
        args.putInt("accion", accion.value);
        args.putString("TituloDialog", Titulo);
        args.putString("ID_VIVIENDA", ID_VIVIENDA.toString());
        args.putBoolean("IsNueva", IsNueva);

        myFragment.setArguments(args);

        return myFragment;
    }
    /*
	public Mapa_Simple_DialogFragment(String Titulo,AccionesMapaSimple accion, UUID ID_VIVIENDA, boolean IsNueva)
	{
		
		this.accion = accion;
		this.TituloDialog = Titulo;
		this.ID_VIVIENDA = ID_VIVIENDA;
		this.IsNueva = IsNueva;
	}
	*/
	
	private AccionesMapaSimple accion;
	private String TituloDialog;	
	public Geometry geometria;
	private UUID ID_VIVIENDA;
	private LayoutInflater miInflater;
	boolean IsNueva;

	GraphicsLayer gl = new GraphicsLayer();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.miInflater = inflater;


        this.accion = AccionesMapaSimple.PUNTO_VIVIENDA;
        this.TituloDialog = getArguments().getString("TituloDialog");
        this.ID_VIVIENDA = UUID.fromString(getArguments().getString("ID_VIVIENDA"));
        this.IsNueva = getArguments().getBoolean("IsNueva");



		View rootView = inflater.inflate(R.layout.mapa_simple, container, false);
		getDialog().setTitle(this.TituloDialog);
		final MapView mapa_simple =  (MapView)rootView.findViewById(R.id.mapa_simple);
		
		//MBTilesLayer mbLayer = new MBTilesLayer(globales.Ruta_MBTILES);
		//mapa_simple.addLayer(mbLayer);
		ArcGISRuntime.setClientId("bGwXe4VPRYAJATlD");

		ArcGISLocalTiledLayer AGltl = new ArcGISLocalTiledLayer(globales.Ruta_TPK + globales.IDPADRE + ".tpk", true);
		mapa_simple.addLayer(AGltl);
		mapa_simple.addLayer(gl);
		

		
		Button btnCerrarMapaSimple = (Button) rootView.findViewById(R.id.btnCerrarMapaSimple);
		btnCerrarMapaSimple.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
		
		mapa_simple.setOnStatusChangedListener(new OnStatusChangedListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onStatusChanged(Object arg0, STATUS arg1) {
				// TODO Auto-generated method stub

				if(STATUS.LAYER_LOADED ==arg1)
				{
					mapa_simple.zoomToScale((Point)geometria, 6D);
					
					DataBaseUtil db = new DataBaseUtil(getActivity());
					db.open();
					Vivienda viv = db.ObtenerVivienda(ID_VIVIENDA);
					db.close();
					
					
					if(accion==AccionesMapaSimple.PUNTO_VIVIENDA)
					{
						if(IsNueva)
						{
							gl.addGraphic(new Graphic(geometria, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.star_32))));
							return;
						}
						else if (!viv.getID_EDIF().equals(globales.UUID_CERO))
						{
							gl.addGraphic(new Graphic(geometria, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.edificio))));
						}
						else
						{
							gl.addGraphic(new Graphic(geometria, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.vivienda_32))));
						}
							
					}

					
					final Callout callout;
					View calloutView;
					
 
					calloutView = miInflater.inflate(R.layout.callout_vivienda, null);
					
					callout = mapa_simple.getCallout();
					callout.setCoordinates((Point)geometria);

					TextView tvCOV_Localidad;
					TextView tvCOV_Entidad;

					TextView tvCOV_Direccion;
					TextView tvCOV_UsoDestino;
					TextView tvCOV_OrdenViv;
					TextView tvCOV_Hogares_Personas;
					TextView tvCOV_Descripcion;

					ImageButton btnCOV_ModificarReg;
					ImageButton btnCOV_AnularViv;
					ImageButton imgbtnOK;
					ImageButton btnCOV_Foto;

					tvCOV_Localidad  = (TextView) calloutView.findViewById(R.id.tvCOV_Localidad);
					tvCOV_Entidad  = (TextView) calloutView.findViewById(R.id.tvCOV_Entidad);

					tvCOV_Direccion = (TextView)calloutView.findViewById(R.id.tvCOV_Direccion);
					tvCOV_UsoDestino = (TextView)calloutView.findViewById(R.id.tvCOV_UsoDestino);
					tvCOV_OrdenViv = (TextView)calloutView.findViewById(R.id.tvCOV_OrdenViv);
					tvCOV_Hogares_Personas = (TextView)calloutView.findViewById(R.id.tvCOV_Hogar_Persona);
					tvCOV_Descripcion = (TextView)calloutView.findViewById(R.id.tvCOV_Descripcion);



					btnCOV_ModificarReg = (ImageButton) calloutView.findViewById(R.id.btnCOV_ModificarReg);
					btnCOV_AnularViv = (ImageButton) calloutView.findViewById(R.id.btnCOV_AnularViv);
					btnCOV_Foto = (ImageButton) calloutView.findViewById(R.id.btnCOV_Foto);
					imgbtnOK = (ImageButton) calloutView.findViewById(R.id.imgbtnOK);

					String direccion;
					String usodestino;

					direccion = viv.getNOMBRE_CALLE_CAMINO() + " N�" + viv.getN_DOMICILIO();
					usodestino = viv.getGLOSA_USODESTINO();

					tvCOV_Localidad.setText("Loc:" + viv.getNOMBRELOCALIDAD());
					tvCOV_Entidad.setText("Ent:" + viv.getNOMBREENTIDAD());

					tvCOV_Direccion.setText("Dir:" + direccion);
					tvCOV_UsoDestino.setText("Uso:" + usodestino);
					tvCOV_OrdenViv.setText("Ord:" + viv.getORDEN() + ".OrdViv:" + viv.getORDEN_VIV());
					tvCOV_Hogares_Personas.setText("Hog.:" + viv.getHOGARES() + ". Pers.:" + viv.getPERSONAS());
					tvCOV_Descripcion.setText("Desc:" + viv.getDESCRIPCION());;

					btnCOV_ModificarReg.setVisibility(View.INVISIBLE);
					btnCOV_AnularViv.setVisibility(View.INVISIBLE);
					btnCOV_Foto.setVisibility(View.INVISIBLE);
					imgbtnOK.setVisibility(View.INVISIBLE);

					calloutView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							callout.hide();
							gl.clearSelection();
						}
					});
	
					
					callout.setContent(calloutView);
					callout.refresh();  
					callout.show(); 
				}
			}
		});
		
		

		
		
		return rootView;	    




	}
	
	public enum AccionesMapaSimple
	{
		PUNTO_VIVIENDA(0);

		
		private int value;
		private AccionesMapaSimple(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
	
	@Override
	public void onResume() {
	    super.onResume();

	    Window window = getDialog().getWindow();
	    android.graphics.Point size = new android.graphics.Point();

	    Display display = window.getWindowManager().getDefaultDisplay();
	    display.getSize(size);

	    int width = size.x;
	    int height = size.y;

	    window.setLayout((int) (width * 0.75), ((int) (height * 0.75)));
	    window.setGravity(Gravity.CENTER);
	}
	
	
}
