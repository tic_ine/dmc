package com.INE.ImasD.EnumeracionMobile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseSpatialite;
import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales.PADRES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import jsqlite.Exception;

public class NuevoEdificio_Activity extends Activity
{
	
	UUID ID_EDIF;
	int Region = 0;
	//int CUT = 0;
	double lat = 0;
	double lng = 0;
	
	List<String> calles;
	
	ProgressDialog pd;
	
	EditText etvEdificio_NombreEdificio;
	EditText etvEdificio_NumeroEdificio;
	AutoCompleteTextView actvEdificio_Calles;
	TextView tvNELatitud;
	TextView tvNELongitud;
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.nuevoedificio_activity);
	    
	    etvEdificio_NombreEdificio = (EditText)findViewById(R.id.etvEdificio_NombreEdificio);
	    etvEdificio_NumeroEdificio = (EditText)findViewById(R.id.acEdificio_NumeroEdificio);
	    actvEdificio_Calles = (AutoCompleteTextView)findViewById(R.id.actvEdificio_Calles);
	    tvNELatitud = (TextView)findViewById(R.id.tvNELatitud);
	    tvNELongitud = (TextView)findViewById(R.id.tvNELongitud);
	    
	    Intent i = getIntent();
	    
	    lat = i.getDoubleExtra("lat", 0);
	    lng = i.getDoubleExtra("lng", 0);
	    
	    
	    tvNELatitud.setText(String.format("%.2f", lat));
	    tvNELongitud.setText(String.format("%.2f", lng));
	    	
	    
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		
	    ID_EDIF = UUID.randomUUID();
	    
	    ///ObtenerRegionCUT();
	    new TaskCargarSugerenciasCalles().execute();
	    
	    
	    actvEdificio_Calles.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				actvEdificio_Calles.showDropDown();
				actvEdificio_Calles.requestFocus();
	            return false;
			}
		});
	    
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	CrearMenu(menu);
    	return true;
    	
    	
    }
    
	private void CrearMenu(Menu menu) {
		MenuItem item1 = menu.add(0, 0, 0, "Grabar");
		{
			// --Copio las imagenes que van en cada item
			item1.setIcon(R.drawable.grabar);
			item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}

		MenuItem item2 = menu.add(0, 1, 1, "Cancelar");
		{
			item2.setIcon(R.drawable.cancelar);
			item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}


	}
	
	// --Sobre escribimos el metodo para saber que item fue pulsado
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// --Llamamos al metodo que sabe que itema fue seleccionado
		return MenuSelecciona(item);
	}
	
	private boolean MenuSelecciona(MenuItem item) {
		switch (item.getItemId()) {

		case 0: //Grabar
			
			if(etvEdificio_NombreEdificio.getText().toString().trim().equals(""))
			{
				Toast.makeText(NuevoEdificio_Activity.this,"Debe ingresar nombre de edificio", Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(actvEdificio_Calles.getText().toString().trim().equals(""))
			{
				Toast.makeText(NuevoEdificio_Activity.this,"Debe ingresar nombre de calle para edificio", Toast.LENGTH_SHORT).show();
				return false;
			}						
			else if(etvEdificio_NumeroEdificio.getText().toString().trim().equals(""))
			{
				Toast.makeText(NuevoEdificio_Activity.this,"Debe ingresar n�mero de domicilio para edificio", Toast.LENGTH_SHORT).show();
				return false;
			}			
			else
			{
				GrabarEdificio();
				Intent resultIntent = new Intent();
				resultIntent.putExtra("ID_EDIF", ID_EDIF);
				setResult(Activity.RESULT_OK, resultIntent);
				finish();
			}
			return true;

		case 1://Cancelar
			
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		case android.R.id.home:
    		setResult(Activity.RESULT_CANCELED);
    		
    		finish();
			return true;

		}
		return false;
	}    
	
	/*
	private void ObtenerRegionCUT()
	{
		try {
			DataBaseSpatialite dbSL = new DataBaseSpatialite();
			int[] data = dbSL.ObtenerRegionCUT(lng, lat);
			
			Region = data[0];
			//Region = globales.region.getRegion_id();
			CUT = data[1];
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	*/
	
	private void GrabarEdificio()
	{
		Edificio edif = new Edificio();
		edif.setId_edif(ID_EDIF);
		edif.setRegion_id(globales.region.getRegion_id());
		edif.setComuna_id(globales.RPC);
		edif.setNombre_edif(etvEdificio_NombreEdificio.getText().toString().toUpperCase());
		edif.setCalle(actvEdificio_Calles.getText().toString().toUpperCase());
		edif.setNumero(etvEdificio_NumeroEdificio.getText().toString());
		edif.setNuevo(true);
		edif.setCoord_lat(lat);
		edif.setCoord_lng(lng);
		
		DataBaseUtil db = new DataBaseUtil(NuevoEdificio_Activity.this);
		db.open();
		db.InsertarEdificio(edif);
		db.close();
		
	}
	private class TaskCargarSugerenciasCalles extends AsyncTask<Void, Void, List<String>>
	{
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(NuevoEdificio_Activity.this);
			pd.setTitle("Cargando Sugerencias Calles..");
			pd.setMessage("Por favor espere...");       
			pd.setIndeterminate(true);
			pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//			pd.setMax(100);
			pd.show();
		}
		
		@Override
		protected List<String> doInBackground(Void... args) 
		{
			
			try {
				return AgregarSugerenciasCallesMapa(new ArrayList<String>());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}
		
	    
		 public List<String> AgregarSugerenciasCallesMapa(List<String> calles) throws Exception {
			 try 
			 {	     
				 //AGREGAR CALLES MEDIANTE CODIGO DE UPM EN BASE SPATIALITE
				 if(globales.padres==PADRES.MANZANAS)
				 {
					 DataBaseUtil db = new DataBaseUtil(NuevoEdificio_Activity.this);
					 db.open();
					 Manzana mz = db.ObtenerManzana(globales.IDPADRE,0);
					 db.close();
					 new DataBaseSpatialite().ObtenerCallesPadre(String.valueOf(mz.getGeocodigo()), calles);
				 }
				 
				 if(globales.padres==PADRES.SECCIONES)
				 {
					 new DataBaseSpatialite().ObtenerCallesPadre(globales.IDPADRE, calles);
				 }	
				 
				 
				 //AGREGAR CALLES MEDIANTE CODIGO DE UPM EN BASE DE DATOS 
		    	DataBaseUtil db = new DataBaseUtil(NuevoEdificio_Activity.this);
		    	db.open();
		    	calles = db.ObtenerSugerenciasCalle(globales.IDPADRE);
		    	db.close();
				 
		    	//AGREGAR CALLES MEDIANTE COORDENADA
				 DataBaseSpatialite sldb = new DataBaseSpatialite();
				 return sldb.AgregarSugerenciasCalles(calles, lat, lng);
				 
				 
	
			 } catch (Exception e) {
		            e.printStackTrace();
		            Log.d("EnumeracionViviendas","open database"  +  e.getMessage().toString());
		            return null; 
		        }
		    }


		
		@Override 
		protected void onPostExecute(List<String> calles) 
		{
			Collections.sort(calles);
			
	    	ArrayAdapter<String> adapterCalles =  new ArrayAdapter<String>(NuevoEdificio_Activity.this, R.layout.item_solo_string, calles);
	    	
	    	actvEdificio_Calles.setThreshold(1);
	    	actvEdificio_Calles.setAdapter(adapterCalles);
	    	adapterCalles.setNotifyOnChange(true);
	    	
			if (pd != null) {
				pd.dismiss();
			}
			//new TaskCargarViviendas().execute();		
			/*
			if(padres.size()>0)
			{
				new TaskCargarViviendas().execute();				
			}
			*/

		}
	}
}
