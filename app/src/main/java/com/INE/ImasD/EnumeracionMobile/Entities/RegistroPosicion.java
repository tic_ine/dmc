package com.INE.ImasD.EnumeracionMobile.Entities;

import java.util.Date;

public class RegistroPosicion {

	/*
		String TablaRegistroPosicion = "create table if not exists "  
	+ " REGISTRO_POSICION (ACT_ID integer not null, "  
	+ " ID_USUARIO integer not null, " 
	+ " FECHA numeric null,"
	+ " NOMBREUSUARIO text null,"
	+ " COORD_X real not null,"
	+ " COORD_Y real not null,"
	+ " NOMBRE_MAQUINA text not null, "
	+ "PRIMARY KEY (ACT_ID, ID_USUARIO,FECHA))";
* */
	
	private int act_id;
	private int id_usuario;
	private Date fecha;
	private String nombreusuario;
	private double coord_x;
	private double coord_y;
	private String nombre_maquina;
	private boolean enviado;
	
	public int getAct_id() {
		return act_id;
	}
	public void setAct_id(int act_id) {
		this.act_id = act_id;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getNombreusuario() {
		return nombreusuario;
	}
	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}
	public double getCoord_x() {
		return coord_x;
	}
	public void setCoord_x(double coord_x) {
		this.coord_x = coord_x;
	}
	public double getCoord_y() {
		return coord_y;
	}
	public void setCoord_y(double coord_y) {
		this.coord_y = coord_y;
	}
	public String getNombre_maquina() {
		return nombre_maquina;
	}
	public void setNombre_maquina(String nombre_maquina) {
		this.nombre_maquina = nombre_maquina;
	}
	public boolean isEnviado() {
		return enviado;
	}
	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	
	
	
}
