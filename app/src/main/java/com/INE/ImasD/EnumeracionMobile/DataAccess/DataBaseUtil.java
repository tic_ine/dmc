package com.INE.ImasD.EnumeracionMobile.DataAccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.INE.ImasD.EnumeracionMobile.Entities.Actualizacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Area_Trabajo;
import com.INE.ImasD.EnumeracionMobile.Entities.Categoria;
import com.INE.ImasD.EnumeracionMobile.Entities.CategoriaPOI;
import com.INE.ImasD.EnumeracionMobile.Entities.CodigoAnotacion;
import com.INE.ImasD.EnumeracionMobile.Entities.Dibujo;
import com.INE.ImasD.EnumeracionMobile.Entities.Edificio;
import com.INE.ImasD.EnumeracionMobile.Entities.Encuesta;
import com.INE.ImasD.EnumeracionMobile.Entities.EncuestaMovil;
import com.INE.ImasD.EnumeracionMobile.Entities.Hogar;
import com.INE.ImasD.EnumeracionMobile.Entities.Imagen;
import com.INE.ImasD.EnumeracionMobile.Entities.Localidad;
import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.POI;
import com.INE.ImasD.EnumeracionMobile.Entities.Parametro;
import com.INE.ImasD.EnumeracionMobile.Entities.RegistroPosicion;
import com.INE.ImasD.EnumeracionMobile.Entities.RegistroPosicionDiario;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoCalle;
import com.INE.ImasD.EnumeracionMobile.Entities.TipoViviendaTemporada;
import com.INE.ImasD.EnumeracionMobile.Entities.UsoDestino;
import com.INE.ImasD.EnumeracionMobile.Entities.Usuario;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.funciones;
import com.INE.ImasD.EnumeracionMobile.OtrasClases.globales;
import com.esri.core.geometry.Point;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
	

public class DataBaseUtil 
{
	private static final String TAG = "DataBaseUtil";
	private static final String DATABASE_NAME = "dv.db";
	private static final int DATABASE_VERSION = 1;
	private static final String FILEDIR = "DV";
	private final Context mCtx;

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	private class DatabaseHelper extends SQLiteOpenHelper 
	{
		DatabaseHelper(Context context) {
			  super(context, Environment.getExternalStorageDirectory()
			            + File.separator + FILEDIR
			            + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
		}


		@Override
		public void onCreate(SQLiteDatabase db) {

			mDb = db;

			funciones.CrearCarpetasRequeridas(mCtx);
			//funciones.LlenarParametroWS(mCtx);
			
			
			CrearTablaActualizaciones();
			CrearTablaUsuarios();
			CrearTablaRegistroPosicion();
			CrearTablaManzanas();
			CrearTablaLocalidades();
			CrearTablaSecciones();
			CrearTablaViviendas();
			
			CrearTablaTipoCalle();
			
			CrearTablaUsoDestino();
			CrearTablaCategoria();
			CrearTablaTipoViviendaTemporada();
			CrearTablaCodigoAnotacion();
			CrearTablaParametros();
			CrearTablaHogares();
			
			CrearTablaEdificios();
			
			CrearTablaDibujos();
			CrearTablaImagenes();

			CrearTablaAudios();

            CrearTablaEncuestas();
            CrearTablaEncuestasMovil();

            CrearTablaCategoriasPOI();
            CrearTablaPOI();

			CrearTablaAreasTrabajo();
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion);
		}
	}


	public DataBaseUtil(Context ctx) {
		this.mCtx = ctx;
	}

	public DataBaseUtil open() throws SQLException {
		try
		{
			String sDbPath = mCtx.getFilesDir().getPath() + "/databases/dv.db";
			
			File fl = new File(sDbPath);
			if(!fl.exists())
			{
				fl.getParentFile().mkdirs();
			}
			
			mDbHelper = new DatabaseHelper(mCtx);
			
			mDb = mDbHelper.getWritableDatabase();			
		}
		catch(Exception e)
		{
			return null;
		}
		return this;
	}
	/**
	 * This method is used for closing the connection.
	 */
	public void close() {
		mDbHelper.close();
	}


	public void CrearTablaAreasTrabajo()
	{

		String crearTablaUsuarios = "create table if not exists "
				+ " AREA_TRABAJO (AT_ID integer primary key, "
				+ " CU_SECCION text null, "
				+ " ACT_ID integer null, "
				+ " ID_USUARIO integer null, "
				+ " ID_USUARIO_SUP integer null, "
				+ " NUMERO_AT integer null, "
				+ " FECHACREACION numeric null, "
				+ " WKT text null, "
				+ " CERRADA numeric null)";


		mDb.execSQL(crearTablaUsuarios);


	}
	public void CrearTablaActualizaciones()
	{

		String crearTablaUsuarios = "create table if not exists "  
				+ " ACTUALIZACIONES (ACT_ID integer primary key, "  
				+ " ACT_GLOSA text null, " 
				+ " FECHAHORA numeric null,"
				+ " ACTIVA numeric null, "
				+ " REGION_ID integer)";

		mDb.execSQL(crearTablaUsuarios);


	}

	public void CrearTablaUsuarios()
	{

		String crearTablaUsuarios = "create table if not exists "  
				+ " USUARIOS (ID_USUARIO integer primary key, "  
				+ " USERNAME text null, " 
				+ " CONTRASENA text null,"
				+ " NOMBRES text null,"
				+ " APELLIDOS text null,"
				+ " EMAIL text null,"
				+ " VIGENTE integer null,"
				+ " PERFIL_ID integer null)";

		mDb.execSQL(crearTablaUsuarios);


	}
	


	public void CrearTablaRegistroPosicion()
	{

		String TablaRegistroPosicion = "create table if not exists "  
				+ " REGISTRO_POSICION (ACT_ID integer not null, "  
				+ " ID_USUARIO integer not null, " 
				+ " FECHA numeric null,"
				+ " NOMBREUSUARIO text null,"
				+ " COORD_X real not null,"
				+ " COORD_Y real not null,"
				+ " NOMBRE_MAQUINA text not null, "
				+ " ENVIADO numeric not null, "
				+ " PRIMARY KEY (ACT_ID, ID_USUARIO,FECHA))";


		mDb.execSQL(TablaRegistroPosicion);


	}
	
	public void CrearTablaViviendas()
	{

		String crearTablaViviendas = "create table if not exists "  
				+ " VIVIENDAS (ID_VIVIENDA text not null, "
				+ " ACT_ID integer not null, "
				+ " ID_USUARIO integer not null, "
				+ " IDPADRE text null, " 
				+ " TIPOCALLE_ID text null, "
				+ " TIPOCALLE_GLOSA text null, "
				+ " NOMBRE_CALLE_CAMINO text null,"
				+ " N_DOMICILIO text null,"
				+ " N_LETRA_BLOCK text null,"
				+ " N_PISO text null,"
				+ " N_LETRA_DEPTO text null,"
				+ " ID_ESE integer null, "
				+ " ID_USODESTINO integer null, "
				+ " USODESTINO_GLOSA text null, "
				+ " OK numeric null, "
				+ " NULO numeric null, "
				+ " FECHAINGRESO INTEGER null,"
				+ " COORD_X real null,"
				+ " COORD_Y real null,"
				+ " DESCRIPCION text null,"
				+ " ID_ORIGEN integer null, "
				+ " FECHAMODIFICACION numeric null,"
				+ " FECHAELIMINACION numeric null,"
				+ " USUARIO_ID_MODIFICACION integer null, "
				+ " USUARIO_ID_ELIMINACION integer null, "
				+ " USUARIO_ID_INGRESO integer null, "
				+ " ORDEN integer null, "
				+ " ORDEN_VIV integer null, "
				+ " CODIGO_ID integer null, "
				+ " POSICION integer null,"
				
				+ " VIVTEMP_ID integer null,"
				+ " VIVTEMP_GLOSA text null,"
				+ " KILOMETRO text null,"
				+ " LOTE text null,"
				+ " SITIO text null,"
				+ " LETRAPARCELA text null,"
				+ " ORDENEDIFICACION integer null,"
				+ " UBICACIONHORIZONTAL text null,"
				+ " UBICACIONVERTICAL text null,"
				
				+ " NUEVA numeric null,"
				+ " MODIFICADA numeric null,"
				+ " ID_EDIF text null,"
				
				+ " NOMBRELOCALIDAD text null,"
				+ " NOMBREENTIDAD text null,"	
				+ " CAT_ID numeric null,"				
				+ " NUMERACION_TERRENO text null,"				
				+ " SITIO_SIN_EDIFICACION text null,"				
				+ " MANZANA_ALDEA numeric null,"
				+ " AT_ID integer null,"
				
				+ " PRIMARY KEY (ACT_ID, ID_USUARIO,ID_VIVIENDA))";

		mDb.execSQL(crearTablaViviendas);


	}
	

	public void CrearTablaManzanas()
	{

	
		String crearTablaManzanas = "create table if not exists "  
				+ " MANZANAS (IDMANZANA integer, "
				+ " COMUNA_ID integer null,"				
				+ " URBANO_ID integer null,"
				+ " URBANO_NOMBRE text null, "
				+ " DISTRITO integer null,"
				+ " ZONA integer null,"
				+ " MANZANA integer null,"
				+ " GEOCODIGO integer null,"
				+ " ACT_ID integer null,"
				+ " ID_USUARIO integer null,"				
				+ " CENTROIDE_LAT real null,"
				+ " CENTROIDE_LNG real null,"
				+ " ORIGEN integer null, PRIMARY KEY (ACT_ID, ID_USUARIO,IDMANZANA))";
		

		mDb.execSQL(crearTablaManzanas);

	}
	
	public void CrearTablaLocalidades()
	{

		/*
		 ENTIDAD_ID
		 COMUNA_ID
		 DISTRITO
		 NOMBRELOCALIDAD
		 NOMBREENTIDAD
		 CAT_ID
		 CAT_GLOSA
		 ACT_ID
		 ID_USUARIO
		 FECHA_CREACION
		 CENTROIDE_LAT
		 CENTROIDE_LNG
		 
		 * */
		String crearTablaEntidades = "create table if not exists "  
				+ " LOCALIDADES (LOC_ID text primary key, "
				+ " LOC_GLOSA text null, "				
				+ " COMUNA_ID integer null,"
				+ " DISTRITO integer null,"
				+ " CENTROIDE_LAT real null," 
				+ " CENTROIDE_LNG real null, "				
				+ " ACT_ID integer null,"
				+ " ID_USUARIO integer null,"
				+ " FECHA_CREACION numeric null,"
				+ " NUEVA integer null)";
		

		mDb.execSQL(crearTablaEntidades);


	}	

	
	public void CrearTablaTipoCalle()
	{

		String crearTablaTipoCalle = "create table if not exists "  
				+ " TIPOCALLE (TIPOCALLE_ID text primary key, "
				+ " TIPOCALLE_GLOSA text null)"; 

		mDb.execSQL(crearTablaTipoCalle);


	}
	
	
	public void CrearTablaUsoDestino()
	{

		String crearTablaTipoCalle = "create table if not exists "  
				+ " USODESTINO (ID_USODESTINO integer primary key, "  
				+ " GLOSA_USODESTINO text null)"; 

		mDb.execSQL(crearTablaTipoCalle);


	}
	
	
	public void CrearTablaCategoria()
	{

		String crearTablaTipoCalle = "create table if not exists "  
				+ " CATEGORIA (CAT_ID integer primary key, "  
				+ " CAT_GLOSA text null)"; 

		mDb.execSQL(crearTablaTipoCalle);


	}
	
	public void CrearTablaTipoViviendaTemporada()
	{

		String crearTablaTipoViviendaTemporada = "create table if not exists "  
				+ " TIPOVIVIENDATEMPORADA (VIVTEMP_ID integer primary key, "  
				+ " VIVTEMP_GLOSA text null)"; 

		mDb.execSQL(crearTablaTipoViviendaTemporada);


	}
	
	public void CrearTablaCodigoAnotacion()
	{

		String crearTablaTipoViviendaTemporada = "create table if not exists "  
				+ " CODIGO_ANOTACIONES (CODIGO_ID integer primary key, "  
				+ " CODIGO_GLOSA text null)"; 

		mDb.execSQL(crearTablaTipoViviendaTemporada);


	}
	
	public void CrearTablaParametros()
	{

		String crearTablaParametros = "create table if not exists "  
				+ " PARAMETROS ([KEY] text primary key, "  
				+ " VALOR text null)"; 

		mDb.execSQL(crearTablaParametros);


	}
	
	
	public void CrearTablaHogares()
	{


		String crearTablaHogares = "create table if not exists "  
				+ " HOGARES (ACT_ID integer, "  
				+ " ID_USUARIO integer, "
				+ " ID_VIVIENDA text, "
				+ " ORDENHOGAR integer,"
				+ " NUMPERSONAS integer, "
				+ " JEFE text, "
				+ " ENVIADO numeric, "
				+ " PRIMARY KEY (ACT_ID, ID_USUARIO,ID_VIVIENDA, ORDENHOGAR))";

		mDb.execSQL(crearTablaHogares);


	}
	
	public void CrearTablaSecciones()
	{

		String crearTablaSecciones = "create table if not exists "  
				+ " SECCIONES (CU_SECCION integer PRIMARY KEY, "  
				+ " CUT integer,"
				+ " COMUNA_GLOSA text,"
				+ " DISTRITO text,"	
				+ " COD_CARTO integer,"
				+ " ESTRATO_MUESTRAL integer,"
				+ " ESTRATO_GEO integer,"				
				+ " ESTRATO_ENE integer,"
				+ " TIPO_ESTRATO integer,"
				+ " CODIGO_SECCION integer,"				
				+ " ACT_ID integer,"
				+ " ID_USUARIO integer)" ;				

		mDb.execSQL(crearTablaSecciones);

	}
	
	public void CrearTablaEdificios()
	{


		String crearTablaEdificios = "create table if not exists "  
				+ " EDIFICIOS (ID_EDIF text PRIMARY KEY, "  
				+ " NOMBRE_EDIF text, "
				+ " COORD_LAT real,"
				+ " COORD_LNG real, "				
				+ " COMUNA_ID integer,"
				+ " REGION_ID integer,"
				+ " CALLE text,"				
				+ " NUMERO text, "
				+ " NUEVO integer)";

		mDb.execSQL(crearTablaEdificios);
	}
	
	public void CrearTablaDibujos()
	{

		String CrearTablaDibujos = "create table if not exists "  
				+ " DIBUJOS (ID_DIBUJO text PRIMARY KEY, "
				+ " ACT_ID integer, "
				+ " ID_USUARIO integer, "
				+ " IDPADRE text, "
				+ " DIB_GLOSA text, "
				+ " DIB_TEXT text, "
				+ " NULO integer, "
				+ " FECHA_CREACION numeric null, "
				+ " DIB_TIPO text, "
				+ " DIB_ESTADO numeric, "
				+ " NUEVO integer) ";


		mDb.execSQL(CrearTablaDibujos);
	}
	
	
	public void CrearTablaImagenes()
	{

		String crearTablaEdificios = "create table if not exists "  
				+ " IMAGENES (ID_VIVIENDA text PRIMARY KEY, "
				+ " ACT_ID integer, "
				+ " ID_USUARIO integer, " 
				+ " IMAGEN BLOB) ";


		mDb.execSQL(crearTablaEdificios);
	}

	public void CrearTablaAudios()
	{

		String crearTablaAudios = "create table if not exists "
				+ " AUDIOS (ID_VIVIENDA text PRIMARY KEY, "
				+ " ACT_ID integer, "
				+ " ID_USUARIO integer, "
				+ " AUDIO BLOB) ";

		mDb.execSQL(crearTablaAudios);
	}

    public void CrearTablaEncuestas()
    {

        String crearTablaEncuestas = "create table if not exists "
                + " ENCUESTAS (IDENCUESTA integer PRIMARY KEY , "
                + " NOMBREENCUESTA text, "
                + " TIPOENCUESTA text, "
                + " ACT_ID integer, "
                + " JSON text, "
                + " IDENCUESTAMOVIL text) ";


        mDb.execSQL(crearTablaEncuestas);
    }


    public void CrearTablaEncuestasMovil()
    {

        String crearTablaEncuestasUsuario = "create table if not exists "
                + " ENCUESTAS (IDENCUESTA integer, "
                + " NOMBREENCUESTA text, "
                + " TIPOENCUESTA text, "
                + " ACT_ID integer, "
                + " ID_USUARIO integer, "
                + " IDPADRE text, "
                + " ID_VIVIENDA text, "
                + " HOGAR integer, "
                + " PERSONA integer, "
                + " JSON text, "
                + " IDENCUESTAMOVIL text, "
                + " PRIMARY KEY (IDENCUESTA, ACT_ID, ID_USUARIO))";

        mDb.execSQL(crearTablaEncuestasUsuario);
    }


	public void CrearTablaCategoriasPOI()
	{

		String crearTablaCATpoi = "create table if not exists "
				+ " CATEGORIA_POI (IDCATPOI integer PRIMARY KEY, "
				+ " GLOSA text, "
				+ " ICON BLOB) ";

		mDb.execSQL(crearTablaCATpoi);
	}

	public void CrearTablaPOI()
	{

		String crearTablapoi = "create table if not exists "
				+ " POI (IDPOI text PRIMARY KEY, "
				+ " IDCATPOI integer, "
				+ " GLOSA text, "
				+ " COORD_X real,"
				+ " COORD_Y real, "
				+ " FECHACAPTURA numeric, "
				+ " ENVIADO integer) ";

		mDb.execSQL(crearTablapoi);
	}



	public boolean BorrarTodosActualizaciones()
	{
		boolean retorno = false;

		mDb.execSQL("DELETE FROM ACTUALIZACIONES");

		return retorno;
	}

	public boolean BorrarTodosUsuarios()
	{
		boolean retorno = false;

		mDb.execSQL("DELETE FROM USUARIOS");

		return retorno;
	}

	public boolean BorrarViviendasEnviadas()
	{

		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};
		return mDb.delete("VIVIENDAS", "(NUEVA = 0 OR MODIFICADA = 0) AND ACT_ID = ? AND ID_USUARIO = ?", args)>0;


	}
	
	public boolean BorrarViviendasEnviadasManzana(String IDPADRE)
	{

		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()), IDPADRE};
		return mDb.delete("VIVIENDAS", "(NUEVA = 0 OR MODIFICADA = 0) AND ACT_ID = ? AND ID_USUARIO = ? AND IDPADRE = ?", args)>0;

	}
	public boolean BorrarManzanas()
	{
		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};
		
		return mDb.delete("MANZANAS", "ACT_ID = ? AND ID_USUARIO = ?", args)>0;
	}

	public boolean BorrarLocalidades()
	{

		mDb.delete("LOCALIDADES", null, null);
		
		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};
		return mDb.delete("LOCALIDADES", "ACT_ID = ? AND ID_USUARIO = ? AND NUEVA = 0", args)>0;
	}
	
	
	public boolean BorrarSecciones()
{
    String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};

    return mDb.delete("SECCIONES", "ACT_ID = ? AND ID_USUARIO = ?", args)>0;
}
	public boolean BorrarTiposCalle()
	{

		return mDb.delete("TIPOCALLE",null,null)>0;
	}
	
	public boolean BorrarUsoDestino()
	{
		
		return mDb.delete("USODESTINO",null,null)>0;
	}
	
	public boolean BorrarCategoria()
	{
		
		return mDb.delete("CATEGORIA",null,null)>0;
	}
	
	
	public boolean BorrarTipoViviendaTemporada()
	{
		
		return mDb.delete("TIPOVIVIENDATEMPORADA",null,null)>0;
	}

	
	public boolean BorrarCodigosAnotacion()
	{

		return (mDb.delete("CODIGO_ANOTACIONES", null, null)>0);

		
	}
	
	
	public boolean BorrarParametros()
	{

		return (mDb.delete("PARAMETROS", null, null)>0);

		
	}
	
	public boolean BorrarHogares()
	{
		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};	
		return (mDb.delete("HOGARES", "ACT_ID= ? AND ID_USUARIO = ? AND ENVIADO = 1", args)>0);

	}
	
	public boolean BorrarHogaresVivienda(Vivienda viv)
	{
		String[] args = new String[] { Integer.toString( viv.getACT_ID()), Integer.toString(viv.getID_USUARIO()), viv.getID_VIVIENDA().toString() };
		
		return (mDb.delete("HOGARES", "ACT_ID= ? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args)>0);

	}

	public boolean BorrarHogaresPadre(String ID_PADRE)
	{
		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()), ID_PADRE};
		mDb.rawQuery("DELETE FROM HOGARES WHERE ENVIADO = 1 AND ID_VIVIENDA  IN (SELECT ID_VIVIENDA FROM VIVIENDAS WHERE ACT_ID = ? AND ID_USUARIO = ? AND IDPADRE = ?)", args);

        return true;

	}
	
	//EDIFICIOS
	public boolean BorrarEdificios()
	{

		return (mDb.delete("EDIFICIOS", "NUEVO = 0", null)>0);

		
	}
	public boolean InsertarUsuario(Usuario usuario)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ID_USUARIO",usuario.getId_usuario());   
		values.put("USERNAME", usuario.getUsername());
		values.put("CONTRASENA", usuario.getContrasena());
		values.put("NOMBRES", usuario.getNombres());
		values.put("APELLIDOS", usuario.getApellidos());
		values.put("EMAIL", usuario.getEmail());
		values.put("VIGENTE", usuario.getVigencia());
		values.put("PERFIL_ID", usuario.getPerfil_id());
		retorno = (mDb.insert("USUARIOS", null, values) > 0);

		return retorno;
	}
	
	public boolean ObtenerUsuario(Usuario usuario)
	{

		String[] args = new String[] {usuario.getUsername(), usuario.getContrasena()};

		Cursor c = mDb.rawQuery("SELECT * FROM USUARIOS WHERE USERNAME = ? AND CONTRASENA = ?", args);

		if (c.moveToNext())
		{
		     //Recorremos el cursor hasta que no haya m�s registros

            usuario.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
            usuario.setUsername(c.getString(c.getColumnIndex("USERNAME")));
            usuario.setContrasena(c.getString(c.getColumnIndex("CONTRASENA")));
            usuario.setNombres(c.getString(c.getColumnIndex("NOMBRES")));
            usuario.setApellidos(c.getString(c.getColumnIndex("APELLIDOS")));
            usuario.setEmail(c.getString(c.getColumnIndex("EMAIL")));
            usuario.setVigente(c.getInt(c.getColumnIndex("VIGENTE")));
            usuario.setPerfil_id(c.getInt(c.getColumnIndex("PERFIL_ID")));

             c.close();
             return true;
		}
		else
		{
			c.close();
			return false;
		}

	}

	public boolean InsertarActualizaciones(Actualizacion act)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ACT_ID",act.getAct_id());   
		values.put("ACT_GLOSA", act.getAct_glosa());
		values.put("FECHAHORA", act.getFechahora().getTime());
		values.put("ACTIVA", act.getActiva());
		values.put("REGION_ID",act.getRegion_id());

		retorno = (mDb.insert("ACTUALIZACIONES", null, values) > 0);

		return retorno;
	}
	
	public boolean InsertarVivienda(Vivienda viv)
	{
		boolean retorno = false;
		ContentValues values;
	
	
		values = new ContentValues();  
		values.put("ACT_ID", viv.getACT_ID());
		values.put("ID_USUARIO",viv.getID_USUARIO());
		values.put("ID_VIVIENDA",viv.getID_VIVIENDA().toString());
		values.put("IDPADRE",viv.getIDPADRE());
		values.put("TIPOCALLE_ID",viv.getTIPOCALLE_ID());
		values.put("TIPOCALLE_GLOSA",viv.getTIPOCALLE_GLOSA());
		values.put("NOMBRE_CALLE_CAMINO",viv.getNOMBRE_CALLE_CAMINO());
		values.put("N_DOMICILIO",viv.getN_DOMICILIO());
		values.put("N_LETRA_BLOCK",viv.getN_LETRA_BLOCK());
		values.put("N_PISO",viv.getN_PISO());
		values.put("N_LETRA_DEPTO",viv.getN_LETRA_DEPTO());
		values.put("ID_ESE",viv.getID_ESE());
		values.put("ID_USODESTINO",viv.getID_USODESTINO());
		values.put("OK",viv.isOK());
		values.put("NULO",viv.isNULO());
		
		//values.put("FECHAINGRESO",viv.getFECHAINGRESO().getTime());
		//values.put("FECHAINGRESO",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(viv.getFECHAINGRESO()));
		values.put("FECHAINGRESO",viv.getFECHAINGRESO().getTime());
		 // 
		values.put("COORD_X",viv.getCOORD_X());
		values.put("COORD_Y",viv.getCOORD_Y());
		values.put("DESCRIPCION",viv.getDESCRIPCION());
		values.put("ID_ORIGEN",viv.getID_ORIGEN());
		
		if(viv.getFECHAMODIFICACION()==null)
		{
			values.putNull("FECHAMODIFICACION");
		}
		else
		{
			//values.put("FECHAMODIFICACION",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(viv.getFECHAMODIFICACION()));
			//values.put("FECHAMODIFICACION",viv.getFECHAMODIFICACION().getTime());
			values.put("FECHAMODIFICACION",viv.getFECHAMODIFICACION().getTime());
		}
		
		if(viv.getFECHAELIMINACION()==null)
		{
			values.putNull("FECHAELIMINACION");
		}
		else
		{
			values.put("FECHAELIMINACION",viv.getFECHAELIMINACION().getTime());
			//values.put("FECHAELIMINACION",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(viv.getFECHAELIMINACION()));
			//values.put("FECHAELIMINACION",viv.getFECHAELIMINACION().getTime());
		}
			
		if(viv.getUSUARIO_ID_MODIFICACION()==0)
		{
			values.putNull("USUARIO_ID_MODIFICACION");
		}
		else
		{
			values.put("USUARIO_ID_MODIFICACION",viv.getUSUARIO_ID_MODIFICACION());
		}
		
		if(viv.getUSUARIO_ID_ELIMINACION()==0)
		{
			values.putNull("USUARIO_ID_ELIMINACION");
		}
		else
		{
			values.put("USUARIO_ID_ELIMINACION",viv.getUSUARIO_ID_ELIMINACION());
		}
	
	
		values.put("USUARIO_ID_INGRESO",viv.getUSUARIO_ID_INGRESO());   
		values.put("ORDEN", viv.getORDEN());
		values.put("ORDEN_VIV", viv.getORDEN_VIV());
		values.put("CODIGO_ID", viv.getCODIGO_ID());
		values.put("POSICION", viv.getPOSICION());
		
		/*
		+ " VIVTEMP_ID integer null,"
		+ " VIVTEMP_GLOSA text null,"
		+ " KILOMETRO text null,"
		+ " LOTE text null,"
		+ " SITIO text null,"
		+ " LETRAPARCELA text null,"
		+ " ORDENEDIFICACION integer null,"
		+ " UBICACIONHORIZONTAL text null,"
		+ " UBICACIONVERTICAL text null,"
		*/
		
		values.put("VIVTEMP_ID", viv.getVIVTEMP_ID());
		values.put("VIVTEMP_GLOSA", viv.getVIVTEMP_GLOSA());
		values.put("KILOMETRO", viv.getKILOMETRO());
		values.put("LOTE", viv.getLOTE());
		values.put("SITIO", viv.getSITIO());
		values.put("LETRAPARCELA", viv.getLETRAPARCELA());
		values.put("ORDENEDIFICACION", viv.getORDENEDIFICACION());
		values.put("UBICACIONHORIZONTAL", viv.getUBICACIONHORIZONTAL());
		values.put("UBICACIONVERTICAL", viv.getUBICACIONVERTICAL());
		
		values.put("NUEVA", viv.isNUEVA());
		values.put("MODIFICADA", viv.isMODIFICADA());
		if(viv.getID_EDIF()==null)
		{
			values.putNull("ID_EDIF");
		}
		else
		{
			values.put("ID_EDIF", viv.getID_EDIF().toString());
		}
				
		
		if(viv.getNOMBRELOCALIDAD()==null)
		{
			values.put("NOMBRELOCALIDAD", "");
		}
		else
		{
			values.put("NOMBRELOCALIDAD", viv.getNOMBRELOCALIDAD());
		}
	
		if(viv.getNOMBREENTIDAD()==null)
		{
			values.put("NOMBREENTIDAD", "");
		}
		else
		{
			values.put("NOMBREENTIDAD", viv.getNOMBREENTIDAD());
		}
	
		if(viv.getCAT_ID()==0)
		{
			values.putNull("CAT_ID");
		}
		else
		{
			values.put("CAT_ID", viv.getCAT_ID());
		}

		values.put("AT_ID", viv.getAT_ID());
		
		retorno = (mDb.insertWithOnConflict("VIVIENDAS",null, values, SQLiteDatabase.CONFLICT_IGNORE) >0);
	
		return retorno;
	}
	public boolean ModificarVivienda(Vivienda viv)
	{
		boolean retorno = false;
		ContentValues values;
	
	
		values = new ContentValues();  
		values.put("ACT_ID", viv.getACT_ID());
		values.put("ID_USUARIO",viv.getID_USUARIO());
		values.put("ID_VIVIENDA",viv.getID_VIVIENDA().toString());
		values.put("IDPADRE",viv.getIDPADRE());
		values.put("TIPOCALLE_ID",viv.getTIPOCALLE_ID());
		values.put("TIPOCALLE_GLOSA",viv.getTIPOCALLE_GLOSA());
		values.put("NOMBRE_CALLE_CAMINO",viv.getNOMBRE_CALLE_CAMINO());
		values.put("N_DOMICILIO",viv.getN_DOMICILIO());
		values.put("N_LETRA_BLOCK",viv.getN_LETRA_BLOCK());
		values.put("N_PISO",viv.getN_PISO());
		values.put("N_LETRA_DEPTO",viv.getN_LETRA_DEPTO());
		values.put("ID_ESE",viv.getID_ESE());
		values.put("ID_USODESTINO",viv.getID_USODESTINO());
		values.put("OK",viv.isOK());
		values.put("NULO",viv.isNULO());
		
		
		values.put("COORD_X",viv.getCOORD_X());
		values.put("COORD_Y",viv.getCOORD_Y());
		values.put("DESCRIPCION",viv.getDESCRIPCION());
		values.put("ID_ORIGEN",viv.getID_ORIGEN());
	
		values.put("FECHAMODIFICACION",viv.getFECHAMODIFICACION().getTime());
	
		if(viv.getFECHAELIMINACION()==null)
		{
			values.putNull("FECHAELIMINACION");
		}
		else
		{
			values.put("FECHAELIMINACION",viv.getFECHAELIMINACION().getTime());
			//values.put("FECHAELIMINACION",viv.getFECHAELIMINACION().getTime());
		}
			
		values.put("USUARIO_ID_MODIFICACION",viv.getUSUARIO_ID_MODIFICACION());
	
		if(viv.getUSUARIO_ID_ELIMINACION()==0)
		{
			values.putNull("USUARIO_ID_ELIMINACION");
		}
		else
		{
			values.put("USUARIO_ID_ELIMINACION",viv.getUSUARIO_ID_ELIMINACION());
		}
	
	
		values.put("ORDEN", viv.getORDEN());
		values.put("ORDEN_VIV", viv.getORDEN_VIV());
		values.put("CODIGO_ID", viv.getCODIGO_ID());
		values.put("POSICION", viv.getPOSICION());
		
		/*
		+ " VIVTEMP_ID integer null,"
		+ " VIVTEMP_GLOSA text null,"
		+ " KILOMETRO text null,"
		+ " LOTE text null,"
		+ " SITIO text null,"
		+ " LETRAPARCELA text null,"
		+ " ORDENEDIFICACION integer null,"
		+ " UBICACIONHORIZONTAL text null,"
		+ " UBICACIONVERTICAL text null,"
		*/
		
		values.put("VIVTEMP_ID", viv.getVIVTEMP_ID());
		values.put("VIVTEMP_GLOSA", viv.getVIVTEMP_GLOSA());
		values.put("KILOMETRO", viv.getKILOMETRO());
		values.put("LOTE", viv.getLOTE());
		values.put("SITIO", viv.getSITIO());
		values.put("LETRAPARCELA", viv.getLETRAPARCELA());
		values.put("ORDENEDIFICACION", viv.getORDENEDIFICACION());
		values.put("UBICACIONHORIZONTAL", viv.getUBICACIONHORIZONTAL());
		values.put("UBICACIONVERTICAL", viv.getUBICACIONVERTICAL());
		
		values.put("NUEVA", false);
		values.put("MODIFICADA", true);
		if(viv.getID_EDIF()==null)
		{
			values.putNull("ID_EDIF");
		}
		else
		{
			values.put("ID_EDIF", viv.getID_EDIF().toString());
		}
				
		
		if(viv.getNOMBRELOCALIDAD()==null)
		{
			values.put("NOMBRELOCALIDAD", "");
		}
		else
		{
			values.put("NOMBRELOCALIDAD", viv.getNOMBRELOCALIDAD());
		}
	
		if(viv.getNOMBREENTIDAD()==null)
		{
			values.put("NOMBREENTIDAD", "");
		}
		else
		{
			values.put("NOMBREENTIDAD", viv.getNOMBREENTIDAD());
		}
	
		if(viv.getCAT_ID()==0)
		{
			values.putNull("CAT_ID");
		}
		else
		{
			values.put("CAT_ID", viv.getCAT_ID());
		}
		
		
		retorno = (mDb.update("VIVIENDAS",values, "ID_VIVIENDA = ?", new String[] {viv.getID_VIVIENDA().toString()}) >0);
	
		return retorno;
	}
	public boolean InsertarRegistroPosicion()
	{
		boolean retorno = false;
		if(!ExisteRegistroPosicion())
		{
		
			ContentValues values;
	
			
			values = new ContentValues();  
			values.put("ACT_ID", globales.actualizacion.getAct_id());
			values.put("ID_USUARIO", globales.usuario.getId_usuario());
			//values.put("FECHA",  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			values.put("FECHA",  new Date().getTime());
			values.put("NOMBREUSUARIO", globales.usuario.getNombres() + " " + globales.usuario.getApellidos());
			values.put("COORD_X", globales.UltimaPuntoObtenido_GPS.getX());
			values.put("COORD_Y", globales.UltimaPuntoObtenido_GPS.getY());
			values.put("NOMBRE_MAQUINA", "Droid_" + android.os.Build.MODEL);
			values.put("ENVIADO", false);
			retorno = (mDb.insert("REGISTRO_POSICION", null, values) > 0);
		}
		else
		{
			retorno = true;
		}
		
		return retorno;
		
	}
	
	public boolean ExisteRegistroPosicion()
	{
		boolean retorno = false;

		String SQL = "SELECT COUNT(*)  as CONTEO FROM REGISTRO_POSICION WHERE ACT_ID = ? AND ID_USUARIO = ? AND COORD_X = ? AND COORD_Y = ?";
		String[] args = 
		new String[]
				{
					Integer.toString(globales.actualizacion.getAct_id()), 
					Integer.toString(globales.usuario.getId_usuario()),
					Double.toString(globales.UltimaPuntoObtenido_GPS.getX()),
					Double.toString(globales.UltimaPuntoObtenido_GPS.getY())
					 
				};

		Cursor c = mDb.rawQuery(SQL, args);
		
		if(c.moveToFirst())
		{
			retorno = c.getInt(0)>0;	
		}
		else
		{
			retorno= false;
		}
		
		
		return retorno;
		
		
	}
	
	public boolean InsertarEdificio(Edificio edif)
	{
		boolean retorno = false;
		ContentValues values;

		
		values = new ContentValues();  
		values.put("ID_EDIF", edif.getId_edif().toString());
		values.put("NOMBRE_EDIF", edif.getNombre_edif());
		values.put("COORD_LAT",  edif.getCoord_lat());
		values.put("COORD_LNG", edif.getCoord_lng());
		values.put("COMUNA_ID", edif.getComuna_id());
		values.put("REGION_ID", edif.getRegion_id());
		values.put("CALLE", edif.getCalle());
		values.put("NUMERO", edif.getNumero());
		values.put("NUEVO", edif.isNuevo());
		retorno = (mDb.insert("EDIFICIOS", null, values) > 0);
		
		return retorno;
		
	}
	
	public List<Edificio> ObtenerEdificios()
	{
		List<Edificio> edificios = new ArrayList<Edificio>();
		Edificio edif;
		String[] args = new String[] { String.valueOf(globales.region.getRegion_id()) };
		
		Cursor c = mDb.query("EDIFICIOS", null, "REGION_ID = ? OR REGION_ID = 0", args, null, null, "REGION_ID");
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
		    	 edif = new Edificio();
		    	 
		    	 edif.setId_edif(UUID.fromString(c.getString(c.getColumnIndex("ID_EDIF"))));
		    	 edif.setNombre_edif(c.getString(c.getColumnIndex("NOMBRE_EDIF")));
		    	 
		    	 edif.setCoord_lat(c.getFloat(c.getColumnIndex("COORD_LAT")));
		    	 edif.setCoord_lng(c.getFloat(c.getColumnIndex("COORD_LNG")));
		    	 
		    	 edif.setComuna_id(c.getInt(c.getColumnIndex("COMUNA_ID")));
		    	 edif.setRegion_id(c.getInt(c.getColumnIndex("REGION_ID")));
		    	 
		    	 edif.setCalle(c.getString(c.getColumnIndex("CALLE")));
		    	 edif.setNumero(c.getString(c.getColumnIndex("NUMERO")));
		    	 
		    	 edif.setNuevo(c.getInt(c.getColumnIndex("NUEVO"))>0);

		    	 edificios.add(edif);
		          
		     } while(c.moveToNext());
		}
		c.close();

		return edificios;
	}
	
	public Cursor ObtenerEdificiosCursor()
	{
		String[] args = new String[] { String.valueOf(globales.region.getRegion_id()) };
		
		Cursor c = mDb.query("EDIFICIOS", null, "REGION_ID = ? AND NUEVO = 1", args, null, null, "REGION_ID");
		 
		return c;
	}
	
	public Edificio ObtenerEdificio(UUID ID_EDIF)
	{
		Edificio edif = new Edificio();
		String[] args = new String[] { ID_EDIF.toString() };
		
		Cursor c = mDb.query("EDIFICIOS", null, "ID_EDIF = ?", args, null, null, null);
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		    	 
		    	 edif.setId_edif(UUID.fromString(c.getString(c.getColumnIndex("ID_EDIF"))));
		    	 edif.setNombre_edif(c.getString(c.getColumnIndex("NOMBRE_EDIF")));
		    	 
		    	 edif.setCoord_lat(c.getFloat(c.getColumnIndex("COORD_LAT")));
		    	 edif.setCoord_lng(c.getFloat(c.getColumnIndex("COORD_LNG")));
		    	 
		    	 edif.setComuna_id(c.getInt(c.getColumnIndex("COMUNA_ID")));
		    	 edif.setRegion_id(c.getInt(c.getColumnIndex("REGION_ID")));
		    	 
		    	 edif.setCalle(c.getString(c.getColumnIndex("CALLE")));
		    	 edif.setNumero(c.getString(c.getColumnIndex("NUMERO")));
		    	 
		    	 edif.setNuevo(c.getInt(c.getColumnIndex("NUEVO"))>0);

		}
		c.close();

		return edif;
	}
	
	public List<Actualizacion>  ObtenerActualizaciones(int region_id)
	{
		List<Actualizacion> misact = new ArrayList<Actualizacion>();
		Actualizacion act;
		String[] campos = new String[] { "ACT_ID AS _id", "ACT_GLOSA"};
		String[] args = new String[] { String.valueOf(region_id) };
		
		Cursor c = mDb.query("ACTUALIZACIONES", campos, "REGION_ID = ?", args, null, null, null);
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
		    	 act = new Actualizacion();
		    	 
		    	 act.setAct_id(c.getInt(0));
		    	 act.setAct_glosa(c.getString(1));
		    	 misact.add(act);
		          
		     } while(c.moveToNext());
		}
		c.close();

		return misact;
	}
	
	public Vivienda ObtenerVivienda(UUID ID_VIVIENDA)
	{
		Vivienda viv = new Vivienda();

		String[] args = new String[] 
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()), ID_VIVIENDA.toString() };
		
		String sql;
		

		sql = 	"SELECT v.ACT_ID, v.ID_USUARIO, v.IDPADRE, v.ID_VIVIENDA, v.TIPOCALLE_ID, tc.TIPOCALLE_GLOSA, v.NOMBRE_CALLE_CAMINO," 
				+ " v.N_DOMICILIO, v.N_LETRA_BLOCK, v.N_PISO,  v.N_LETRA_DEPTO, v.ID_ESE, v.ID_USODESTINO, v.OK, v.NULO, v.FECHAINGRESO, " 
				+ " v.COORD_X, v.COORD_Y, v.DESCRIPCION, v.ID_ORIGEN, v.FECHAMODIFICACION, v.FECHAELIMINACION," 
				+ " v.USUARIO_ID_MODIFICACION, v.USUARIO_ID_ELIMINACION, v.USUARIO_ID_INGRESO, v.ORDEN, v.ORDEN_VIV," 
				+ " v.CODIGO_ID, v.POSICION, v.NUEVA, v.MODIFICADA, ude.GLOSA_USODESTINO,"
				+ " v.KILOMETRO, v.LOTE, v.SITIO, v.LETRAPARCELA,"
				+ " v.VIVTEMP_ID, vt.VIVTEMP_GLOSA,"
				+ " v.ORDENEDIFICACION, v.UBICACIONHORIZONTAL, v.UBICACIONVERTICAL, ID_EDIF, IFNULL(hog.HOGARES,0) AS HOGARES, IFNULL(hog.PERSONAS,0) AS PERSONAS, "
				+ " v.NOMBRELOCALIDAD, v.NOMBREENTIDAD, v.CAT_ID, v.NUMERACION_TERRENO, v.SITIO_SIN_EDIFICACION,  v.MANZANA_ALDEA "
				+ " FROM VIVIENDAS v INNER JOIN TIPOCALLE tc ON v.TIPOCALLE_ID = tc.TIPOCALLE_ID" 
				+ " INNER JOIN USODESTINO ude ON ude.ID_USODESTINO = v.ID_USODESTINO"
				+ " INNER JOIN TIPOVIVIENDATEMPORADA vt ON vt.VIVTEMP_ID = v.VIVTEMP_ID"
				+ " LEFT JOIN (SELECT h.ID_VIVIENDA, MAX(h.ORDENHOGAR) AS HOGARES, SUM(h.NUMPERSONAS) AS PERSONAS FROM HOGARES h GROUP BY h.ID_VIVIENDA) hog "
				+ " ON hog.ID_VIVIENDA = v.ID_VIVIENDA "				
				+ " WHERE v.ACT_ID = ? AND  v.ID_USUARIO = ? AND  v.ID_VIVIENDA = ?" 
				+ " ORDER BY v.ORDEN";
				
		
		Cursor c = mDb.rawQuery(sql, args);
		
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					 new java.util.Date();
		
		    	 
		    	 viv = new Vivienda();
		    	 
		    	 viv.setACT_ID(c.getInt(c.getColumnIndex("ACT_ID")));
		    	 viv.setID_USUARIO(c.getInt(c.getColumnIndex("ID_USUARIO")));
		    	 viv.setIDPADRE(c.getString(c.getColumnIndex("IDPADRE")));
		    	 viv.setID_VIVIENDA(UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA"))));
		    	 viv.setTIPOCALLE_ID(c.getString(c.getColumnIndex("TIPOCALLE_ID")));
		    	 viv.setTIPOCALLE_GLOSA(c.getString(c.getColumnIndex("TIPOCALLE_GLOSA")));
		    	 viv.setNOMBRE_CALLE_CAMINO(c.getString(c.getColumnIndex("NOMBRE_CALLE_CAMINO")));
		    	 viv.setN_DOMICILIO(c.getString(c.getColumnIndex("N_DOMICILIO")));
		    	 viv.setN_LETRA_BLOCK(c.getString(c.getColumnIndex("N_LETRA_BLOCK")));
		    	 viv.setN_PISO(c.getString(c.getColumnIndex("N_PISO")));
		    	 viv.setN_LETRA_DEPTO(c.getString(c.getColumnIndex("N_LETRA_DEPTO")));
		    	 viv.setID_ESE(c.getInt(c.getColumnIndex("ID_ESE")));
		    	 viv.setID_USODESTINO(c.getInt(c.getColumnIndex("ID_USODESTINO")));
		    	 viv.setGLOSA_USODESTINO(c.getString(c.getColumnIndex("GLOSA_USODESTINO")));
		    	 
		    	 viv.setOK(c.getInt(c.getColumnIndex("OK"))>0);
		    	 viv.setNULO(c.getInt(c.getColumnIndex("NULO"))>0);

		    	 viv.setFECHAINGRESO( new Date(c.getLong(c.getColumnIndex("FECHAINGRESO"))));

		    	 viv.setCOORD_X(c.getDouble(c.getColumnIndex("COORD_X")));
		    	 viv.setCOORD_Y(c.getDouble(c.getColumnIndex("COORD_Y")));
		    	 viv.setDESCRIPCION(c.getString(c.getColumnIndex("DESCRIPCION")));
		    	 viv.setID_ORIGEN(c.getInt(c.getColumnIndex("ID_ORIGEN")));
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("FECHAMODIFICACION")))
		    	 {
						viv.setFECHAMODIFICACION(new Date(c.getLong(c.getColumnIndex("FECHAMODIFICACION"))));
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("FECHAELIMINACION")))
		    	 {
						viv.setFECHAELIMINACION(new Date(c.getLong(c.getColumnIndex("FECHAELIMINACION"))));
	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("USUARIO_ID_MODIFICACION")))
		    	 {
			    	 viv.setUSUARIO_ID_MODIFICACION(c.getInt(c.getColumnIndex("USUARIO_ID_MODIFICACION")));	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("USUARIO_ID_ELIMINACION")))
		    	 {
			    	 viv.setUSUARIO_ID_ELIMINACION(c.getInt(c.getColumnIndex("USUARIO_ID_ELIMINACION")));
		    	 }
		    	 
		    	 
		    	 viv.setUSUARIO_ID_INGRESO(c.getInt(c.getColumnIndex("USUARIO_ID_INGRESO")));
		    	 viv.setORDEN(c.getInt(c.getColumnIndex("ORDEN")));
		    	 viv.setORDEN_VIV(c.getInt(c.getColumnIndex("ORDEN_VIV")));
		    	 viv.setCODIGO_ID(c.getInt(c.getColumnIndex("CODIGO_ID")));
		    	 viv.setPOSICION(c.getInt(c.getColumnIndex("POSICION")));
		    	 
		    	 viv.setVIVTEMP_ID(c.getInt(c.getColumnIndex("VIVTEMP_ID")));
		    	 viv.setVIVTEMP_GLOSA(c.getString(c.getColumnIndex("VIVTEMP_GLOSA")));
		    	 
		    	 viv.setKILOMETRO(c.getString(c.getColumnIndex("KILOMETRO")));
		    	 viv.setLOTE(c.getString(c.getColumnIndex("LOTE")));
		    	 viv.setSITIO(c.getString(c.getColumnIndex("SITIO")));
		    	 viv.setLETRAPARCELA(c.getString(c.getColumnIndex("LETRAPARCELA")));
		    	 viv.setORDENEDIFICACION(c.getInt(c.getColumnIndex("ORDENEDIFICACION")));
		    	 viv.setUBICACIONHORIZONTAL(c.getString(c.getColumnIndex("UBICACIONHORIZONTAL")));
		    	 viv.setUBICACIONVERTICAL(c.getString(c.getColumnIndex("UBICACIONVERTICAL")));
		    	 
		    	 viv.setID_EDIF(UUID.fromString(c.getString(c.getColumnIndex("ID_EDIF"))));
		    	 viv.setNUEVA(c.getInt(c.getColumnIndex("NUEVA"))>0);
		    	 viv.setMODIFICADA(c.getInt(c.getColumnIndex("MODIFICADA"))>0);
		    	 
		         viv.setHOGARES(c.getInt(c.getColumnIndex("HOGARES")));
		         viv.setPERSONAS(c.getInt(c.getColumnIndex("PERSONAS")));
		         
		    	 viv.setNOMBRELOCALIDAD(c.getString(c.getColumnIndex("NOMBRELOCALIDAD")));
		    	 viv.setNOMBREENTIDAD(c.getString(c.getColumnIndex("NOMBREENTIDAD")));
		    	 viv.setCAT_ID(c.getInt(c.getColumnIndex("CAT_ID")));
		    	 viv.setNUMERACION_TERRENO(c.getString(c.getColumnIndex("NUMERACION_TERRENO")));
		    	 viv.setSITIO_SIN_EDIFICACION(c.getString(c.getColumnIndex("SITIO_SIN_EDIFICACION")));
		    	 viv.setMANZANA_ALDEA(c.getInt(c.getColumnIndex("MANZANA_ALDEA")));
		    
		}
		c.close();

		return viv;
	}
	
	public List<Vivienda> ObtenerViviendas()
	{
		List<Vivienda> viviendas = new ArrayList<Vivienda>();
		Vivienda viv;

		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()), globales.IDPADRE};
		 
		String sql;
		
		sql = 	"SELECT v.ACT_ID, v.ID_USUARIO, v.IDPADRE, v.ID_VIVIENDA, v.TIPOCALLE_ID, tc.TIPOCALLE_GLOSA, v.NOMBRE_CALLE_CAMINO,"
				+ " v.N_DOMICILIO, v.N_LETRA_BLOCK, v.N_PISO,  v.N_LETRA_DEPTO, v.ID_ESE, v.ID_USODESTINO, v.OK, v.NULO, v.FECHAINGRESO, " 
				+ " v.COORD_X, v.COORD_Y, v.DESCRIPCION, v.ID_ORIGEN, v.FECHAMODIFICACION, v.FECHAELIMINACION," 
				+ " v.USUARIO_ID_MODIFICACION, v.USUARIO_ID_ELIMINACION, v.USUARIO_ID_INGRESO, v.ORDEN, v.ORDEN_VIV," 
				+ " v.CODIGO_ID, v.POSICION, v.NUEVA, v.MODIFICADA, ude.GLOSA_USODESTINO,"
				+ " v.KILOMETRO, v.LOTE, v.SITIO, v.LETRAPARCELA,"
				+ " v.VIVTEMP_ID, vt.VIVTEMP_GLOSA,"
				+ " v.ORDENEDIFICACION, v.UBICACIONHORIZONTAL, v.UBICACIONVERTICAL, IFNULL(edif.ID_EDIF, '00000000-0000-0000-0000-000000000000') AS ID_EDIF,  " 
				+ " IFNULL(hog.HOGARES,0) AS HOGARES, IFNULL(hog.PERSONAS,0) AS PERSONAS, edif.NOMBRE_EDIF, "
				+ " v.NOMBRELOCALIDAD, v.NOMBREENTIDAD, v.CAT_ID, v.NUMERACION_TERRENO, v.SITIO_SIN_EDIFICACION,  v.MANZANA_ALDEA, v.AT_ID "
				+ " FROM VIVIENDAS v INNER JOIN TIPOCALLE tc ON v.TIPOCALLE_ID = tc.TIPOCALLE_ID" 
				+ " INNER JOIN USODESTINO ude ON ude.ID_USODESTINO = v.ID_USODESTINO"
				+ " INNER JOIN TIPOVIVIENDATEMPORADA vt ON vt.VIVTEMP_ID = v.VIVTEMP_ID"
				+ " LEFT JOIN EDIFICIOS edif ON edif.ID_EDIF = v.ID_EDIF "					
				+ " LEFT JOIN (SELECT h.ID_VIVIENDA, MAX(h.ORDENHOGAR) AS HOGARES, SUM(h.NUMPERSONAS) AS PERSONAS FROM HOGARES h GROUP BY h.ID_VIVIENDA) hog " 
				+ " ON hog.ID_VIVIENDA = v.ID_VIVIENDA "		
				+ " WHERE v.ACT_ID = ? AND  v.ID_USUARIO = ? AND  v.IDPADRE = ? " 
				+ " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'"
				+  (globales.boolOcultarNulos ? " AND v.NULO = 0" : "")
				+ (globales.AT_ID > 0? " AND AT_ID = " + String.valueOf(globales.AT_ID) : "")
				+ " ORDER BY v.ORDEN";
				
		
		Cursor c = mDb.rawQuery(sql, args);
		
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
					//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					 //java.util.Date fecha = new java.util.Date();

                 viv = new Vivienda();
		    	 viv.setACT_ID(c.getInt(c.getColumnIndex("ACT_ID")));
		    	 viv.setID_USUARIO(c.getInt(c.getColumnIndex("ID_USUARIO")));
		    	 viv.setIDPADRE(c.getString(c.getColumnIndex("IDPADRE")));
		    	 viv.setID_VIVIENDA(UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA"))));
		    	 viv.setTIPOCALLE_ID(c.getString(c.getColumnIndex("TIPOCALLE_ID")));
		    	 viv.setTIPOCALLE_GLOSA(c.getString(c.getColumnIndex("TIPOCALLE_GLOSA")));
		    	 viv.setNOMBRE_CALLE_CAMINO(c.getString(c.getColumnIndex("NOMBRE_CALLE_CAMINO")));
		    	 viv.setN_DOMICILIO(c.getString(c.getColumnIndex("N_DOMICILIO")));
		    	 viv.setN_LETRA_BLOCK(c.getString(c.getColumnIndex("N_LETRA_BLOCK")));
		    	 viv.setN_PISO(c.getString(c.getColumnIndex("N_PISO")));
		    	 viv.setN_LETRA_DEPTO(c.getString(c.getColumnIndex("N_LETRA_DEPTO")));
		    	 viv.setID_ESE(c.getInt(c.getColumnIndex("ID_ESE")));
		    	 viv.setID_USODESTINO(c.getInt(c.getColumnIndex("ID_USODESTINO")));
		    	 viv.setGLOSA_USODESTINO(c.getString(c.getColumnIndex("GLOSA_USODESTINO")));
		    	 
		    	 viv.setOK(c.getInt(c.getColumnIndex("OK"))>0);
		    	 viv.setNULO(c.getInt(c.getColumnIndex("NULO"))>0);

				 Date fechaing = new Date(c.getLong(c.getColumnIndex("FECHAINGRESO")));
				 
				 //viv.setFECHAINGRESO(sdf.parse(c.getString(c.getColumnIndex("FECHAINGRESO"))));
				 viv.setFECHAINGRESO(fechaing);

		    	 viv.setCOORD_X(c.getDouble(c.getColumnIndex("COORD_X")));
		    	 viv.setCOORD_Y(c.getDouble(c.getColumnIndex("COORD_Y")));
		    	 viv.setDESCRIPCION(c.getString(c.getColumnIndex("DESCRIPCION")));
		    	 viv.setID_ORIGEN(c.getInt(c.getColumnIndex("ID_ORIGEN")));
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("FECHAMODIFICACION")))
		    	 {
		    		 Date fechamod = new Date(c.getLong(c.getColumnIndex("FECHAMODIFICACION")));
					viv.setFECHAMODIFICACION(fechamod);	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("FECHAELIMINACION")))
		    	 {
		    		 Date fechaelim = new Date(c.getLong(c.getColumnIndex("FECHAELIMINACION")));
					viv.setFECHAELIMINACION(fechaelim);	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("USUARIO_ID_MODIFICACION")))
		    	 {
			    	 viv.setUSUARIO_ID_MODIFICACION(c.getInt(c.getColumnIndex("USUARIO_ID_MODIFICACION")));	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("USUARIO_ID_ELIMINACION")))
		    	 {
			    	 viv.setUSUARIO_ID_ELIMINACION(c.getInt(c.getColumnIndex("USUARIO_ID_ELIMINACION")));
		    	 }
		    	 
		    	 
		    	 viv.setUSUARIO_ID_INGRESO(c.getInt(c.getColumnIndex("USUARIO_ID_INGRESO")));
		    	 viv.setORDEN(c.getInt(c.getColumnIndex("ORDEN")));
		    	 viv.setORDEN_VIV(c.getInt(c.getColumnIndex("ORDEN_VIV")));
		    	 viv.setCODIGO_ID(c.getInt(c.getColumnIndex("CODIGO_ID")));
		    	 viv.setPOSICION(c.getInt(c.getColumnIndex("POSICION")));
		    	 
		    	 viv.setVIVTEMP_ID(c.getInt(c.getColumnIndex("VIVTEMP_ID")));
		    	 viv.setVIVTEMP_GLOSA(c.getString(c.getColumnIndex("VIVTEMP_GLOSA")));
		    	 
		    	 viv.setKILOMETRO(c.getString(c.getColumnIndex("KILOMETRO")));
		    	 viv.setLOTE(c.getString(c.getColumnIndex("LOTE")));
		    	 viv.setSITIO(c.getString(c.getColumnIndex("SITIO")));
		    	 viv.setLETRAPARCELA(c.getString(c.getColumnIndex("LETRAPARCELA")));
		    	 viv.setORDENEDIFICACION(c.getInt(c.getColumnIndex("ORDENEDIFICACION")));
		    	 viv.setUBICACIONHORIZONTAL(c.getString(c.getColumnIndex("UBICACIONHORIZONTAL")));
		    	 viv.setUBICACIONVERTICAL(c.getString(c.getColumnIndex("UBICACIONVERTICAL")));
		    	 
		    	 
		    	 viv.setID_EDIF(UUID.fromString(c.getString(c.getColumnIndex("ID_EDIF"))));
		    	 viv.setNOMBRE_EDIF(c.getString(c.getColumnIndex("NOMBRE_EDIF")));
		    	 viv.setNUEVA(c.getInt(c.getColumnIndex("NUEVA"))>0);
		    	 viv.setMODIFICADA(c.getInt(c.getColumnIndex("MODIFICADA"))>0);
		    	 viv.setHOGARES(c.getInt(c.getColumnIndex("HOGARES")));
		    	 viv.setPERSONAS(c.getInt(c.getColumnIndex("PERSONAS")));
		    	 
		    	 /*
		    	  * NOMBRELOCALIDAD text null,"
				+ " NOMBREENTIDAD text null,"	
				+ " CAT_ID numeric null,"				
				+ " NUMERACION_TERRENO text null,"				
				+ " SITIO_SIN_EDIFICACION text null,"				
				+ " MANZANA_ALDEA numeric null,"				
				
		    	  * 
		    	  * */
		    	 viv.setNOMBRELOCALIDAD(c.getString(c.getColumnIndex("NOMBRELOCALIDAD")));
		    	 viv.setNOMBREENTIDAD(c.getString(c.getColumnIndex("NOMBREENTIDAD")));
		    	 viv.setCAT_ID(c.getInt(c.getColumnIndex("CAT_ID")));
		    	 viv.setNUMERACION_TERRENO(c.getString(c.getColumnIndex("NUMERACION_TERRENO")));
		    	 viv.setSITIO_SIN_EDIFICACION(c.getString(c.getColumnIndex("SITIO_SIN_EDIFICACION")));
		    	 viv.setMANZANA_ALDEA(c.getInt(c.getColumnIndex("MANZANA_ALDEA")));
				 viv.setAT_ID(c.getInt(c.getColumnIndex("AT_ID")));
		    	 
		    	 
		    	 viviendas.add(viv);
		          
		     } while(c.moveToNext());
		}
		c.close();

		return viviendas;
	}
	
	
	public List<Vivienda> ObtenerViviendasEdificio(UUID ID_EDIF)
	{
		List<Vivienda> viviendas = new ArrayList<Vivienda>();
		Vivienda viv;

		String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()), globales.IDPADRE, ID_EDIF.toString()};
		 
		String sql;
		

		sql = 	"SELECT v.ACT_ID, v.ID_USUARIO, v.IDPADRE, v.ID_VIVIENDA, v.TIPOCALLE_ID, tc.TIPOCALLE_GLOSA, v.NOMBRE_CALLE_CAMINO," 
				+ " v.N_DOMICILIO, v.N_LETRA_BLOCK, v.N_PISO,  v.N_LETRA_DEPTO, v.ID_ESE, v.ID_USODESTINO, v.OK, v.NULO, v.FECHAINGRESO, " 
				+ " v.COORD_X, v.COORD_Y, v.DESCRIPCION, v.ID_ORIGEN, v.FECHAMODIFICACION, v.FECHAELIMINACION," 
				+ " v.USUARIO_ID_MODIFICACION, v.USUARIO_ID_ELIMINACION, v.USUARIO_ID_INGRESO, v.ORDEN, v.ORDEN_VIV," 
				+ " v.CODIGO_ID, v.POSICION, v.NUEVA, v.MODIFICADA, ude.GLOSA_USODESTINO,"
				+ " v.KILOMETRO, v.LOTE, v.SITIO, v.LETRAPARCELA,"
				+ " v.VIVTEMP_ID, vt.VIVTEMP_GLOSA,"
				+ " v.ORDENEDIFICACION, v.UBICACIONHORIZONTAL, v.UBICACIONVERTICAL, edif.ID_EDIF,  " 
				+ " IFNULL(hog.HOGARES,0) AS HOGARES, IFNULL(hog.PERSONAS,0) AS PERSONAS, edif.NOMBRE_EDIF, "
				+ " v.NOMBRELOCALIDAD, v.NOMBREENTIDAD, v.CAT_ID, v.NUMERACION_TERRENO, v.SITIO_SIN_EDIFICACION,  v.MANZANA_ALDEA " 
				+ " FROM VIVIENDAS v INNER JOIN TIPOCALLE tc ON v.TIPOCALLE_ID = tc.TIPOCALLE_ID" 
				+ " INNER JOIN USODESTINO ude ON ude.ID_USODESTINO = v.ID_USODESTINO"
				+ " INNER JOIN TIPOVIVIENDATEMPORADA vt ON vt.VIVTEMP_ID = v.VIVTEMP_ID"
				+ " LEFT JOIN EDIFICIOS edif ON edif.ID_EDIF = v.ID_EDIF "					
				+ " LEFT JOIN (SELECT h.ID_VIVIENDA, MAX(h.ORDENHOGAR) AS HOGARES, SUM(h.NUMPERSONAS) AS PERSONAS FROM HOGARES h GROUP BY h.ID_VIVIENDA) hog " 
				+ " ON hog.ID_VIVIENDA = v.ID_VIVIENDA "		
				+ " WHERE v.ACT_ID = ? AND  v.ID_USUARIO = ? AND  v.IDPADRE = ? AND v.ID_EDIF = ?" 
				+ (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'")
				+ (globales.boolOcultarNulos ? " AND v.NULO = 0" : "")
				+ " ORDER BY v.ORDEN";
				
		
		Cursor c = mDb.rawQuery(sql, args);
		
		if (c.moveToFirst()) {
		     do {

		    	 viv = new Vivienda();
		    	 
		    	 viv.setACT_ID(c.getInt(c.getColumnIndex("ACT_ID")));
		    	 viv.setID_USUARIO(c.getInt(c.getColumnIndex("ID_USUARIO")));
		    	 viv.setIDPADRE(c.getString(c.getColumnIndex("IDPADRE")));
		    	 viv.setID_VIVIENDA(UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA"))));
		    	 viv.setTIPOCALLE_ID(c.getString(c.getColumnIndex("TIPOCALLE_ID")));
		    	 viv.setTIPOCALLE_GLOSA(c.getString(c.getColumnIndex("TIPOCALLE_GLOSA")));
		    	 viv.setNOMBRE_CALLE_CAMINO(c.getString(c.getColumnIndex("NOMBRE_CALLE_CAMINO")));
		    	 viv.setN_DOMICILIO(c.getString(c.getColumnIndex("N_DOMICILIO")));
		    	 viv.setN_LETRA_BLOCK(c.getString(c.getColumnIndex("N_LETRA_BLOCK")));
		    	 viv.setN_PISO(c.getString(c.getColumnIndex("N_PISO")));
		    	 viv.setN_LETRA_DEPTO(c.getString(c.getColumnIndex("N_LETRA_DEPTO")));
		    	 viv.setID_ESE(c.getInt(c.getColumnIndex("ID_ESE")));
		    	 viv.setID_USODESTINO(c.getInt(c.getColumnIndex("ID_USODESTINO")));
		    	 viv.setGLOSA_USODESTINO(c.getString(c.getColumnIndex("GLOSA_USODESTINO")));
		    	 
		    	 viv.setOK(c.getInt(c.getColumnIndex("OK"))>0);
		    	 viv.setNULO(c.getInt(c.getColumnIndex("NULO"))>0);

				 Date fechaing = new Date(c.getLong(c.getColumnIndex("FECHAINGRESO")));
				 
				viv.setFECHAINGRESO(fechaing);

		    	 viv.setCOORD_X(c.getDouble(c.getColumnIndex("COORD_X")));
		    	 viv.setCOORD_Y(c.getDouble(c.getColumnIndex("COORD_Y")));
		    	 viv.setDESCRIPCION(c.getString(c.getColumnIndex("DESCRIPCION")));
		    	 viv.setID_ORIGEN(c.getInt(c.getColumnIndex("ID_ORIGEN")));
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("FECHAMODIFICACION")))
		    	 {
		    		 Date fechamod = new Date(c.getLong(c.getColumnIndex("FECHAMODIFICACION")));
					viv.setFECHAMODIFICACION(fechamod);	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("FECHAELIMINACION")))
		    	 {
		    		 Date fechaelim = new Date(c.getLong(c.getColumnIndex("FECHAELIMINACION")));
					viv.setFECHAELIMINACION(fechaelim);	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("USUARIO_ID_MODIFICACION")))
		    	 {
			    	 viv.setUSUARIO_ID_MODIFICACION(c.getInt(c.getColumnIndex("USUARIO_ID_MODIFICACION")));	 
		    	 }
		    	 
		    	 if ( !c.isNull(c.getColumnIndex("USUARIO_ID_ELIMINACION")))
		    	 {
			    	 viv.setUSUARIO_ID_ELIMINACION(c.getInt(c.getColumnIndex("USUARIO_ID_ELIMINACION")));
		    	 }
		    	 
		    	 
		    	 viv.setUSUARIO_ID_INGRESO(c.getInt(c.getColumnIndex("USUARIO_ID_INGRESO")));
		    	 viv.setORDEN(c.getInt(c.getColumnIndex("ORDEN")));
		    	 viv.setORDEN_VIV(c.getInt(c.getColumnIndex("ORDEN_VIV")));
		    	 viv.setCODIGO_ID(c.getInt(c.getColumnIndex("CODIGO_ID")));
		    	 viv.setPOSICION(c.getInt(c.getColumnIndex("POSICION")));
		    	 
		    	 viv.setVIVTEMP_ID(c.getInt(c.getColumnIndex("VIVTEMP_ID")));
		    	 viv.setVIVTEMP_GLOSA(c.getString(c.getColumnIndex("VIVTEMP_GLOSA")));
		    	 
		    	 viv.setKILOMETRO(c.getString(c.getColumnIndex("KILOMETRO")));
		    	 viv.setLOTE(c.getString(c.getColumnIndex("LOTE")));
		    	 viv.setSITIO(c.getString(c.getColumnIndex("SITIO")));
		    	 viv.setLETRAPARCELA(c.getString(c.getColumnIndex("LETRAPARCELA")));
		    	 viv.setORDENEDIFICACION(c.getInt(c.getColumnIndex("ORDENEDIFICACION")));
		    	 viv.setUBICACIONHORIZONTAL(c.getString(c.getColumnIndex("UBICACIONHORIZONTAL")));
		    	 viv.setUBICACIONVERTICAL(c.getString(c.getColumnIndex("UBICACIONVERTICAL")));
		    	 
		    	 
		    	 viv.setID_EDIF(UUID.fromString(c.getString(c.getColumnIndex("ID_EDIF"))));
		    	 viv.setNOMBRE_EDIF(c.getString(c.getColumnIndex("NOMBRE_EDIF")));
		    	 viv.setNUEVA(c.getInt(c.getColumnIndex("NUEVA"))>0);
		    	 viv.setMODIFICADA(c.getInt(c.getColumnIndex("MODIFICADA"))>0);
		    	 viv.setHOGARES(c.getInt(c.getColumnIndex("HOGARES")));
		    	 viv.setPERSONAS(c.getInt(c.getColumnIndex("PERSONAS")));
		    	 
		    	 viv.setNOMBRELOCALIDAD(c.getString(c.getColumnIndex("NOMBRELOCALIDAD")));
		    	 viv.setNOMBREENTIDAD(c.getString(c.getColumnIndex("NOMBREENTIDAD")));
		    	 viv.setCAT_ID(c.getInt(c.getColumnIndex("CAT_ID")));
		    	 viv.setNUMERACION_TERRENO(c.getString(c.getColumnIndex("NUMERACION_TERRENO")));
		    	 viv.setSITIO_SIN_EDIFICACION(c.getString(c.getColumnIndex("SITIO_SIN_EDIFICACION")));
		    	 viv.setMANZANA_ALDEA(c.getInt(c.getColumnIndex("MANZANA_ALDEA")));
		    	 
		    	 
		    	 viviendas.add(viv);
		          
		     } while(c.moveToNext());
		}
		c.close();

		return viviendas;
	}

	public int ObtenerTotalViviendasEdificio(UUID ID_EDIF)
	{
		int retorno = 0;


		Cursor c= mDb.rawQuery("SELECT COUNT(*) AS CONTEO FROM VIVIENDAS WHERE ID_EDIF = ? AND NULO = 0"
				, new String[]{ID_EDIF.toString()});

		if(c.moveToFirst())
		{
			retorno = c.getInt(c.getColumnIndex("CONTEO"));
		}

		c.close();
		return retorno;
	}

	public List<Vivienda> ObtenerViviendasID_Todas()
	{
		List<Vivienda> viviendas = new ArrayList<Vivienda>();
		Vivienda viv;

		String[] args = new String[] 
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()) };
		
		String sql;

		sql = 	"SELECT v.ID_VIVIENDA, v.COORD_X, v.COORD_Y, NULO, ID_USODESTINO, ORDEN, ORDEN_VIV, ID_EDIF, DESCRIPCION, OK "
				+ " FROM VIVIENDAS v " 
				+ " WHERE v.ACT_ID = ? AND  v.ID_USUARIO = ?" 
				+  (globales.boolOcultarNulos ? " AND v.NULO = 0" : "")
				+ " AND IDPADRE = '" + globales.IDPADRE + "'"  
				+ (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'")				
				+ " ORDER BY v.ORDEN";
				
		
		Cursor c = mDb.rawQuery(sql, args);
		
		if (c.moveToFirst()) {
		     do {
    	 
		    	 viv = new Vivienda();

		    	 viv.setID_VIVIENDA(UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA"))));
		    	 viv.setCOORD_X(c.getDouble(c.getColumnIndex("COORD_X")));
		    	 viv.setCOORD_Y(c.getDouble(c.getColumnIndex("COORD_Y")));
		    	 viv.setNULO(c.getInt(c.getColumnIndex("NULO"))>0);
		    	 viv.setID_USODESTINO(c.getInt(c.getColumnIndex("ID_USODESTINO")));
		    	 viv.setORDEN_VIV(c.getInt(c.getColumnIndex("ORDEN_VIV")));
		    	 viv.setORDEN(c.getInt(c.getColumnIndex("ORDEN")));
		    	 viv.setID_EDIF(UUID.fromString(c.getString(c.getColumnIndex("ID_EDIF"))));
		    	 viv.setDESCRIPCION(c.getString(c.getColumnIndex("DESCRIPCION")));
				 viv.setOK(c.getInt(c.getColumnIndex("OK"))>0);

		    	 viviendas.add(viv);
		          
		     } while(c.moveToNext());
		}
		c.close();

		return viviendas;
	}
	public boolean InsertarManzana(Manzana mz)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("IDMANZANA",mz.getIdmanzana());   
		values.put("URBANO_ID", mz.getUrbano_id());
		values.put("URBANO_NOMBRE", mz.getUrbano_nombre());
		values.put("COMUNA_ID", mz.getComuna_id());
		values.put("DISTRITO", mz.getDistrito());
		values.put("ZONA", mz.getZona());
		values.put("MANZANA", mz.getManzana());
		values.put("GEOCODIGO", mz.getGeocodigo());
		values.put("CENTROIDE_LAT", mz.getCentroide_lat());
		values.put("CENTROIDE_LNG", mz.getCentroide_lng());
		values.put("ORIGEN", mz.getOrigen());
		values.put("ACT_ID", mz.getAct_id());
		values.put("ID_USUARIO", mz.getId_usuario());	
		retorno = (mDb.insert("MANZANAS", null, values) > 0);

		return retorno;
	}
	
	public boolean InsertarLocalidad(Localidad loc)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("LOC_ID",loc.getLoc_id().toString());
		values.put("LOC_GLOSA", loc.getLoc_glosa());		
		values.put("COMUNA_ID", loc.getComuna_id());
		values.put("DISTRITO", loc.getDistrito());
		values.put("CENTROIDE_LAT", loc.getCentroide_lat());
		values.put("CENTROIDE_LNG", loc.getCentroide_lng());		
		values.put("ACT_ID", loc.getAct_id());
		values.put("ID_USUARIO", loc.getId_usuario());	
		values.put("FECHA_CREACION", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(loc.getFecha_creacion()));
		values.put("NUEVA", loc.getNueva());

		retorno = (mDb.insert("LOCALIDADES", null, values) > 0);

		return retorno;
	}
	
	public List<Manzana> ObtenerManzanas()
	{
		List<Manzana> manzanas = new ArrayList<Manzana>();
		Manzana mz;
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())};
		String sql;
		
		sql = 	"SELECT IDMANZANA, URBANO_NOMBRE, COMUNA_ID, DISTRITO, ZONA, MANZANA, CENTROIDE_LAT, CENTROIDE_LNG, GEOCODIGO FROM MANZANAS WHERE ACT_ID = ? AND ID_USUARIO = ?";
		
		Cursor c = mDb.rawQuery(sql, args);

		while (c.moveToNext()) {
		     mz = new Manzana();
			mz.setId_manzana(c.getInt(c.getColumnIndex("IDMANZANA")));
			
		
			mz.setComuna_id(c.getInt(c.getColumnIndex("COMUNA_ID")));
			mz.setUrbano_nombre(c.getString(c.getColumnIndex("URBANO_NOMBRE")));
			mz.setDistrito(c.getInt(c.getColumnIndex("DISTRITO")));
			mz.setZona(c.getInt(c.getColumnIndex("ZONA")));
			mz.setManzana(c.getInt(c.getColumnIndex("MANZANA")));
			
			mz.setCentroide_lat(c.getDouble(c.getColumnIndex("CENTROIDE_LAT")));
			mz.setCentroide_lng(c.getDouble(c.getColumnIndex("CENTROIDE_LNG")));
			mz.setGeocodigo(c.getLong(c.getColumnIndex("GEOCODIGO")));
			mz.setOrigen(1);
			manzanas.add(mz);
		}
		
		c.close();
		
		/*
		sql = 	"SELECT LOC_ID, COMUNA_ID, COMUNA_NOMBRE, DISTRITO, NOMBRELOCALIDAD, NOMBREENTIDAD, CAT_GLOSA, CENTROIDE_LAT, CENTROIDE_LNG FROM LOCALIDADES WHERE ACT_ID = ? AND ID_USUARIO = ?";
		
		c = mDb.rawQuery(sql, args);

		while (c.moveToNext()) {
			valor = "";
		     //Recorremos el cursor hasta que no haya m?s registros
			padre = new Padre();
			padre.setId(c.getString(c.getColumnIndex("LOC_ID")));
		
			padre.setComuna_id(c.getString(c.getColumnIndex("COMUNA_ID")));
			padre.setComuna_nombre(c.getString(c.getColumnIndex("COMUNA_NOMBRE")));
			padre.setDistrito( c.getString(c.getColumnIndex("DISTRITO")));
			
			padre.setNombrelocalidad(c.getString(c.getColumnIndex("NOMBRELOCALIDAD")));
			padre.setNombreentidad(c.getString(c.getColumnIndex("NOMBREENTIDAD")));
			padre.setNombrecategoria(c.getString(c.getColumnIndex("CAT_GLOSA")));
			//
			
			padre.setCentroide_lat(c.getDouble(c.getColumnIndex("CENTROIDE_LAT")));
			padre.setCentroide_lng(c.getDouble(c.getColumnIndex("CENTROIDE_LNG")));
			padre.setOrigen(9);
			padres.add(padre);
		}
		c.close();
		*/
		return manzanas;

		
		
	}
	
	
	
	public Manzana ObtenerManzana(String IDMANZANA, long GEOCODIGO)
	{
		Manzana mz = null;
		String[] args; 
		
		String sql;
		
		if(null !=IDMANZANA)
		{
			sql = 	"SELECT IDMANZANA, URBANO_NOMBRE, COMUNA_ID, DISTRITO, ZONA, MANZANA, CENTROIDE_LAT, CENTROIDE_LNG, GEOCODIGO FROM MANZANAS WHERE ACT_ID = ? AND ID_USUARIO = ? AND IDMANZANA = ?";
			args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), IDMANZANA};
		}
		else
		{
			sql = 	"SELECT IDMANZANA, URBANO_NOMBRE, COMUNA_ID, DISTRITO, ZONA, MANZANA, CENTROIDE_LAT, CENTROIDE_LNG, GEOCODIGO FROM MANZANAS WHERE ACT_ID = ? AND ID_USUARIO = ? AND GEOCODIGO = ?";
			args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), String.valueOf(GEOCODIGO)};
		}
		
		
		Cursor c = mDb.rawQuery(sql, args);

		if (c.moveToNext()) {
			 mz = new Manzana();
		     mz = new Manzana();
			mz.setId_manzana(c.getInt(c.getColumnIndex("IDMANZANA")));
			
		
			mz.setComuna_id(c.getInt(c.getColumnIndex("COMUNA_ID")));
			mz.setUrbano_nombre(c.getString(c.getColumnIndex("URBANO_NOMBRE")));
			mz.setDistrito(c.getInt(c.getColumnIndex("DISTRITO")));
			mz.setZona(c.getInt(c.getColumnIndex("ZONA")));
			mz.setManzana(c.getInt(c.getColumnIndex("MANZANA")));
			
			mz.setCentroide_lat(c.getDouble(c.getColumnIndex("CENTROIDE_LAT")));
			mz.setCentroide_lng(c.getDouble(c.getColumnIndex("CENTROIDE_LNG")));
			mz.setGeocodigo(c.getLong(c.getColumnIndex("GEOCODIGO")));
			mz.setOrigen(1);
		}
		
		c.close();
		
		return mz;

		
		
	}
	
	
	public List<Seccion> ObtenerSecciones()
	{
		List<Seccion> secciones = new ArrayList<Seccion>();
		Seccion sec;
		String[] args = new String[] { String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()) };
		
		Cursor c = mDb.query("SECCIONES", null, "ACT_ID = ? AND ID_USUARIO = ?", args, null, null, "CU_SECCION");
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
		    	 sec = new Seccion();
		    	 try{
					 sec.setCu_seccion(c.getLong(c.getColumnIndex("CU_SECCION")));
					 sec.setCut(c.getInt(c.getColumnIndex("CUT")));
					 sec.setComuna_glosa(c.getString(c.getColumnIndex("COMUNA_GLOSA")));
					 sec.setCod_distrito(c.getString(c.getColumnIndex("DISTRITO")));
					 sec.setCod_carto(c.getInt(c.getColumnIndex("COD_CARTO")));
					 sec.setEstrato_muestral(c.getInt(c.getColumnIndex("ESTRATO_MUESTRAL")));
					 sec.setEstrato_geo(c.getInt(c.getColumnIndex("ESTRATO_GEO")));
					 sec.setEstrato_ene(c.getInt(c.getColumnIndex("ESTRATO_ENE")));
					 sec.setTipo_est(c.getInt(c.getColumnIndex("TIPO_ESTRATO")));
					 sec.setCodigo_seccion(c.getInt(c.getColumnIndex("CODIGO_SECCION")));
					 sec.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
					 sec.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));


					 secciones.add(sec);

				 }catch(Exception ex){

					 Log.d(TAG, "ObtenerSecciones: ");
				 }

		          
	    	 
		     } while(c.moveToNext());
		}
		c.close();

		return secciones;
	}
	

	
	public Seccion ObtenerSeccion(String CU_SECCION)
	{
		Seccion sec = new Seccion();
		String[] args = new String[] { String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), CU_SECCION };
		
		Cursor c = mDb.query("SECCIONES", null, "ACT_ID = ? AND ID_USUARIO = ? AND CU_SECCION = ?", args, null, null, "CU_SECCION");
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
		    	
		    	 
		    	 sec.setCu_seccion(c.getLong(c.getColumnIndex("CU_SECCION")));
		    	 sec.setCut(c.getInt(c.getColumnIndex("CUT")));
		    	 sec.setComuna_glosa(c.getString(c.getColumnIndex("COMUNA_GLOSA")));
		    	 sec.setCod_distrito(c.getString(c.getColumnIndex("DISTRITO")));
		    	 sec.setCod_carto(c.getInt(c.getColumnIndex("COD_CARTO")));
		    	 sec.setEstrato_muestral(c.getInt(c.getColumnIndex("ESTRATO_MUESTRAL")));
		    	 sec.setEstrato_geo(c.getInt(c.getColumnIndex("ESTRATO_GEO")));
		    	 sec.setEstrato_ene(c.getInt(c.getColumnIndex("ESTRATO_ENE")));
		    	 sec.setTipo_est(c.getInt(c.getColumnIndex("TIPO_ESTRATO")));
		    	 sec.setCodigo_seccion(c.getInt(c.getColumnIndex("CODIGO_SECCION")));
		    	 sec.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
		    	 sec.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
	    	 
		     } while(c.moveToNext());
		}
		c.close();

		return sec;
	}	
	public List<RegistroPosicion>  ObtenerRegistroPosicion(boolean SoloEnviados) throws ParseException
	{
		/*
		 		String TablaRegistroPosicion = "create table if not exists "  
				+ " REGISTRO_POSICION (ACT_ID integer not null, "  
				+ " ID_USUARIO integer not null, " 
				+ " FECHA numeric null,"
				+ " NOMBREUSUARIO text null,"
				+ " COORD_X real not null,"
				+ " COORD_Y real not null,"
				+ " NOMBRE_MAQUINA text not null, "
				+ "PRIMARY KEY (ACT_ID, ID_USUARIO,FECHA))";
		 * */
		List<RegistroPosicion> misreg = new ArrayList<RegistroPosicion>();
		RegistroPosicion reg;
		String[] campos = new String[] { "ACT_ID", "ID_USUARIO", "FECHA", "NOMBREUSUARIO", "COORD_X", "COORD_Y", "NOMBRE_MAQUINA", "ENVIADO"};
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())};
		String whereclause;
		
		if(SoloEnviados)
		{
			whereclause = "ENVIADO = 1 AND ACT_ID = ? AND ID_USUARIO = ?";
				
		}
		else
		{
			whereclause = "ACT_ID = ? AND ID_USUARIO = ?";
		}

		Cursor c = mDb.query("REGISTRO_POSICION", campos, whereclause, args, null, null, "FECHA");
		
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
		    	 reg = new RegistroPosicion();
		    	 
		    	 reg.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
		    	 reg.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
		    	 //reg.setFecha( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(c.getString(c.getColumnIndex("FECHA"))));
		    	 reg.setFecha(new Date(c.getLong(c.getColumnIndex("FECHA"))));
		    	 reg.setNombreusuario(c.getString(c.getColumnIndex("NOMBREUSUARIO")));
		    	 reg.setCoord_x(c.getDouble(c.getColumnIndex("COORD_X")));
		    	 reg.setCoord_y(c.getDouble(c.getColumnIndex("COORD_Y")));
		    	 reg.setNombre_maquina(c.getString(c.getColumnIndex("NOMBRE_MAQUINA")));
		    	 reg.setEnviado(c.getInt(c.getColumnIndex("ENVIADO"))>0);
		    	 misreg.add(reg);
		          
		     } while(c.moveToNext());
		}
		c.close();

		return misreg;
	}
	
	public Cursor ObtenerRegistroPosicionEnvio() throws ParseException
	{
		new ArrayList<RegistroPosicion>();
		String[] campos = new String[] { "ACT_ID", "ID_USUARIO", "FECHA", "NOMBREUSUARIO", "COORD_X", "COORD_Y", "NOMBRE_MAQUINA", "ENVIADO"};
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())};
		String whereclause;
		

		whereclause = "ENVIADO = 0 AND ACT_ID = ? AND ID_USUARIO = ?";

		Cursor c = mDb.query("REGISTRO_POSICION", campos, whereclause, args, null, null, "FECHA");

		return c;
	}
	
	public boolean ActualizarEnviadasRegistroPosicion()
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ENVIADO",1);
		

		retorno = (mDb.update("REGISTRO_POSICION", values, null, null) > 0);

		return retorno;
	}
	
	public boolean ActualizarEdificiosEnviados()
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("NUEVO",0);
		

		retorno = (mDb.update("EDIFICIOS", values, null, null) > 0);

		return retorno;
	}	
	public boolean InsertarTiposCalle(TipoCalle tc)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("TIPOCALLE_ID",tc.getTipocalle_id());
		values.put("TIPOCALLE_GLOSA", tc.getTipocalle_glosa());

		retorno = (mDb.insert("TIPOCALLE", null, values) > 0);

		return retorno;
	}
	

	
	
	public List<TipoCalle> ObtenerTiposCalle()
	{

		Cursor c = mDb.query("TIPOCALLE", new String[]{ "TIPOCALLE_ID", "TIPOCALLE_GLOSA"},null, null, null, null, "TIPOCALLE_ID");
		List<TipoCalle> tipos = new ArrayList<TipoCalle>();
		TipoCalle tc;

		if (c.moveToFirst()) {
			do
			{
				tc = new TipoCalle();
				tc.setTipocalle_id(c.getString(c.getColumnIndex("TIPOCALLE_ID")));
				tc.setTipocalle_glosa(c.getString(c.getColumnIndex("TIPOCALLE_GLOSA")));
				tipos.add(tc);
				
			}
			while(c.moveToNext());
		
		}
		c.close();
			
		return tipos;
	}
	
	
	public boolean InsertarUsosDestino(UsoDestino uso)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ID_USODESTINO",uso.getId_usodestino());
		values.put("GLOSA_USODESTINO", uso.getGlosa_usodestino());

		retorno = (mDb.insert("USODESTINO", null, values) > 0);

		return retorno;
	}
	
	public List<UsoDestino> ObtenerUsosDestino()
	{

		Cursor c = mDb.query("USODESTINO", new String[]{ "ID_USODESTINO", "GLOSA_USODESTINO"},null, null, null, null, "ID_USODESTINO");
		List<UsoDestino> usos = new ArrayList<UsoDestino>();
		UsoDestino uso;

		if (c.moveToFirst()) {
			do
			{
				uso = new UsoDestino();
				uso.setId_usodestino(c.getInt(c.getColumnIndex("ID_USODESTINO")));
				uso.setGlosa_usodestino(c.getString(c.getColumnIndex("GLOSA_USODESTINO")));
				usos.add(uso);
				
			}
			while(c.moveToNext());
		}
			
		c.close();
		return usos;
	}
	
	
	public boolean InsertarCategoria(Categoria cat)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("CAT_ID",cat.getCat_id());
		values.put("CAT_GLOSA", cat.getCat_glosa());

		retorno = (mDb.insert("CATEGORIA", null, values) > 0);

		return retorno;
	}
	

	
	
	public List<Categoria> ObtenerCategoria()
	{

		Cursor c = mDb.query("CATEGORIA", new String[]{ "CAT_ID", "CAT_GLOSA"},null, null, null, null, "CAT_ID");
		List<Categoria> cats = new ArrayList<Categoria>();
		Categoria cat;

		if (c.moveToFirst()) {
			do
			{
				cat = new Categoria();
				cat.setCat_id(c.getInt(c.getColumnIndex("CAT_ID")));
				cat.setCat_glosa(c.getString(c.getColumnIndex("CAT_GLOSA")));
				cats.add(cat);
				
			}
			while(c.moveToNext());
		}
		c.close();
			
		return cats;
	}

	
	public boolean InsertarTipoViviendaTemporada(TipoViviendaTemporada tipo)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("VIVTEMP_ID",tipo.getVivtemp_id());
		values.put("VIVTEMP_GLOSA", tipo.getVivtemp_glosa());

		retorno = (mDb.insert("TIPOVIVIENDATEMPORADA", null, values) > 0);

		return retorno;
	}
	

	
	
	public List<TipoViviendaTemporada> ObtenerTiposViviendaTemporada()
	{

		Cursor c = mDb.query("TIPOVIVIENDATEMPORADA", new String[]{ "VIVTEMP_ID", "VIVTEMP_GLOSA"},null, null, null, null, "VIVTEMP_ID");
		List<TipoViviendaTemporada> tipos = new ArrayList<TipoViviendaTemporada>();
		TipoViviendaTemporada tipo;

		
		if (c.moveToFirst()) {
			do
			{
				tipo = new TipoViviendaTemporada();
				tipo.setVivtemp_id(c.getInt(c.getColumnIndex("VIVTEMP_ID")));
				tipo.setVivtemp_glosa(c.getString(c.getColumnIndex("VIVTEMP_GLOSA")));
				tipos.add(tipo);
				
			}
			while(c.moveToNext());
		}
		c.close();
			
		return tipos;
	}
	
	
	public boolean InsertarCodigosAnotacion(CodigoAnotacion cod)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("CODIGO_ID",cod.getCodigo_id());
		values.put("CODIGO_GLOSA", cod.getCodigo_glosa());

		retorno = (mDb.insert("CODIGO_ANOTACIONES", null, values) > 0);

		return retorno;
	}
	

	
	
	public List<CodigoAnotacion> ObtenerCodigosAnotacion()
	{

		Cursor c = mDb.query("CODIGO_ANOTACIONES", new String[]{ "CODIGO_ID", "CODIGO_GLOSA"},null, null, null, null, "CODIGO_ID");
		List<CodigoAnotacion> codigos = new ArrayList<CodigoAnotacion>();
		CodigoAnotacion codigo;

		if (c.moveToFirst()) {
			do
			{
				codigo = new CodigoAnotacion();
				codigo.setCodigo_id(c.getInt(c.getColumnIndex("CODIGO_ID")));
				codigo.setCodigo_glosa(c.getString(c.getColumnIndex("CODIGO_GLOSA")));
				codigos.add(codigo);
				
			}
			while(c.moveToNext());
		}
		c.close();
			
		return codigos;
	}
	
	public boolean InsertarParametro(Parametro param)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("KEY",param.getKey());
		values.put("VALOR", param.getValor());

		retorno = (mDb.insert("PARAMETROS", null, values) > 0);

		return retorno;
	}
	

	
	
	public Parametro ObtenerParametro(String Key)
	{

		String[] args = new String[] {Key};

		Cursor c = mDb.query("PARAMETROS", new String[]{ "[KEY]", "VALOR"}, "[KEY]= ?", args, null, null, null);
		Parametro param = new Parametro();

		if(c.moveToNext());
		{
			param.setKey(c.getString(c.getColumnIndex("KEY")));
			param.setValor(c.getString(c.getColumnIndex("VALOR")));
			
		}
		
		c.close();
			
		return param;
	}
	
	
	public List<Parametro> ObtenerParametrosMapa(String Key, String CampoOrdenamiento)
	{

		Cursor c = mDb.rawQuery("SELECT * FROM PARAMETROS WHERE [KEY] LIKE '" + Key + "%' ORDER BY VALOR", null);
		Parametro param;
		List<Parametro> params = new ArrayList<Parametro>();

		if (c.moveToFirst()) {
			
			do
			{
				param = new Parametro();
				param.setKey(c.getString(c.getColumnIndex("KEY")));
				param.setValor(c.getString(c.getColumnIndex("VALOR")));
				
				params.add(param);
				
			}
			while(c.moveToNext());
		}
		
		c.close();
			
		return params;
	}
	
	public boolean InsertarHogar(Hogar hog)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ACT_ID",hog.getAct_id());
		values.put("ID_USUARIO",hog.getId_usuario());
		values.put("ID_VIVIENDA",hog.getId_vivienda().toString());
		values.put("ORDENHOGAR",hog.getOrdenhogar());
		values.put("NUMPERSONAS",hog.getNumpersonas());
		values.put("JEFE",hog.getJefe());
		values.put("ENVIADO", hog.isEnviado());
		

		retorno = (mDb.insert("HOGARES", null, values) > 0);

		return retorno;
	}
	

	
	
	public List<Hogar> ObtenerHogares(UUID ID_VIVIENDA)
	{
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), ID_VIVIENDA.toString()};
		
		Cursor c = mDb.query("HOGARES", new String[]{ "ACT_ID", "ID_USUARIO", "ID_VIVIENDA", "ORDENHOGAR", "JEFE", "NUMPERSONAS"},"ACT_ID = ? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args, null, null, "ORDENHOGAR");
		//Cursor c = mDb.query("HOGARES", new String[]{ "ACT_ID", "ID_USUARIO", "ID_VIVIENDA", "ORDENHOGAR", "NUMPERSONAS"},"ACT_ID = ? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args, null, null, "ORDENHOGAR");
		List<Hogar> hogares = new ArrayList<Hogar>();
		Hogar hogar;

		if (c.moveToFirst()) {
			do
			{
				hogar = new Hogar();
				hogar.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
				hogar.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
				hogar.setId_vivienda(UUID.fromString(c.getString(c.getColumnIndex("ID_VIVIENDA"))));
				hogar.setOrdenhogar(c.getInt(c.getColumnIndex("ORDENHOGAR")));
				hogar.setNumpersonas(c.getInt(c.getColumnIndex("NUMPERSONAS")));
				hogar.setJefe(c.getString(c.getColumnIndex("JEFE")));

				hogares.add(hogar);
				
			}
			while(c.moveToNext());
		}
		c.close();
			
		return hogares;
	}
	
	
	public Cursor ObtenerViviendas(boolean SOLONuevasModificadas)
	{
		new ArrayList<Vivienda>();
		String[] campos = new String[] 
				{  "ACT_ID, ID_USUARIO, IDPADRE, ID_VIVIENDA, TIPOCALLE_ID, NOMBRE_CALLE_CAMINO, N_DOMICILIO, N_LETRA_BLOCK, N_PISO, " 
				 + " N_LETRA_DEPTO, ID_ESE, ID_USODESTINO, OK, NULO, FECHAINGRESO, COORD_X, COORD_Y, DESCRIPCION, ID_ORIGEN, FECHAMODIFICACION,"
				 + " FECHAELIMINACION, USUARIO_ID_MODIFICACION, USUARIO_ID_ELIMINACION, USUARIO_ID_INGRESO, ORDEN, ORDEN_VIV, " 
				 + " CODIGO_ID, POSICION, KILOMETRO, LOTE, SITIO, LETRAPARCELA, VIVTEMP_ID, ORDENEDIFICACION," 
				 + " UBICACIONHORIZONTAL, UBICACIONVERTICAL, ID_EDIF, NOMBRELOCALIDAD, NOMBREENTIDAD, CAT_ID "};

		String[] args = new String[] 
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()) };
		
		String whereclause;
		
		if(SOLONuevasModificadas)
		{
			whereclause = "ACT_ID=? AND ID_USUARIO =? AND (NUEVA = 1 OR MODIFICADA = 1) ";
		}
		else
		{
			whereclause = "ACT_ID=? AND ID_USUARIO = ?";
		}
		
		Cursor c = mDb.query("VIVIENDAS", campos, whereclause, args, null, null, "ORDEN");
		 
		
		return c;
	}
	
	public Cursor ObtenerHogaresParaEnvio()
	{
		String[] campos = new String[] 
				{  "ACT_ID, ID_USUARIO, ID_VIVIENDA, NUMPERSONAS, JEFE, ORDENHOGAR"};

		String[] args = new String[] 
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()) };

		Cursor c = mDb.rawQuery(" SELECT ACT_ID, ID_USUARIO, ID_VIVIENDA, NUMPERSONAS, JEFE, ORDENHOGAR " +
					 " FROM HOGARES " +
					 " WHERE ID_VIVIENDA IN (" +
					 " SELECT ID_VIVIENDA FROM VIVIENDAS WHERE ACT_ID=? AND ID_USUARIO = ? " +
					 "AND (NUEVA = 1 OR MODIFICADA = 1)) "
				
				, args);
		
		return c;
	}	
	
	public Cursor ObtenerHogaresViviendaParaEnvio(String ID_VIVIENDA)
	{
		String[] campos = new String[] 
				{  "ACT_ID, ID_USUARIO, ID_VIVIENDA, NUMPERSONAS, JEFE, ORDENHOGAR"};

		String[] args = new String[] 
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()), ID_VIVIENDA.toString() };

		
		Cursor c = mDb.rawQuery(" SELECT ACT_ID, ID_USUARIO, ID_VIVIENDA, ORDENHOGAR, JEFE, NUMPERSONAS " +
					 " FROM HOGARES " +
					 " WHERE ID_VIVIENDA IN (" +
					 " SELECT ID_VIVIENDA FROM VIVIENDAS WHERE ACT_ID=? AND ID_USUARIO = ? " +
					 " AND (NUEVA = 1 OR MODIFICADA = 1)) " + 
					 " AND ID_VIVIENDA = ?"

				, args);
		
		return c;
	}		
	public boolean ActualizarViviendasEnviadas()
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("NUEVA",0);
		values.put("MODIFICADA",0);

		String[] args = new String[]
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};

		retorno = (mDb.update("VIVIENDAS", values, "ACT_ID=? AND ID_USUARIO = ? ", args) > 0);

		return retorno;
	}

    public boolean ActualizaHogaresEnviados()
    {
        boolean retorno = false;
        ContentValues values;

        values = new ContentValues();
        values.put("ENVIADO",1);

        String[] args = new String[]
                { Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario())};

        retorno = (mDb.update("HOGARES", values, "ACT_ID=? AND ID_USUARIO = ? ", args) > 0);

        return retorno;
    }
	
	public boolean MarcarViviendaComoActualizada(UUID ID_VIVIENDA)
	{
		boolean retorno = false;
		ContentValues values;
		
		String[] args = new String[] 
				{ 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()), 
				ID_VIVIENDA.toString() 
				};

		values = new ContentValues();  
		values.put("ID_VIVIENDA", ID_VIVIENDA.toString());
		values.put("FECHAMODIFICACION", new Date().getTime());
		values.put("MODIFICADA", true);
		retorno = (mDb.update("VIVIENDAS",values, "ACT_ID=? AND ID_USUARIO = ? and ID_VIVIENDA = ?", args) >0);

		return retorno;
	}
	
	public boolean MarcarViviendaComoEnviada(UUID ID_VIVIENDA)
	{
		boolean retorno = false;
		ContentValues values;
		
		String[] args = new String[] 
				{ 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()), 
				ID_VIVIENDA.toString() 
				};

		values = new ContentValues();  
		values.put("ID_VIVIENDA", ID_VIVIENDA.toString());
		values.put("MODIFICADA", false);
		values.put("NUEVA", false);
		retorno = (mDb.update("VIVIENDAS",values, "ACT_ID=? AND ID_USUARIO = ? and ID_VIVIENDA = ?", args) >0);

		return retorno;
	}
	
	public int ObtenerCorrelativoVivienda(String IDPADRE)
	{
		
		
		Cursor mCount= mDb.rawQuery("select count(*) from VIVIENDAS where IDPADRE='" + IDPADRE + "'"+ (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'"), null);
		mCount.moveToFirst();
		int count= mCount.getInt(0);
		mCount.close();
		return count;
	}
	
	public int ObtenerOrdenViviendaMaximo(String IDPADRE)
	{
		
		
		Cursor mCount= mDb.rawQuery("select MAX(ORDEN_VIV) from VIVIENDAS where IDPADRE='" + IDPADRE + "'" + (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'"), null);
		mCount.moveToFirst();
		int count= mCount.getInt(0);
		mCount.close();
		return count;
	}
	
	public int ObtenerMaxOrdenEdificio(String IDPADRE)
	{
		
		
		Cursor mCount= mDb.rawQuery("select MAX(ORDENEDIFICACION) from VIVIENDAS where IDPADRE='" + IDPADRE + "'" + (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'"), null);
		mCount.moveToFirst();
		int count= mCount.getInt(0);
		mCount.close();
		return count;
	}
	
	public int ObtenerTotalRegistrosUPM(String IDPADRE)
	{
		
		
		Cursor mCount= mDb.rawQuery("select Count(ID_VIVIENDA) from VIVIENDAS where IDPADRE='" + IDPADRE + "'" + (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'"), null);
		mCount.moveToFirst();
		int count= mCount.getInt(0);
		mCount.close();
		return count;
	}
	
	public List<String> ObtenerSugerenciasCalle(String IDPADRE)
	{
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), IDPADRE};
		
		Cursor c = mDb.query(true, "VIVIENDAS", new String[]{ "NOMBRE_CALLE_CAMINO"},"ACT_ID = ? AND ID_USUARIO = ? AND IDPADRE = ?", args, null, null, "NOMBRE_CALLE_CAMINO", null);
		
		List<String> calles = new ArrayList<String>();
		if (c.moveToFirst()) {
			do
			{
				calles.add(c.getString(c.getColumnIndex("NOMBRE_CALLE_CAMINO")).trim());
			}
			while(c.moveToNext());
		}
		c.close();
			
		return calles;
	}
	
	
	public List<String> ObtenerEntidadesPadre(String IDPADRE)
	{
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), IDPADRE};
		
		Cursor c = mDb.query(true, "VIVIENDAS", new String[]{ "NOMBREENTIDAD"},"ACT_ID = ? AND ID_USUARIO = ? AND IDPADRE = ?", args, null, null, "NOMBREENTIDAD", null);
		String entidad;
		List<String> entidades = new ArrayList<String>();
		if (c.moveToFirst()) {
			
			do
			{
				entidad = c.getString(c.getColumnIndex("NOMBREENTIDAD")).trim();
				if(!entidades.contains(entidad) && !entidad.equals(""))
				{
					entidades.add(entidad.toUpperCase());	
				}
				
			}
			while(c.moveToNext());
		}
		c.close();
			
		return entidades;
	}
	
	public boolean AnularViviendas(UUID ID_VIVIENDA, boolean Anular)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("NULO",Anular);
		values.put("MODIFICADA",true);
		
		if(Anular)
		{
			values.put("FECHAELIMINACION",new Date().getTime());	
		}
		else
		{
			values.putNull("FECHAELIMINACION");
		}
		

		retorno = (mDb.update("VIVIENDAS", values, "ID_VIVIENDA= ?", new String[]{ID_VIVIENDA.toString()}) > 0);

		return retorno;
	}
	
	public boolean ReordenarVivienda(Vivienda viv)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ORDEN",viv.getORDEN());
		values.put("ORDEN_VIV",viv.getORDEN_VIV());
		values.put("MODIFICADA",true);
		values.put("FECHAMODIFICACION",new Date().getTime());

		retorno = (mDb.update("VIVIENDAS", values, "ID_VIVIENDA= ?", new String[]{viv.getID_VIVIENDA().toString()}) > 0);

		return retorno;
	}
	
	
	public boolean ReordenarPuntoViviendaConDescripcion(Vivienda viv)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ORDEN",viv.getORDEN());
		values.put("DESCRIPCION",viv.getDESCRIPCION());
		values.put("MODIFICADA",true);
		values.put("FECHAMODIFICACION",new Date().getTime());

		retorno = (mDb.update("VIVIENDAS", values, "ID_VIVIENDA= ?", new String[]{viv.getID_VIVIENDA().toString()}) > 0);

		return retorno;
	}	


	
	public boolean InsertarSeccion(Seccion sec)
	{
		boolean retorno = false;
		ContentValues values;
	
		values = new ContentValues();  
		values.put("CU_SECCION",sec.getCu_seccion());   
		values.put("CUT", sec.getCut());
		values.put("COMUNA_GLOSA", sec.getComuna_glosa());
		values.put("DISTRITO", sec.getCod_distrito());
		values.put("COD_CARTO", sec.getCod_carto());
		values.put("ESTRATO_MUESTRAL", sec.getEstrato_muestral());
		values.put("ESTRATO_GEO", sec.getEstrato_geo());
		values.put("ESTRATO_ENE", sec.getEstrato_ene());
		values.put("TIPO_ESTRATO", sec.getTipo_est());
		values.put("CODIGO_SECCION", sec.getCodigo_seccion());
		values.put("ACT_ID", globales.actualizacion.getAct_id());
		values.put("ID_USUARIO", globales.usuario.getId_usuario());
		retorno = (mDb.insert("SECCIONES", null, values) > 0);
	
		return retorno;
		
	}
	public boolean GeorreferenciarVivienda(UUID ID_VIVIENDA, Point pt)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("COORD_Y",pt.getY());
		values.put("COORD_X",pt.getX());
		values.put("MODIFICADA",true);
		values.put("FECHAMODIFICACION",new Date().getTime());

		retorno = (mDb.update("VIVIENDAS", values, "ID_VIVIENDA= ?", new String[]{ID_VIVIENDA.toString()}) > 0);

		return retorno;
	}

	public List<RegistroPosicionDiario> ObtenerDiasRegistroPosicion() throws ParseException
	{
		List<RegistroPosicionDiario> registros_diarios = new ArrayList<RegistroPosicionDiario>();
		
		RegistroPosicionDiario regdia;
		String[] args = new String[] 
				{ Integer.toString(globales.actualizacion.getAct_id()), Integer.toString(globales.usuario.getId_usuario()) };

		/*
		String SQL ="select strftime('%Y-%m %d %H:%M:%S', fecha/1000, 'unixepoch', 'localtime') as DIA,   " +
					"COUNT(*) AS TOTAL_REGISTROS, " + 
					"MIN(strftime('%Y-%m %d %H:%M:%S', fecha/1000, 'unixepoch', 'localtime')) AS MIN_FECHA, " + 
					"MAX(strftime('%Y-%m %d %H:%M:%S', fecha/1000, 'unixepoch', 'localtime')) AS MAX_FECHA " + 
					"FROM REGISTRO_POSICION " + 
					"WHERE ACT_ID = ? AND ID_USUARIO = ? " +
					"GROUP BY strftime('%Y-%m %d %H:%M:%S', fecha/1000, 'unixepoch', 'localtime')  " + 
					"ORDER BY DIA" ;
		*/
		String SQL ="select strftime('%d/%m/%Y', fecha/1000, 'unixepoch', 'localtime') as DIA,   " +
				"COUNT(*) AS TOTAL_REGISTROS, " + 
				"MIN(fecha) AS MIN_FECHA, " + 
				"MAX(fecha) AS MAX_FECHA " + 
				"FROM REGISTRO_POSICION " + 
				"WHERE ACT_ID = ? AND ID_USUARIO = ? " +
				"GROUP BY strftime('%d/%m/%Y', fecha/1000, 'unixepoch', 'localtime')  " + 
				"ORDER BY DIA" ;		
		Cursor c = mDb.rawQuery(SQL, args);
		
		if (c.moveToFirst()) {
			do
			{
				regdia = new RegistroPosicionDiario();
				regdia.setDia(c.getString(c.getColumnIndex("DIA")));
				regdia.setTotal_registros(c.getInt(c.getColumnIndex("TOTAL_REGISTROS")));
				regdia.setMindate(new Date(c.getLong(c.getColumnIndex("MIN_FECHA"))));
				regdia.setMaxdate(new Date(c.getLong(c.getColumnIndex("MAX_FECHA"))));
				registros_diarios.add(regdia);
			}

			while(c.moveToNext());
		}
		c.close();

		return registros_diarios;
	}

	
	public List<RegistroPosicion> ObtenerRegistrosPosicionPorDia(String dia) throws ParseException
	{

		List<RegistroPosicion> misreg = new ArrayList<RegistroPosicion>();
		RegistroPosicion reg;
		
		String[] campos = new String[] { "ACT_ID", "ID_USUARIO", "FECHA", "NOMBREUSUARIO", "COORD_X", "COORD_Y", "NOMBRE_MAQUINA", "ENVIADO"};
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), dia};
		
		String whereclause;

		whereclause = " ACT_ID = ? AND ID_USUARIO = ? AND strftime('%d/%m/%Y', fecha/1000, 'unixepoch', 'localtime')  = ? ";

		Cursor c = mDb.query("REGISTRO_POSICION", campos, whereclause, args, null, null, "FECHA");
		
		 
		//Nos aseguramos de que existe al menos un registro
		if (c.moveToFirst()) {
		     //Recorremos el cursor hasta que no haya m?s registros
		     do {
		    	 reg = new RegistroPosicion();
		    	 
		    	 reg.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
		    	 reg.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
		    	 //reg.setFecha(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(c.getString(c.getColumnIndex("FECHA"))));
		    	 reg.setFecha(new Date(c.getLong(c.getColumnIndex("FECHA"))));
		    	 
		    	 reg.setNombreusuario(c.getString(c.getColumnIndex("NOMBREUSUARIO")));
		    	 reg.setCoord_x(c.getDouble(c.getColumnIndex("COORD_X")));
		    	 reg.setCoord_y(c.getDouble(c.getColumnIndex("COORD_Y")));
		    	 reg.setNombre_maquina(c.getString(c.getColumnIndex("NOMBRE_MAQUINA")));
		    	 reg.setEnviado(c.getInt(c.getColumnIndex("ENVIADO"))>0);
		    	 misreg.add(reg);
		          
		     } while(c.moveToNext());
		}
		c.close();
		
		return misreg;
	}
	
	public boolean ActualizarOKVivienda(UUID ID_VIVIENDA, boolean OK)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("OK",OK);
		values.put("MODIFICADA",true);
		values.put("FECHAMODIFICACION",new Date().getTime());

		retorno = (mDb.update("VIVIENDAS", values, "ID_VIVIENDA= ?", new String[]{ID_VIVIENDA.toString()}) > 0);

		return retorno;
	}
	
	public int ObtenerTotalOKPadre(String IDPADRE, boolean OK)
	{
		int retorno = 0;
 

		Cursor c= mDb.rawQuery("SELECT COUNT(*) AS CONTEO FROM VIVIENDAS WHERE OK = ? AND IDPADRE = ? AND ACT_ID = ? AND ID_USUARIO = ?"
				+ (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'")
				, new String[]{OK? "1": "0", IDPADRE, String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())});
		
		if(c.moveToFirst())
		{
			retorno = c.getInt(c.getColumnIndex("CONTEO"));
		}

		return retorno;
	}
	
	public int ObtenerTotalRegistrosPadre(String IDPADRE)
	{
		int retorno = 0;

		Cursor c= mDb.rawQuery("SELECT COUNT(*) AS CONTEO FROM VIVIENDAS WHERE IDPADRE = ? AND ACT_ID = ? AND ID_USUARIO = ?"
				+ (globales.LOCALIDAD.equals("") ? "" : " AND NOMBRELOCALIDAD = '" + globales.LOCALIDAD + "'")
				, new String[]{IDPADRE, String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())});
		
		if(c.moveToFirst())
		{
			retorno = c.getInt(c.getColumnIndex("CONTEO"));
		}

		c.close();

		return retorno;
	}

    public int ObtenerTotalSoloPadre(String IDPADRE)
    {
        int retorno = 0;

        Cursor c= mDb.rawQuery("SELECT COUNT(*) AS CONTEO FROM VIVIENDAS WHERE IDPADRE = ? AND ACT_ID = ? AND ID_USUARIO = ?"
                , new String[]{IDPADRE, String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())});

        if(c.moveToFirst())
        {
            retorno = c.getInt(c.getColumnIndex("CONTEO"));
        }

        c.close();

        return retorno;
    }
	
	public boolean ExistenModificadas()
	{
		int retorno = 0;
 

		Cursor c= mDb.rawQuery("SELECT COUNT(*) AS CONTEO FROM VIVIENDAS WHERE (NUEVA = 1 OR MODIFICADA = 1) AND ACT_ID = ? AND ID_USUARIO = ?", new String[]{String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())});
		
		if(c.moveToFirst())
		{
			retorno = c.getInt(c.getColumnIndex("CONTEO"));
		}

		return retorno >0;
	}
	
	public int ObtenerMaxDibujo()
	{

		Cursor mCount= mDb.rawQuery("SELECT COUNT(*) AS CONTEO FROM DIBUJOS  WHERE ACT_ID = ? AND ID_USUARIO = ?", new String[]{String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())});
		mCount.moveToFirst();
		int count= mCount.getInt(0);
		mCount.close();
		return count;
		
	}
	public boolean InsertarDibujo(Dibujo dib)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("ID_DIBUJO",dib.getID_DIBUJO().toString());   
		values.put("DIB_TEXT", dib.getDIB_TEXT());
		values.put("ACT_ID", dib.getACT_ID());
		values.put("ID_USUARIO", dib.getID_USUARIO());
		values.put("IDPADRE", dib.getIDPADRE());
		values.put("DIB_GLOSA", dib.getDIB_GLOSA());
		values.put("NULO", dib.isNULO());
		values.put("FECHA_CREACION", dib.getFECHA_CREACION().getTime());
		values.put("DIB_TIPO", dib.getDIB_TIPO());
		values.put("DIB_ESTADO", dib.getDIB_ESTADO());
		values.put("NUEVO", dib.isNUEVO());

		retorno = (mDb.insert("DIBUJOS", null, values) > 0);

		return retorno;
	}
	
	public boolean ModificarDibujo(Dibujo dib)
	{

		boolean retorno;
		ContentValues values;

		values = new ContentValues();  
		values.put("ID_DIBUJO",dib.getID_DIBUJO().toString());   
		values.put("DIB_TEXT", dib.getDIB_TEXT());
		values.put("ACT_ID", dib.getACT_ID());
		values.put("ID_USUARIO", dib.getID_USUARIO());
		values.put("IDPADRE", dib.getIDPADRE());
		values.put("DIB_GLOSA", dib.getDIB_GLOSA());
		values.put("DIB_TIPO", dib.getDIB_TIPO());
		values.put("DIB_ESTADO", dib.getDIB_ESTADO());
		values.put("NULO", dib.isNULO());
		retorno = (mDb.update("DIBUJOS", values, "ID_DIBUJO = ?", new String[] {dib.getID_DIBUJO().toString()}) >0); //(table, values, whereClause, whereArgs)> 0);

		return retorno;
	}
	
	public boolean BorrarDibujo(UUID ID_DIBUJO)
	{

		boolean retorno;
		ContentValues values;

		values = new ContentValues();  
		values.put("ID_DIBUJO",ID_DIBUJO.toString());   
		values.put("NULO", true);
		retorno = (mDb.update("DIBUJOS",values, "ID_DIBUJO = ?", new String[] {ID_DIBUJO.toString()}) >0); //(table, values, whereClause, whereArgs)> 0);

		return retorno;
	}
	
	
	public boolean BorrarDibujosAntiguos()
	{

		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())};
		boolean retorno;

		retorno = (mDb.delete("DIBUJOS","ACT_ID=? AND ID_USUARIO = ? AND NUEVO = 0",args) >0); //(table, values, whereClause, whereArgs)> 0);

		return retorno;
	}
	/*
	public boolean BorrarDibujosNulos()
	{

		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario())};
		boolean retorno;

		retorno = (mDb.delete("DIBUJOS","ACT_ID=? AND ID_USUARIO = ? AND NULO = 1",args) >0); //(table, values, whereClause, whereArgs)> 0);

		return retorno;
	}
	*/
	public boolean ActualizarDibujosEnviados()
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();  
		values.put("NUEVO",0);

		retorno = (mDb.update("DIBUJOS", values, null, null) > 0);

		return retorno;
	}
	
	public Dibujo ObtenerDibujo(UUID ID_DIBUJO)
	{


		
		String[] args = new String[] { 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()),
				ID_DIBUJO.toString()
				};


		Cursor c = mDb.query("DIBUJOS",null,"ACT_ID=? AND ID_USUARIO = ? AND ID_DIBUJO = ?", args, null, null, null);	

		Dibujo dib = null;


		if (c.moveToFirst()) {
				
				dib = new Dibujo();
				dib.setID_DIBUJO(UUID.fromString(c.getString(c.getColumnIndex("ID_DIBUJO"))));
				dib.setDIB_TEXT(c.getString(c.getColumnIndex("DIB_TEXT")));
				dib.setACT_ID(c.getInt(c.getColumnIndex("ACT_ID")));
				dib.setID_USUARIO(c.getInt(c.getColumnIndex("ID_USUARIO")));
				dib.setIDPADRE(c.getString(c.getColumnIndex("IDPADRE")));

				
				dib.setDIB_GLOSA(c.getString(c.getColumnIndex("DIB_GLOSA")));
				
				dib.setFECHA_CREACION(new Date(c.getLong(c.getColumnIndex("FECHA_CREACION"))));
				dib.setDIB_TIPO(c.getString(c.getColumnIndex("DIB_TIPO")));
				dib.setDIB_ESTADO(c.getInt(c.getColumnIndex("DIB_ESTADO")));
				dib.setNULO(c.getInt(c.getColumnIndex("NULO"))>0);
				
		}
		c.close();
			
		return dib;
	}	
	
	public Cursor ObtenerInfoDibujo(UUID ID_DIBUJO)
	{


		
		String[] args = new String[] { 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()),
				ID_DIBUJO.toString()
				};


		Cursor c = mDb.rawQuery(
				" SELECT D.DIB_GLOSA, D.FECHA_CREACION, D.DIB_ESTADO, TC.TIPOCALLE_GLOSA, D.NUEVO FROM DIBUJOS D INNER JOIN TIPOCALLE TC ON TC.TIPOCALLE_ID = D.DIB_TIPO" + 
				" WHERE ACT_ID=? AND ID_USUARIO = ? AND ID_DIBUJO = ?", args);	

		return c;
	}	
	
	
	public List<Dibujo> ObtenerDibujos(boolean SoloNulos)
	{


		
		String[] args = new String[] { 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()),
				SoloNulos ? "1" : "0"
				};


		Cursor c = mDb.query("DIBUJOS",null,"ACT_ID=? AND ID_USUARIO = ? AND NULO = ?", args, null, null, "ID_DIBUJO");	


		List<Dibujo> dibujos = new ArrayList<Dibujo>();
		Dibujo dib;

		
		
		if (c.moveToFirst()) {
			do
			{
				//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				dib = new Dibujo();
				dib.setID_DIBUJO(UUID.fromString(c.getString(c.getColumnIndex("ID_DIBUJO"))));
				dib.setDIB_TEXT(c.getString(c.getColumnIndex("DIB_TEXT")));
				dib.setACT_ID(c.getInt(c.getColumnIndex("ACT_ID")));
				dib.setID_USUARIO(c.getInt(c.getColumnIndex("ID_USUARIO")));
				
				dib.setIDPADRE(c.getString(c.getColumnIndex("IDPADRE")));
				
				dib.setDIB_GLOSA(c.getString(c.getColumnIndex("DIB_GLOSA")));
				
				dib.setFECHA_CREACION(new Date(c.getLong(c.getColumnIndex("FECHA_CREACION"))));

				dib.setDIB_TIPO(c.getString(c.getColumnIndex("DIB_TIPO")));
				dib.setDIB_ESTADO(c.getInt(c.getColumnIndex("DIB_ESTADO")));
				
				dib.setNULO(c.getInt(c.getColumnIndex("NULO"))>0);
				
				dibujos.add(dib);
				
			}
			while(c.moveToNext());
		}
		c.close();
			
		return dibujos;
	}
	
	public Cursor ObtenerDibujosCursor(boolean nulos, boolean Todos)
	{


		
		String[] args ;
		Cursor c = null;
		if(Todos)
		{
			args = new String[] { 
					Integer.toString(globales.actualizacion.getAct_id()), 
					Integer.toString(globales.usuario.getId_usuario())
					};
			c = mDb.query("DIBUJOS",null,"ACT_ID=? AND ID_USUARIO = ?", args, null, null, "ID_DIBUJO");
		}
		else
		{
			args = new String[] { 
					Integer.toString(globales.actualizacion.getAct_id()), 
					Integer.toString(globales.usuario.getId_usuario()),
					nulos ? "1" : "0"
					};
			c = mDb.query("DIBUJOS",null,"ACT_ID=? AND ID_USUARIO = ? AND NULO = ?", args, null, null, "ID_DIBUJO");
		}
		


		return c;
	}
	
	public boolean InsertarImagen(UUID ID_VIVIENDA, byte[] IMAGEN)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();
		values.put("ACT_ID",globales.actualizacion.getAct_id());
		values.put("ID_USUARIO",globales.usuario.getId_usuario());		
		values.put("ID_VIVIENDA",ID_VIVIENDA.toString());   
		values.put("IMAGEN", IMAGEN);


		retorno = (mDb.insert("IMAGENES", null, values) > 0);

		return retorno;
	}

	public boolean InsertarAudio(UUID ID_VIVIENDA, byte[] AUDIO)
	{

		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();
		values.put("ACT_ID",globales.actualizacion.getAct_id());
		values.put("ID_USUARIO",globales.usuario.getId_usuario());
		values.put("ID_VIVIENDA",ID_VIVIENDA.toString());
		values.put("AUDIO", AUDIO);


		retorno = (mDb.insert("AUDIOS", null, values) > 0);

		return retorno;
	}

	
	public boolean BorrarImagen(UUID ID_VIVIENDA)
	{

		boolean retorno = false;
		
		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), ID_VIVIENDA.toString()};

		retorno = (mDb.delete("IMAGENES", "ACT_ID=? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args) >0);

		return retorno;

	}

	public boolean BorrarAudio(UUID ID_VIVIENDA)
	{

		boolean retorno = false;

		String[] args = new String[] {String.valueOf(globales.actualizacion.getAct_id()), String.valueOf(globales.usuario.getId_usuario()), ID_VIVIENDA.toString()};

		retorno = (mDb.delete("AUDIOS", "ACT_ID=? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args) >0);

		return retorno;

	}
	public Cursor ObtenerImagenes()
	{

		String[] args = new String[] { 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario())
				};

		Cursor c = mDb.rawQuery("SELECT ID_VIVIENDA, IMAGEN FROM IMAGENES WHERE ACT_ID = ? AND ID_USUARIO = ?", args);

		return c;
	}
	
	public Imagen ObtenerImagen(UUID ID_VIVIENDA)
	{

		String[] args = new String[] { 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()),
				ID_VIVIENDA.toString()
				
				};

		Cursor c = mDb.rawQuery("SELECT IMAGEN FROM IMAGENES WHERE ACT_ID = ? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args);
		Imagen img = null;
		if(c.moveToFirst())
		{
			img = new Imagen();
			img.setID_VIVIENDA(ID_VIVIENDA);
			img.setIMAGEN(c.getBlob(c.getColumnIndex("IMAGEN")));
			
		}
		
		return img;
	}

	public Imagen ObtenerAudio(UUID ID_VIVIENDA)
	{

		String[] args = new String[] {
				Integer.toString(globales.actualizacion.getAct_id()),
				Integer.toString(globales.usuario.getId_usuario()),
				ID_VIVIENDA.toString()

		};

		Cursor c = mDb.rawQuery("SELECT AUDIO FROM AUDIOS WHERE ACT_ID = ? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args);
		Imagen img = null;
		if(c.moveToFirst())
		{
			img = new Imagen();
			img.setID_VIVIENDA(ID_VIVIENDA);
			img.setIMAGEN(c.getBlob(c.getColumnIndex("AUDIO")));

		}

		c.close();
		return img;
	}

	public Cursor ObtenerAudios() {

        Cursor c = null;

        String[] args = new String[]{
                Integer.toString(globales.actualizacion.getAct_id()),
                Integer.toString(globales.usuario.getId_usuario())

        };

        c = mDb.rawQuery("SELECT ID_VIVIENDA, AUDIO FROM AUDIOS WHERE ACT_ID = ? AND ID_USUARIO = ?", args);

        return c;

	}

	public Boolean ExisteAudio(UUID ID_VIVIENDA)
	{

		String[] args = new String[] {
				Integer.toString(globales.actualizacion.getAct_id()),
				Integer.toString(globales.usuario.getId_usuario()),
				ID_VIVIENDA.toString()

		};

		Cursor c = mDb.rawQuery("SELECT ID_VIVIENDA FROM AUDIOS WHERE ACT_ID = ? AND ID_USUARIO = ? AND ID_VIVIENDA = ?", args);
		Boolean existeaudio = false;

		existeaudio = c.moveToFirst();

		c.close();
		return existeaudio;
	}

	public boolean BorrarImagenes()
	{

		boolean retorno = false;

		String[] args = new String[] { 	
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario())
				};

		retorno = (mDb.delete("IMAGENES", "ACT_ID = ? AND ID_USUARIO = ?", args) > 0);

		return retorno;
	}

	public boolean BorrarAudios()
	{

		boolean retorno = false;

		String[] args = new String[] {
				Integer.toString(globales.actualizacion.getAct_id()),
				Integer.toString(globales.usuario.getId_usuario())
		};

		retorno = (mDb.delete("AUDIOS", "ACT_ID = ? AND ID_USUARIO = ?", args) > 0);

		return retorno;
	}

	public List<String> ObtenerLocalidadesPadre(String IDPADRE)
	{

		String[] args = new String[] { 
				Integer.toString(globales.actualizacion.getAct_id()), 
				Integer.toString(globales.usuario.getId_usuario()),
				IDPADRE
				
				};

		List<String> localidades = new ArrayList<String>();
		
		Cursor c = mDb.rawQuery("SELECT DISTINCT NOMBRELOCALIDAD FROM VIVIENDAS WHERE ACT_ID = ? AND ID_USUARIO = ? AND IDPADRE = ? ORDER BY NOMBRELOCALIDAD", args);
		Imagen img = null;
		String loc = null;

			if(c.moveToFirst())
			{
				do
				{
					loc = c.getString(c.getColumnIndex("NOMBRELOCALIDAD")).trim();
					
					if(!localidades.contains(loc))
					{
						localidades.add(loc);
					}
					
				} while (c.moveToNext());		
			}
		
		
		return localidades;
	}


    public boolean InsertarEncuesta(Encuesta enc)
    {

        boolean retorno = false;

        ContentValues values;

        values = new ContentValues();
        values.put("IDENCUESTA",enc.getIdencuesta());
        values.put("NOMBREENCUESTA",enc.getNombreencuesta());
        values.put("ACT_ID",enc.getAct_id());
        values.put("TIPOENCUESTA",enc.getTipoencuesta());
        values.put("JSON", enc.getJson());

        retorno = (mDb.insert("ENCUESTAS", null, values) > 0);

        return retorno;
    }

    public boolean BorrarEncuestas()
    {
        String[] args = new String[] { Integer.toString(globales.actualizacion.getAct_id())};

        return mDb.delete("ENCUESTAS", "ACT_ID = ?", args)>0;
    }


    /*
        public boolean InsertarEncuesta(Encuesta enc)
    {

        boolean retorno = false;

        ContentValues values;

        values = new ContentValues();
        values.put("IDENCUESTA",enc.getIdencuesta());
        values.put("NOMBREENCUESTA",enc.getNombreencuesta());
        values.put("ACT_ID",enc.getAct_id());
        values.put("ID_USUARIO",enc.getId_usuario());
        values.put("TIPOENCUESTA",enc.getTipoencuesta());
        values.put("IDPADRE",enc.getIdpadre());
        values.put("ID_VIVIENDA", enc.getId_vivienda().toString());
        values.put("HOGAR",enc.getHogar());
        values.put("PERSONA",enc.getPersona());
        //values.put("IDENCUESTAMOVIL", enc.getIdEncuestaMovil().toString());
        values.put("JSON", enc.getJson());

        retorno = (mDb.insert("ENCUESTAS", null, values) > 0);

        return retorno;
    }
     */

    public List<Encuesta> ObtenerEncuestas()
    {

        String[] args = new String[]
		{
			Integer.toString(globales.actualizacion.getAct_id())
        };

        List<Encuesta> encuestas = new ArrayList<Encuesta>();

        Cursor c = mDb.query("ENCUESTAS", null, "ACT_ID = ?", args, null, null, null);
		Encuesta enc;

        if(c.moveToFirst())
        {
            do
            {

                enc = new Encuesta();
                enc.setIdencuesta(c.getInt(c.getColumnIndex("IDENCUESTA")));
                enc.setNombreencuesta(c.getString(c.getColumnIndex("NOMBREENCUESTA")));
                enc.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
                enc.setTipoencuesta(c.getString(c.getColumnIndex("TIPOENCUESTA")));
                enc.setJson(c.getString(c.getColumnIndex("JSON")));
                encuestas.add(enc);

            }

            while (c.moveToNext());
        }


        return encuestas;
    }


    public Encuesta ObtenerEncuestaTipo(String TIPO)
    {

        String[] args = new String[] {
                Integer.toString(globales.actualizacion.getAct_id()),
                TIPO
        };

        Cursor c = mDb.query("ENCUESTAS", null, "ACT_ID = ? AND TIPOENCUESTAS = ?", args, null, null, null);
			Encuesta enc = null;

			if(c.moveToFirst())
			{
				enc = new Encuesta();
				enc.setIdencuesta(c.getInt(c.getColumnIndex("IDENCUESTA")));
				enc.setNombreencuesta(c.getString(c.getColumnIndex("NOMBREENCUESTA")));
				enc.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
				enc.setTipoencuesta(c.getString(c.getColumnIndex("TIPOENCUESTA")));
				enc.setJson(c.getString(c.getColumnIndex("JSON")));
			}

        return enc;
    }


    public boolean InsertarCategoriaPOI(CategoriaPOI cat)
    {
        boolean retorno = false;
        ContentValues values;

        values = new ContentValues();
        values.put("IDCATPOI",cat.getIdcatpoi());
        values.put("GLOSA", cat.getGlosa());
        values.put("ICON", cat.getIcon());
        retorno = (mDb.insert("CATEGORIA_POI", null, values) > 0);

        return retorno;
    }

    public boolean InsertarPOI(POI poi)
    {
        boolean retorno = false;
        ContentValues values;

        values = new ContentValues();
        values.put("IDPOI",UUID.randomUUID().toString());
        values.put("IDCATPOI",poi.getIdcatpoi());
        values.put("GLOSA", poi.getGlosa());
        values.put("COORD_X", poi.getCoord_x());
        values.put("COORD_Y", poi.getCoord_y());
        values.put("FECHACAPTURA", poi.getFechacaptura().getTime());
		values.put("ENVIADO", 0);
        retorno = (mDb.insert("POI", null, values) > 0);

        return retorno;
    }

    public boolean BorrarCategoriasPOI()
    {

        return (mDb.delete("CATEGORIA_POI", null, null)>0);
    }

    public List<CategoriaPOI> ObtenerCategoriasPOI()
    {

        List<CategoriaPOI> cats = new ArrayList<CategoriaPOI>();

        Cursor c = mDb.query("CATEGORIA_POI", null, null, null, null, null, null);
        CategoriaPOI cat = null;

        if(c.moveToFirst())
        {
            do {
                cat = new CategoriaPOI();
                cat.setIdcatpoi(c.getInt(c.getColumnIndex("IDCATPOI")));
                cat.setGlosa(c.getString(c.getColumnIndex("GLOSA")));
                cat.setIcon(c.getBlob(c.getColumnIndex("ICON")));

                cats.add(cat);
            }
            while (c.moveToNext());
        }
        return cats;
    }

	public Cursor ObtenerPOISCursor()
	{

		List<POI> pois = new ArrayList<POI>();

		Cursor c = mDb.rawQuery("SELECT P.IDPOI, P.GLOSA, P.FECHACAPTURA, P.COORD_X, P.COORD_Y, P.IDCATPOI, C.GLOSA as GLOSACAT, C.ICON  FROM POI as P INNER JOIN CATEGORIA_POI C ON P.IDCATPOI = C.IDCATPOI ORDER BY P.GLOSA ", null);

		return c;
	}
	public List<POI> ObtenerPOIS()
	{

		List<POI> pois = new ArrayList<POI>();

		Cursor c = mDb.rawQuery("SELECT P.IDPOI, P.GLOSA, P.FECHACAPTURA, P.COORD_X, P.COORD_Y, P.IDCATPOI, C.GLOSA as GLOSACAT, C.ICON  FROM POI as P INNER JOIN CATEGORIA_POI C ON P.IDCATPOI = C.IDCATPOI ORDER BY P.GLOSA ", null);
		POI poi = null;

		if(c.moveToFirst())
		{
			do {
				poi = new POI();
				poi.setIdpoi(UUID.fromString(c.getString(c.getColumnIndex("IDPOI"))));
				poi.setGlosa(c.getString(c.getColumnIndex("GLOSA")));
				poi.setFechacaptura(new Date(c.getLong(c.getColumnIndex("FECHACAPTURA"))));
                poi.setCoord_x(c.getDouble(c.getColumnIndex("COORD_X")));
                poi.setCoord_y(c.getDouble(c.getColumnIndex("COORD_Y")));
                poi.setIdcatpoi(c.getInt(c.getColumnIndex("IDCATPOI")));

                CategoriaPOI cat = new CategoriaPOI();
                cat.setIdcatpoi(c.getInt(c.getColumnIndex("IDCATPOI")));
                cat.setGlosa(c.getString(c.getColumnIndex("GLOSACAT")));
                cat.setIcon(c.getBlob(c.getColumnIndex("ICON")));
                poi.setCATEGORIAPOI(cat);

				pois.add(poi);
			}
			while (c.moveToNext());
		}
		return pois;
	}

	public POI ObtenerPOI(String idpoi)
	{


		Cursor c = mDb.rawQuery("SELECT P.IDPOI, P.GLOSA, P.FECHACAPTURA, P.COORD_X, P.COORD_Y, P.IDCATPOI, C.GLOSA as GLOSACAT, C.ICON  FROM POI as P INNER JOIN CATEGORIA_POI C ON P.IDCATPOI = C.IDCATPOI WHERE P.IDPOI = ?", new String[] { idpoi});
		POI poi = null;

		if(c.moveToFirst())
		{
				poi = new POI();
				poi.setIdpoi(UUID.fromString(c.getString(c.getColumnIndex("IDPOI"))));
				poi.setGlosa(c.getString(c.getColumnIndex("GLOSA")));
				poi.setFechacaptura(new Date(c.getLong(c.getColumnIndex("FECHACAPTURA"))));
				poi.setCoord_x(c.getDouble(c.getColumnIndex("COORD_X")));
				poi.setCoord_y(c.getDouble(c.getColumnIndex("COORD_Y")));
				poi.setIdcatpoi(c.getInt(c.getColumnIndex("IDCATPOI")));

				CategoriaPOI cat = new CategoriaPOI();
				cat.setIdcatpoi(c.getInt(c.getColumnIndex("IDCATPOI")));
				cat.setGlosa(c.getString(c.getColumnIndex("GLOSACAT")));
				cat.setIcon(c.getBlob(c.getColumnIndex("ICON")));
				poi.setCATEGORIAPOI(cat);


		}
		return poi;
	}

    public boolean ActualizarPOIEnviados(String IDPOI )
    {
        boolean retorno = false;
        ContentValues values;

        values = new ContentValues();
        values.put("ENVIADO",1);

        retorno = (mDb.update("POI", values, "IDPOI = ?", new String[] {IDPOI} ) > 0);

        return retorno;
    }

    public boolean ActualizarTodosPOIEnviados()
    {
        boolean retorno = false;
        ContentValues values;

        values = new ContentValues();
        values.put("ENVIADO",1);

        retorno = (mDb.update("POI", values, null, null ) > 0);

        return retorno;
    }


	public boolean InsertarAreasTrabajo(Area_Trabajo at)
	{
		boolean retorno = false;
		ContentValues values;

		values = new ContentValues();
		values.put("AT_ID",at.getAt_id());
		values.put("CU_SECCION",at.getCu_seccion());
		values.put("ACT_ID", at.getAct_id());
		values.put("ID_USUARIO", at.getId_usuario());
		values.put("ID_USUARIO_SUP", at.getId_usuario_sup());
		values.put("NUMERO_AT", at.getNumero_at());
		values.put("FECHACREACION", at.getFechacreacion().getTime());
		values.put("WKT", at.getWkt());
		retorno = (mDb.insert("AREA_TRABAJO", null, values) > 0);

		return retorno;
	}

	public boolean BorrarAreasTrabajo()
	{
		String[] args = new String[] {
				Integer.toString(globales.actualizacion.getAct_id()),
				Integer.toString(globales.usuario.getId_usuario()),
				globales.IDPADRE
		};
		int ret = mDb.delete("AREA_TRABAJO", "ACT_ID = ? AND ID_USUARIO = ? AND CU_SECCION = ?", args);
		return (ret>0);
	}

	public List<Area_Trabajo> ObtenerAreasTrabajo(boolean addTodos)
	{

		List<Area_Trabajo> ats = new ArrayList<Area_Trabajo>();

        String where;
		String[] args;

		if(globales.AT_ID==0)
		    args = new String[] { Integer.toString(globales.actualizacion.getAct_id()),Integer.toString(globales.usuario.getId_usuario()),globales.IDPADRE};
		else
            args = new String[] { Integer.toString(globales.actualizacion.getAct_id()),Integer.toString(globales.usuario.getId_usuario()),globales.IDPADRE, String.valueOf(globales.AT_ID)};

        where = "ACT_ID = ? AND ID_USUARIO = ? AND CU_SECCION = ?" + (globales.AT_ID==0 ? "" : " AND AT_ID = ?") ;

		Cursor c = mDb.query("AREA_TRABAJO", null, where, args, null, null, "NUMERO_AT ASC");

		Area_Trabajo at = null;

		if(c.moveToFirst())
		{
			do {
				at = new Area_Trabajo();
				at.setAt_id(c.getInt(c.getColumnIndex("AT_ID")));
				at.setCu_seccion(c.getLong(c.getColumnIndex("CU_SECCION")));
				at.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
				at.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
				at.setId_usuario_sup(c.getInt(c.getColumnIndex("ID_USUARIO_SUP")));
				at.setNumero_at(c.getInt(c.getColumnIndex("NUMERO_AT")));
				at.setFechacreacion(new Date(c.getLong(c.getColumnIndex("FECHACREACION"))));
				at.setWkt(c.getString(c.getColumnIndex("WKT")));



				ats.add(at);
			}
			while (c.moveToNext());
		}

		if(addTodos)
		{
			at = new Area_Trabajo();
			at.setAt_id(0);
			at.setNumero_at(0);
			ats.add(0, at);
		}
		return ats;
	}

	public Area_Trabajo ObtenerAreaTrabajo(int at_id)
	{

		List<Area_Trabajo> ats = new ArrayList<Area_Trabajo>();

		String[] args = new String[] {
				Integer.toString(at_id)
		};

		Cursor c = mDb.query("AREA_TRABAJO", null, "AT_ID = ?", args, null, null, null);
		Area_Trabajo at = null;

		if(c.moveToFirst())
		{
			at = new Area_Trabajo();
			at.setAt_id(c.getInt(c.getColumnIndex("AT_ID")));
			at.setCu_seccion(c.getLong(c.getColumnIndex("CU_SECCION")));
			at.setAct_id(c.getInt(c.getColumnIndex("ACT_ID")));
			at.setId_usuario(c.getInt(c.getColumnIndex("ID_USUARIO")));
			at.setId_usuario_sup(c.getInt(c.getColumnIndex("ID_USUARIO_SUP")));
			at.setNumero_at(c.getInt(c.getColumnIndex("NUMERO_AT")));
			at.setFechacreacion(new Date(c.getLong(c.getColumnIndex("FECHACREACION"))));
			at.setWkt(c.getString(c.getColumnIndex("WKT")));
		}


		return at;
	}

}
