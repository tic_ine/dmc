package com.INE.ImasD.EnumeracionMobile.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.R;

import java.util.List;

public class Spin_Secciones_Adapter  extends BaseAdapter{
	//Your sent context
	private Context context;
	//Your custom values for the spinner (User)
	private List<Seccion> values;


	public Spin_Secciones_Adapter(Context context, List<Seccion> padres) {
		super();
		this.context = context;
		this.values = padres;
	}

	public int getCount(){
		return values.size();
	}

	public Seccion getItem(int position){
		return values.get(position);
	}

	public long getItemId(int position){
		return position;
	}

    public int getPositionById(int id)
    {
    	int retorno = -1;
    	for(int i = 0; i< values.size();i++)
    	{
    		if(values.get(i).getCu_seccion()==id)
    		{
    			retorno = i;
    			break;
    		}
    	}
		return retorno;
    	
    }

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    public View getView(int position, View convertView, ViewGroup parent) {
    	 View v = convertView; 
    	 TextView txt;
    	 Seccion sec = getItem(position);
    	   	
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_padre_passive , null);
	        }
	        
	        txt = (TextView)v.findViewById(R.id.tvitem_Padre);
	        //txt.setCompoundDrawables(context.getResources().getDrawable(R.drawable.bluepushpin32), null, null, null);
	        
		        txt.setText(sec.getCut()  + " - " + sec.getComuna_glosa() + " Distritos:" + sec.getCod_distrito()+ " Carto:" + sec.getCod_carto() + " Sec:" + sec.getCodigo_seccion());
	       
	        return v;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	
   	 View v = convertView; 
   	 TextView txt1;
   	TextView txt2;
   	TextView txt3;
   	TextView txt4;
   	TextView txtPadreOrigen;
   	Seccion sec = getItem(position);
   	
	        if (convertView == null) 
	        { // if it's not recycled, initialize some
	            // attributes
				LayoutInflater vi = 
				(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_padre , null);
				
	        }
	        // txtPadreInfoGeografica1
	        
	        txtPadreOrigen = (TextView)v.findViewById(R.id.tvGlosaAreaPadre);
	        txt1 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica1);
	        txt2 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica2);
	        txt3 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica3);
	        txt4 = (TextView)v.findViewById(R.id.txtPadreInfoGeografica4);
	        
	        
	        txt3.setVisibility(View.VISIBLE);
	        txt4.setVisibility(View.VISIBLE);
        	v.setBackgroundResource(R.drawable.rounded_corner_seccion);		        
            txtPadreOrigen.setText("CU_SEC:" + sec.getCu_seccion());
	        txt1.setText("RPC:" + sec.getCut() + " - " + sec.getComuna_glosa());
	        txt2.setText("Distritos: " + sec.getCod_distrito());
	        txt3.setText("Estrato Geo:" + sec.getEstrato_geo());
	        txt4.setText("Cod Sec: " + sec.getCodigo_seccion());
	        
	    
	        return v;

    }
    
}