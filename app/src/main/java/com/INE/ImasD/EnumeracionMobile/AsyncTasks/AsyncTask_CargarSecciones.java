package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.INE.ImasD.EnumeracionMobile.DataAccess.DataBaseUtil;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;

import java.util.List;



public class AsyncTask_CargarSecciones extends AsyncTask<Integer, Void, List<Seccion>>
{
	private Context context;
    private AsyncTaskCompleteListener<List<Seccion>> listener;
    ProgressDialog pd;

    
    public AsyncTask_CargarSecciones(Context ctx, AsyncTaskCompleteListener<List<Seccion>> listener)
    {
        this.context = ctx;
        this.listener = listener;
    }
    
    
	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();
		
		// TODO Auto-generated method stub
		pd = new ProgressDialog(context);
		pd.setTitle("Cargando Secciones");
		pd.setMessage("Por favor espere...");       
		pd.setIndeterminate(true);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//		pd.setMax(100);
		//pd.show();
	}
	
	@Override
	protected List<Seccion> doInBackground(Integer... args) 
	{
		//int area = args[0];
		
		return ObtenerSecciones();

	}
	

	
	@Override 
	protected void onPostExecute(List<Seccion> secciones) 
	{
		super.onPostExecute(secciones);
		listener.onTaskCompleteSecciones(secciones);
		pd.dismiss();

	}
	
	private List<Seccion> ObtenerSecciones()
	{
		DataBaseUtil dbUtil = new DataBaseUtil(context);
		dbUtil.open();
		List<Seccion> secciones= dbUtil.ObtenerSecciones();
		dbUtil.close();
		
		return secciones;

	}
}



