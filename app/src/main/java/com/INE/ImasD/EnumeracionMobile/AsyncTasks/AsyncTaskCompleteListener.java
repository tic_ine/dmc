package com.INE.ImasD.EnumeracionMobile.AsyncTasks;

import com.INE.ImasD.EnumeracionMobile.Entities.Manzana;
import com.INE.ImasD.EnumeracionMobile.Entities.Seccion;
import com.INE.ImasD.EnumeracionMobile.Entities.Vivienda;

import java.util.List;

public interface AsyncTaskCompleteListener<T> {
	 /**
     * Invoked when the AsyncTask has completed its execution.
     * @param viviendas The resulting object from the AsyncTask.
     */
	
	
    public void onTaskCompleteViviendas(List<Vivienda> viviendas);
    
    public void onTaskCompleteManzanas(List<Manzana> manzanas);
    public void onTaskCompleteSecciones(List<Seccion> secciones);
    
    public void onTaskCompleteEnvioDatos(Integer resultado);
    public void onTaskCompleteDescargarDatos(Integer resultado);
    
}
