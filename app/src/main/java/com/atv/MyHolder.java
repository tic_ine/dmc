package com.atv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.INE.ImasD.EnumeracionMobile.R;
import com.atv.model.TreeNode;

/**
 * Created by admin on 31-03-2016.
 */
public class MyHolder extends TreeNode.BaseNodeViewHolder<MyHolder.IconTreeItem> {
    ImageView img;
    TextView tvValue;
    ImageView imgItemEncuesta_Respondida;
    IconTreeItem value;
    public MyHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, IconTreeItem value)
    {
        this.value = value;
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.item_arbol_encuesta, null, false);

        img = (ImageView) view.findViewById(R.id.imgItemEncuesta);
        tvValue = (TextView) view.findViewById(R.id.tvItem_encuesta);
        imgItemEncuesta_Respondida = (ImageView) view.findViewById(R.id.imgItemEncuesta_Respondida);


        img.setImageResource(value.icon);
        tvValue.setText(value.text);

        imgItemEncuesta_Respondida.setImageResource(value.IconoRespondido);

        return view;
    }

    public void toggle(boolean active) {
        img.setImageResource(active ? this.value.icon : R.drawable.add);
    }

    public static class IconTreeItem {
        public int icon;
        public String text;
        public int OrdenSeccion;
        public int OrdenPregunta;
        public int IconoRespondido;

        public IconTreeItem(int icon, String text, int ordensec, int ordenpreg, int IconoRespondido) {

            this.icon = icon;
            this.text = text;
            this.OrdenSeccion = ordensec;
            this.OrdenPregunta = ordenpreg;
            this.IconoRespondido = IconoRespondido;
        }
    }
}

